
#define FUSE_USE_VERSION 22

#include <fuse.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <metric/common.h>
#include "iirc_fuse.h"
#include "iirc/client.h"


static const char *hello_str = "Hello World!\n";
static const char *hello_path = "/hello";


static int iirc_getattr(const char *path, struct stat *stbuf)
{
    int res = 0;

    memset(stbuf, 0, sizeof(struct stat));
    if(strcmp(path, "/") == 0) {
        stbuf->st_mode = S_IFDIR | 0755;
        stbuf->st_nlink = 2;
    }
    else if(strcmp(path, hello_path) == 0) {
        stbuf->st_mode = S_IFREG | 0444;
        stbuf->st_nlink = 1;
        stbuf->st_size = strlen(hello_str);
    }
    else
        res = -ENOENT;

    return res;
}

static int iirc_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
                        off_t offset, struct fuse_file_info *fi)
{
    (void) offset;
    (void) fi;

    if(strcmp(path, "/") != 0)
        return -ENOENT;

    filler(buf, ".", NULL, 0);
    filler(buf, "..", NULL, 0);
    filler(buf, hello_path + 1, NULL, 0);

    return 0;
}

static int iirc_open(const char *path, struct fuse_file_info *fi)
{
    if(strcmp(path, hello_path) != 0)
        return -ENOENT;

    if((fi->flags & 3) != O_RDONLY)
        return -EACCES;

    return 0;
}

static int iirc_read(const char *path, char *buf, size_t size, off_t offset,
                      struct fuse_file_info *fi)
{
    size_t len;
    (void) fi;
    if(strcmp(path, hello_path) != 0)
        return -ENOENT;

    len = strlen(hello_str);
    if (offset < len) {
        if (offset + size > len)
            size = len - offset;
        memcpy(buf, hello_str + offset, size);
    } else
        size = 0;

    return size;
}


static struct fuse_operations iirc_fuse_ops = {
    .getattr    = iirc_getattr,
    .readdir    = iirc_readdir,
    .open       = iirc_open,
    .read       = iirc_read,
};

void* iirc_fuse_main(void *fuse_args)
{
    fuse_args_t *args = (fuse_args_t*)fuse_args;
    char *argv[] = { "iirc", args->mount_point };

    //
    // hand over execution to the FUSE library
    //
    fuse_main(2, argv, &iirc_fuse_ops);

    return NULL;
}

