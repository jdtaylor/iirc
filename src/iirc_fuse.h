
#ifndef _IIRC_FUSE_H
#define _IIRC_FUSE_H

#include "limits.h"

#define MAX_FUSE_NAME 64

typedef struct {
    char name[MAX_FUSE_NAME];
    char mount_point[PATH_MAX];
    int hClient;
} fuse_args_t;

void* iirc_fuse_main(void *fuse_args);

#endif

