
/*
 * Copyright (c) 2001-2002,  James D. Taylor.
 * All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * contact James D. Taylor:  jtaylor5@bayou.uh.edu
 */

#include <stdio.h>
#include <ctype.h>
#include <time.h>
#include <unistd.h>
#include "commonChar.h"
#include "commute.h"
#include "iirc_module.h"
#include "iirc_cargo.h"
#include "iirc_defs.h"

#define INITIAL_ECHO_DELAY 4

struct echoQueue {
    struct echoQueue *next;
    time_t timeo;
    time_t delay;
    cargoIrcMsg *msg;
};

static char g_modName[] = "echo";
static char g_modInfo[] = "Example IIRC channel module that "
              "echoes channel text.";
static iircModule_t g_iircMod;
static struct echoQueue *g_echoFirst = NULL;
static struct echoQueue *g_echoLast = NULL;

#ifdef __cplusplus
extern "C" {
#endif

static int main_echo();
static int echoTruckHandler(truck *pTruck);
#ifndef MOD_COPROC
void *iircServerModEntry(void *pVoid);
#endif

#ifdef __cplusplus
}
#endif


#ifdef MOD_COPROC

int main(int argc, char *argv[])
{
    int value = 0;
    char *pEnv;
    iircModHead head;
    pipe_t stdioPipe;

    iircModuleInit(&g_iircMod);
    unicode_cpy(g_iircMod.name, g_modName, UTF8, MAX_IIRC_MOD_NAME);
    unicode_cpy(g_iircMod.info, g_modInfo, UTF8, MAX_IIRC_MOD_INFO);
    g_iircMod.truckSize = sizeof(iircLargestTruck);

    pEnv = getenv(IIRC_ENV_MOD_FLAG);
    if(pEnv == NULL) {
        //
        // send iircModHead to stdout and exit
        iircModHeadInit(&head, 0, g_modName, g_modInfo);
        write(STDOUT_FILENO, &head, sizeof(head));
        return 0;
    }
    sscanf(pEnv, "%u", &g_iircMod.flags);

    if(g_iircMod.flags & IIRC_MOD_QUERY) {
        //
        // fill out g_iircMod and send it to parent
        write(STDOUT_FILENO, &g_iircMod, sizeof(iircModule_t));
        return 0;
    }
    g_iircMod.locationType = IIRC_MOD_COPROC;
    pipeFromStdio(&stdioPipe);
    commuteUsingPipe(&g_iircMod.com, &stdioPipe);
    //
    // We have a timer running, so set all read I/O to non-blocking.
    pipeOptionSet(&g_iircMod.com.pipe, PIPE_NONBLOCK | PIPE_READ, &value);
    //    sleep(60);
    return main_echo();
}

#else // local thread

void *iircServerModEntry(void *pVoid)
{
    int value = 0;
    Uint32 ack = 0;
    iircModule_t *pMod = (iircModule_t*)pVoid;

    if(pMod->flags & IIRC_MOD_QUERY) {
        unicode_cpy(pMod->name, g_modName, UTF8, MAX_IIRC_MOD_NAME);
        unicode_cpy(pMod->info, g_modInfo, UTF8, MAX_IIRC_MOD_INFO);
        pMod->truckSize = sizeof(iircLargestTruck);
        return pVoid;
    }
    if(pMod->truckSize < sizeof(truck)) {
        LOG_MSG(LOG_LOW,"iircServerModEntry - mod_echo - truckSize too small");
        return NULL;
    }
    LOG_MSG(LOG_LOW,"MOD_ECHO STARTING");
    iircModuleCopy(&g_iircMod, pMod);
    //
    // Setting this end of the pipe is a bit different when both ends
    // of the pipe are in the same process (2 threads).  We cannot
    // simply close the parents end like the textbooks do.  Instead
    // we bypass pipeAttach and set the flags manually.
    //
    g_iircMod.com.pipe.flags &= ~PIPE_PARENT; // unset parent flag
    g_iircMod.com.pipe.flags |= PIPE_CHILD; // set child flag
    //
    // send acknowledgement
    pipeWrite(&g_iircMod.com.pipe, &ack, sizeof(Uint32));
    //
    // We have a timer running, so set all I/O to non-blocking.  An
    // alternative would be to use 'select' with blocking I/O.
    //
    pipeOptionSet(&g_iircMod.com.pipe, PIPE_NONBLOCK | PIPE_READ |
            PIPE_WRITE | PIPE_ERR, &value);
    main_echo();
    return pVoid;
}

#endif

/*
 * module entry-neutral functions
 */

int main_echo()
{
    int status;
    time_t now;
    truck *pTruck;
    struct echoQueue *pEcho;
    struct echoQueue *pNode;

    truckQueueAlloc(&g_iircMod.parking, 1, 1, g_iircMod.truckSize);
    g_iircMod.com.localParking = &g_iircMod.parking;

    while(42)
    {
        time(&now);
        if(g_echoFirst != NULL && g_echoFirst->timeo <= now) {
            //
            // send message, modify timeo
            commuteSend(&g_iircMod.com, (truck*)g_echoFirst->msg);
            g_echoFirst->delay--;
            g_echoFirst->timeo += g_echoFirst->delay;
            if(!g_echoFirst->delay) {
                //
                //  delete the echo node
                pNode = g_echoFirst;
                truckEnqueue(&g_iircMod.parking, (truck*)g_echoFirst->msg);
                g_echoFirst = g_echoFirst->next;
                if(g_echoFirst == NULL)
                    g_echoLast = NULL;
                FREE(pNode);
            }
            else if(g_echoFirst->next != NULL) {
                //
                // requeue the echo
                pEcho = g_echoFirst;
                pNode = g_echoFirst;
                g_echoFirst = pEcho->next;
                while(pNode->next != NULL && pEcho->timeo >= pNode->next->timeo)
                    pNode = pNode->next;
                if(pNode->next == NULL)
                    g_echoLast = pEcho;
                pEcho->next = pNode->next;
                pNode->next = pEcho;
            }
            // else requeued in the same spot
        }
        pTruck = commuteReceive(&g_iircMod.com, &status);
        if(pTruck == NULL) {
            if(status < 0)
                break; // most likely, parent closed pipe
            usleep(5000); // 5 milliseconds
            continue; // no data ready.  WOULDBLOCK
        }
        echoTruckHandler(pTruck);
    }
    truckQueueFree(&g_iircMod.parking);
    commuteFree(&g_iircMod.com);
    return 0;
}

int echoTruckHandler(truck *pTruck)
{
    struct echoQueue *node;
    time_t now_time;
    iircTruck *pIrcTruck;

    if(pTruck->type != CARGO_UNKNOWN) {
        truckEnqueue(&g_iircMod.parking, pTruck);
        return 0;
    }
    pIrcTruck = (iircTruck*)pTruck;
    if(pIrcTruck->type != CARGO_IIRC_MSG) {
        truckEnqueue(&g_iircMod.parking, pTruck);
        return 0;
    }
    time(&now_time);
    node = (struct echoQueue*)MALLOC(sizeof(struct echoQueue));
    node->timeo = now_time + INITIAL_ECHO_DELAY;
    node->delay = INITIAL_ECHO_DELAY;
    node->msg = (cargoIrcMsg*)pTruck;
    node->next = NULL;
    if(g_echoLast == NULL) {
        g_echoFirst = node;
        g_echoLast = node;
    } else {
        g_echoLast->next = node;
        g_echoLast = node;
    }
    return 1;
}



