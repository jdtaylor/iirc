
/*
 * Copyright (c) 2001-2002,  James D. Taylor.
 * All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * contact James D. Taylor:  james.d.taylor@gmail.com
 */

#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <ltdl.h>
#include <unistd.h>
#include <errno.h>
#include <ctype.h>
#include "main_common.h"
#include "iirc/server.h"

#define DEFAULT_IP 0
#define DEFAULT_PORT 55479
#define SERVER_HOSTNAME "earth"
#define MAX_CONSOLE_LINE 128

static void iircdExit();
static int initLog();
static int printUsage(char *appname);
static int initIrcServer(char *confFile);
static int initConsole();
static int OnLogMessage(char *logMsg);
static int consoleCallback(unsigned int hIntr, int data, void *pData);
static int consoleProcessLine(char *line);
static int parseConfFile(FILE *filp);

static int g_hServer;
static unsigned int g_fdConsole = 0; // stdin
static char g_consoleBuf[MAX_CONSOLE_LINE];
static unsigned int g_nConsoleBytes;

int main(int argc, char *argv[])
{
    char c;
    char confFile[PATH_MAX];
    int bConsole = FALSE;

    lt_dlinit();

    atexit(iircdExit);

    initLog();
    printGreeting(FALSE); // not client
    confFile[0] = '\0';

    while( (c = getopt(argc, argv, "h::v::i:c:")) != EOF) {
        switch(c) {
            case 'h':
                printUsage(argv[0]);
                return 0;
                break;
            case 'v':
                printUsage(argv[0]);
                return 0;
                break;
            case 'i':
                bConsole = TRUE;
                break;
            case 'c':
                strncpy(confFile, optarg, PATH_MAX);
                confFile[PATH_MAX-1] = '\0';
                break;
        }
    }
    if(initIrcServer(confFile) < 0)
        return 0;
    if(bConsole)
        initConsole();

    //
    // while life, the universe, and everything
    //
    while(6*9) {
        if(iircServerExecute(g_hServer, 999999) <= 0)
            break;
    } // while(42)
    return 1; 
}

void iircdExit()
{
    lt_dlexit();
    return;
}

int printUsage(char *appName)
{
    printf("iirc daemon v%.3f [coded by metric]\n", IIRC_SERVER_VERSION);
    printf("usage: %s [options]\n", appName);
    printf("options:\n");
    printf("  help:    -h\n");
    printf("  listen:  -l <ip-address> (default=%d)\n", DEFAULT_IP);
    printf("  port:    -p <port>       (default=%d)\n", DEFAULT_PORT);
    printf("  module:  -m <file>\n");
    return 1;
}

int initLog()
{
    initLogCallback((logFunc)&OnLogMessage);
    return 1;
}

int initIrcServer(char *confFile)
{
    FILE *filp;
    char localConf[PATH_MAX];
    iircServerDescr iircServer;

    iircServer.version = IIRC_SERVER_DESCR_VERSION;
    strncpy(iircServer.name, "Metric IIRCD", MAX_IIRC_SERVER_NAME);
    iircServer.name[MAX_IIRC_SERVER_NAME-1] = '\0';
    strncpy(iircServer.hostname, SERVER_HOSTNAME, MAX_HOST_NAME);
    iircServer.hostname[MAX_HOST_NAME-1] = '\0';

    iircServer.isRoot = TRUE;
    iircServer.parentName[0] = '\0';
    iircServer.parentPort = 0;
    iircServer.listenForServers = FALSE;
    iircServer.serverName[0] = '\0';
    iircServer.serverPort = 0;
    iircServer.listenForClients = TRUE;
    strncpy(iircServer.clientName, "0.0.0.0", MAX_HOST_NAME);
    iircServer.clientName[MAX_HOST_NAME-1] = '\0';
    iircServer.clientPort = DEFAULT_PORT;
    iircServer.listenForRFC1459 = TRUE;
    strncpy(iircServer.rfc1459Name, "0.0.0.0", MAX_HOST_NAME);
    iircServer.rfc1459Port = 6667;
    g_hServer = iircServerAdd(&iircServer);
    if(g_hServer < 0) {
        LOG_MSG(LOG_LOW,"initIrcServer - failed to add new IIRC server.");
        return -1;
    }

    filp = fopen("/etc/iircd.conf", "r");
    if(filp != NULL) {
        parseConfFile(filp);
        fclose(filp);
    }
    if(getcwd(localConf, PATH_MAX) != NULL) {
        //
        // fix to point to "../etc/iircd.conf"
    }
    if(confFile[0] != '\0') {
        filp = fopen(confFile, "r");
        if(filp != NULL) {
            parseConfFile(filp);
            fclose(filp);
        }
    }
    //
    // start the server
    if(iircServerStart(g_hServer, 99999) < 0) {
        iircServerDel(g_hServer);
        return -1;
    }
    return 1; // server should be up and spankin now
}

int initConsole()
{
    iircInterruptDescr intr;

    if(g_hServer < 0)
        return -1;
    intr.version = IIRC_INTR_DESCR_VERSION;
    intr.fd = g_fdConsole; // stdin
    intr.fnCallback = consoleCallback;
    intr.data = g_hServer;
    intr.pData = NULL;
    if(iircServerIntrAdd(g_hServer, &intr) < 0) {
        LOG_MSG(LOG_WARN,"initConsole - unable to add console interrupt");
    }
    return 1;
}

int OnLogMessage(char *logMsg)
{
    printf("%s\n", logMsg);
    return 1;
}

int consoleCallback(unsigned int hIntr, int data, void *pData)
{
    unsigned int i, j, marker;
    ssize_t nread;
    char line[MAX_CONSOLE_LINE];
    unsigned int lineLen = g_nConsoleBytes;
    char *buf = g_consoleBuf + lineLen;
    size_t buflen = MAX_CONSOLE_LINE - lineLen;

    nread = read(g_fdConsole, buf, buflen-1);
    if(nread <= 0) {
        if(nread == 0)
            return 1; // EOF on standard in?
        if(errno == EINTR)
            return 1;
        if(errno == EWOULDBLOCK || errno == EAGAIN)
            return 1;
        LOG_MSG(LOG_SYS,"consoleCallback - read failed");
        iircServerIntrDel(g_hServer, hIntr);
        return -1;
    }
    lineLen += nread;
    i = marker = 0;
    while(TRUE) {
        j = 0;
        while(i < lineLen && g_consoleBuf[i] != '\n')
            line[j++] = g_consoleBuf[i++];
        if(i >= lineLen)
            break;
        line[j] = '\0'; // in the place of new-line
        consoleProcessLine(line);
        i++; // skip new line
        marker = i;
    }
    for(i = 0; i < lineLen - marker; i++)
        g_consoleBuf[i] = g_consoleBuf[i+marker];
    g_nConsoleBytes = lineLen - marker;
    return 1;
}

int consoleProcessLine(char *pLine)
{
    int i;
    char cmdBuf[32];
    char *curPos = pLine;
    size_t len;
    len = strlen(pLine);
    if(!Strntok(cmdBuf, &curPos, '\n', 32)) {
        LOG_MSG(LOG_USER,"Command not found.");
        return 0;
    }
    i = 0;
    while(i < 32 && cmdBuf[i] != '\0') {
        cmdBuf[i] = toupper(cmdBuf[i]);
        i++;
    }
    switch(cmdBuf[0]) {
        case 'Q':
            if(strncmp(cmdBuf, "QUIT", 4) == 0) {
                iircServerDel(g_hServer);
                exit(0);
            }
        default:
            break;

    }
    VLOG_MSG(LOG_USER,"iircd: %s: command not found", cmdBuf);
    return 1;
}

int parseConfFile(FILE *filp)
{
    char line[MAX_IIRC_SERVER_CMD];

    while(fgets(line, sizeof(line)-1, filp) != NULL) {
        //
        // filter out config file specifics.
        if(strlen(line) < 4)
            continue;
        if(line[0] == '\n' || line[0] == '#')
            continue;
        iircServerCmd(g_hServer, line);
    }
    return 1;
}



