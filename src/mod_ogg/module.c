
/*
 * Copyright (c) 2001-2002,  James D. Taylor.
 * All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * contact James D. Taylor:  jtaylor5@bayou.uh.edu
 */

#include <ctype.h>
#include "commonChar.h"
#include "commute.h"
#include "iirc_module.h"
#include "ogg_cargo.h"


static char g_modName[] = "ogg_vorbis";
static char g_modInfo[] = "Audio streaming.";
static iircModule_t g_iircMod;


static int main_ogg();

#ifdef MOD_COPROC

int main(int argc, char *argv[])
{
    iircModHead head;
    iircChanModHeadInit(&head);
    return main_ogg();
}

#else

void *iircServerModEntry(void *pVoid)
{
    iircModule_t *pMod = (iircModule_t*)pVoid;

    if(pMod->flags & IIRC_MOD_QUERY) {
        //
        // we are being queried.  fill our information into the module
        // description structure.
        unicode_cpy(pMod->name, g_modName, UTF8, MAX_IIRC_MOD_NAME);
        unicode_cpy(pMod->info, g_modInfo, UTF8, MAX_IIRC_MOD_INFO);
        pMod->truckSize = sizeof(cargoOgg);
        return pMod;
    }
    // TODO: verify pMod contents. (ie. 0 truck size etc)
    iircModuleCopy(&g_iircMod, pMod);

    pipeAttach(&g_iircMod.com.pipe, FALSE); // attach pipe to us (child)

    main_ogg();
    return pVoid;
}

#endif

int main_ogg()
{
    int status;
    truck *pTruck;

    g_iircMod.com.pTruckIn = (truck*)MALLOC(g_iircMod.truckSize);
    if(g_iircMod.com.pTruckIn == NULL) {
        logMsg(LOG_SYS,"main_pig_latin - malloc failed");
        return -1;
    }
    pTruck = commuteReceive(&g_iircMod.com, &status);
    while(pTruck != NULL) {
        pTruck = commuteReceive(&g_iircMod.com, &status);
    }
    commuteFree(&g_iircMod.com);
    return 0;
}

