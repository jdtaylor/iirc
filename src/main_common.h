
/*
 * Copyright (c) 2001-2002,  James D. Taylor.
 * All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * contact James D. Taylor:  james.d.taylor@gmail.com
 */

#include <stdio.h>

#define IIRC_CLIENT_VERSION 0.01f
#define IIRC_SERVER_VERSION 0.01f


void printGreeting(char bClient)
{
    printf("\n");
    printf("        ##       #####       ##\n");
    printf("       ## #######     ####### ##\n");
    printf("      ##                       ##\n");
    printf("     ##   I N T E R S T A T E   ##\n");
    printf("     #___________________________#\n");
    printf("     #                           #\n");
    printf("     #  #####   ####     ####    #\n");
    printf("     #    #     #   #   #        #\n");
    printf(".. . #    #     ####    #        # .. . ..\n");
    printf("     #    #     #  #    #        #\n");
    printf("     #  ##### # #   # #  #### #  #\n");
    printf("      #                         #\n");
    if(bClient)
    printf("       ##       client v%1.3f ##\n", IIRC_CLIENT_VERSION);
    else
    printf("       ##       server v%1.3f ##\n", IIRC_SERVER_VERSION);
    printf("        ##                   ##\n");
    printf("          ###             ###\n");
    printf("            ###         ###\n");
    printf("               #########\n");
    printf("                  | :\n");
    printf("                  | :\n");
    printf("                  | :\n");
    printf("   Copyright (C) 2007 James D. Taylor\n");
    printf("                      james.d.taylor@gmail.com\n");
    printf("\n");
}


