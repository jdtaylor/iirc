
/*
 * Copyright (c) 2001-2002,  James D. Taylor.
 * All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * contact James D. Taylor:  james.d.taylor@gmail.com
 */

#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <unistd.h>
#include <limits.h>
#include <ltdl.h>
#include <errno.h>
#include <ctype.h>
#include "config.h"
#include "main_common.h"
#include "metric/common.h"
#include "metric/char.h"
#include "iirc/common.h"
#include "iirc/client.h"
#include "iirc/cargo.h"

#ifdef USE_METRIC_THREADS
#include "metric/thread.h"
#endif

#if HAVE_FUSE
#include "iirc_fuse.h"
#endif

#define DEFAULT_IP 0
#define DEFAULT_PORT 55479
#define MAX_CONSOLE_LINE 128


static void iircExit();
static int initLog();
static int printUsage(char *appname);
static int initIrcClient(char *hostname, short port);
static int OnLogMessage(char *logMsg);

static int myCargoHandler(iircClientCargo *pCargo, int *pStatus);

static int consoleCallback(unsigned int hIntr, int data, void *pData);
static int consoleProcessLine(char *pLine);

static int g_hClient;
//static int g_hUser;
//static int g_hChan;
static unsigned int g_fdConsole = 0; // stdin
static char g_consoleBuf[MAX_CONSOLE_LINE];
static unsigned int g_nConsoleBytes;

int main(int argc, char *argv[])
{
    char c;
    char hostname[MAX_HOST_NAME] = "\0";
    char mount_point[PATH_MAX] = "\0";
    short port;
#if HAVE_FUSE
    pthread_t fuse_thread;
    fuse_args_t fuse_args;
#endif

    // initialize libtool's dlopen utility
    lt_dlinit();

    atexit(iircExit);

    initLog();
    printGreeting(TRUE);

    strncpy(hostname, "127.0.0.1", MAX_HOST_NAME);
    port = (short)DEFAULT_PORT;

    while( (c = getopt(argc, argv, "h::v::l:p:m:")) != EOF)
    {
        switch(c)
        {
            case 'h':
                printUsage(argv[0]);
                return 0;
            case 'v':
                printUsage(argv[0]);
                return 0;
            case 'l':
                strncpy(hostname, optarg, MAX_HOST_NAME);
                hostname[MAX_HOST_NAME-1] = '\0';
                break;
            case 'p':
                sscanf(optarg,"%hd", &port);
                break;
#if HAVE_FUSE
            case 'm':
                //
                // mount the iirc client to a spot on the filesystem
                //
                strncpy(mount_point, optarg, PATH_MAX);
                mount_point[PATH_MAX-1] = '\0';
                break;
#endif
            case '?':
            default:
                printf("\nUnknown option '-%c'. See Dcomumentation:\n\n", c);
                printUsage(argv[0]);
                return 1;
        }
    }

    if(initIrcClient(hostname, port) < 0)
        return 1;

#if HAVE_FUSE
    if(mount_point[0])
    {
        //
        // map client-side IIRC info to  FUSE filesystem at 'mountpoint'
        // FUSE insists on having its own context, so we need to spawn
        // a separate thread for it.
        //
        strncpy(fuse_args.name, "IIRC", MAX_FUSE_NAME);
        strncpy(fuse_args.mount_point, mount_point, PATH_MAX);
        fuse_args.hClient = g_hClient;

        /*
        fuse_args.argv = (char*)malloc(sizeof(char*) * argc);
        while(i = 0; i < argc, i++)
            fuse_args.argv[i] = argv[i];
        */

        if(thread_create(&fuse_thread, &iirc_fuse_main, &fuse_args) <= 0)
        {
            return 1;
        }
    }
#endif

    //
    // while life, the universe, and everything
    //
    while(6*9) {
        if(iircClientExecute(g_hClient, TRUE) <= 0)
            break;
    } // while(42)

    return EXIT_SUCCESS;
}

void iircExit()
{
    lt_dlexit();
    return;
}

int printUsage(char *appName)
{
    printf("iirc client v%.3f [coded by metric]\n", IIRC_CLIENT_VERSION);
    printf("usage: %s [options]\n", appName);
    printf("options:\n");
    printf("  help:    -h\n");
    printf("  server:  -s <ip-address> (default=%d)\n", DEFAULT_IP);
    printf("  port:    -p <port>       (default=%d)\n", DEFAULT_PORT);
#if HAVE_FUSE
    printf("  mount:   -m <dir>\n");
#endif
    return 1;
}

int initLog()
{
    initLogCallback((logFunc)&OnLogMessage);
    return 1;
}

int initIrcClient(char *hostname, short port)
{
    int i;
    int timeo;
    iircInterruptDescr intr;
    iircClientDescr iircClient;
    iircUserDescr user;
    char clientHostName[] = "earth";
    char clientNick[] = "IIRC_Client";
    char clientRealname[] = "IIRC_Realname";
    char clientIdent[] = "IIRC_Identity";

    //
    // Find and load any IIRC client modules
    //if(iircModuleGet(&iircModule) < 0) // load ASS client module TODO: name
    //  return -1;
    //    iircRegisterModule(&iircModule); // register module with the iirc client
    iircClient.version = IIRC_CLIENT_DESCR_VERSION;
    strncpy(iircClient.hostname, clientHostName, MAX_HOST_NAME);
    iircClient.hostname[MAX_HOST_NAME-1] = '\0';
    strncpy(iircClient.servername, hostname, MAX_HOST_NAME);
    iircClient.servername[MAX_HOST_NAME-1] = '\0';
    iircClient.serverPort = port;
    iircClient.fnPreCargoHandler = myCargoHandler;
    iircClient.fnPostCargoHandler = myCargoHandler;
    g_hClient = iircClientAdd(&iircClient);
    if(g_hClient < 0) {
        LOG_MSG(LOG_LOW,"initIrcClient - iirc client failed to start.");
        return -1;
    }
    //
    // start connection to server
    timeo = 0;
    while((i = iircClientStart(g_hClient, 10000)) < 0) {
        if(i == IIRC_CLIENT_ERR_FATAL) {
            LOG_MSG(LOG_WARN,"initIrcClient - client failed to start.");
            return -1;
        }
        if(timeo++ >= 10000000) {
            LOG_MSG(LOG_USER,"Time-out while waiting for response from server");
            return -1;
        }
    }
    //
    // add console interrupt to client
    intr.version = IIRC_INTR_DESCR_VERSION;
    intr.fd = 0; // stdin
    intr.fnCallback = consoleCallback;
    intr.data = g_hClient;
    intr.pData = NULL;
    if(iircClientIntrAdd(g_hClient, &intr) < 0) {
        LOG_MSG(LOG_WARN,"initIrcClient - unable to add console interrupt");
    }
    //
    // add new user
    user.version = IIRC_USER_DESCR_VERSION;
    unicode_cpy(user.nick, clientNick, UTF8, MAX_IIRC_NICK);
    unicode_cpy(user.realname, clientRealname, UTF8, MAX_IIRC_REAL_NAME);
    strncpy(user.identity, clientIdent, MAX_IIRC_IDENTITY);
    user.identity[MAX_IIRC_IDENTITY-1] = '\0';
    if(iircClientUserAdd(g_hClient, &user) < 0) {
        LOG_MSG(LOG_LOW,"initIrcClient - failed to add new user.");
        return -1;
    }
    //
    // wait for user reply from IIRC server
    /*
       timeo = 0;
       LOG_MSG(LOG_USER,"Waiting for user reply from IIRC server");
       g_hUser = iircClientUserFind(g_hClient, user.nick);
       while(g_hUser < 0) {
       iircClientExecute(g_hClient, FALSE);
       if(timeo++ >= 1000) {
       LOG_MSG(LOG_USER,"Timed out while waiting for user reply.");
       return -1;
       }
       g_hUser = iircClientUserFind(g_hClient, user.nick);
       }
     */
    return 1; // client should be up and spankin now
}

int myCargoHandler(iircClientCargo *pCargo, int *pStatus)
{
    //iircUserDescr userDesc;
    //cargoIrcMsg *pMsg;
    truck *pTruck = pCargo->pTruck;
    if(pTruck->type == CARGO_IIRC_MSG) {
        /*
           pMsg = (cargoIrcMsg*)pTruck;
           if(iircClientUserGet(pCargo->pSelf, pTruck->srcUser, &userDesc) < 0)
           return -1;
           VLOG_MSG(LOG_USER,"<%s> %s", userDesc.nick, pMsg->msg);
         */
    }
    return 1;
}

int OnLogMessage(char *logMsg)
{
    printf("%s\n", logMsg);
    return 1;
}

int consoleCallback(unsigned int hIntr, int data, void *pData)
{
    unsigned int i, j, marker;
    ssize_t nread;
    char line[MAX_CONSOLE_LINE];
    unsigned int lineLen = g_nConsoleBytes;
    char *buf = g_consoleBuf + lineLen;
    size_t buflen = MAX_CONSOLE_LINE - lineLen;

    nread = read(g_fdConsole, buf, buflen-1);
    if(nread <= 0) {
        if(nread == 0)
            return 1; // EOF on standard in?
        if(errno == EINTR)
            return 1;
        if(errno == EWOULDBLOCK || errno == EAGAIN)
            return 1;
        LOG_MSG(LOG_SYS,"consoleCallback - read failed");
        //iircClientIntrDel(g_pClient, hIntr);
        return -1; // delete this interrupt
    }
    lineLen += nread;
    i = marker = 0;
    while(TRUE) {
        j = 0;
        while(i < lineLen && g_consoleBuf[i] != '\n')
            line[j++] = g_consoleBuf[i++];
        if(i >= lineLen)
            break;
        line[j] = '\0'; // in the place of new-line
        consoleProcessLine(line);
        i++; // skip new line
        marker = i;
    }
    for(i = 0; i < lineLen - marker; i++)
        g_consoleBuf[i] = g_consoleBuf[i+marker];
    g_nConsoleBytes = lineLen - marker;
    return 1;
}

int consoleProcessLine(char *line)
{
    //    iircClientCmd(g_pClient, g_pUser, g_pChan, line);
    return 1;
}

