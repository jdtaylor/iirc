
/*
 * Copyright (c) 2001-2002,  James D. Taylor.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * contact James D. Taylor:  james.d.taylor@gmail.com
 */

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "iirc/rfc1459.h"
#include "metric/char.h"


#define SERV iircServer_t *pServ

/*!
 * from "Internet Relay Chat Protocol":
 *
 * Network Working Group                               J. Oikarinen
 * Request for Comments: 1459                          D. Reed
 *                             May 1993
 *
 * http://www.faqs.org/rfcs/rfc1459.html
 */

/*
 * I don't see anything in the RFC about the next two, so
 * I guess it is unlimited
 */
#define RFC1459_CHAN_MAX RFC1459_MSG_MAX
#define RFC1459_NICK_MAX RFC1459_MSG_MAX

enum {
    ERR_NOSUCHNICK = 401,   //!
    ERR_NOSUCHSERVER,       //!
    ERR_NOSUCHCHANNEL,      //!
    ERR_CANNOTSENDTOCHAN,   //!
    ERR_TOOMANYCHANNELS,    //!
    ERR_WASNOSUCHNICK,      //!
    ERR_TOOMANYTARGETS,     //!
    ERR_NOORIGIN = 409,     //!
    ERR_NORECIPIENT = 411,  //!
    ERR_NOTEXTTOSEND,       //!
    ERR_NOTOPLEVEL,     //!
    ERR_WILDTOPLEVEL,       //!
    ERR_UNKNOWNCOMMAND = 421,   //!
    ERR_NOMOTD,         //!
    ERR_NOADMININFO,        //!
    ERR_FILEERROR,      //!
    ERR_NONICKNAMEGIVEN = 431,  //!
    ERR_ERRONEUSNICKNAME,   //!
    ERR_NICKNAMEINUSE,      //!
    ERR_NICKCOLLISION = 436,    //!
    ERR_USERNOTINCHANNEL = 441, //!
    ERR_NOTONCHANNEL,       //!
    ERR_USERONCHANNEL,      //!
    ERR_NOLOGIN,        //!
    ERR_SUMMONDISABLED,     //!
    ERR_USERSDISABLED,      //!
    ERR_NOTREGISTERED = 451,    //!
    ERR_NEEDMOREPARAMS = 461,   //!
    ERR_ALREADYREGISTRED,   //!
    ERR_NOPERMFORHOST,      //!
    ERR_PASSWDMISMATCH,     //!
    ERR_YOUREBANNEDCREEP,   //!
    ERR_YOUWILLBEBANNED,    //! reserved
    ERR_KEYSET,         //!
    ERR_CHANNELISFULL = 471,    //!
    ERR_UNKNOWNMODE,        //!
    ERR_INVITEONLYCHAN,     //!
    ERR_BANNEDFROMCHAN,     //!
    ERR_BADCHANNELKEY,      //!
    ERR_BADCHANMASK,        //! reserved
    ERR_NOPRIVILEGES = 481, //!
    ERR_CHANOPRIVSNEEDED,   //!
    ERR_CANTKILLSERVER,     //!
    ERR_NOOPERHOST = 491,   //!
    ERR_NOSERVICEHOST,      //! reserved
    ERR_UMODEUNKNOWNFLAG = 501, //!
    ERR_USERSDONTMATCH      //!
};

enum {
    RPL_TRACELINK = 200,    //!
    RPL_TRACECONNECTING,    //!
    RPL_TRACEHANDSHAKE,     //!
    RPL_TRACEUNKNOWN,       //!
    RPL_TRACEOPERATOR,      //!
    RPL_TRACEUSER,      //!
    RPL_TRACESERVER,        //!
    RPL_TRACENEWTYPE = 208, //!
    RPL_TRACECLASS,     //! reserved
    RPL_STATSLINKINFO = 211,    //!
    RPL_STATSCOMMANDS,      //!
    RPL_STATSCLINE,     //!
    RPL_STATSNLINE,     //!
    RPL_STATSILINE,     //!
    RPL_STATSKLINE,     //!
    RPL_STATSQLINE,     //! reserved
    RPL_STATSYLINE,     //!
    RPL_ENDOFSTATS,     //!
    RPL_UMODEIS = 221,      //!
    RPL_SERVICEINFO = 231,  //! reserved
    RPL_ENDOFSERVICES,      //! reserved
    RPL_SERVICE,        //! reserved
    RPL_SERVLIST,       //! reserved
    RPL_SERVLISTEND,        //! reserved
    RPL_STATSLLINE = 241,   //!
    RPL_STATSUPTIME,        //!
    RPL_STATSOLINE,     //!
    RPL_STATSHLINE,     //!
    RPL_LUSERCLIENT = 251,  //!
    RPL_LUSEROP,        //!
    RPL_LUSERUNKNOWN,   //!
    RPL_LUSERCHANNELS,  //!
    RPL_LUSERME,    //!
    RPL_ADMINME,    //!
    RPL_ADMINLOC1,  //!
    RPL_ADMINLOC2,  //!
    RPL_ADMINEMAIL, //!
    RPL_TRACELOG = 261, //!
    RPL_NONE = 300, //! not used
    RPL_AWAY,       //!
    RPL_USERHOST,   //! ":<nick>['*'] '=' <'+'|'-'><hostname>"
    RPL_ISON,       //!
    RPL_UNAWAY,     //!
    RPL_NOWAWAY,    //!
    RPL_WHOISUSER = 311,//!
    RPL_WHOISSERVER,    //!
    RPL_WHOISOPERATOR,  //!
    RPL_WHOWASUSER, //!
    RPL_ENDOFWHO,   //!
    RPL_WHOISCHANOP,    //! reserved
    RPL_WHOISIDLE,  //!
    RPL_ENDOFWHOIS, //!
    RPL_WHOISCHANNELS,  //!
    RPL_LISTSTART = 321,//!
    RPL_LIST,       //!
    RPL_LISTEND,    //!
    RPL_CHANNELMODEIS,  //!
    RPL_NOTOPIC = 331,  //!
    RPL_TOPIC,      //!
    RPL_INVITING = 341, //!
    RPL_SUMMONING,  //!
    RPL_VERSION = 351,  //!
    RPL_WHOREPLY,   //!
    RPL_NAMREPLY,   //! "<channel> :[[@|+]<nick> [[@|+]<nick> [...]]]"
    RPL_KILLDONE = 361, //! reserved
    RPL_CLOSING,    //! reserved
    RPL_CLOSEEND,   //! reserved
    RPL_LINKS,      //!
    RPL_ENDOFLINKS, //!
    RPL_ENDOFNAMES, //! "<channel> :End of /NAMES list"
    RPL_BANLIST,    //!
    RPL_ENDOFBANLIST,   //!
    RPL_ENDOFWHOWAS,    //!
    RPL_INFO = 371, //!
    RPL_MOTD,       //! ":- <text>"
    RPL_INFOSTART,  //! reserved
    RPL_ENDOFINFO,  //!
    RPL_MOTDSTART,  //! ":- <server> Message of the day - "
    RPL_ENDOFMOTD,  //! ":End of /MOTD command"
    RPL_YOUREOPER = 381,//!
    RPL_REHASHING,  //!
    RPL_MYPORTIS = 384, //! reserved
    RPL_TIME = 391, //!
    RPL_USERSSTART, //!
    RPL_USERS,      //!
    RPL_ENDOFUSERS, //!
    RPL_NOUSERS     //!
};

enum {
    RFC1459_CMD_JOIN = 600,
    RFC1459_CMD_KICK,
    RFC1459_CMD_KILL,
    RFC1459_CMD_MODE,
    RFC1459_CMD_PART,
    RFC1459_CMD_PING,
    RFC1459_CMD_PONG,
    RFC1459_CMD_QUIT,
    RFC1459_CMD_ERROR,
    RFC1459_CMD_TOPIC,
    RFC1459_CMD_NOTICE,
    RFC1459_CMD_PRIVMSG,
    RFC1459_CMD_MAX
};

//!
// <chstring> = <any 8bit code except SPACE, BELL, NUL, CR, LF and comma (',')>
// SPACE=0x20; BELL=0x07; NUL=0x0; CR=0xd; LF=0xa; comma=0x2c
static char chreject[] = {0x20, 0x07, 0xd, 0xa, 0x2c, 0x00};

extern int servTruckProcess(SERV, truck *pTruck, int *pStatus);
extern int servTruckInternal(SERV, mUint32 hCom, truck *pTruck, int *pStatus);

/*!
 * 'msg', 'prefix', and 'cmd' should all be character buffers of
 * length RFC1459_MSG_MAX.  'params' should be an array of 15 buffers
 * of length RFC1459_MSG_MAX.
 * 'msg' is split into a prefix, command, and up to 15 parameters.
 */
int rfc1459SplitMsg(char **msg, char *prefix, char *cmd,
        char (*params)[RFC1459_MSG_MAX]);
//!
// 'chan' is an array of RFC1459_CHAN_MAX characters
int rfc1459GetChannel(char **haystack, char *chan);
//!
// 'nick' is an array of RFC1459_NICK_MAX characters
int rfc1459GetNick(char **haystack, char *nick);

static int rfc1459SendMOTD(iircServer_t *pServ, iircUser_t *pUser);
static int rfc1459EmulateInterstate(SERV, rawClient_t *pClient, truck *pTruck);


static int rfc1459HandleNICK(SERV, rawClient_t *pClient, char *msg,
        char *prefix, char (*params)[RFC1459_MSG_MAX]);
static int rfc1459HandleUSER(SERV, rawClient_t *pClient, char *msg,
        char *prefix, char (*params)[RFC1459_MSG_MAX]);
static int rfc1459HandleJOIN(SERV, rawClient_t *pClient, char *msg,
        char *prefix, char (*params)[RFC1459_MSG_MAX]);
static int rfc1459HandleMODE(SERV, rawClient_t *pClient, char *msg,
        char *prefix, char (*params)[RFC1459_MSG_MAX]);
static int rfc1459HandlePING(SERV, rawClient_t *pClient, char *msg,
        char *prefix, char (*params)[RFC1459_MSG_MAX]);
static int rfc1459HandlePONG(SERV, rawClient_t *pClient, char *msg,
        char *prefix, char (*params)[RFC1459_MSG_MAX]);
static int rfc1459HandlePRIV(SERV, rawClient_t *pClient, char *msg,
        char *prefix, char (*params)[RFC1459_MSG_MAX]);
static int rfc1459HandleNOTI(SERV, rawClient_t *pClient, char *msg,
        char *prefix, char (*params)[RFC1459_MSG_MAX]);
static int rfc1459HandleQUIT(SERV, rawClient_t *pClient, char *msg,
        char *prefix, char (*params)[RFC1459_MSG_MAX]);
static int rfc1459HandleWHO(SERV, rawClient_t *pClient, char *msg,
        char *prefix, char (*params)[RFC1459_MSG_MAX]);
static int rfc1459HandleWHOIS(SERV, rawClient_t *pClient, char *msg,
        char *prefix, char (*params)[RFC1459_MSG_MAX]);
static int rfc1459HandleNAMES(SERV, rawClient_t *pClient, char *msg,
        char *prefix, char (*params)[RFC1459_MSG_MAX]);
static int rfc1459HandleUSERHOST(SERV, rawClient_t *pClient, char *msg,
        char *prefix, char (*params)[RFC1459_MSG_MAX]);

static int rfc1459DispatchUser(     SERV, rawClient_t*, iircUser_t*, truck*);
static int rfc1459DispatchEvent(    SERV, rawClient_t*, iircUser_t*, truck*);
static int rfc1459DispatchNick(     SERV, rawClient_t*, iircUser_t*, truck*);
static int rfc1459DispatchChanMode( SERV, rawClient_t*, iircUser_t*, truck*);
static int rfc1459DispatchChanTopic(SERV, rawClient_t*, iircUser_t*, truck*);
static int rfc1459DispatchChanUsers(SERV, rawClient_t*, iircUser_t*, truck*);
static int rfc1459DispatchJoin(     SERV, rawClient_t*, iircUser_t*, truck*);
static int rfc1459DispatchChanModArg(SERV, rawClient_t*, iircUser_t*, truck*);
static int rfc1459DispatchMsg(      SERV, rawClient_t*, iircUser_t*, truck*);
static int rfc1459DispatchQuit(     SERV, rawClient_t*, iircUser_t*, truck*);


/*!
 * 'msg', 'prefix', and 'cmd' should all be character buffers of
 * length RFC1459_MSG_MAX.  'params' should be an array of 15 buffers
 * of length RFC1459_MSG_MAX.
 * 'msg' is split into a prefix, command, and up to 15 parameters.
 */
int rfc1459SplitMsg(char **msg, char *prefix, char *cmd,
        char (*params)[RFC1459_MSG_MAX])
{
    unsigned int i;
    unsigned int msgLen;
    char *curMsg = *msg;
    char *nextMsg;
    char *pChar;

    prefix[0] = '\0';
    msgLen = strlen(*msg);
    if(!msgLen)
        return 0;  // no more messages
    i = 0;
    /*
     * Chatzilla uses just a newline, not \r\n as in rfc1459.
    //
    // find next cr-lf and replace with '\0'
    while(i <= msgLen-2 && curMsg[i] != '\r' && curMsg[i+1] != '\n')
    i++;
     */
    while(i < msgLen && curMsg[i] != '\n')
        i++;
    if(i < msgLen) {
        // more valid messages after this one
        if(i > 0 && curMsg[i-1] == '\r')
            curMsg[i-1] = '\0'; // kill the carriage-return character
        else
            curMsg[i] = '\0';
        nextMsg = &(curMsg[i+1]); // skip newline for next parameter
    } else {
        // this is the last valid message
        nextMsg = &(curMsg[msgLen]); // set to '\0'
    }
    msgLen = strlen(curMsg);
    if(msgLen <= 2)
        return 0; // no more messages
    if(curMsg[0] == ':') {
        // Process Prefix.
        // : Clients should not use prefix when sending a message from
        // : themselves; if they use a prefix, the only valid prefix
        // : is the registered nickname associated with the client.
        curMsg++; // skip ':'
        if(Strntok(prefix, &curMsg, ' ', RFC1459_MSG_MAX) == 0) {
            *msg = nextMsg;
            return 0;
        }
    }
    // find the command in this message
    if(Strntok(cmd, &curMsg, ' ', RFC1459_MSG_MAX) == 0) {
        *msg = nextMsg;
        return 0;
    }
    if(strlen(cmd) <= 2) {
        *msg = nextMsg;
        return 0; // command must be at least 3 characters
    }
    for(i = 0; i < strlen(cmd); i++)
        cmd[i] = toupper(cmd[i]);
    pChar = curMsg; // backup curMsg, because Strntok will change it
    //
    // get parameters
    i = 0;
    while(i < 15 && Strntok(params[i], &curMsg, ' ', RFC1459_MSG_MAX)) {
        if(params[i][0] == ':') {
            pChar++; // skip ':'
            strncpy(params[i], pChar, RFC1459_MSG_MAX);
            params[i][RFC1459_MSG_MAX-1] = '\0';
            i++;
            while(i < 15) {
                params[i][0] = '\0';
                i++;
            }
            *msg = nextMsg;
            return 1;
        }
        params[i][RFC1459_MSG_MAX-1] = '\0';
        pChar = curMsg; // backup curMsg, because Strntok will change it
        i++;
    }
    while(i < 15) {
        params[i][0] = '\0';
        i++;
    }
    *msg = nextMsg;
    return 1;
}


//!
// <channel> = ('#' | '&') <chstring>
// TODO: aUDIT tHIS pLEASE!
int rfc1459GetChannel(char **haystack, char *chan)
{
    unsigned int i;
    int chanLen = 0;
    size_t len = strcspn(*haystack, chreject);

    for(i = 0; i < len; i++) {
        if(chanLen) {
            if(chanLen >= RFC1459_CHAN_MAX) {
                chan[RFC1459_CHAN_MAX-1] = '\0';
                *haystack += len;
                return RFC1459_CHAN_MAX-1;
            }
            chan[chanLen++] = (*haystack)[i];
            continue;
        }
        if((*haystack)[i] == '#' || (*haystack)[i] == '&') {
            chan[0] = (*haystack)[i];
            chanLen = 1;
        }
    }
    chan[chanLen] = '\0';
    *haystack += len;
    return chanLen;
}

//!
// <nick> = <letter> { <letter> | <number> | <special> }
// <special> = '-' | '[' | ']' | '\' | '`' | '^' | '{' | '}'
int rfc1459GetNick(char **haystack, char *nick)
{
    char cur;
    int nickLen = 0;
    cur = *(*haystack);
    while(cur != '\0')
    {
        if(nickLen)
        {
            if(nickLen >= RFC1459_NICK_MAX) {
                nick[RFC1459_NICK_MAX-1] = '\0';
                return RFC1459_NICK_MAX-1;
            }
            if(isalnum(cur) ||
                    cur == '-' ||
                    cur == '[' ||
                    cur == ']' ||
                    cur == '\\' ||
                    cur == '`' ||
                    cur == '^' ||
                    cur == '{' ||
                    cur == '}')
            {
                nick[nickLen++] = cur;
            }
            else
                break;
        }
        else if(isalpha(cur)) {
            nick[0] = cur;
            nickLen = 1;
        }
        *haystack += 1;
        cur = *(*haystack);
    }
    nick[nickLen] = '\0';
    return nickLen;
}

int rfc1459TruckDispatch(iircServer_t *pServ, iircUser_t *pUser, truck *pTruck)
{
    rawClient_t *pClient;

    if(!(pUser->flags & IIRC_USER_1459)) {
        LOG_MSG(LOG_LOW,"rfc1459TruckDispatch - not an rfc1459 client");
        return -1;
    }
    if(pTruck->type < CARGO_IIRC_INTERNAL) {
        // Interstate Truck
        switch(pTruck->type) {
            case CARGO_IIRC_USER:
                return rfc1459DispatchUser(pServ, NULL, pUser, pTruck);
        }
        return 1;
    }
    pClient = (rawClient_t*)rawClientSet(Get)(&pServ->rfc1459Set, pUser->hCom);
    if(pClient == NULL) {
        VLOG_MSG(LOG_LOW,"rfc1459TruckDispatch - unknown hCom: %d",pUser->hCom);
        return -1;
    }
    switch(pTruck->type) {
        case CARGO_IIRC_EVENT:
            return rfc1459DispatchEvent(pServ, pClient, pUser, pTruck);
        case CARGO_IIRC_CHAN_MODE:
            return rfc1459DispatchChanMode(pServ, pClient, pUser, pTruck);
        case CARGO_IIRC_CHAN_TOPIC:
            return rfc1459DispatchChanTopic(pServ, pClient, pUser, pTruck);
        case CARGO_IIRC_CHAN_USERS:
            return rfc1459DispatchChanUsers(pServ, pClient, pUser, pTruck);
        case CARGO_IIRC_NICK:
            return rfc1459DispatchNick(pServ, pClient, pUser, pTruck);
        case CARGO_IIRC_JOIN:
            return rfc1459DispatchJoin(pServ, pClient, pUser, pTruck);
        case CARGO_IIRC_QUIT:
            return rfc1459DispatchQuit(pServ, pClient, pUser, pTruck);
        case CARGO_IIRC_CHAN_MOD_ARG:
            return rfc1459DispatchChanModArg(pServ, pClient, pUser, pTruck);
        case CARGO_IIRC_MSG:
            return rfc1459DispatchMsg(pServ, pClient, pUser, pTruck);
        default:
            return 1;
    }
    return 1;
}

int rfc1459MsgHandler(iircServer_t *pServ, rawClient_t *pClient, char *msg)
{
    int i, j;
    int cmd;
    int msgLen = strlen(msg);
    char *pMsg;
    char prefix[RFC1459_MSG_MAX];
    char cmdBuf[RFC1459_MSG_MAX];
    char params[15][RFC1459_MSG_MAX]; // 15 parameters

    if(msgLen <= 0 || msgLen > RFC1459_MSG_MAX)
        return 0;
    pMsg = msg;
    i = rfc1459SplitMsg(&pMsg, prefix, cmdBuf, params);
    while(i > 0)
    {
        if(isdigit(cmdBuf[0]) && isdigit(cmdBuf[1]) && isdigit(cmdBuf[2])) {
            cmd = 0;
            sscanf(cmdBuf, "%3d", &cmd);
            if(cmd < RFC1459_CMD_JOIN || cmd >= RFC1459_CMD_MAX) {
                VLOG_MSG(LOG_LOW,"rfc1459MsgHandler - invalid command %d", cmd);
                return -1; // fatal?
            }
            switch(cmd) {
                case RFC1459_CMD_JOIN:
                    j = rfc1459HandleJOIN(pServ, pClient, msg, prefix, params);
                    break;
                case RFC1459_CMD_KICK:
                    break;
                case RFC1459_CMD_KILL:
                    break;
                case RFC1459_CMD_MODE:
                    j = rfc1459HandleMODE(pServ, pClient, msg, prefix, params);
                    break;
                case RFC1459_CMD_PART:
                    break;
                case RFC1459_CMD_PING:
                    break;
                case RFC1459_CMD_PONG:
                    break;
                case RFC1459_CMD_QUIT:
                    break;
                case RFC1459_CMD_ERROR:
                    break;
                case RFC1459_CMD_TOPIC:
                    break;
                case RFC1459_CMD_NOTICE:
                    break;
                case RFC1459_CMD_PRIVMSG:
                    break;
            }
            if(j < 0) {
                LOG_MSG(LOG_LOW,"rfc1459MsgHandler - disconnecting client.");
                return -1;
            }
            //
            // get next command in the message
            i = rfc1459SplitMsg(&pMsg, prefix, cmdBuf, params);
            continue;
        }
        switch(cmdBuf[0])
        {
            case 'A':
                if(strncmp(cmdBuf, "ADMIN", RFC1459_MSG_MAX) == 0) {
                }
                if(strncmp(cmdBuf, "AWAY", RFC1459_MSG_MAX) == 0) {
                }
                break;
            case 'C':
                if(strncmp(cmdBuf, "CONNECT", RFC1459_MSG_MAX) == 0) {
                }
                break;
            case 'E':
                if(strncmp(cmdBuf, "ERROR", RFC1459_MSG_MAX) == 0) {
                }
                break;
            case 'I':
                if(strncmp(cmdBuf, "INVITE", RFC1459_MSG_MAX) == 0) {
                }
                if(strncmp(cmdBuf, "INFO", RFC1459_MSG_MAX) == 0) {
                }
                if(strncmp(cmdBuf, "ISON", RFC1459_MSG_MAX) == 0) {
                }
                break;
            case 'J':
                if(strncmp(cmdBuf, "JOIN", RFC1459_MSG_MAX) == 0) {
                    j = rfc1459HandleJOIN(pServ, pClient, msg, prefix, params);
                    break;
                }
                break;
            case 'K':
                if(strncmp(cmdBuf, "KICK", RFC1459_MSG_MAX) == 0) {
                }
                if(strncmp(cmdBuf, "KILL", RFC1459_MSG_MAX) == 0) {
                }
                break;
            case 'L':
                if(strncmp(cmdBuf, "LIST", RFC1459_MSG_MAX) == 0) {
                }
                if(strncmp(cmdBuf, "LINKS", RFC1459_MSG_MAX) == 0) {
                }
                break;
            case 'M':
                if(strncmp(cmdBuf, "MODE", RFC1459_MSG_MAX) == 0) {
                    j = rfc1459HandleMODE(pServ, pClient, msg, prefix, params);
                    break;
                }
                break;
            case 'N':
                if(strncmp(cmdBuf, "NICK", RFC1459_MSG_MAX) == 0) {
                    j = rfc1459HandleNICK(pServ, pClient, msg, prefix, params);
                    break;
                }
                if(strncmp(cmdBuf, "NAMES", RFC1459_MSG_MAX) == 0) {
                    j = rfc1459HandleNAMES(pServ, pClient, msg, prefix, params);
                }
                if(strncmp(cmdBuf, "NOTICE", RFC1459_MSG_MAX) == 0) {
                    j = rfc1459HandleNOTI(pServ, pClient, msg, prefix, params);
                    break;
                }
                break;
            case 'O':
                if(strncmp(cmdBuf, "OPER", RFC1459_MSG_MAX) == 0) {
                }
                break;
            case 'P':
                if(strncmp(cmdBuf, "PART", RFC1459_MSG_MAX) == 0) {
                }
                if(strncmp(cmdBuf, "PRIVMSG", RFC1459_MSG_MAX) == 0) {
                    j = rfc1459HandlePRIV(pServ, pClient, msg, prefix, params);
                    break;
                }
                if(strncmp(cmdBuf, "PING", RFC1459_MSG_MAX) == 0) {
                    j = rfc1459HandlePING(pServ, pClient, msg, prefix, params);
                    break;
                }
                if(strncmp(cmdBuf, "PONG", RFC1459_MSG_MAX) == 0) {
                    j = rfc1459HandlePONG(pServ, pClient, msg, prefix, params);
                    break;
                }
                break;
            case 'Q':
                if(strncmp(cmdBuf, "QUIT", RFC1459_MSG_MAX) == 0) {
                    j = rfc1459HandleQUIT(pServ, pClient, msg, prefix, params);
                    break;
                }
                break;
            case 'R':
                if(strncmp(cmdBuf, "REHASH", RFC1459_MSG_MAX) == 0) {
                }
                if(strncmp(cmdBuf, "RESTART", RFC1459_MSG_MAX) == 0) {
                }
                break;
            case 'S':
                if(strncmp(cmdBuf, "SERVER", RFC1459_MSG_MAX) == 0) {
                }
                if(strncmp(cmdBuf, "STATS", RFC1459_MSG_MAX) == 0) {
                }
                if(strncmp(cmdBuf, "SUMMON", RFC1459_MSG_MAX) == 0) {
                }
                break;
            case 'T':
                if(strncmp(cmdBuf, "TOPIC", RFC1459_MSG_MAX) == 0) {
                }
                if(strncmp(cmdBuf, "TIME", RFC1459_MSG_MAX) == 0) {
                }
                if(strncmp(cmdBuf, "TRACE", RFC1459_MSG_MAX) == 0) {
                }
                break;
            case 'U':
                if(strncmp(cmdBuf, "USER", RFC1459_MSG_MAX) == 0) {
                    j = rfc1459HandleUSER(pServ, pClient, msg, prefix, params);
                    break;
                }
                if(strncmp(cmdBuf, "USERS", RFC1459_MSG_MAX) == 0) {
                }
                if(strncmp(cmdBuf, "USERHOST", RFC1459_MSG_MAX) == 0) {
                    j = rfc1459HandleUSERHOST(pServ, pClient, msg, prefix,
                            params);
                    break;
                }
                break;
            case 'V':
                if(strncmp(cmdBuf, "VERSION", RFC1459_MSG_MAX) == 0) {
                }
                break;
            case 'W':
                if(strncmp(cmdBuf, "WHO", RFC1459_MSG_MAX) == 0) {
                    j = rfc1459HandleWHO(pServ, pClient, msg, prefix, params);
                    break;
                }
                if(strncmp(cmdBuf, "WHOIS", RFC1459_MSG_MAX) == 0) {
                    j = rfc1459HandleWHOIS(pServ, pClient, msg, prefix, params);
                    break;
                }
                if(strncmp(cmdBuf, "WHOWAS", RFC1459_MSG_MAX) == 0) {
                }
                if(strncmp(cmdBuf, "WALLOPS", RFC1459_MSG_MAX) == 0) {
                }
                break;
        }
        if(j < 0) {
            LOG_MSG(LOG_LOW,"rfc1459MsgHandler - disconnecting client.");
            return -1;
        }
        //
        // get next command in the message
        i = rfc1459SplitMsg(&pMsg, prefix, cmdBuf, params);
    }
    if(i < 0) {
        VLOG_MSG(LOG_LOW,"rfc1459MsgHandler - invalid message: %s", msg);
        return -1;
    }
    return 1;
}

int rfc1459SendMOTD(SERV, iircUser_t *pUser)
{
    socket_t clientSock;
    char serverResponse[RFC1459_MSG_MAX];
    rawClient_t *pClient;
    //
    // get rfc1459 socket of this user
    pClient = (rawClient_t*)rawClientSet(Get)(&pServ->rfc1459Set, pUser->hCom);
    if(pClient == NULL) {
        VLOG_MSG(LOG_LOW,"rfc1459SendMOTD - %s's hCom is invalid.",
                pUser->nick);
        return -1;
    }
    clientSock = pClient->sock;
    //
    // start sending responses to client
    snprintf(serverResponse, RFC1459_MSG_MAX,
            ":%s 001 %s :Hi %s; Welcome to %s\r\n", pServ->hostname,
            pUser->nick, pUser->nick, pServ->name);
    socketWrite(&clientSock, serverResponse, strlen(serverResponse));
    snprintf(serverResponse, RFC1459_MSG_MAX,
            ":%s 002 %s :Your host is %s/6667, running InterstateIRC v%d\r\n",
            pServ->hostname, pUser->nick, pServ->hostname,IIRC_SERVER_VERSION);
    socketWrite(&clientSock, serverResponse, strlen(serverResponse));
    snprintf(serverResponse, RFC1459_MSG_MAX,
            ":%s 003 %s :blah blha blha Dec. 6th 1977 haaaw.\r\n",
            pServ->hostname, pUser->nick);
    socketWrite(&clientSock, serverResponse, strlen(serverResponse));
    snprintf(serverResponse, RFC1459_MSG_MAX,
            ":%s 004 %s %s %s abBcCdDeEfFgGhHiIkKlLmMnNopPrRsSUvVwW"
            "xXyYzZ0123459*@ bcdefFghiIklmnoPqstv\r\n",
            pServ->hostname, pUser->nick, pUser->hostname, "InterstateIRC");
    socketWrite(&clientSock, serverResponse, strlen(serverResponse));
    snprintf(serverResponse, RFC1459_MSG_MAX,
            ":%s %d %s :There are %d victims and %d hiding on %d servers\r\n",
            pServ->hostname, RPL_LUSERCLIENT, pUser->nick, 0, 0, 0);
    socketWrite(&clientSock, serverResponse, strlen(serverResponse));
    snprintf(serverResponse, RFC1459_MSG_MAX,
            ":%s %d %s %d :flagged staff members\r\n",
            pServ->hostname, RPL_LUSEROP, pUser->nick, 0);
    socketWrite(&clientSock, serverResponse, strlen(serverResponse));
    snprintf(serverResponse, RFC1459_MSG_MAX,
            ":%s %d %s %d :channels formed\r\n",
            pServ->hostname, RPL_LUSERCHANNELS, pUser->nick, 0);
    socketWrite(&clientSock, serverResponse, strlen(serverResponse));
    snprintf(serverResponse, RFC1459_MSG_MAX,
            ":%s %d %s :I have %d clients and %d servers\r\n",
            pServ->hostname, RPL_LUSERME, pUser->nick, 0, 0);
    socketWrite(&clientSock, serverResponse, strlen(serverResponse));
    snprintf(serverResponse, RFC1459_MSG_MAX,
            ":%s %d %s :Current local users: %d Max: %d\r\n",
            pServ->hostname, 265, pUser->nick, 0, 0);
    socketWrite(&clientSock, serverResponse, strlen(serverResponse));
    snprintf(serverResponse, RFC1459_MSG_MAX,
            ":%s %d %s :Current global users: %d Max: %d\r\n",
            pServ->hostname, 266, pUser->nick, 0, 0);
    socketWrite(&clientSock, serverResponse, strlen(serverResponse));
    snprintf(serverResponse, RFC1459_MSG_MAX,
            ":%s %d %s :Highest connection count: %d (%d clients)"
            " (%d since server was (re)startedr\n",
            pServ->hostname, 250, pUser->nick, 0, 0, 0);
    socketWrite(&clientSock, serverResponse, strlen(serverResponse));
    snprintf(serverResponse, RFC1459_MSG_MAX,
            ":%s %d %s :- %s Message of the Day -\r\n", pServ->hostname,
            RPL_MOTDSTART, pUser->nick, pServ->name);
    socketWrite(&clientSock, serverResponse, strlen(serverResponse));
    snprintf(serverResponse, RFC1459_MSG_MAX,
            ":%s %d %s :- Welcome to metric's iIRCD. -\r\n", pServ->hostname,
            RPL_MOTD, pUser->nick);
    socketWrite(&clientSock, serverResponse, strlen(serverResponse));
    snprintf(serverResponse, RFC1459_MSG_MAX,
            ":%s %d %s :- \r\n", pServ->hostname, RPL_MOTD, pUser->nick);
    socketWrite(&clientSock, serverResponse, strlen(serverResponse));
    snprintf(serverResponse, RFC1459_MSG_MAX,
            ":%s %d %s :End of /MOTD command.\r\n", pServ->hostname,
            RPL_ENDOFMOTD, pUser->nick);
    socketWrite(&clientSock, serverResponse, strlen(serverResponse));
    return 1;
}


static int rfc1459EmulateInterstate(SERV, rawClient_t *pClient, truck *pTruck)
{
    int status = 0; // truck is not authoritative (it's from a client)
    //
    // Hi.  This is where the rfc1459 code injects the truck into the
    // iIRC server code.  If the truck cargo is needed for low level
    // stuff, then pass a handle to the communication method (pClient->id).
    //
    if(pTruck->type < CARGO_IIRC_INTERNAL)
        servTruckInternal(pServ, pClient->id, pTruck, &status);
    else
        servTruckProcess(pServ, pTruck, &status); // server.c
    //
    // check for flags returned by the iIRC code
    if(status & PROC_OUT_DISCONNECT) {
        LOG_MSG(LOG_USER,"<server> Closing RFC1459 client connection.");
        if(!(status & PROC_OUT_TRUCK_USED))
            FREE(pTruck);
        rawClientSet(Del)(&pServ->rfc1459Set, pClient->id);
        return -1;
    }
    if(status & PROC_OUT_STOP_ROUTING) {
        if(!(status & PROC_OUT_TRUCK_USED))
            FREE(pTruck);
        return 1;
    }
    // TODO: TODO: TODO: where dost thou memory go?
    //    if(cityTruckDispatch(pServ->hCity, pTruck) < 0) {
    //  LOG_MSG(LOG_LOW,"rfc1459EmulateInterstate - TODO");
    //  }
    if(!(status & PROC_OUT_TRUCK_USED))
        FREE(pTruck);
    return 1;
}


//
// Command: NICK
//    Parameters: <nickname> [ <hopcount> ]
//
// Numeric Replies:
//     ERR_NONICKNAMEGIVEN             ERR_ERRONEUSNICKNAME
//     ERR_NICKNAMEINUSE               ERR_NICKCOLLISION
//
int rfc1459HandleNICK(SERV, rawClient_t *pClient, char *msg,
        char *prefix, char (*params)[RFC1459_MSG_MAX])
{
    int status;
    char *curPos;
    char nick[RFC1459_NICK_MAX];
    cargoIrcNick *pCargo;
    iircUser_t *pUser = NULL;

    curPos = params[0];
    if(rfc1459GetNick(&curPos, nick) <= 0)
        return -1;
    iircStateLock(&pServ->state, FALSE);
    if(pClient->hUser >= 0)
        pUser = iircStateUserGet(&pServ->state, pClient->hUser, &status);
    if(pUser == NULL) {
        //VLOG_MSG(LOG_LOW,"rfc1459HandleNICK - unknown hUser: %d",
        //  pClient->hUser);
        unicode_cpy(pClient->name, nick, UTF8, MAX_RAW_CLIENT_NAME);
        iircStateUnlock(&pServ->state, FALSE);
        return 1;
    }
    VLOG_MSG(LOG_DUMP,"rfc1459HandleNICK - Got NICK from client: %d",
            pClient->id);
    pCargo = (cargoIrcNick*)MALLOC(pServ->state.truckSize);
    if(pCargo == NULL) {
        LOG_MSG(LOG_SYS,"rfc1459HandleNick - MALLOC failed");
        iircStateUnlock(&pServ->state, FALSE);
        return -1;
    }
    cargoIrcNickInit(pCargo);
    unicode_cpy(pCargo->nick, nick, UTF8, MAX_IIRC_NICK);
    pCargo->header.srcChan = 0;
    pCargo->header.srcUser = pUser->id;
    iircStateUnlock(&pServ->state, FALSE);
    rfc1459EmulateInterstate(pServ, pClient, (truck*)pCargo);
    return 1;
}

//
// Command: USER
//    Parameters: <username> <hostname> <servername> <realname>
int rfc1459HandleUSER(SERV, rawClient_t *pClient, char *msg,
        char *prefix, char (*params)[RFC1459_MSG_MAX])
{
    cargoIrcUser *pCargo;

    VLOG_MSG(LOG_DUMP,"rfc1459HandleUSER - Got USER from client: %d",
            pClient->id);
    pCargo = (cargoIrcUser*)MALLOC(pServ->state.truckSize);
    if(pCargo == NULL) {
        LOG_MSG(LOG_SYS,"rfc1459HandleUSER - MALLOC failed");
        return -1;
    }
    cargoIrcUserInit(pCargo);
    //cargoIrcUserPack(pCargo, pUser);
    //TODO: check if this user has already connected (don't USER twice!)
    pCargo->flags |= IIRC_USER_1459 | IIRC_USER_CONNECT;
    unicode_cpy(pCargo->nick, pClient->name, UTF8, MAX_IIRC_NICK);
    strncpy(pCargo->identity, params[0], MAX_IIRC_IDENTITY);
    strncpy(pCargo->hostname, params[1], MAX_HOST_NAME);
    // TODO: check with the interstate for valid hostname, possibly
    // just override whatever the client gives.
    strncpy(pCargo->hostname, pServ->hostname, MAX_HOST_NAME);
    strncpy(pCargo->servername, params[2], MAX_HOST_NAME);
    unicode_cpy(pCargo->realname, params[3], UTF8, MAX_IIRC_REAL_NAME);
    rfc1459EmulateInterstate(pServ, pClient, (truck*)pCargo);
    return 1;
}


//
// Command: JOIN
//    Parameters: <channel>{,<channel>} [<key>{,<key>}]
int rfc1459HandleJOIN(SERV, rawClient_t *pClient, char *msg,
        char *prefix, char (*params)[RFC1459_MSG_MAX])
{
    int status;
    char *curPos;
    char chan[RFC1459_CHAN_MAX];
    cargoIrcJoin *pCargo;
    iircUser_t *pUser;

    iircStateLock(&pServ->state, FALSE);
    pUser = iircStateUserGet(&pServ->state, pClient->hUser, &status);
    if(pUser == NULL) {
        VLOG_MSG(LOG_LOW,"rfc1459HandleJOIN - unknown hUser: %d",
                pClient->hUser);
        iircStateUnlock(&pServ->state, FALSE);
        return -1;
    }
    iircStateUnlock(&pServ->state, FALSE); // pUser
    curPos = params[0];
    if(rfc1459GetChannel(&curPos, chan) <= 0) {
        return -1;
    }
    pCargo = (cargoIrcJoin*)MALLOC(pServ->state.truckSize);
    if(pCargo == NULL) {
        LOG_MSG(LOG_SYS,"rfc1459HandleJOIN - MALLOC failed");
        return -1;
    }
    cargoIrcJoinInit(pCargo);
    unicode_cpy(pCargo->chan, chan, UTF8, MAX_IIRC_CHAN_NAME);
    pCargo->flags |= IIRC_JOIN_BY_NAME;
    // TODO: process multiple channels at one time
    //while(curPos[0] == ',') {
    //  chanLen = rfc1459GetChannel(&curPos, chan);
    //   }
    // TODO process keys
    //if(Strntok(pCargo->key, &curPos, ' ', MAX_IIRC_KEY) < 0) {
    //  return -1;
    //   }
    pCargo->header.srcChan = 0;
    pCargo->header.srcUser = pClient->hUser;
    rfc1459EmulateInterstate(pServ, pClient, (truck*)pCargo);
    return 1;
}

int rfc1459HandleMODE(SERV, rawClient_t *pClient, char *msg,
        char *prefix, char (*params)[RFC1459_MSG_MAX])
{
    int i;
    int status;
    int paramPos;
    int bExpectUserLimit, bExpectKey;
    int paramLen;
    unsigned int mode, modeSwitch;
    char *iParam;
    char msgOut[RFC1459_MSG_MAX];
    char msgBuf[RFC1459_MSG_MAX];
    char chan[RFC1459_CHAN_MAX];
    char modName[MAX_IIRC_MOD_NAME];
    char modArg[MAX_IIRC_MOD_ARG];
    mUint8 iircChan[MAX_IIRC_CHAN_NAME];
    mUint8 iircNick[MAX_IIRC_NICK];
    mUint8 iircModName[MAX_IIRC_MOD_NAME];
    iircChan_t *pChan;
    iircUser_t *pUser;
    cargoIrcChanMode *pChanCargo;
    cargoIrcChanModArg *pModCargo;
    cargoIrcModInfo *pMod;
    iircUser_t *pSrcUser;

    iircStateLock(&pServ->state, FALSE);
    pSrcUser = iircStateUserGet(&pServ->state, pClient->hUser, &status);
    if(pSrcUser == NULL) {
        iircStateUnlock(&pServ->state, FALSE);
        VLOG_MSG(LOG_LOW,"rfc1459HandleMODE - unknown hUser: %d",
                pClient->hUser);
        return -1;
    }
    VLOG_MSG(LOG_DUMP,"rfc1459HandleMODE - Got MODE from client: %d",
            pSrcUser->id);
    iParam = params[0];
    if(rfc1459GetChannel(&iParam, chan) <= 0) {
        //
        // no channel found as the first parameter, so maybe it is a
        // nick eh?
        iParam = params[0];
        if(rfc1459GetNick(&iParam, msgBuf) <= 0) {
            iircStateUnlock(&pServ->state, FALSE);
            return -1;
        }
        unicode_cpy(iircNick, msgBuf, UTF8, MAX_IIRC_NICK);
        pUser = iircStateUserFind(&pServ->state, iircNick, &status);
        if(pUser == NULL) {
            iircStateUnlock(&pServ->state, FALSE);
            VLOG_MSG(LOG_LOW,"rfc1459HandleMODE - unknown nick: %s", msgBuf);
            return -1;
        }
        iParam = params[1];
        if(iParam[0] == '\0') {
            // query for the mode
            snprintf(msgOut, RFC1459_MSG_MAX, ":%s %d %s +", pServ->hostname,
                    RPL_UMODEIS, pSrcUser->nick);
            if(pUser->modes & IIRC_USER_INVISIBLE)
                Strncat(msgOut, "i", RFC1459_MSG_MAX);
            if(pUser->modes & IIRC_USER_SERVER_NOTICE)
                Strncat(msgOut, "n", RFC1459_MSG_MAX);
            if(pUser->modes & IIRC_USER_WALLOPS)
                Strncat(msgOut, "w", RFC1459_MSG_MAX);
            if(pUser->modes & IIRC_USER_SYSOP)
                Strncat(msgOut, "s", RFC1459_MSG_MAX);
        }
        Strncat(msgOut, "\r\n", RFC1459_MSG_MAX);
        msgOut[RFC1459_MSG_MAX-1] = '\0';
        socketWrite(&pClient->sock, msgOut, strlen(msgOut));
        iircStateUnlock(&pServ->state, FALSE);
        return 1;
    }
    chan[MAX_IIRC_CHAN_NAME-1] = '\0';
    unicode_cpy(iircChan, chan, UTF8, MAX_IIRC_CHAN_NAME);
    pChan = iircStateChanFind(&pServ->state, iircChan, &status);
    if(pChan == NULL) {
        iircStateUnlock(&pServ->state, FALSE);
        VLOG_MSG(LOG_LOW,"rfc1459HandleMODE - unknown channel: %s", chan);
        return -1;
    }
    // next parameter
    iParam = params[1];
    if(iParam[0] == '\0') {
        // query for the mode
        snprintf(msgOut, RFC1459_MSG_MAX, ":%s %d %s %s +", pServ->hostname,
                RPL_CHANNELMODEIS, pSrcUser->nick, chan);
        if(pChan->modes & IIRC_CHAN_PRIVATE)
            Strncat(msgOut, "p", RFC1459_MSG_MAX);
        if(pChan->modes & IIRC_CHAN_SECRET)
            Strncat(msgOut, "s", RFC1459_MSG_MAX);
        if(pChan->modes & IIRC_CHAN_INVITE)
            Strncat(msgOut, "i", RFC1459_MSG_MAX);
        if(pChan->modes & IIRC_CHAN_EXTERN_MSG)
            Strncat(msgOut, "n", RFC1459_MSG_MAX);
        if(pChan->modes & IIRC_CHAN_TOPIC_PROT)
            Strncat(msgOut, "t", RFC1459_MSG_MAX);
        if(pChan->modes & IIRC_CHAN_USER_LIMIT)
            Strncat(msgOut, "l", RFC1459_MSG_MAX);
        if(pChan->modes & IIRC_CHAN_KEY)
            Strncat(msgOut, "k", RFC1459_MSG_MAX);
        if(pChan->modes & IIRC_CHAN_USER_LIMIT) {
            snprintf(msgBuf, RFC1459_MSG_MAX, " %d", pChan->userLimit);
            msgBuf[RFC1459_MSG_MAX-1] = '\0';
            Strncat(msgOut, msgBuf, RFC1459_MSG_MAX);
        }
        if(pChan->modes & IIRC_CHAN_KEY) {
            Strncat(msgOut, " ", RFC1459_MSG_MAX);
            Strncat(msgOut, (char*)pChan->key, RFC1459_MSG_MAX);
        }
        Strncat(msgOut, "\r\n", RFC1459_MSG_MAX);
        msgOut[RFC1459_MSG_MAX-1] = '\0';
        socketWrite(&pClient->sock, msgOut, strlen(msgOut));
        snprintf(msgOut, RFC1459_MSG_MAX, ":%s %d %s %d\r\n", pServ->hostname,
                329, chan, 1021463254);  // who knows
        socketWrite(&pClient->sock, msgOut, strlen(msgOut));
        snprintf(msgOut, RFC1459_MSG_MAX,
                ":%s %d %s %s :End of Channel Ban List\r\n", pServ->hostname,
                RPL_ENDOFBANLIST, pSrcUser->nick, chan);
        socketWrite(&pClient->sock, msgOut, strlen(msgOut));
        iircStateUnlock(&pServ->state, FALSE);
        return 1;
    }
    pChanCargo = (cargoIrcChanMode*)MALLOC(pServ->state.truckSize);
    if(pChanCargo == NULL) {
        iircStateUnlock(&pServ->state, FALSE);
        LOG_MSG(LOG_SYS,"rfc1459HandleMODE - MALLOC failed");
        return -1;
    }
    // fill the cargo with the current channel info
    cargoIrcChanModeInit(pChanCargo);
    bExpectUserLimit = FALSE;
    bExpectKey = FALSE;
    mode = 0;
    for(paramPos = 1; paramPos < 15; paramPos++)
    {
        // string which increments to each parameter
        iParam = params[paramPos];
        if(iParam[0] == '\0')
            break; // no more parameters
        paramLen = strlen(iParam);
        if(paramLen >= RFC1459_MSG_MAX-1) {
            paramLen = RFC1459_MSG_MAX-1;
            iParam[RFC1459_MSG_MAX-1] = '\0';
        }
        i = 0;
        while(i < paramLen && iParam[i] != '+' && iParam[i] != '-')
            i++;
        if(i >= paramLen-1) {// room for +|- and one mode char
            paramPos++;
            continue;
        }
        if(bExpectUserLimit && isdigit(iParam[0])) {
            //
            // looks like a user limit to me, so use it
            sscanf(iParam, "%d", &pChanCargo->userLimit);
            bExpectUserLimit = FALSE;
            paramPos++;
            continue;
        }
        if(bExpectKey) {
            //
            // we are expecting a channel key, so this must be it
            unicode_cpy(pChanCargo->key, iParam, UTF8, MAX_IIRC_CHAN_KEY);
            bExpectKey = FALSE;
            paramPos++;
            continue;
        }
        // magic flag which means that no switch has been specified yet
        modeSwitch = 0x0FFFFFFF;
        while(i < paramLen)
        {
            switch(iParam[i])
            {
                case '+':
                    modeSwitch = 0xFFFFFFFF;
                    break;
                case '-':
                    modeSwitch = 0x00000000;
                    break;
                case 'p':
                    mode |= (IIRC_CHAN_PRIVATE & modeSwitch);
                    pChanCargo->modeXOR |= IIRC_CHAN_PRIVATE;
                    break;
                case 's':
                    mode |= (IIRC_CHAN_SECRET & modeSwitch);
                    pChanCargo->modeXOR |= IIRC_CHAN_SECRET;
                    break;
                case 'i':
                    mode |= (IIRC_CHAN_INVITE & modeSwitch);
                    pChanCargo->modeXOR |= IIRC_CHAN_INVITE;
                    break;
                case 't':
                    mode |= (IIRC_CHAN_TOPIC_PROT & modeSwitch);
                    pChanCargo->modeXOR |= IIRC_CHAN_TOPIC_PROT;
                    break;
                case 'n':
                    mode |= (IIRC_CHAN_EXTERN_MSG & modeSwitch);
                    pChanCargo->modeXOR |= IIRC_CHAN_EXTERN_MSG;
                    break;
                case 'l':
                    mode |= (IIRC_CHAN_USER_LIMIT & modeSwitch);
                    // only expect a user limit if '+l' was used
                    if(modeSwitch)
                        bExpectUserLimit = TRUE;
                    pChanCargo->modeXOR |= IIRC_CHAN_USER_LIMIT;
                    break;
                case 'k':
                    mode |= (IIRC_CHAN_KEY & modeSwitch);
                    if(modeSwitch)
                        bExpectKey = TRUE;
                    pChanCargo->modeXOR |= IIRC_CHAN_KEY;
                    break;
                case '!':
                    // module
                    strncpy(modName, iParam + i + 1, MAX_IIRC_MOD_NAME);
                    modName[MAX_IIRC_MOD_NAME-1] = '\0';
                    unicode_cpy(iircModName, modName,UTF8,MAX_IIRC_MOD_NAME);
                    pMod = iircStateModuleFind(&pServ->state, iircModName);
                    if(pMod == NULL) {
                        VLOG_MSG(LOG_LOW,"rfc1459HandleMODE - unknown mod: %s",
                                modName);
                        paramPos = 15-1; // skip the rest because of error
                        break;
                    }
                    pModCargo = (cargoIrcChanModArg*)
                        MALLOC(pServ->state.truckSize);
                    if(pModCargo == NULL) {
                        LOG_MSG(LOG_SYS,"rfc1459HandleMODE - MALLOC failed");
                        FREE(pChanCargo);
                        iircStateUnlock(&pServ->state, FALSE);
                        return -1;
                    }
                    cargoIrcChanModArgInit(pModCargo);
                    pModCargo->hChan = pChan->id;
                    pModCargo->hMod = pMod->global_id;
                    modArg[0] = '\0';
                    if(modeSwitch == 0xFFFFFFFF) {
                        pModCargo->function = IIRC_MOD_FUNC_ENABLE;
                        paramPos++;
                        //
                        // use the rest of the parameters as a module argument
                        while(paramPos < 15) {
                            iParam = params[paramPos];
                            if(iParam[0] == '\0')
                                break; // no more parameters
                            Strncat(modArg, iParam, MAX_IIRC_MOD_ARG);
                            Strncat(modArg, " ", MAX_IIRC_MOD_ARG);
                            paramPos++;
                        }
                    } else if(!modeSwitch) {
                        pModCargo->function = IIRC_MOD_FUNC_DISABLE;
                    } else {
                        pModCargo->function = IIRC_MOD_FUNC_MODIFY;
                        paramPos++;
                        while(paramPos < 15) {
                            iParam = params[paramPos];
                            if(iParam[0] == '\0')
                                break; // no more parameters
                            Strncat(modArg, iParam, MAX_IIRC_MOD_ARG);
                            Strncat(modArg, " ", MAX_IIRC_MOD_ARG);
                            paramPos++;
                        }
                    }
                    unicode_cpy(pModCargo->arg, modArg,
                            UTF8, MAX_IIRC_MOD_ARG);
                    pModCargo->header.srcChan = 0;
                    pModCargo->header.srcUser = pSrcUser->id;
                    pModCargo->header.dstChan = pChan->id;
                    pModCargo->header.dstUser = IIRC_USER_BROADCAST;
                    rfc1459EmulateInterstate(pServ, pClient, (truck*)pModCargo);
                    i = paramLen; // finish off the rest of the param
                    break;
                default:
                    VLOG_MSG(LOG_USER,"rfc1459HandleMODE - unknown mode: %s",
                            iParam);
                    break;
            }
            i++;
        }
        //
        // positive modes will always overwrite negative modes
        pChanCargo->modeState |= mode;
        paramPos++;
    }
    pChanCargo->header.srcChan = 0;
    pChanCargo->header.srcUser = pSrcUser->id;
    pChanCargo->header.dstChan = pChan->id;
    pChanCargo->header.dstUser = IIRC_USER_BROADCAST;
    iircStateUnlock(&pServ->state, FALSE);
    rfc1459EmulateInterstate(pServ, pClient, (truck*)pChanCargo);
    return 1;
}

int rfc1459HandlePING(SERV, rawClient_t *pClient, char *msg,
        char *prefix, char (*params)[RFC1459_MSG_MAX])
{
    int status;
    char msgOut[RFC1459_MSG_MAX];
    mUint8 nick[MAX_IIRC_NICK];
    cargoIrcEvent *pCargo;
    iircUser_t *pDstUser;
    iircUser_t *pUser;

    iircStateLock(&pServ->state, FALSE);
    pUser = iircStateUserGet(&pServ->state, pClient->hUser, &status);
    if(pUser == NULL) {
        iircStateUnlock(&pServ->state, FALSE);
        VLOG_MSG(LOG_LOW,"rfc1459HandlePING - unknown hUser: %d",
                pClient->hUser);
        return -1;
    }
    if(params[0] == '\0') {
        iircStateUnlock(&pServ->state, FALSE);
        return 1;
    }
    unicode_cpy(nick, params[0], UTF8, MAX_IIRC_NICK);
    //
    // client is sending a PING to someone on the IIRC network
    pDstUser = iircStateUserFind(&pServ->state, nick, &status);
    if(pDstUser == NULL) {
        iircStateUnlock(&pServ->state, FALSE);
        //
        // return serve
        snprintf(msgOut, RFC1459_MSG_MAX, ":%s PONG %s :%s %s\r\n",
                pServ->serverName, pServ->serverName, params[0], params[1]);
        socketWrite(&pClient->sock, msgOut, strlen(msgOut));
        return 1;
    }
    pCargo = (cargoIrcEvent*)MALLOC(pServ->state.truckSize);
    if(pCargo == NULL) {
        iircStateUnlock(&pServ->state, FALSE); // pDstUser
        LOG_MSG(LOG_SYS,"rfc1459HandlePING - MALLOC failed");
        return -1;
    }
    cargoIrcEventInit(pCargo);
    pCargo->code = CODE_IIRC_PING;
    pCargo->header.srcChan = 0;
    pCargo->header.srcUser = pClient->hUser;
    pCargo->header.dstChan = 0;
    pCargo->header.dstUser = pDstUser->id;
    iircStateUnlock(&pServ->state, FALSE); // pDstUser
    rfc1459EmulateInterstate(pServ, pClient, (truck*)pCargo);
    /*
       if(strlen(params[0]) >= 3 &&
       params[0][0] == 'L' &&
       params[0][1] == 'A' &&
       params[0][2] == 'G')
       {
    //
    // client is sending a ping to us.  I know.. kludge
    snprintf(msgOut, RFC1459_MSG_MAX, ":%s PONG %s :%s\r\n",
    pServ->serverName, pServ->serverName, params[0]);
    socketWrite(&pClient->sock, msgOut, strlen(msgOut));
    }
     */
    return 1;
}

int rfc1459HandlePONG(SERV, rawClient_t *pClient, char *msg,
        char *prefix, char (*params)[RFC1459_MSG_MAX])
{
    int status;
    cargoIrcEvent *pCargo;
    mUint8 nick[MAX_IIRC_NICK];
    iircUser_t *pDstUser;
    iircUser_t *pUser;

    iircStateLock(&pServ->state, FALSE);
    pUser = iircStateUserGet(&pServ->state, pClient->hUser, &status);
    if(pUser == NULL) {
        iircStateUnlock(&pServ->state, FALSE);
        VLOG_MSG(LOG_LOW,"rfc1459HandlePONG - unknown hUser: %d",
                pClient->hUser);
        return -1;
    }
    unicode_cpy(nick, params[0], UTF8, MAX_IIRC_NICK);
    pDstUser = iircStateUserFind(&pServ->state, nick, &status);
    if(pDstUser == NULL) {
        iircStateUnlock(&pServ->state, FALSE);
        if(status <= IIRC_STATE_ERR_FATAL)
            return -1;
        return 0;
    }
    pCargo = (cargoIrcEvent*)MALLOC(pServ->state.truckSize);
    if(pCargo == NULL) {
        iircStateUnlock(&pServ->state, FALSE);
        LOG_MSG(LOG_SYS,"rfc1459HandlePING - MALLOC failed");
        return -1;
    }
    cargoIrcEventInit(pCargo);
    pCargo->code = CODE_IIRC_PONG;
    pCargo->header.srcChan = 0;
    pCargo->header.srcUser = pUser->id;
    pCargo->header.dstChan = 0;
    pCargo->header.dstUser = pDstUser->id;
    iircStateUnlock(&pServ->state, FALSE);
    rfc1459EmulateInterstate(pServ, pClient, (truck*)pCargo);
    return 1;
}

/*!
 *  Command: PRIVMSG
 *     Parameters: <receiver>{,<receiver>} <text to be sent>
 */
int rfc1459HandlePRIV(SERV, rawClient_t *pClient, char *msg,
        char *prefix, char (*params)[RFC1459_MSG_MAX])
{
    int i;
    int status;
    char *pDstName;
    char dstName[RFC1459_MSG_MAX];
    char chan[RFC1459_CHAN_MAX];
    char nick[RFC1459_NICK_MAX];
    mUint8 chan8[MAX_IIRC_CHAN_NAME];
    mUint8 nick8[MAX_IIRC_NICK];
    int dstChan, dstUser;
    iircChan_t *pDstChan;
    iircUser_t *pDstUser;
    cargoIrcMsg *pCargo;
    iircUser_t *pUser;

    iircStateLock(&pServ->state, FALSE);
    pUser = iircStateUserGet(&pServ->state, pClient->hUser, &status);
    if(pUser == NULL) {
        iircStateUnlock(&pServ->state, FALSE);
        VLOG_MSG(LOG_LOW,"rfc1459HandlePRIV - unknown hUser: %d",
                pClient->hUser);
        return -1;
    }
    VLOG_MSG(LOG_DUMP,"rfc1459HandlePRIV - Got PRIVMSG from user: %d",
            pUser->id);
    unicode_cpy(nick, pUser->nick, UTF8, RFC1459_NICK_MAX);
    VLOG_MSG(LOG_DUMP,"<%s> %s", nick, msg);
    iircStateUnlock(&pServ->state, FALSE); // pUser
    //
    //  Numeric Replies:
    //      ERR_NORECIPIENT     ERR_NOTEXTTOSEND
    //      ERR_CANNOTSENDTOCHAN    ERR_NOTOPLEVEL
    //      ERR_WILDTOPLEVEL    ERR_TOOMANYTARGETS
    //      ERR_NOSUCHNICK      RPL_AWAY
    //
    if(params[0][0] == '\0') {
        // not enough parameters, no destination
        return -1;
    }
    strncpy(dstName, params[0], RFC1459_MSG_MAX);
    pDstName = dstName;
    if(dstName[0] == '#' || dstName[0] == '&') {
        rfc1459GetChannel(&pDstName, chan);
        unicode_cpy(chan8, chan, UTF8, MAX_IIRC_CHAN_NAME);
        // search for this channel name
        iircStateLock(&pServ->state, FALSE);
        pDstChan = iircStateChanFind(&pServ->state, chan8, &status);
        if(pDstChan == NULL) {
            VLOG_MSG(LOG_USER,"rfc1459HandlePRIV - unknown dstChan %s", chan);
            iircStateUnlock(&pServ->state, FALSE);
            return -1;
        }
        dstChan = pDstChan->id;
        dstUser = IIRC_USER_BROADCAST;
        iircStateUnlock(&pServ->state, FALSE);
    } else {
        rfc1459GetNick(&pDstName, nick);
        unicode_cpy(nick8, nick, UTF8, MAX_IIRC_NICK);
        // search for this client name
        iircStateLock(&pServ->state, FALSE);
        pDstUser = iircStateUserFind(&pServ->state, nick8, &status);
        if(pDstUser == NULL) {
        }
        dstChan = 0;
        dstUser = pDstUser->id;
        iircStateUnlock(&pServ->state, FALSE);
    }
    if(params[1][0] == '\0') {
        // sending empty message?
        return -1;
    }
    i = 1;
    //
    // find the last valid parameter (this is the text message)
    while(i < 15 && params[i][0] != '\0')
        i++;
    i--;
    pCargo = (cargoIrcMsg*)MALLOC(pServ->state.truckSize);
    if(pCargo == NULL) {
        LOG_MSG(LOG_SYS,"rfc1459HandlePRIV - MALLOC failed");
        return -1;
    }
    cargoIrcMsgInit(pCargo);
    unicode_cpy(pCargo->msg, params[i], UTF8, MAX_IIRC_MSG);
    pCargo->header.srcChan = 0;
    pCargo->header.srcUser = pClient->hUser;
    pCargo->header.dstChan = dstChan;
    pCargo->header.dstUser = dstUser;
    rfc1459EmulateInterstate(pServ, pClient, (truck*)pCargo);
    return 1;
}

int rfc1459HandleNOTI(SERV, rawClient_t *pClient, char *msg,
        char *prefix, char (*params)[RFC1459_MSG_MAX])
{
    int status;
    iircUser_t *pUser;

    iircStateLock(&pServ->state, FALSE);
    pUser = iircStateUserGet(&pServ->state, pClient->hUser, &status);
    if(pUser == NULL) {
        VLOG_MSG(LOG_LOW,"rfc1459HandleNOTI - unknown hUser: %d",
                pClient->hUser);
        return -1;
    }
    VLOG_MSG(LOG_DUMP,"[%s] %s", pUser->nick, msg);
    iircStateUnlock(&pServ->state, FALSE);
    return 1;
}

int rfc1459HandleQUIT(SERV, rawClient_t *pClient, char *msg,
        char *prefix, char (*params)[RFC1459_MSG_MAX])
{
    int status;
    iircUser_t *pUser;
    cargoIrcQuit *pCargo;
    cargoIrcUser *pUserCargo;

    //
    // emulate a cargoIrcQuit
    pCargo = (cargoIrcQuit*)MALLOC(pServ->state.truckSize);
    if(pCargo == NULL) {
        LOG_MSG(LOG_SYS,"rfc1459HandleQUIT - MALLOC failed");
        return -1;
    }
    cargoIrcQuitInit(pCargo);
    unicode_cpy(pCargo->reason, params[0], UTF8, MAX_IIRC_QUIT_REASON);
    //
    // emulate a cargoIrcUser with the DISCONNECT flag set.
    pUserCargo = (cargoIrcUser*)MALLOC(pServ->state.truckSize);
    if(pUserCargo == NULL) {
        LOG_MSG(LOG_SYS,"rfc1459HandleQUIT - MALLOC failed");
        FREE(pCargo);
        return -1;
    }
    iircStateLock(&pServ->state, FALSE);
    pUser = iircStateUserGet(&pServ->state, pClient->hUser, &status);
    if(pUser == NULL) {
        VLOG_MSG(LOG_LOW,"rfc1459HandleQUIT - unknown hUser: %d",
                pClient->hUser);
        FREE(pCargo);
        FREE(pUserCargo);
        iircStateUnlock(&pServ->state, FALSE); // pUser
        return -1;
    }
    VLOG_MSG(LOG_DUMP,"[%s] %s", pUser->nick, msg);
    pCargo->header.srcChan = 0;
    pCargo->header.srcUser = pUser->id;
    pCargo->header.dstChan = 0;
    pCargo->header.dstUser = 0;
    cargoIrcUserPack(pUserCargo, pUser);
    pUserCargo->flags |= IIRC_USER_DISCONNECT;
    pUserCargo->header.srcChan = 0;
    pUserCargo->header.srcUser = pUser->id;
    pUserCargo->header.dstChan = 0;
    pUserCargo->header.dstUser = 0;
    iircStateUnlock(&pServ->state, FALSE); // pUser

    rfc1459EmulateInterstate(pServ, pClient, (truck*)pCargo);
    rfc1459EmulateInterstate(pServ, pClient, (truck*)pUserCargo);
    return 1;
}

int rfc1459HandleWHO(SERV, rawClient_t *pClient, char *msg,
        char *prefix, char (*params)[RFC1459_MSG_MAX])
{
    int status;
    cargoIrcWho *pCargo;
    iircUser_t *pUser;

    iircStateLock(&pServ->state, FALSE);
    pUser = iircStateUserGet(&pServ->state, pClient->hUser, &status);
    if(pUser == NULL) {
        VLOG_MSG(LOG_LOW,"rfc1459HandleWHO - unknown hUser: %d",
                pClient->hUser);
        iircStateUnlock(&pServ->state, FALSE);
        return -1;
    }
    VLOG_MSG(LOG_DUMP,"[%s] %s", pUser->nick, msg);
    iircStateUnlock(&pServ->state, FALSE);

    pCargo = (cargoIrcWho*)MALLOC(pServ->state.truckSize);
    if(pCargo == NULL) {
        LOG_MSG(LOG_SYS,"rfc1459HandleWHO - MALLOC failed");
        return -1;
    }
    cargoIrcWhoInit(pCargo);
    unicode_cpy(pCargo->name, params[0], UTF8, MAX_IIRC_WHO_NAME);
    pCargo->header.srcChan = 0;
    pCargo->header.srcUser = pClient->hUser;
    pCargo->header.dstChan = 0;
    pCargo->header.dstUser = 0;
    rfc1459EmulateInterstate(pServ, pClient, (truck*)pCargo);
    return 1;
}

//      Command: WHOIS
//      Parameters: [<server>] <nickmask>[,<nickmask>[,...]]
int rfc1459HandleWHOIS(SERV, rawClient_t *pClient, char *msg,
        char *prefix, char (*params)[RFC1459_MSG_MAX])
{
    int status;
    cargoIrcWho *pCargo;
    iircUser_t *pUser;

    VLOG_MSG(LOG_DUMP,"rfc1459HandleMODE - Got MODE from client: %d",
            pClient->id);
    iircStateLock(&pServ->state, FALSE);
    pUser = iircStateUserGet(&pServ->state, pClient->hUser, &status);
    if(pUser == NULL) {
        VLOG_MSG(LOG_LOW,"rfc1459HandleWHOIS - unknown hUser: %d",
                pClient->hUser);
        iircStateUnlock(&pServ->state, FALSE);
        return -1;
    }
    VLOG_MSG(LOG_DUMP,"[%s] %s", pUser->nick, msg);
    iircStateUnlock(&pServ->state, FALSE);

    pCargo = (cargoIrcWho*)MALLOC(pServ->state.truckSize);
    if(pCargo == NULL) {
        LOG_MSG(LOG_SYS,"rfc1459HandleWHOIS - MALLOC failed");
        return -1;
    }
    cargoIrcWhoInit(pCargo);
    pCargo->flags |= IIRC_WHO_FLAG_WHOIS;
    unicode_cpy(pCargo->name, params[0], UTF8, MAX_IIRC_WHO_NAME);
    pCargo->header.srcChan = 0;
    pCargo->header.srcUser = pClient->hUser;
    pCargo->header.dstChan = 0;
    pCargo->header.dstUser = 0;
    rfc1459EmulateInterstate(pServ, pClient, (truck*)pCargo);
    return 1;
}


int rfc1459HandleNAMES(SERV, rawClient_t *pClient, char *msg,
        char *prefix, char (*params)[RFC1459_MSG_MAX])
{
    int status;
    cargoIrcWho *pCargo;
    iircUser_t *pUser;

    iircStateLock(&pServ->state, FALSE);
    pUser = iircStateUserGet(&pServ->state, pClient->hUser, &status);
    if(pUser == NULL) {
        VLOG_MSG(LOG_LOW,"rfc1459HandleNAMES - unknown hUser: %d",
                pClient->hUser);
        iircStateUnlock(&pServ->state, FALSE);
        return -1;
    }
    VLOG_MSG(LOG_DUMP,"[%s] %s", pUser->nick, msg);
    iircStateUnlock(&pServ->state, FALSE);

    pCargo = (cargoIrcWho*)MALLOC(pServ->state.truckSize);
    if(pCargo == NULL) {
        LOG_MSG(LOG_SYS,"rfc1459HandleNames - MALLOC failed");
        return -1;
    }
    cargoIrcWhoInit(pCargo);
    pCargo->flags |= IIRC_WHO_FLAG_NAMES;
    unicode_cpy(pCargo->name, params[0], UTF8, MAX_IIRC_WHO_NAME);
    pCargo->header.srcChan = 0;
    pCargo->header.srcUser = pClient->hUser;
    pCargo->header.dstChan = 0;
    pCargo->header.dstUser = 0;
    rfc1459EmulateInterstate(pServ, pClient, (truck*)pCargo);
    return 1;
}

/*!
 * Command: USERHOST
 *    Parameters: <nickname>{<space><nickname>}
 *
 * Numeric Replies:
 *
 *    RPL_USERHOST                    ERR_NEEDMOREPARAMS
 */
int rfc1459HandleUSERHOST(SERV, rawClient_t *pClient, char *msg,
        char *prefix, char (*params)[RFC1459_MSG_MAX])
{
    int status;
    iircUser_t *pU;
    mUint8 nick8[MAX_IIRC_NICK];
    char msgFuck[RFC1459_MSG_MAX];
    //iircUser_t *pC;
    iircUser_t *pUser;

    iircStateLock(&pServ->state, FALSE);
    pUser = iircStateUserGet(&pServ->state, pClient->hUser, &status);
    if(pUser == NULL) {
        VLOG_MSG(LOG_LOW,"rfc1459HandleUSERHOST - unknown hUser: %d",
                pClient->hUser);
        iircStateUnlock(&pServ->state, FALSE);
        return -1;
    }
    VLOG_MSG(LOG_DUMP,"rfc1459HandleUSERHOST - Got USERHOST from client: %d",
            pUser->id);
    unicode_cpy(nick8, params[0], UTF8, MAX_IIRC_NICK);
    pU = iircStateUserFind(&pServ->state, nick8, &status);
    if(pU == NULL) {
        iircStateUnlock(&pServ->state, FALSE);
        VLOG_MSG(LOG_DUMP,"rfc1459HandleUSERHOST - unknown user %s", params[0]);
        if(status <= IIRC_STATE_ERR_FATAL)
            return -1;
        return 0;
    }
    //!
    //  ":[<reply>{<space><reply>}]"
    //  <reply> ::= <nick>['*'] '=' <'+'|'-'><hostname>
    //
    // The '*' indicates whether the client has registered
    // as an Operator.  The '-' or '+' characters represent
    // whether the client has set an AWAY message or not respectively.
    //
    snprintf(msgFuck, RFC1459_MSG_MAX, ":%s %d :%s=+%s@%s\r\n",
            pServ->hostname, RPL_USERHOST, params[0],
            pU->identity, pU->hostname);
    msgFuck[RFC1459_MSG_MAX-1] = '\0';
    socketWrite(&pClient->sock, msgFuck, strlen(msgFuck));
    iircStateUnlock(&pServ->state, FALSE);
    return 1;
}

int rfc1459DispatchUser(SERV, rawClient_t *pClient,
        iircUser_t *pUser, truck *pTruck)
{
    //
    // if we already received a NICK and the MOTD hasn't been sent yet
    if(pUser->nick[0] != 0 && !(pUser->flags & IIRC_USER_MOTD)) {
        //
        // user needs to be sent MOTD
        rfc1459SendMOTD(pServ, pUser);
        pUser->flags |= IIRC_USER_MOTD;
    }
    return 1;
}

int rfc1459DispatchEvent(SERV, rawClient_t *pClient,
        iircUser_t *pUser, truck *pTruck)
{
    char msg[RFC1459_MSG_MAX];
    cargoIrcEvent *pCargo = (cargoIrcEvent*)pTruck;

    //iircUser_t *pSrcUser;
    //pSrcUser = iircStateSrcUserGet(&pServ->state, (truck*)pTruck, &status);
    switch(pCargo->code) {
        case CODE_IIRC_PING:
            snprintf(msg, RFC1459_MSG_MAX, "PING %s\r\n", pUser->nick);
            socketWrite(&pClient->sock, msg, strlen(msg));
            break;
        case CODE_IIRC_PONG:
            snprintf(msg, RFC1459_MSG_MAX, "PONG %s\r\n", pUser->nick);
            socketWrite(&pClient->sock, msg, strlen(msg));
            break;
        default:
            VLOG_MSG(LOG_LOW,"rfc1459DispatchEvent - unknown code: %d",
                    pCargo->code);
            return -1;
    }
    return 1;
}

int rfc1459DispatchNick(SERV, rawClient_t *pClient,
        iircUser_t *pUser, truck *pTruck)
{
    int status;
    char msg[RFC1459_MSG_MAX];
    iircUser_t *pSrcUser;
    //cargoIrcNick *pCargo = (cargoIrcNick*)pTruck;

    pSrcUser = iircStateSrcUserGet(&pServ->state, (truck*)pTruck, &status);
    if(pSrcUser == NULL) {
        LOG_MSG(LOG_LOW,"rfc1459DispatchNick - invalid source client");
        return -1;
    }
    //
    // user has already been initialized and been sent the MOTD.
    //
    // :sendak.openprojects.net 376 firstNick :End of /MOTD command.
    // NICK secondNick
    // :firstNick!~doug@goobers.org NICK :secondNick
    //
    snprintf(msg, RFC1459_MSG_MAX, ":%s!%s@%s NICK :%s\r\n",
            pSrcUser->nick_old, pSrcUser->identity,
            pSrcUser->hostname, pSrcUser->nick);
    msg[RFC1459_MSG_MAX-1] = '\0';
    socketWrite(&pClient->sock, msg, strlen(msg));
    //
    // if we already received a USER and the MOTD hasn't been sent...
    if(pUser->identity[0] != '\0' && !(pUser->flags & IIRC_USER_MOTD)) {
        // send MOTD
        rfc1459SendMOTD(pServ, pUser);
        pUser->flags |= IIRC_USER_MOTD;
    }
    return 1;
}

int rfc1459DispatchChanMode(SERV, rawClient_t *pClient,
        iircUser_t *pUser, truck *pTruck)
{
    int status;
    char msg[RFC1459_MSG_MAX];
    char tempMsg[RFC1459_MSG_MAX];
    cargoIrcChanMode *pCargo = (cargoIrcChanMode*)pTruck;
    iircChan_t *pChan;

    pChan = iircStateChanGet(&pServ->state, pCargo->header.dstChan, &status);
    if(pChan == NULL) {
        LOG_MSG(LOG_LOW,"rfc1459DispatchChanMode - invalid channel");
        return -1;
    }
    /*
       if(strncmp(pCargo->name, pChan->name, MAX_IIRC_CHAN_NAME) != 0) {
//
// send client... ugh. this is imposible I think.
LOG_MSG(LOG_LOW,"rfc1459DispatchChan - cannot change channel name");
return -1;
}
     */
if(!pCargo->modeXOR)
    return 1; // no mode change
    //
    // start message reply
    snprintf(msg, RFC1459_MSG_MAX-3, ":%s MODE %s ",
            pServ->hostname, pChan->name);
//
// retrieve a character string representing what modes have changed
if(cargoIrcChanModeDiff(pCargo, (mUint8*)tempMsg, sizeof(tempMsg)) < 0)
    return -1;
    Strncat(msg, tempMsg, RFC1459_MSG_MAX-3);
    Strncat(msg, "\r\n", RFC1459_MSG_MAX);
    socketWrite(&pClient->sock, msg, strlen(msg));
    return 1;
    }

int rfc1459DispatchChanTopic(SERV, rawClient_t *pClient,
        iircUser_t *pUser, truck *pTruck)
{
    int status;
    char msg[RFC1459_MSG_MAX];
    cargoIrcChanTopic *pCargo = (cargoIrcChanTopic*)pTruck;
    iircChan_t *pChan;

    pChan = iircStateChanGet(&pServ->state, pCargo->header.dstChan, &status);
    if(pChan == NULL) {
        LOG_MSG(LOG_LOW,"rfc1459DispatchChanTopic - invalid channel");
        return -1;
    }
    snprintf(msg, RFC1459_MSG_MAX, ":%s MODE %s :%s\r\n", pServ->hostname,
            pChan->name, pChan->topic);
    msg[RFC1459_MSG_MAX-1] = '\0';
    socketWrite(&pClient->sock, msg, strlen(msg));
    return 1;
}

int rfc1459DispatchChanUsers(SERV, rawClient_t *pClient,
        iircUser_t *pUser, truck *pTruck)
{
    unsigned int i, nLeft;
    int status;
    int msgBase;
    char msg[RFC1459_MSG_MAX];
    char nickBuf[RFC1459_MSG_MAX];
    iircChan_t *pChan;
    iircUser_t *pU;
    cargoIrcChanUsers *pCargo = (cargoIrcChanUsers*)pTruck;

    pChan = iircStateChanGet(&pServ->state, pCargo->hChan, &status);
    if(pChan == NULL) {
        LOG_MSG(LOG_LOW,"rfc1459DispatchChanUsers - invalid channel");
        return -1;
    }
    if(pCargo->flags & IIRC_CHAN_USERS_WHOIS) {
        // :server.org 311 nanoHz nanoHz vtovnmx 207.235.123.10 realname\r\n
        // :server.org 312 nanoHz nanoHz server.org serverInfo\r\n
        // :server.org 317 nanoHz nanoHz 2 1036206610 :seconds idle\r\n
        // :server.org 318 nanoHz nanoHz :End of /WHOIS list.\r\n
        if(pCargo->nUser == 0)
            return -1;
        pU = iircStateUserGet(&pServ->state, pCargo->hClient[0], &status);
        if(pU == NULL)
            return -1;
        snprintf(msg, RFC1459_MSG_MAX,":%s %d %s %s %s %s %s\r\n",
                pServ->hostname, RPL_WHOISUSER, pUser->nick, pU->nick,
                "wtf?", pU->hostname, pU->realname);
        socketWrite(&pClient->sock, msg, strlen(msg));
        snprintf(msg, RFC1459_MSG_MAX,":%s %d %s %s %s %s\r\n",
                pServ->hostname, RPL_WHOISSERVER, pUser->nick, pU->nick,
                pU->servername, "server info");
        socketWrite(&pClient->sock, msg, strlen(msg));
        snprintf(msg, RFC1459_MSG_MAX,":%s %d %s %s %d %d :seconds idle\r\n",
                pServ->hostname, RPL_WHOISIDLE, pUser->nick, pU->nick,
                2, 424242);
        socketWrite(&pClient->sock, msg, strlen(msg));
        snprintf(msg, RFC1459_MSG_MAX,":%s %d %s %s :End of /WHOIS list.\r\n",
                pServ->hostname, RPL_ENDOFWHOIS, pUser->nick, pU->nick);
        socketWrite(&pClient->sock, msg, strlen(msg));
        return 1;
    }
    //
    //  RPL_NAMREPLY
    //      "<channel> :[[@|+]<nick> [[@|+]<nick> [...]]]"
    //  RPL_ENDOFNAMES
    //      "<channel> :End of /NAMES list"
    //
    // -- though why does the client expect something like this?:
    //
    // :zahn.openprojects.net 353 metric = #absnothing :@metric
    // :zahn.openprojects.net 366 metric #absnothing :End of /NAMES list.
    //

    if(pCargo->flags & IIRC_CHAN_USERS_WHO) {
        snprintf(msg, RFC1459_MSG_MAX,":%s %d %s %s ", pServ->hostname,
                RPL_WHOREPLY, pUser->nick, pChan->name);
    } else {
        snprintf(msg, RFC1459_MSG_MAX,":%s %d %s = %s :", pServ->hostname,
                RPL_NAMREPLY, pUser->nick, pChan->name);
    }
    msgBase = strlen(msg);
    nLeft = 0;
    for(i = 0; i < pCargo->nUser; i++)
    {
        pU = iircStateUserGet(&pServ->state, pCargo->hClient[i], &status);
        if(pU == NULL)
            continue;
        if(pCargo->flags & IIRC_CHAN_USERS_WHO) {
            snprintf(nickBuf, RFC1459_MSG_MAX, "%s %s %s %s H :0 %s\r\n",
                    pU->identity, pU->hostname, pServ->hostname, pU->nick,
                    pU->realname);
            nickBuf[RFC1459_MSG_MAX-1] = '\0';
            Strncat(msg, nickBuf, RFC1459_MSG_MAX);
            msg[RFC1459_MSG_MAX-1] = '\0';
            socketWrite(&pClient->sock, msg, strlen(msg));
            msg[msgBase] = '\0';
        } else {
            snprintf(nickBuf, RFC1459_MSG_MAX, "@%s ", pU->nick);
            if(strlen(nickBuf) >= RFC1459_MSG_MAX - (strlen(msg) + 2)) {
                Strncat(msg, "\r\n", RFC1459_MSG_MAX);
                socketWrite(&pClient->sock, msg, strlen(msg));
                msg[msgBase] = '\0';
            }
            if(strlen(nickBuf) >= RFC1459_MSG_MAX - (strlen(msg) + 2))
                continue;
            nLeft = 0;
            Strncat(msg, nickBuf, RFC1459_MSG_MAX);
            nLeft++;
        }
    }
    if(nLeft) {
        Strncat(msg, "\r\n", RFC1459_MSG_MAX);
        socketWrite(&pClient->sock, msg, strlen(msg));
    }
    if(pCargo->flags & IIRC_CHAN_USERS_WHO) {
        snprintf(msg, RFC1459_MSG_MAX,
                ":%s %d %s %s :End of /WHO list.\r\n", pServ->hostname,
                RPL_ENDOFWHO, pUser->nick, pChan->name);
    } else {
        snprintf(msg, RFC1459_MSG_MAX,
                ":%s %d %s %s :End of /NAMES list.\r\n", pServ->hostname,
                RPL_ENDOFNAMES, pUser->nick, pChan->name);
    }
    socketWrite(&pClient->sock, msg, strlen(msg));
    return 1;
}

// JOIN #absnothing
// :metric!ajvfg@204.2.54.109 JOIN :#absnothing
// :zahn.openprojects.net MODE #absnothing +n
// :zahn.openprojects.net 353 metric = #absnothing :@metric
// :zahn.openprojects.net 366 metric #absnothing :End of /NAMES list.
int rfc1459DispatchJoin(SERV, rawClient_t *pClient,
        iircUser_t *pUser, truck *pTruck)
{
    int status;
    char msg[RFC1459_MSG_MAX];
    iircChan_t *pChan;
    iircUser_t *pSrcUser;
    cargoIrcJoin *pCargo = (cargoIrcJoin*)pTruck;

    pSrcUser = iircStateSrcUserGet(&pServ->state, (truck*)pCargo, &status);
    if(pSrcUser == NULL) {
        LOG_MSG(LOG_LOW,"rfc1459DispatchJoin - invalid source client");
        return -1;
    }
    if(pCargo->flags & IIRC_JOIN_BY_NAME) {
        snprintf(msg, RFC1459_MSG_MAX, ":%s!%s@%s JOIN :%s\r\n",
                pSrcUser->nick, pSrcUser->identity,
                pSrcUser->hostname, pCargo->chan);
        socketWrite(&pClient->sock, msg, strlen(msg));
        snprintf(msg, RFC1459_MSG_MAX,":%s MODE %s +n\r\n",
                pServ->hostname, pCargo->chan);
        socketWrite(&pClient->sock, msg, strlen(msg));
    } else {
        pChan = iircStateChanGet(&pServ->state, pCargo->hChan, &status);
        if(pChan == NULL) {
            LOG_MSG(LOG_LOW,"rfc1459DispatchJoin - invalid source channel");
            return -1;
        }
        snprintf(msg, RFC1459_MSG_MAX, ":%s!%s@%s JOIN :%s\r\n",
                pSrcUser->nick, pSrcUser->identity,
                pSrcUser->hostname, pChan->name);
        socketWrite(&pClient->sock, msg, strlen(msg));
        snprintf(msg, RFC1459_MSG_MAX,":%s MODE %s +n\r\n",
                pServ->hostname, pChan->name);
        socketWrite(&pClient->sock, msg, strlen(msg));
    }
    return 1;
}

int rfc1459DispatchChanModArg(SERV, rawClient_t *pClient,
        iircUser_t *pUser, truck *pTruck)
{
    /*
       int status;
       char msg[RFC1459_MSG_MAX];
       char arg[MAX_IIRC_MOD_ARG];
       cargoIrcChanModArg *pCargo = (cargoIrcChanModArg*)pTruck;
       iircUser_t *pSrcUser;
       iircModule_t *pMod;
       iircChan_t *pChan;

       pSrcUser = iircStateSrcUserGet(&pServ->state, (truck*)pTruck, &status);
       if(pUser == NULL) {
       LOG_MSG(LOG_LOW,"rfc1459DispatchChanModArg - unknown src client");
       return -1;
       }
       pChan = iircStateChanGet(&pServ->state, pCargo->hChan, &status);
       if(pChan == NULL) {
       VLOG_MSG(LOG_LOW,"rfc1459DispatchChanModArg - unknown channel %d",
       pCargo->hChan);
       return -1;
       }
       pMod = iircStateModuleGet(&pServ->state, pCargo->hMod);
       if(pMod == NULL) {
       VLOG_MSG(LOG_LOW,"rfc1459DispatchChanModArg - unknown hMod: %d",
       pCargo->hMod);
       return -1;
       }
       switch(pCargo->function) {
       case IIRC_MOD_FUNC_DISABLE:
       snprintf(msg, RFC1459_MSG_MAX,
       ":%s PRIVMSG %s :%s sets mode -!%s\r\n",
       pServ->hostname, pChan->name, pSrcUser->nick, pMod->name);
       break;
       case IIRC_MOD_FUNC_ENABLE:
       snprintf(msg, RFC1459_MSG_MAX,
       ":%s PRIVMSG %s :%s sets mode +!%s\r\n",
       pServ->hostname, pChan->name, pSrcUser->nick, pMod->name);
       break;
       case IIRC_MOD_FUNC_MODIFY:
       unicode_cpy(arg, pCargo->arg, UTF8, MAX_IIRC_MOD_ARG);
       snprintf(msg, RFC1459_MSG_MAX,
       ":%s PRIVMSG %s :%s sets mode !%s %s\r\n",
       pServ->hostname, pChan->name, pSrcUser->nick,
       pMod->name, arg);
       break;
       }
       socketWrite(&pClient->sock, msg, strlen(msg));
     */
    return 1;
}

/*!
 *  Command: PRIVMSG
 *     Parameters: <receiver>{,<receiver>} <text to be sent>
 */
int rfc1459DispatchMsg(SERV, rawClient_t *pClient,
        iircUser_t *pUser, truck *pTruck)
{
    int status;
    char msg[RFC1459_MSG_MAX];
    cargoIrcMsg *pCargo = (cargoIrcMsg*)pTruck;
    iircUser_t *pSrcUser;
    iircChan_t *pDstChan;

    pDstChan = iircStateChanGet(&pServ->state, pTruck->dstChan, &status);
    if(pDstChan == NULL) {
        LOG_MSG(LOG_LOW,"rfc1459DispatchMsg - invalid dest channel");
        return -1;
    }
    pSrcUser = iircStateSrcUserGet(&pServ->state, pTruck, &status);
    if(pSrcUser == NULL) {
        LOG_MSG(LOG_LOW,"rfc1459DispatchMsg - invalid source client");
        return -1;
    }
    if(pUser->id == pSrcUser->id)
        return 1;  // don't send privmsg to originator
    if(pTruck->dstChan) {
        snprintf(msg, RFC1459_MSG_MAX, ":%s!%s@%s PRIVMSG %s :%s\r\n",
                pSrcUser->nick, pSrcUser->identity, pSrcUser->hostname,
                pDstChan->name, pCargo->msg);
    } else {
        snprintf(msg, RFC1459_MSG_MAX, ":%s!%s@%s PRIVMSG %s :%s\r\n",
                pSrcUser->nick, pSrcUser->identity, pSrcUser->hostname,
                pUser->nick, pCargo->msg);
    }
    socketWrite(&pClient->sock, msg, strlen(msg));
    return 1;
}

int rfc1459DispatchQuit(SERV, rawClient_t *pClient,
        iircUser_t *pUser, truck *pTruck)
{
    int status;
    char msg[RFC1459_MSG_MAX];
    cargoIrcQuit *pCargo = (cargoIrcQuit*)pTruck;
    iircUser_t *pSrcUser;

    pSrcUser = iircStateSrcUserGet(&pServ->state, pTruck, &status);
    if(pSrcUser == NULL) {
        LOG_MSG(LOG_LOW,"rfc1459DispatchQuit - invalid source user");
        return -1;
    }
    snprintf(msg, RFC1459_MSG_MAX, ":%s!%s@%s QUIT :%s\r\n",
            pSrcUser->nick, pSrcUser->identity, pSrcUser->hostname,
            pCargo->reason);
    socketWrite(&pClient->sock, msg, strlen(msg));
    return 1;
}


