
/*
 * Copyright (c) 2001-2002,  James D. Taylor.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * contact James D. Taylor:  james.d.taylor@gmail.com
 */

#include <stdio.h>
#include <unistd.h>
#include <ltdl.h>
#include <iirc/module.h>
#include "metric/char.h"
#ifdef USE_METRIC_THREADS
#include "metric/thread.h"
#endif


static int genModInit(generic_t *pGen);
static int genModFree(generic_t *pGen);
static int genModCmp(generic_t *pGen1, generic_t *pGen2);
static int genModCopy(generic_t *pDst, generic_t *pSrc);

genericInfo_t iircGenModInfo = {
    size: sizeof(iircModule_t),
    fnInit: genModInit,
    fnAlloc: NULL,
    fnFree: genModFree,
    fnCmp: genModCmp,
    fnCopy: genModCopy
};


int iircModHeadInit(iircModHead *pModHead, mUint32 truckSize,
            char *name, char *info)
{
    mUint32 i;

    for(i = 0; i < 12; i++) {
        pModHead->magic[i] = ' ';
    }
    strncpy(pModHead->magic, IIRC_MOD_HEAD_MAGIC, 11);
    pModHead->magic[strlen(pModHead->magic)] = ' '; // overwrite '\0'
    pModHead->version = IIRC_MOD_HEAD_VERSION;
    pModHead->_space = ' ';
    for(i = 0; i < 8; i++) {
        pModHead->truckSize[i] = ' ';
    }
    snprintf(pModHead->truckSize, 7, "%u", truckSize);
    pModHead->truckSize[strlen(pModHead->truckSize)] = ' '; // overwrite '\0'
    pModHead->_space2 = ' ';
    for(i = 0; i < MAX_IIRC_MOD_NAME; i++) {
        pModHead->name[i] = ' ';
    }
    strncpy(pModHead->name, name, MAX_IIRC_MOD_NAME-1);
    pModHead->name[strlen(pModHead->name)] = ' '; // overwrite '\0'
    pModHead->_newline1 = '\n';
    for(i = 0; i < MAX_IIRC_MOD_INFO; i++) {
        pModHead->info[i] = ' ';
    }
    strncpy(pModHead->info, info, MAX_IIRC_MOD_INFO-1);
    pModHead->info[strlen(pModHead->info)] = ' '; // yeah
    pModHead->_newline2 = '\n';
    return 1;
}

/***********************************
 * @iircModule
 */

int iircModuleInit(iircModule_t *pMod)
{
    pMod->version = 0;
    pMod->ref = 0;
    pMod->global_id = -1;
    pMod->local_id = -1;
    pMod->hChan = 0;
    pMod->hParent = 0;
    pMod->flags = 0;
    pMod->truckSize = 0;
    pMod->name[0] = 0;
    pMod->info[0] = 0;
    pMod->truckSize = 0;
    pMod->locationType = 0;
    pMod->modPath[0] = 0;
    pMod->hModFile = NULL;
    truckQueueInit(&pMod->parking);
    commuteInit(&pMod->com);
    pMod->fnTruckHandler = NULL;
    pMod->fnModEntry = NULL;
    pMod->tid = 0;
    return 1;
}

int iircModuleCopy(iircModule_t *pDst, iircModule_t *pSrc)
{
    pDst->version = pSrc->version;
    pDst->ref = pSrc->ref;
    pDst->global_id = pSrc->global_id;
    pDst->local_id = pSrc->local_id;
    pDst->hChan = pSrc->hChan;
    pDst->hParent = pSrc->hParent;
    pDst->flags = pSrc->flags;
    unicode_cpy(pDst->name, pSrc->name, UTF8, MAX_IIRC_MOD_NAME);
    unicode_cpy(pDst->info, pSrc->info, UTF8, MAX_IIRC_MOD_INFO);
    pDst->truckSize = pSrc->truckSize;
    pDst->locationType = pSrc->locationType;
    strncpy(pDst->modPath, pSrc->modPath, PATH_MAX);
    pDst->hModFile = pSrc->hModFile;
    truckQueueInit(&pDst->parking);
    commuteCopy(&pDst->com, &pSrc->com);
    pDst->fnTruckHandler = pSrc->fnTruckHandler;
    pDst->fnModEntry = pSrc->fnModEntry;
    pDst->tid = pSrc->tid;
    return 1;
}

int iircModuleQuery(iircModule_t *pMod)
{
    char *sz;
    char *argv[2];
    char modExe[PATH_MAX];
    char szFlags[12];
    iircModule_t module;
    pid_t process_id;
#ifdef HAVE_METRIC_PIPES
    pipe_t modPipe;
#endif

    if(pMod->version < IIRC_MOD_VERSION) {
        VLOG_MSG(LOG_LOW,"iircModuleQuery - unsupported descr version: %d",
                pMod->version);
        return -1;
    }
    switch(pMod->locationType)
    {
        case IIRC_MOD_LOCAL:
            if(pMod->fnModEntry == NULL) {
                LOG_MSG(LOG_LOW,"iircModuleQuery - local entry point NULL");
                return -1;
            }
            pMod->flags |= IIRC_MOD_QUERY;
            if (pMod->fnModEntry(pMod) == NULL) {
                LOG_MSG(LOG_LOW,"iircModuleQuery - local mod query failed");
                return -1;
            }
            break;
        case IIRC_MOD_LIB:
            //
            // module is located in this process as a shared lib
            if(pMod->hModFile == NULL || pMod->fnModEntry == NULL)
            {
                //pMod->hModFile = dlopen(pMod->modPath, RTLD_NOW);
                pMod->hModFile = lt_dlopenext(pMod->modPath);
                if(pMod->hModFile == NULL) {
                    VLOG_MSG(LOG_LOW,"iircModuleQuery - %s", lt_dlerror());
                    return -1;
                }
                pMod->fnModEntry = (iircModEntry_t)
                    lt_dlsym((lt_dlhandle)pMod->hModFile, "iircServerModEntry");
                if(pMod->fnModEntry == NULL) {
                    VLOG_MSG(LOG_LOW,"iircModuleQuery - %s", lt_dlerror());
                    lt_dlclose((lt_dlhandle)pMod->hModFile);
                    return -1;
                }
            }
            pMod->flags |= IIRC_MOD_QUERY;
            if (pMod->fnModEntry(pMod) == NULL) {
                LOG_MSG(LOG_LOW,"iircModuleQuery - lib mod query failed");
                return -1;
            }
            break;
#ifdef HAVE_METRIC_PIPES
        case IIRC_MOD_EXE:
            //
            // module is located in an executable coprocessor
            pMod->flags |= IIRC_MOD_QUERY;
            pipeCreate(&modPipe, PIPE_READ | PIPE_WRITE | PIPE_ERR);
            process_id = fork();
            if(process_id < 0) {
                LOG_MSG(LOG_SYS,"iircModuleQuery - fork failed");
                pipeDestroy(&modPipe);
                return -1;
            }
            if(process_id == 0) {
                // small child
                pipeAttach(&modPipe, FALSE); // attach one side of pipe to child
                pipeDupStdio(&modPipe); // dup pipe onto child's standard I/O
                snprintf(szFlags, sizeof(szFlags), "%u", pMod->flags);
                setenv(IIRC_ENV_MOD_FLAG, szFlags, 1);
                strncpy(modExe, pMod->modPath, PATH_MAX);
                sz = strrchr(modExe, '/');
                if(sz != NULL)
                    argv[0] = sz + 1;
                else
                    argv[0] = modExe;
                argv[1] = NULL;
                //
                // execute the module.
                if(execv(pMod->modPath, argv) < 0) {
                    LOG_MSG(LOG_SYS,"iircModuleQuery - execve failed");
                    pipeDestroy(&modPipe);
                    return -1;
                }
                // execv does not return if successfull
            }
            // parent
            pipeAttach(&modPipe, TRUE);
            if(pipeRead(&modPipe, &module, sizeof(iircModule_t)) < 0) {
                LOG_MSG(LOG_LOW,"iircModuleQuery - pipeRead failed");
                pipeDestroy(&modPipe);
                return -1; // it's up to the child to exit itself
            }
            pipeDestroy(&modPipe);
            strncpy(pMod->name, module.name, MAX_IIRC_MOD_NAME);
            strncpy(pMod->info, module.info, MAX_IIRC_MOD_INFO);
            pMod->truckSize = module.truckSize;
            pMod->flags &= ~IIRC_MOD_QUERY;
            break;
#endif
            /*
               case IIRC_MOD_NET:
            //
            // module is located using the network
            LOG_MSG(LOG_USER,"iircModuleQuery - net modules not supported");
            break;
             */
        default:
            VLOG_MSG(LOG_USER,"iircModuleQuery - unknown mod location: %d",
                    pMod->locationType);
            return -1;
    }
    return 1;
}

static int iircModuleThread(iircModule_t *pMod)
{
#ifdef USE_METRIC_THREADS
    int value = 0;
    pipe_t modPipe;
    mUint32 ack;
    thread_t tid = (thread_t)pMod->tid;
    thread_entry tent = (thread_entry)pMod->fnModEntry;
    //
    // set up pipe for commute to the thread and back
    truckQueueAlloc(&pMod->parking, 1, 1, pMod->truckSize);
    pMod->com.localParking = &pMod->parking;
    pipeCreate(&modPipe, PIPE_READ | PIPE_WRITE | PIPE_ERR);
    modPipe.flags |= PIPE_PARENT; // attach to pipe as parent.
    commuteUsingPipe(&pMod->com, &modPipe);
    //
    // start the module thread.  It attaches the pipe as a child.
    if(thread_create(&tid, tent, pMod) < 0) {
        LOG_MSG(LOG_LOW,"iircModuleOpen - thread creation failed");
        pipeDestroy(&modPipe);
        return -1;
    }
    // wait for module to respond
    pipeRead(&modPipe, &ack, sizeof(mUint32));
    // set pipe to non-blocking
    pipeOptionSet(&modPipe, PIPE_NONBLOCK | PIPE_READ |
            PIPE_WRITE | PIPE_ERR, &value);
    return 1;
#endif
    logMsg(LOG_LOW,"iircModuleOpen - thread support not compiled.");
    return -1;
}

int iircModuleOpen(iircModule_t *pMod, mUint32 hChan)
{
    int value = 0;
    char *sz;
    char *argv[2];
    char modExe[PATH_MAX];
    char szFlags[12];
    pid_t process_id;
#ifdef HAVE_METRIC_PIPES
    pipe_t modPipe;
#endif

    if(pMod->ref) {
        pMod->ref++;
        return 1;  // yup, already opened.
    }
    pMod->hChan = hChan;
    pMod->flags &= ~IIRC_MOD_QUERY;

    switch(pMod->locationType)
    {
        case IIRC_MOD_LOCAL:
        case IIRC_MOD_LIB:
            if(pMod->fnModEntry == NULL) {
                LOG_MSG(LOG_LOW,"iircModuleOpen - local entry point NULL");
                return -1;
            }
            if(pMod->flags & IIRC_MOD_THREAD) {
                return iircModuleThread(pMod);
            }
            if(pMod->fnModEntry(pMod) == NULL) {
                LOG_MSG(LOG_LOW,"iircModuleOpen - local mod open failed");
                return -1;
            }
            break;
#ifdef HAVE_METRIC_PIPES
        case IIRC_MOD_EXE:
            //
            // module is located in an executable coprocessor
            pipeCreate(&modPipe, PIPE_READ | PIPE_WRITE | PIPE_ERR);
            process_id = fork();
            if(process_id < 0) {
                LOG_MSG(LOG_SYS,"iircModuleOpen - fork failed");
                pipeDestroy(&modPipe);
                return -1;
            }
            if(process_id == 0)
            {
                // child
                pipeAttach(&modPipe, FALSE); // attach one side of pipe to child
                pipeDupStdio(&modPipe); // dup pipe onto child's standard I/O
                snprintf(szFlags, 12, "%u", pMod->flags);
                setenv(IIRC_ENV_MOD_FLAG, szFlags, 1);
                strncpy(modExe, pMod->modPath, PATH_MAX);
                sz = strrchr(modExe, '/');
                if(sz != NULL)
                    argv[0] = sz + 1;
                else
                    argv[0] = modExe;
                argv[1] = NULL;
                //
                // execute the module.
                if(execv(pMod->modPath, argv) < 0) {
                    LOG_MSG(LOG_SYS,"iircModuleOpen - execve failed");
                    pipeDestroy(&modPipe);
                    return -1;
                }
                // execv does not return if successfull
            }
            pipeAttach(&modPipe, TRUE);
            pipeOptionSet(&modPipe, PIPE_NONBLOCK | PIPE_READ |
                    PIPE_WRITE | PIPE_ERR, &value);
            truckQueueAlloc(&pMod->parking, 1, 1, pMod->truckSize);
            pMod->com.localParking = &pMod->parking;
            commuteUsingPipe(&pMod->com, &modPipe);
            break;
#endif
        default:
            VLOG_MSG(LOG_USER,"iircModuleOpen - unknown mod location: %d",
                    pMod->locationType);
            return -1;
    }
    pMod->ref++;
    return 1;
}

int iircModuleClose(iircModule_t *pMod)
{
    // TODO free parking lot
    if(--pMod->ref == 0) {
        // free stuff / close child
    }
    return 1;
}

int iircModuleSend(iircModule_t *pMod, truck *pTruck)
{
    int status;

    if(!pMod->ref)
        return 0; // module hasn't been opened yet
    if(pTruck->size > pMod->truckSize) {
        VLOG_MSG(LOG_WARN,"iircModuleSend - truck is too large for mod: %s",
                pMod->name);
        return -1;
    }
    if(pMod->locationType <= IIRC_MOD_LIB && !(pMod->flags & IIRC_MOD_THREAD))
    {
        // module isn't using a pipe.  using a bong maybe?
        if(pMod->fnTruckHandler == NULL) {
            LOG_MSG(LOG_LOW,"iircModuleSend - truck handler null");
            return -1;
        }
        return pMod->fnTruckHandler(0, pTruck, &status);
    }
    return commuteSend(&pMod->com, pTruck);
}

truck *iircModuleReceive(iircModule_t *pMod, int *pStatus)
{
    truck *pTruck;

    if(!pMod->ref)
        return NULL;
    if(pMod->locationType <= IIRC_MOD_LIB && !(pMod->flags & IIRC_MOD_THREAD))
        return NULL; // receive is async.
    pTruck = commuteReceive(&pMod->com, pStatus);
    if(*pStatus < 0) {
        pMod->ref = 1; // guarantee an absolute module closure
        iircModuleClose(pMod);
        return NULL;
    }
    return pTruck;
}

int iircModuleRecycle(iircModule_t *pMod, truck *pTruck)
{
    if(pMod->locationType <= IIRC_MOD_LIB && !(pMod->flags & IIRC_MOD_THREAD))
        return 1; // no parking necessary
    truckEnqueue(&pMod->parking, pTruck);
    return 1;
}

int genModInit(generic_t *pGen)
{
    iircModule_t *pMod = (iircModule_t*)pGen;
    return iircModuleInit(pMod);
}

int genModFree(generic_t *pGen)
{
    iircModule_t *pMod = (iircModule_t*)pGen;
    commuteFree(&pMod->com);
    return 1;
}

int genModCmp(generic_t *pGen1, generic_t *pGen2)
{
    iircModule_t *pMod1 = (iircModule_t*)pGen1;
    iircModule_t *pMod2 = (iircModule_t*)pGen2;
    return unicode_cmp(pMod1->name, pMod2->name, UTF8, MAX_IIRC_MOD_NAME);
}

int genModCopy(generic_t *pDst, generic_t *pSrc)
{
    return iircModuleCopy((iircModule_t*)pDst, (iircModule_t*)pSrc);
}
