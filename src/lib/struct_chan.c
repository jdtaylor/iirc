
/*
 * Copyright (c) 2001-2002,  James D. Taylor.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * contact James D. Taylor:  james.d.taylor@gmail.com
 */

#include "iirc/struct_chan.h"
#include "metric/char.h"


char iircChanMode[IIRC_CHAN_NUM_MODES+1] = "psitnlk";

//!
// UCS4 characters rejected from channel names
static mUint32 g_chanReject[] = {
    0x00000020, // SPACE
    0x00000007, // BELL
    0x0000000d, // CR
    0x0000000a, // LF
    0x0000002c, // comma
    0x00000000  // NUL
};
static mUint32 g_chanRejectOR = 0x20 | 0x7 | 0xd | 0xa | 0x2c;

//!
// Generic channel operations.  for use with genericSet (metric/set.h)
static int genChanInit(generic_t *pGen);
static int genChanAlloc(generic_t *pGen);
static int genChanFree(generic_t *pGen);
static int genChanCmp(generic_t *pGen1, generic_t *pGen2);
static int genChanCopy(generic_t *pGenDst, generic_t *pGenSrc);

genericInfo_t iircChanInfo = {
    size: sizeof(iircChan_t),
    fnInit: genChanInit,
    fnAlloc: genChanAlloc,
    fnFree: genChanFree,
    fnCmp: genChanCmp,
    fnCopy: genChanCopy
};

//!
// <channel> = ('#' | '&') <chstring>
int chanNameVerify(mUint8 *chan)
{
    mUint32 wchan[MAX_IIRC_CHAN_NAME];
    unsigned int i, j;
    //
    // convert UTF8 character stream to 32bit UCS4 characters
    unicode_decode(wchan, chan, UTF8, MAX_IIRC_CHAN_NAME);
    //
    // touch all characters in the channel name
    i = 0;
    while(i < MAX_IIRC_CHAN_NAME && wchan[i]) {
        //
        // if the channel name character has only bits of combined char
        if(wchan[i] & g_chanRejectOR) {
            //
            // verify if this is a rejected character
            j = 0;
            while(g_chanReject[j]) {
                if(wchan[i] == g_chanReject[j])
                    return 0; // bad channel name
                j++;
            }
        }
        i++;
    }
    if(i < 2)
        return 0;
    if(wchan[0] == (mUint32)'#' || wchan[0] == (mUint32)'&')
        return 1;
    return 0;
}


/**********************************
 * @iircChan
 */

int iircChanInit(iircChan_t *pChan)
{
    pChan->id = -1;
    pChan->name[0] = 0;
    pChan->topic[0] = 0;
    pChan->key[0] = 0;
    pChan->flags = 0;
    pChan->modes = 0;
    pChan->userLimit = 0;
    hChanModSet(Init)(&pChan->modules, &genericInt, 5, INCR_LINEAR);
    hUserSet(Init)(&pChan->users, &genericInt, 5, INCR_EXPONENTIAL);
    userFlagSet(Init)(&pChan->userFlags, &genericInt, 5, INCR_EXPONENTIAL);
    pChan->fnCargoReceiver = NULL;
    return 1;
}

int iircChanAlloc(iircChan_t *pChan)
{
    hChanModSet(Alloc)(&pChan->modules);
    hUserSet(Alloc)(&pChan->users);
    userFlagSet(Alloc)(&pChan->userFlags);
    return 1;
}

int iircChanFree(iircChan_t *pChan)
{
    hChanModSet(Free)(&pChan->modules);
    hUserSet(Free)(&pChan->users);
    userFlagSet(Free)(&pChan->userFlags);
    return 1;
}

int iircChanCmp(iircChan_t *pChan1, iircChan_t *pChan2)
{
    int i;
    i = pChan1->id - pChan2->id;
    if(i) return i;
    i = unicode_cmp(pChan1->name, pChan2->name, UTF8, MAX_IIRC_CHAN_NAME);
    if(i) return i;
    i = unicode_cmp(pChan1->topic, pChan2->topic, UTF8, MAX_IIRC_CHAN_TOPIC);
    if(i) return i;
    i = unicode_cmp(pChan1->key, pChan2->key, UTF8, MAX_IIRC_CHAN_KEY);
    if(i) return i;
    i = pChan1->flags - pChan2->flags;
    if(i) return i;
    i = pChan1->modes - pChan2->modes;
    if(i) return i;
    i = pChan1->userLimit - pChan2->userLimit;
    if(i) return i;
    // TODO
    //i = genericArrayCmp(&pChan1->users, &pChan2->users);
    //if(i) return i;
    //i = genericArrayCmp(&pChan1->modules, &pChan2->modules);
    //if(i) return i;
    return 0; // equal
}

int iircChanCopy(iircChan_t *pDst, iircChan_t *pSrc)
{
    pDst->id = pSrc->id;
    unicode_cpy(pDst->name, pSrc->name, UTF8, MAX_IIRC_CHAN_NAME);
    unicode_cpy(pDst->topic, pSrc->topic, UTF8, MAX_IIRC_CHAN_TOPIC);
    unicode_cpy(pDst->key, pSrc->key, UTF8, MAX_IIRC_CHAN_KEY);
    pDst->flags = pSrc->flags;
    pDst->modes = pSrc->modes;
    pDst->userLimit = pSrc->userLimit;
    hChanModSet(Copy)(&pDst->modules, &pSrc->modules);
    hUserSet(Copy)(&pDst->users, &pSrc->users);
    userFlagSet(Copy)(&pDst->userFlags, &pSrc->userFlags);
    pDst->fnCargoReceiver = pSrc->fnCargoReceiver;
    return 1;
}

int iircChanUserNum(iircChan_t *pChan)
{
    return pChan->users.num; // TODO: kludge
}

//!
// generic operations on struct _iircChan
//

int genChanInit(generic_t *pGen)
{
    return iircChanInit((iircChan_t*)pGen);
}

int genChanAlloc(generic_t *pGen)
{
    return iircChanAlloc((iircChan_t*)pGen);
}

int genChanFree(generic_t *pGen)
{
    return iircChanFree((iircChan_t*)pGen);
}

int genChanCmp(generic_t *pGen1, generic_t *pGen2)
{
    return iircChanCmp((iircChan_t*)pGen1, (iircChan_t*)pGen2);
}

int genChanCopy(generic_t *pGenDst, generic_t *pGenSrc)
{
    return iircChanCopy((iircChan_t*)pGenDst, (iircChan_t*)pGenSrc);
}

