
/*
 * Copyright (c) 2001-2002,  James D. Taylor.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * contact James D. Taylor:  james.d.taylor@gmail.com
 */

#include "iirc/struct_client.h"
#include "iirc/struct_user.h"


static int genIrcClientInit(generic_t *pGeneric);
static int genIrcClientAlloc(generic_t *pGeneric);
static int genIrcClientFree(generic_t *pGeneric);
genericInfo_t iircClientInfo = {
    size: sizeof(iircClient_t),
    fnInit: genIrcClientInit,
    fnAlloc: genIrcClientAlloc,
    fnFree: genIrcClientFree,
    fnCmp: NULL,
    fnCopy: NULL
};


int iircClientInit(iircClient_t *pClient)
{
    unsigned int i;

    pClient->id = 0;
    pClient->flags = 0;
    pClient->hostname[0] = '\0';
    pClient->bStarted = FALSE;
    iircStateInit(&pClient->state);
    clientUserSet(Init)(&pClient->users, &iircUserInfo, 1, INCR_LINEAR);
    truckQueueInit(&pClient->parking);
    socketSetInit(&pClient->sockets);
    commuteInit(&pClient->parentCom);
    intrSet(Init)(&pClient->interrupts, &interruptInfo, 1, INCR_EXPONENTIAL);
    for(i = 0; i < MAX_IIRC_CARGO_TYPE; i++) {
        pClient->fnPreHandler[i] = NULL;
        pClient->fnPostHandler[i] = NULL;
    }
    pClient->fnPreClientHandler = NULL;
    pClient->fnPostClientHandler = NULL;
    pClient->pClientData = NULL;
    //    for(i = 0; i < MAX_IIRC_MODULE; i++)
    //  pClient->modules[i] = NULL;
    //    pClient->nModule = 0;
    pClient->servername[0] = '\0';
    pClient->serverPort = 0;
    // default is to wait for certain initial trucks
    pClient->waitFlags = IIRC_WAIT_MODS;// | IIRC_WAIT_ADDR;
    for(i = 0; i < MAX_IIRC_CCMDS; i++)
        pClient->fnCmd[i] = NULL;
    return 1;
}

int iircClientAlloc(iircClient_t *pClient)
{
    if(clientUserSet(Alloc)(&pClient->users) < 0) {
        iircClientFree(pClient);
        return -1;
    }
    if(intrSet(Alloc)(&pClient->interrupts) < 0) {
        iircClientFree(pClient);
        return -1;
    }
    return 1;
}

int iircClientFree(iircClient_t *pClient)
{
    pClient->bStarted = FALSE;
    iircStateFree(&pClient->state);
    clientUserSet(Free)(&pClient->users);
    intrSet(Free)(&pClient->interrupts);
    truckQueueFree(&pClient->parking);
    socketSetFree(&pClient->sockets);
    commuteFree(&pClient->parentCom);
    //    for(i = 0; i < MAX_IIRC_MODULE; i++) {
    //  if(pClient->modules[i] != NULL)
    //      FREE(pClient->modules[i]);
    //    }
    return 1;
}

int genIrcClientInit(generic_t *pGeneric)
{
    return iircClientInit((iircClient_t*)pGeneric);
}

int genIrcClientAlloc(generic_t *pGeneric)
{
    return iircClientAlloc((iircClient_t*)pGeneric);
}

int genIrcClientFree(generic_t *pGeneric)
{
    return iircClientFree((iircClient_t*)pGeneric);
}


