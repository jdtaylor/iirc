
/*
 * Copyright (c) 2001-2002,  James D. Taylor.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * contact James D. Taylor:  james.d.taylor@gmail.com
 */

#ifndef IIRC_STRUCT_USER_H
#define IIRC_STRUCT_USER_H

#include "iirc/common.h"
#include "metric/type.h"
#include "metric/set.h"
#include "metric/sock.h" // MAX_HOST_NAME
#include "iirc/truck.h"
#include "iirc/defs.h"

#define hUserModSet_t   genericInsArray
#define hUserModSet(op) genericInsArray##op

#define hChanSet_t  genericIncrArray
#define hChanSet(op)    genericIncrArray##op

#define hChanUserSet_t   genericInsArray
#define hChanUserSet(op) genericInsArray##op

//!
// iircUser_t flags
enum {
    IIRC_USER_1459      = 0x00000001, //! user is using rfc1459
    IIRC_USER_LOCAL     = 0x00000002, //! user is directly connected
    IIRC_USER_MOTD      = 0x00000008, //! whether MOTD been sent yet.
    //
    // flags used for temporary information passing
    IIRC_USER_CONNECT       = 0x80000000, //! user is connecting
    IIRC_USER_DISCONNECT    = 0x40000000, //! user is closing connection
};

//!
// iircUser_t modes
enum {
    IIRC_USER_INVISIBLE     = 0x00000001,
    IIRC_USER_SERVER_NOTICE = 0x00000002,
    IIRC_USER_WALLOPS       = 0x00000004,
    IIRC_USER_SYSOP     = 0x00000008,
};

typedef struct iircUser {
    mInt32 id;  //! handle to ourself
    mUint32 flags;
    mUint32 modes;
    mUint8 nick[MAX_IIRC_NICK]; //! (32)
    mUint8 nick_old[MAX_IIRC_NICK]; //! previous nick of user (32)
    mInt8 identity[MAX_IIRC_IDENTITY]; //! user's ident name (32)
    mInt8 hostname[MAX_HOST_NAME]; // 
    mInt8 servername[MAX_HOST_NAME];
    mUint8 realname[MAX_IIRC_REAL_NAME]; // 32
    hUserModSet_t modules; //! module handles this user supports.
    hUserModSet_t modEnabled; //! module handles this user has enabled.
    hChanSet_t chans; //! channels the user is on. "hChan[i]"
    hChanUserSet_t chanUsers;  //! user's offset in each chan. "hChanUser[i]"
    mUint32 hCom; //! handle to a structure responsible for communication
    cargoProcessor fnCargoReceiver;
} iircUser_t;

extern genericInfo_t iircUserInfo; // declared in struct_user.c


#ifdef __cplusplus
extern "C" {
#endif

int nickVerify(mUint8 *nick);

int iircUserInit(iircUser_t *pUser);
int iircUserAlloc(iircUser_t *pUser);
int iircUserFree(iircUser_t *pUser);
int iircUserCmp(iircUser_t *pUser1, iircUser_t *pUser2);
int iircUserCopy(iircUser_t *pDst, iircUser_t *pSrc);

#ifdef __cplusplus
}
#endif

#endif

