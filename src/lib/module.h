
/*
 * Copyright (c) 2001-2002,  James D. Taylor.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * contact James D. Taylor:  james.d.taylor@gmail.com
 */

#ifndef IIRC_MODULE_H
#define IIRC_MODULE_H

#include "iirc/common.h"
// for PATH_MAX
#include "metric/type.h"
#include "metric/set.h"
#include "iirc/commute.h"
#include "iirc/defs.h"
#include "iirc/truck.h"
#ifdef HAVE_METRIC_PIPES
  #include "metric/pipe.h"
#endif

#define chanModSet_t   genericOctetTree
#define chanModSet(op) genericOctetTree##op
extern genericInfo_t iircGenModInfo;

//!
// possible types of chan module locations
// iircModule.locationType
//
enum {
    IIRC_MOD_LOCAL = 0, //! module's execution is already handled
    IIRC_MOD_LIB, //! module should be spawned with a thread
    IIRC_MOD_NET, //! somewhere on the network
#ifdef HAVE_METRIC_PIPES
    IIRC_MOD_EXE, //! local coprocess
#endif
};

//
// needs to be the same as 'thread_entry' (metric/thread.h)
typedef void* (*iircModEntry_t)(void*);

//!
// iircModule.flags
#define IIRC_MOD_QUERY  0x00000001 /* module is being queried */
#define IIRC_MOD_THREAD 0x00000002 /* module has it's own thread */

typedef struct iircModule {
    //!
    // version of this structure - CHAN_MOD_DESCR_VERSION
    mUint32 version;
    mUint32 ref; //! number of times the module has been opened
    //!
    // absolute chan module handle (or -1 if root server)
    mInt32 global_id; //! global handle of this iirc chan module
    mInt32 local_id; //! process' handle to this structure
    mUint32 hChan; //! handle to channel used on module startup
    mUint32 hParent; //! iircServer handle  or  iircClient handle
    mUint32 flags;
    //!
    // module information which is returned by the module when queried.
    mUint8 name[MAX_IIRC_MOD_NAME];
    mUint8 info[MAX_IIRC_MOD_INFO];
    mUint32 truckSize;
    //!
    // where the module can be located
    // (one of the IIRC_MOD_* enumerations above)
    mUint32 locationType;
    //!
    // for lib/exe modules, we need modPath for the
    // location of the module file in the local filesystem.
    mInt8 modPath[PATH_MAX];
    void *hModFile; //! handle from lt_dlopen (lt_dlhandle)
    //!
    // internal
    truckQueue parking;
    commute_t com;
    cargoProcessor fnTruckHandler; // send trucks here for local modules
    //!
    // for local/lib modules, this is the entry point
    // when starting it's thread.
    iircModEntry_t fnModEntry;
    mUint32 tid; // thread_t  thread id
} iircModule_t;
#define IIRC_MOD_VERSION 0x00000100

//!
// environment variable name that is sent to each coproc module
// in order to communicate the module flags
#define IIRC_ENV_MOD_FLAG "IIRC_MOD_FLAG"

#define IIRC_MOD_HEAD_MAGIC "IIRC_module-"
#define IIRC_MOD_HEAD_VERSION '1'
//!
// sent from module to module-loader when opened.  All fields are UTF8
// character encoded so as not to disturb peoples terminals.
//
typedef struct _iircModHead {
    mInt8 magic[12]; //! IIRC_MOD_MAGIC (NO NUL termination!)
    mInt8 version; //! iirc chan module interface version
    mInt8 _space; //! .. the final frontier.
    mInt8 truckSize[8];
    mInt8 _space2; //! .. the final final frontier.
    mInt8 name[MAX_IIRC_MOD_NAME];
    mInt8 _newline1; //! for pretty print on terminal
    mInt8 info[MAX_IIRC_MOD_INFO]; //! module supplied (arbitrary)
    mInt8 _newline2;
} iircModHead;



#ifdef __cplusplus
extern "C" {
#endif

int iircModuleInit(iircModule_t *pMod);
int iircModuleQuery(iircModule_t *pMod);
int iircModuleOpen(iircModule_t *pMod, mUint32 hChan);
int iircModuleClose(iircModule_t *pMod);
int iircModuleSend(iircModule_t *pMod, truck *pTruck);
truck* iircModuleReceive(iircModule_t *pMod, int *pStatus);
int iircModuleCopy(iircModule_t *pDst, iircModule_t *pSrc);
int iircModuleRecycle(iircModule_t *pMod, truck *pTruck);

int iircModHeadInit(iircModHead *pModHead, mUint32 truckSize,
            char *name, char *info);

#ifdef __cplusplus
}
#endif

#endif


