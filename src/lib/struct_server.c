
/*
 * Copyright (c) 2001-2002,  James D. Taylor.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * contact James D. Taylor:  james.d.taylor@gmail.com
 */


#include "metric/char.h"
#include "iirc/server.h"
#include "iirc/struct_server.h"


static int genIrcServerInit(generic_t *pGeneric);
static int genIrcServerAlloc(generic_t *pGeneric);
static int genIrcServerFree(generic_t *pGeneric);
genericInfo_t iircServerInfo = {
size: sizeof(iircServer_t),
      fnInit: genIrcServerInit,
      fnAlloc: genIrcServerAlloc,
      fnFree: genIrcServerFree,
      fnCmp: NULL,
      fnCopy: NULL
};

static int genRawClientInit(generic_t *pGeneric);
static int genRawClientFree(generic_t *pGeneric);
genericInfo_t rawClientInfo = {
size: sizeof(rawClient_t),
      fnInit: genRawClientInit,
      fnAlloc: NULL,
      fnFree: genRawClientFree,
      fnCmp: NULL,
      fnCopy: NULL
};


int iircServerInit(iircServer_t *pServer)
{
    unsigned int i;

    pServer->version = IIRC_SERVER_VERSION;
    pServer->id = 0;
    pServer->name[0] = '\0';
    pServer->hostname[0] = '\0';
    pServer->bStarted = FALSE;
    iircStateInit(&pServer->state);
    truckQueueInit(&pServer->parking);
    socketSetInit(&pServer->sockets);
    commuteInit(&pServer->parentCom);
    socketInit(&pServer->serverConnect);
    socketInit(&pServer->clientConnect);
    clientComSet(Init)(&pServer->clientComs, &commuteInfo, 1, INCR_EXPONENTIAL);
    serverComSet(Init)(&pServer->serverComs, &commuteInfo, 1, INCR_LINEAR);
    intrSet(Init)(&pServer->interrupts, &interruptInfo, 1, INCR_EXPONENTIAL);
    for(i = 0; i < MAX_IIRC_CARGO_TYPE; i++) {
        pServer->fnPreHandler[i] = NULL;
        pServer->fnPostHandler[i] = NULL;
    }
    pServer->isRoot = TRUE;
    pServer->parentName[0] = '\0';
    pServer->parentPort = 0;
    pServer->listenForServers = FALSE;
    pServer->serverName[0] = '\0';
    pServer->serverPort = 0;
    pServer->listenForClients = FALSE;
    pServer->clientName[0] = '\0';
    pServer->clientPort = 0;
    pServer->listenForRFC1459 = FALSE;
    pServer->rfc1459Name[0] = '\0';
    pServer->rfc1459Port = 0;
    socketInit(&pServer->rfc1459Sock);
    socketAddrInit(&pServer->rfc1459SockAddr);
    rawClientSet(Init)(&pServer->rfc1459Set, &rawClientInfo,2,INCR_EXPONENTIAL);
    for(i = 0; i < MAX_IIRC_SCMDS; i++)
        pServer->fnCmd[i] = NULL;
    return 1;
}

int iircServerAlloc(iircServer_t *pServer)
{
    if(clientComSet(Alloc)(&pServer->clientComs) < 0) {
        iircServerFree(pServer);
        return -1;
    }
    if(serverComSet(Alloc)(&pServer->serverComs) < 0) {
        iircServerFree(pServer);
        return -1;
    }
    if(intrSet(Alloc)(&pServer->interrupts) < 0) {
        iircServerFree(pServer);
        return -1;
    }
    if(rawClientSet(Alloc)(&pServer->rfc1459Set) < 0) {
        iircServerFree(pServer);
        return -1;
    }
    return 1;
}

int iircServerFree(iircServer_t *pServer)
{
    iircStateFree(&pServer->state);
    truckQueueFree(&pServer->parking);
    socketSetFree(&pServer->sockets);
    commuteFree(&pServer->parentCom);
    socketClose(&pServer->serverConnect);
    socketClose(&pServer->clientConnect);
    clientComSet(Free)(&pServer->clientComs);
    serverComSet(Free)(&pServer->serverComs);
    intrSet(Free)(&pServer->interrupts);
    rawClientSet(Free)(&pServer->rfc1459Set);
    socketClose(&pServer->rfc1459Sock);
    return 1;
}


int genIrcServerInit(generic_t *pGeneric)
{
    return iircServerInit((iircServer_t*)pGeneric);
}

int genIrcServerAlloc(generic_t *pGeneric)
{
    return iircServerAlloc((iircServer_t*)pGeneric);
}

int genIrcServerFree(generic_t *pGeneric)
{
    return iircServerFree((iircServer_t*)pGeneric);
}

int genRawClientInit(generic_t *pGeneric)
{
    rawClient_t *pClient = (rawClient_t*)pGeneric;

    pClient->id = 0;
    pClient->name[0] = 0;
    socketInit(&pClient->sock);
    pClient->hInterrupt = 0;
    pClient->pServer = NULL;
    pClient->hUser = 0;
    return 1;
}

int genRawClientFree(generic_t *pGeneric)
{
    rawClient_t *pClient = (rawClient_t*)pGeneric;

    if(pClient->pServer != NULL) {
        socketSetDel(&pClient->pServer->sockets, &pClient->sock);
        iircServerIntrDel(pClient->pServer->id, pClient->hInterrupt);
        // TODO iircUser_t needs to be freed and *_DISCONNECT sent
    }
    socketClose(&pClient->sock);
    return 1;
}

