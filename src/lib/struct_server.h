

/*
 * Copyright (c) 2001-2002,  James D. Taylor.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * contact James D. Taylor:  james.d.taylor@gmail.com
 */

#ifndef IIRC_STRUCT_SERVER_H
#define IIRC_STRUCT_SERVER_H

#include "iirc/common.h"
#include "iirc/truck.h"
#include "iirc/defs.h"
#include "iirc/commute.h"
#include "iirc/state.h"
#include "iirc/struct_intr.h"

//!
// set of clients (independent of IIRC) that are connecting
#define rawClientSet_t genericIncrArray
#define rawClientSet(op) genericIncrArray##op
//!
// set of client connections
#define clientComSet_t   genericOctetTree
#define clientComSet(op) genericOctetTree##op

#define serverComSet_t genericOctetTree
#define serverComSet(op) genericOctetTree##op

#define MAX_IIRC_SCMD_PARAMS 32

#define IIRC_SERVER_WAIT_MODS 0x00000001
//!
// flag for determining if a user is directly connected to a server
#define IIRC_COM_REMOTE 0xFFFFFFFF

//! server commands
enum {
    IIRC_SCMD_NOP = 0,
    IIRC_SCMD_HOSTNAME,
    IIRC_SCMD_SERVERNAME,
    IIRC_SCMD_LOAD,
//    IIRC_SCMD_START,
    IIRC_SCMD_QUIT,
    MAX_IIRC_SCMDS
};

struct iircServer;

//! server cargo handler type
typedef int (*iircSCargoHandler)(struct iircServer *pSelf, truck *pTruck,
                int *status);
//! server command handler type
typedef int (*iircSCmdHandler)(struct iircServer *pSelf, mUint32 *cmd);


typedef struct iircServer {
    int version;
    unsigned int id;
    char name[MAX_IIRC_SERVER_NAME];
    char hostname[MAX_HOST_NAME];
    int bStarted;
    unsigned int waitFlags;
    iircState_t state;  //! iirc state data
    truckQueue parking; //! trucks waiting to be used
    socketSet_t sockets; //! socket set of all clients on this server
    commute_t parentCom;
    socket_t serverConnect; //! socket for incoming SYN's from servers
    socket_t clientConnect; //! socket for incoming SYN's from clients
    clientComSet_t clientComs; //! connection to each clients on this server
    serverComSet_t serverComs;
    intrSet_t interrupts; //! set of I/O interrupts to watch for
    iircSCargoHandler fnPreHandler[MAX_IIRC_CARGO_TYPE]; //! pre-state handler
    iircSCargoHandler fnPostHandler[MAX_IIRC_CARGO_TYPE];//! post-state handler
    int isRoot; //! whether this server is the root server. (no parent)
    char parentName[MAX_HOST_NAME]; //! remote hostname of parent city
    short parentPort;       //! remote tcp port of parent city
    int listenForServers; //! set to TRUE and fill in 'listenServer'
    char serverName[MAX_HOST_NAME]; //! address to listen for child servers
    short serverPort;
    int listenForClients;   //! set to TRUE and fill in 'listenClient'
    char clientName[MAX_HOST_NAME]; //! address to listen for clients
    short clientPort;
    int listenForRFC1459;
    char rfc1459Name[MAX_HOST_NAME];
    short rfc1459Port;
    socket_t rfc1459Sock; //! socket for RFC1459 (usually port 6667)
    socketAddr_t rfc1459SockAddr;
    rawClientSet_t rfc1459Set; //! iirc clients connected via RFC1459
    iircSCmdHandler fnCmd[MAX_IIRC_SCMDS]; //! server command handlers
} iircServer_t;
#define IIRC_SERVER_VERSION 0x00000100

extern genericInfo_t iircServerInfo;

#define MAX_RAW_CLIENT_NAME MAX_IIRC_NICK

typedef struct rawClient {
    unsigned int id;
    mUint8 name[MAX_RAW_CLIENT_NAME];
    socket_t sock;
    unsigned int hInterrupt;
    iircServer_t *pServer;
    mUint32 hUser;
} rawClient_t;

extern genericInfo_t rawClientInfo;


#ifdef __cplusplus
extern "C" {
#endif

int iircServerInit(iircServer_t *pServer);
int iircServerAlloc(iircServer_t *pServer);
int iircServerFree(iircServer_t *pServer);

#ifdef __cplusplus
}
#endif

#endif


