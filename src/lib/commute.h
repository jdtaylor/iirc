
/*
 * Copyright (c) 2001-2002,  James D. Taylor.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * contact James D. Taylor:  james.d.taylor@gmail.com
 */

#ifndef IIRC_COMMUTE_H
#define IIRC_COMMUTE_H

#include "iirc/common.h"
#include "metric/set.h"
#include "metric/sock.h"
#include "iirc/truck.h"
#ifdef HAVE_METRIC_PIPES
#include "metric/pipe.h"
#endif

enum {
    COMMUTE_LOCAL = 0, //! commute is local to process
    COMMUTE_SOCKET, //! commute is over a bsd socket
#ifdef HAVE_METRIC_PIPES
    COMMUTE_PIPE, //! commute is through a pipe
#endif
    COMMUTE_TYPE_MAX
};

enum {
    COMMUTE_PASSIVE =        0x00000001, //! waiting for other end
    COMMUTE_CONNECTED =      0x00000002, //! commute is active
    COMMUTE_CONNECT_FAILED = 0x00000004, //! previous connect failed
};

/*!
 * the commute_t is a structure to abstract the transfer of trucks.
 * Mainly it hides the details of whether the remote party is local or
 * not.  It also handles partial truck reading; so it doesn't reveal a
 * truck unless it is complete.
 */
typedef struct _commute {
    int type; //! local / pipe / socket..
    int flags; //! one or more of the COMMUTE enumerations above
    void *pData; //! optional object which this commute leads to.
    socket_t sock; //! socket if isLocal == FALSE
    truckQueue trafficIn; //! truck storage for local delivery
    truckQueue *trafficOut; //! pointer to someone else's 'trafficIn'
    truckQueue *localParking; //! pointer to the local parking lot
    truckQueue *remoteParking; //! remote parking lot for trucks
    unsigned int remotePID; //! process id of remote if(type == PIPE)
    socketAddr_t localSockAddr; //! socket address information of local
    socketAddr_t remoteSockAddr; //! socket address of remote
    truck *pTruckIn; //! partial truck used while transfering
    unsigned int nBytesIn; //! bytes transfered so far for 'pTruckIn'
    unsigned int *dataReady; //! pointer to city/houses dataReady
#ifdef HAVE_METRIC_PIPES
    pipe_t pipe;
#endif
} commute_t;

extern genericInfo_t commuteInfo;

#ifdef __cplusplus
extern "C" {
#endif

int commuteInit(commute_t *pCom);
int commuteFree(commute_t *pCom);
int commuteCopy(commute_t *pDst, commute_t *pSrc);
int commuteSend(commute_t *pCom, truck *pTruck);
truck*  commuteReceive(commute_t *pCom, int *pStatus);
int commuteUsingSocket(commute_t *pCom, socket_t *pSock);
#ifdef HAVE_METRIC_PIPES
    int commuteUsingPipe(commute_t *pCom, pipe_t *pPipe);
#endif

#ifdef __cplusplus
}
#endif

#endif

