
/*
 * Copyright (c) 2001-2002,  James D. Taylor.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * contact James D. Taylor:  james.d.taylor@gmail.com
 */

#ifndef IIRC_CARGO_H
#define IIRC_CARGO_H

#include "iirc/common.h"
#include "iirc/truck.h"
#include "iirc/defs.h"
#include "iirc/struct_user.h"
#include "iirc/struct_chan.h"
#include "iirc/module.h"

#define MAX_IIRC_CHAN_USERS 10


enum {
    CARGO_IIRC_USER = 0,
    CARGO_IIRC_CHAN,
    CARGO_IIRC_MOD_INFO,
    CARGO_IIRC_EVENT,
    CARGO_IIRC_SYS,
    CARGO_IIRC_SERVER,
    CARGO_IIRC_INTERNAL, //! cargo's >= this will be sent to all modules
    //! cargo is a module cargo.. follow moduleId
    CARGO_IIRC_MODULE = CARGO_IIRC_INTERNAL,
    CARGO_IIRC_CHAN_MODE,
    CARGO_IIRC_CHAN_TOPIC,
    CARGO_IIRC_CHAN_USERS, //! carries users that are on a channel
    //CARGO_IIRC_USERNAME,
    CARGO_IIRC_NICK,
    CARGO_IIRC_JOIN,
    CARGO_IIRC_PART,
    CARGO_IIRC_QUIT,
    CARGO_IIRC_WHO,
    CARGO_IIRC_USER_MODE,
    CARGO_IIRC_CHAN_BAN,
    CARGO_IIRC_MSG,
    CARGO_IIRC_CHAN_MOD_ARG,
    CARGO_IIRC_USER_MOD_ARG,
    MAX_IIRC_CARGO_TYPE
};

//! if you change the above enumeration, change the definition of
//iircCargoNames in iirc_cargo.c as well!
extern char iircCargoNames[MAX_IIRC_CARGO_TYPE][32];


typedef struct _cargoIrcUser {
    truck header;
    mInt32 id; //! handle to this user
    mUint8 nick[MAX_IIRC_NICK];
    mInt8 identity[MAX_IIRC_IDENTITY];  //! whats returned by ident
    mInt8 hostname[MAX_HOST_NAME];
    mInt8 servername[MAX_HOST_NAME];
    mUint8 realname[MAX_IIRC_REAL_NAME];
    mUint32 flags;
    mUint32 modes;
} cargoIrcUser;

typedef struct _cargoIrcChan {
    truck header;
    mUint32 hChan; //! handle to this channel
    mUint8 name[MAX_IIRC_CHAN_NAME]; //! name of channel starting with '#'
    mUint32 flags;
} cargoIrcChan;

typedef struct _cargoIrcModInfo {
    truck header;
    mUint32 version;
    mUint32 bSupported;
    mInt32  global_id; //! absolute module id
    mInt32  local_id; //! local process handle to the channel module
    mUint32 truckSize;
    mUint8 name[MAX_IIRC_MOD_NAME];
    mUint8 info[MAX_IIRC_MOD_INFO];
} cargoIrcModInfo;


/*!
 *  cargoIrcEvent codes
 */
enum {
    CODE_IIRC_NOP = 0,
    CODE_IIRC_MODS_READY,
    CODE_IIRC_PING,
    CODE_IIRC_PONG,
    CODE_IIRC_MAX
};

typedef struct _cargoIrcEvent {
    truck header;
    mInt32 code;
    mInt32 data1;
} cargoIrcEvent;

typedef struct _cargoIrcSys {
    truck header;
} cargoIrcSys;

enum {
    IIRC_SERVER_CONNECT =   1,
    IIRC_SERVER_DISCONNECT =    2
};

typedef struct _cargoIrcServer {
    truck header;
    mUint8 name[MAX_IIRC_SERVER_NAME];
    mUint32 flags;
} cargoIrcServer;

typedef struct _cargoIrcNick {
    truck header;
    mUint8 nick[MAX_IIRC_NICK];
    mUint32 modes;
} cargoIrcNick;

typedef struct _cargoIrcChanMode {
    truck header;
    mUint32 modeXOR;    //! modes that have changed.
    mUint32 modeState;  //! new state of modes that have changed.
    mUint32 modeActive; //! currently active modes.
    mUint32 userLimit;  //! new user limit
    mUint8 key[MAX_IIRC_CHAN_KEY]; //! new channel key
} cargoIrcChanMode;

typedef struct _cargoIrcChanTopic {
    truck header;
    mUint8 topic[MAX_IIRC_CHAN_TOPIC];
} cargoIrcChanTopic;

enum {
    IIRC_CHAN_USERS_LAST =   0x00000001,
    IIRC_CHAN_USERS_WHO =    0x00000002, // format rfc1459 as a who reply
    IIRC_CHAN_USERS_NAME =   0x00000004, // format as NAMRPLY
    IIRC_CHAN_USERS_WHOIS =  0x00000008, // format as WHOIS reply
    IIRC_CHAN_USERS_UNUSED = 0xFFFFFFF0
};

typedef struct _cargoIrcChanUsers {
    truck header;
    mUint32 flags; //! flags above
    mUint32 hChan; //! handle to channel the users are on
    mUint32 nUser; //! number of users used in 'hUser' and 'hClient'
    mUint32 hUser[MAX_IIRC_CHAN_USERS];  //! relative to hChan
    mUint32 hUserFlags[MAX_IIRC_CHAN_USERS];  //! relative to hChan
    mUint32 hClient[MAX_IIRC_CHAN_USERS]; //! absolute user id
} cargoIrcChanUsers;

/*
typedef struct _cargoIrcUsername {
    truck header;
    mInt8 username[MAX_IIRC_USERNAME];
    mInt8 hostname[MAX_HOST_NAME];
    mInt8 servername[MAX_HOST_NAME];
    mInt8 realname[MAX_IIRC_REAL_NAME];
} cargoIrcUsername;
*/

enum {
    IIRC_JOIN_BY_NAME = 0x00000001,
    IIRC_JOIN_USE_KEY = 0x00000002,
    IIRC_JOIN_FLAGS_UNUSED = 0xFFFFFFFC
};

typedef struct _cargoIrcJoin {
    truck header;
    mUint32 flags; //! pirate flags; arrgh
    mUint32 hChan; //! channel id if not joining by name
    mUint32 hChanUser; //! user id relative to hChan
    mUint8 chan[MAX_IIRC_CHAN_NAME]; //! channel name
    mUint8 key[MAX_IIRC_CHAN_KEY];
} cargoIrcJoin;

typedef struct _cargoIrcPart {
    truck header;
    mUint32 hChan;
    mUint8 reason[MAX_IIRC_PART_REASON];
} cargoIrcPart;

typedef struct _cargoIrcQuit {
    truck header;
    mUint8 reason[MAX_IIRC_QUIT_REASON];
} cargoIrcQuit;

enum {
    IIRC_WHO_FLAG_OPS = 0x00000001, // whether to list operators
    IIRC_WHO_FLAG_VOICE = 0x00000002, // whether to list voiced users
    IIRC_WHO_FLAG_USER = 0x00000004, // whether to list normal users
    IIRC_WHO_FLAG_NAMES = 0x00000008, //! whether this is a NAMES request
    IIRC_WHO_FLAG_WHOIS = 0x00000010, //! whether this is a WHOIS request
    IIRC_WHO_FLAG_UNUSED = 0xFFFFF80
};

typedef struct _cargoIrcWho {
    truck header;
    mInt32 flags;
    mUint8 name[MAX_IIRC_WHO_NAME];  // name/channel to who
} cargoIrcWho;

typedef struct _cargoIrcUserMode {
    truck header;
    mUint32 hChan;
    //! users for parameters to chan modes
    mUint32 nUser;
    mUint32 hChanUser[MAX_IIRC_USER_MODE];
    mUint32 modeXOR[MAX_IIRC_USER_MODE];    //! which modes have changed.
    mUint32 modeState[MAX_IIRC_USER_MODE];//! new state of each changed mode.
    mUint32 modeActive[MAX_IIRC_USER_MODE];//! currently active modes.
} cargoIrcUserMode;

enum { //! cargoIrcChanModArg's 'function' member values
    IIRC_MOD_FUNC_DISABLE = 0,
    IIRC_MOD_FUNC_ENABLE,
    IIRC_MOD_FUNC_MODIFY,
    IIRC_MOD_FUNC_MAX
};

typedef struct _cargoIrcChanModArg {
    truck header;
    mUint32 hChan; //! global channel handle
    mUint32 hMod; //! global handle of module that has changed
    mUint32 function; //! one of the enumerations above
    mUint8 arg[MAX_IIRC_MOD_ARG]; //! argument to be sent to module
} cargoIrcChanModArg;

typedef struct _cargoIrcUserModArg {
    truck header;
    mUint32 hUser; //! global user handle
    mUint32 hMod; //! global handle of module that has changed
    mUint32 function; //! one of the enumerations above
    mUint8 arg[MAX_IIRC_MOD_ARG]; //! argument to be sent to module
} cargoIrcUserModArg;

typedef struct _cargoIrcChanBan {
    truck header;
    mUint32 hChan;
    mUint8 mask[MAX_IIRC_BAN_MASK];
} cargoIrcChanBan;

typedef struct _cargoIrcMsg {
    truck header;
    mUint8 msg[MAX_IIRC_MSG]; //! UTF-8 character stream
} cargoIrcMsg;

enum {
    BAN_GLOBAL = 0,
    BAN_CHAN,
};

typedef struct _cargoIrcBan {
    truck header;
    mUint32 type; //! type of ban, see enumerations above
    mInt8 hostname; //! hostname of banned host
    mUint8 channel[MAX_IIRC_CHAN_NAME]; //! channel to ban from if BAN_CHAN
} cargoIrcBan;

/*!
 * union to find the largest truck from the IIRC protocol.
 * sizeof(iircLargestTruck)
 */
typedef union _iircLargestTruck {
    cargoIrcEvent   event;
    cargoIrcSys     sys;
    cargoIrcServer  server;
    cargoIrcChanMode    chanMode;
    cargoIrcChanTopic   chanTopic;
    cargoIrcChanUsers   chanUsers;
    //cargoIrcUsername  username;
    cargoIrcNick    nick;
    cargoIrcJoin    join;
    cargoIrcPart    part;
    cargoIrcQuit    quit;
    cargoIrcWho     who;
    cargoIrcUserMode    userMode;
    cargoIrcChanModArg  chanModArg;
    cargoIrcUserModArg  userModArg;
    cargoIrcChanBan chanBan;
    cargoIrcMsg     msg;
} iircLargestTruck;

typedef int (*iircCargoVerify_t)(truck *pTruck);
typedef int (*iircCargoByteSwap_t)(truck *pTruck);
typedef int (*iircCargoChop_t)(truck *pTruck);
extern iircCargoVerify_t g_iircCargoVerify[MAX_IIRC_CARGO_TYPE];
extern iircCargoByteSwap_t g_iircCargoByteSwap[MAX_IIRC_CARGO_TYPE];
extern iircCargoChop_t g_iircCargoChop[MAX_IIRC_CARGO_TYPE];

#ifdef __cplusplus
extern "C" {
#endif

int iircCargoGlobalInit();
int iircCargoFix(truck *pTruck);
int iircCargoVerify(truck *pTruck);
int iircCargoChop(truck *pTruck);

//!
// INTERSTATE CARGOS

int cargoIrcChanInit(cargoIrcChan *pCargo);
int cargoIrcChanVerify(cargoIrcChan *pCargo);
int cargoIrcChanByteSwap(cargoIrcChan *pCargo);
int cargoIrcChanPack(cargoIrcChan *pCargo, iircChan_t *pChan);
int cargoIrcChanUnpack(cargoIrcChan *pCargo, iircChan_t *pChan);

int cargoIrcUserInit(cargoIrcUser *pCargo);
int cargoIrcUserVerify(cargoIrcUser *pCargo);
int cargoIrcUserByteSwap(cargoIrcUser *pCargo);
int cargoIrcUserPack(cargoIrcUser *pCargo, iircUser_t *pUser);
int cargoIrcUserUnpack(cargoIrcUser *pCargo, iircUser_t *pUser);

int cargoIrcModInfoInit(cargoIrcModInfo *pCargo);
int cargoIrcModInfoVerify(cargoIrcModInfo *pCargo);
int cargoIrcModInfoByteSwap(cargoIrcModInfo *pCargo);
int cargoIrcModInfoCopy(cargoIrcModInfo *pDst, cargoIrcModInfo *pSrc);
int cargoIrcModInfoPack(cargoIrcModInfo *pCargo, iircModule_t *pMod);

//! 
//  IIRC TRUCK AND CARGOS
//

int truckInit(truck *pTruck);
int truckVerify(truck *pTruck);
int truckDump(truck *pTruck);

int cargoIrcEventInit(cargoIrcEvent *pCargo);
int cargoIrcEventVerify(truck *pTruck);
int cargoIrcEventByteSwap(truck *pTruck);

int cargoIrcSysInit(cargoIrcSys *pCargo);
int cargoIrcSysVerify(truck *pTruck);
int cargoIrcSysByteSwap(truck *pTruck);

int cargoIrcServerInit(cargoIrcServer *pCargo);
int cargoIrcServerVerify(truck *pTruck);
int cargoIrcServerByteSwap(truck *pTruck);

int cargoIrcChanModeInit(cargoIrcChanMode *pCargo);
int cargoIrcChanModeVerify(truck *pTruck);
int cargoIrcChanModeByteSwap(truck *pTruck);
int cargoIrcChanModeActive(cargoIrcChanMode *pCargo, mUint8 *state, mUint32 nBuf);
int cargoIrcChanModeDiff(cargoIrcChanMode *pCargo, mUint8 *diff, mUint32 nBuf);

int cargoIrcChanTopicInit(cargoIrcChanTopic *pCargo);
int cargoIrcChanTopicVerify(truck *pTruck);
int cargoIrcChanTopicByteSwap(truck *pTruck);

int cargoIrcChanUsersInit(cargoIrcChanUsers *pCargo);
int cargoIrcChanUsersVerify(truck *pTruck);
int cargoIrcChanUsersByteSwap(truck *pTruck);

/*
int cargoIrcUsernameInit(cargoIrcUsername *pCargo);
int cargoIrcUsernameVerify(truck *pTruck);
int cargoIrcUsernameByteSwap(truck *pTruck);
*/

int cargoIrcNickInit(cargoIrcNick *pCargo);
int cargoIrcNickVerify(truck *pTruck);
int cargoIrcNickByteSwap(truck *pTruck);

int cargoIrcJoinInit(cargoIrcJoin *pCargo);
int cargoIrcJoinVerify(truck *pTruck);
int cargoIrcJoinByteSwap(truck *pTruck);

int cargoIrcPartInit(cargoIrcPart *pCargo);
int cargoIrcPartVerify(truck *pTruck);
int cargoIrcPartByteSwap(truck *pTruck);

int cargoIrcQuitInit(cargoIrcQuit *pCargo);
int cargoIrcQuitVerify(truck *pTruck);
int cargoIrcQuitByteSwap(truck *pTruck);

int cargoIrcWhoInit(cargoIrcWho *pCargo);
int cargoIrcWhoVerify(truck *pTruck);
int cargoIrcWhoByteSwap(truck *pTruck);

int cargoIrcUserModeInit(cargoIrcUserMode *pCargo);
int cargoIrcUserModeVerify(truck *pTruck);
int cargoIrcUserModeByteSwap(truck *pTruck);

int cargoIrcChanBanInit(cargoIrcChanBan *pCargo);
int cargoIrcChanBanVerify(truck *pTruck);
int cargoIrcChanBanByteSwap(truck *pTruck);

int cargoIrcMsgInit(cargoIrcMsg *pCargo);
int cargoIrcMsgVerify(truck *pTruck);
int cargoIrcMsgByteSwap(truck *pTruck);
int cargoIrcMsgChop(truck *pTruck);

int cargoIrcChanModArgInit(cargoIrcChanModArg *pCargo);
int cargoIrcChanModArgVerify(truck *pTruck);
int cargoIrcChanModArgByteSwap(truck *pTruck);

int cargoIrcUserModArgInit(cargoIrcUserModArg *pCargo);
int cargoIrcUserModArgVerify(truck *pTruck);
int cargoIrcUserModArgByteSwap(truck *pTruck);

#ifdef __cplusplus
}
#endif

#endif


