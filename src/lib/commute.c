
/*
 * Copyright (c) 2001-2002,  James D. Taylor.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * contact James D. Taylor:  james.d.taylor@gmail.com
 */

#include "iirc/commute.h"


#ifdef HAVE_METRIC_PIPE
static truck* pipeReceive(commute_t *pCom, int *pStatus);
static int pipeSend(commute_t *pCom, truck *pTruck);
#endif

static truck* socketReceive(commute_t *pCom, int *pStatus);
static int socketSend(commute_t *pCom, truck *pTruck);


static int genCommuteInit(generic_t *pGen);
static int genCommuteFree(generic_t *pGen);
static int genCommuteCopy(generic_t *pDst, generic_t *pSrc);
genericInfo_t commuteInfo = {
size: sizeof(commute_t),
      fnInit: genCommuteInit,
      fnAlloc: NULL,
      fnFree: genCommuteFree,
      fnCmp: NULL,
      fnCopy: genCommuteCopy
};

/*
 * @commute ops
 */

int commuteInit(commute_t *pCom)
{
    pCom->type = COMMUTE_LOCAL;
    pCom->flags = 0;
    pCom->pData = NULL;
    socketInit(&pCom->sock);
    truckQueueInit(&pCom->trafficIn);
    pCom->trafficOut = NULL;
    pCom->localParking = NULL;
    pCom->remoteParking = NULL;
    pCom->remotePID = 0;
    socketAddrInit(&pCom->localSockAddr);
    socketAddrInit(&pCom->remoteSockAddr);
    pCom->pTruckIn = NULL;
    pCom->nBytesIn = 0;
    pCom->dataReady = NULL;
#ifdef HAVE_METRIC_PIPES
    pipeInit(&pCom->pipe);
#endif
    return 1;
}

int commuteFree(commute_t *pCom)
{
    socketClose(&pCom->sock);
    truckQueueFree(&pCom->trafficIn);
    pCom->trafficOut = NULL;
    pCom->localParking = NULL;
    pCom->remoteParking = NULL;
    if(pCom->pTruckIn != NULL) {
        FREE(pCom->pTruckIn);
        pCom->pTruckIn = NULL;
    }
    pCom->nBytesIn = 0;
    pCom->dataReady = NULL;
#ifdef HAVE_METRIC_PIPES
    pipeDestroy(&pCom->pipe);
#endif
    return 1;
}

int commuteCopy(commute_t *pDst, commute_t *pSrc)
{
    pDst->type = pSrc->type;
    pDst->flags = pSrc->flags;
    pDst->pData = pSrc->pData;
    socketCopy(&pDst->sock, &pSrc->sock);
    /*
       pDst->trafficIn = pSrc->trafficIn;
       pDst->*trafficOut = pSrc->*trafficOut;
       pDst->*localParking = pSrc->*localParking;
       pDst->*remoteParking = pSrc->*remoteParking;
     */
    pDst->remotePID = pSrc->remotePID;
    socketAddrCopy(&pDst->localSockAddr, &pSrc->localSockAddr);
    socketAddrCopy(&pDst->remoteSockAddr, &pSrc->remoteSockAddr);
    //pDst->pTruckIn = pSrc->pTruckIn;
    pDst->nBytesIn = pSrc->nBytesIn;
    pDst->dataReady = pSrc->dataReady;
#ifdef HAVE_METRIC_PIPES
    pipeCopy(&pDst->pipe, &pSrc->pipe);
#endif
    return 1;
}

int commuteUsingSocket(commute_t *pCom, socket_t *pSock)
{
    pCom->type = COMMUTE_SOCKET;
    socketCopy(&pCom->sock, pSock);
    return 1;
}

#ifdef HAVE_METRIC_PIPES
int commuteUsingPipe(commute_t *pCom, pipe_t *pPipe)
{
    pCom->type = COMMUTE_PIPE;
    pipeCopy(&pCom->pipe, pPipe);
    return 1;
}


truck* pipeReceive(commute_t *pCom, int *pStatus)
{
    int i;
    mUint32 nBytes;
    char *pBytes;
    truck *pTruck;

    if(pCom->nBytesIn)
    {
        if(pCom->pTruckIn == NULL) {
            LOG_MSG(LOG_LOW,"pipeReceive - continuation, but pTruckIn == NULL");
            *pStatus = -1;
            return NULL;
        }
        pTruck = pCom->pTruckIn;
        //
        // we are continuing a read on previously attempt
        if(pCom->nBytesIn < sizeof(truck)) {
            //
            // didn't finish reading the truck's header. finish it.
            pBytes = (char*)pCom->pTruckIn + pCom->nBytesIn;
            i = pipeRead(&pCom->pipe, pBytes, sizeof(truck) - pCom->nBytesIn);
            if(i <= 0) {
                if(i == 0) {
                    *pStatus = 0; // no data ready
                    return pTruck;
                }
                //TODO close pipe, err/closed connection
                LOG_MSG(LOG_LOW,"pipeReceive - pipeRead failed");
                truckEnqueue(pCom->localParking, pTruck);
                *pStatus = -1;
                return pTruck; // kick client
            }
            nBytes = (mUint32)i;
            if(nBytes + pCom->nBytesIn < sizeof(truck)) {
                //
                // didn't finish reading truck header.
                pCom->nBytesIn += nBytes;
                *pStatus = 0; // no truck ready yet
                return pTruck;
            }
            pCom->nBytesIn = sizeof(truck);
        }
        // We should have a full truck header now. Read the size member
        // of the header to know how much else to read
        truckFix(pTruck); // convert to host byte order
        if(pTruck->size <= sizeof(truck) ||
                pTruck->size > pCom->localParking->size)
        {
            VLOG_MSG(LOG_LOW,"pipeReceive - invalid truck size: %d",
                    pTruck->size);
            truckEnqueue(pCom->localParking, pTruck);
            *pStatus = -1; // error
            return pTruck;
        }
        if(pCom->nBytesIn >= pTruck->size) {
            // since we have what we need, return it while ignoring
            // any extra data which I don't think can happen.
            pCom->pTruckIn = NULL;
            pCom->nBytesIn = 0;
            *pStatus = 1; // return full truck. soon-to-be fatal error
            return pTruck;
        }
        // attempt to read the rest of the truck. (non-blocking)
        pBytes = (char*)pCom->pTruckIn + pCom->nBytesIn;
        i = pipeRead(&pCom->pipe, pBytes, pTruck->size - pCom->nBytesIn);
        if(i <= 0) {
            if(i == 0) {
                *pStatus = 0;
                return pTruck;
            }
            truckEnqueue(pCom->localParking, pTruck);
            VLOG_MSG(LOG_LOW,"pipeReceive - pipeRead fail: %d", i);
            *pStatus = -1; // filthy error
            return pTruck;
        }
        nBytes = (mUint32)i;
        if(nBytes < pTruck->size - pCom->nBytesIn) {
            pCom->pTruckIn = pTruck; // for clarity
            pCom->nBytesIn += nBytes;
            *pStatus = 0; // no truck ready, amigo
            return pTruck;
        }
        // reset partial truck pointer, because we have a full truck.
        pCom->pTruckIn = NULL;
        pCom->nBytesIn = 0;
    } else {
        // unpark a truck for use as our 'partial truck' buffer.
        // since this truck isn't commuting, get it from localParking.
        pTruck = truckDequeue(pCom->localParking);
        if(pTruck == NULL) {
            if(truckQueueRefill(pCom->localParking) < 0) {
                LOG_MSG(LOG_LOW,"pipeReceive - truckQueueRefill failed");
                *pStatus = -1;
                return NULL;
            }
            pTruck = truckDequeue(pCom->localParking);
            if(pTruck == NULL) {
                LOG_MSG(LOG_LOW,"pipeReceive - dequeue local failed");
                *pStatus = -1;
                return NULL;
            }
        }
        // read the truck header first
        i = pipeRead(&pCom->pipe, pTruck, sizeof(truck));
        if(i <= 0) {
            // put the truck back where we found it.
            truckEnqueue(pCom->localParking, pTruck);
            pCom->pTruckIn = NULL;
            pCom->nBytesIn = 0;
            if(!i) {
                *pStatus = 0; // no data read
                return NULL;
            }
            LOG_MSG(LOG_LOW,"pipeReceive - pipeRead failed");
            *pStatus = -1;
            return NULL;
        }
        nBytes = (mUint32)i;
        pCom->pTruckIn = pTruck;
        pCom->nBytesIn = nBytes;
        if(nBytes < sizeof(truck)) {
            // didn't finish reading truck header.
            *pStatus = 0; // truck is still in the powder room.
            return pTruck;
        }
        // We should have a full truck header now. Read the size member
        // of the header to know how much else to read
        truckFix(pTruck); // convert to host byte order
        if(pTruck->size <= sizeof(truck) ||
                pTruck->size > pCom->localParking->size)
        {
            VLOG_MSG(LOG_LOW,"pipeReceive - invalid truck size: %d",
                    pTruck->size);
            truckEnqueue(pCom->localParking, pTruck);
            pCom->pTruckIn = NULL;
            pCom->nBytesIn = 0;
            *pStatus = -1;
            return NULL;
        }
        // read rest of truck to the location after the header
        pBytes = (char*)pCom->pTruckIn + pCom->nBytesIn;
        i = pipeRead(&pCom->pipe, pBytes, pTruck->size - pCom->nBytesIn);
        if(i <= 0) {
            if(i == 0) {
                *pStatus = 0; // truck's body is tardy
                return pTruck;
            }
            VLOG_MSG(LOG_LOW,"pipeReceive - pipeRead fail: %d", i);
            truckEnqueue(pCom->localParking, pTruck);
            pCom->pTruckIn = NULL;
            pCom->nBytesIn = 0;
            *pStatus = -1;
            return NULL;
        }
        nBytes = (mUint32)i;
        if(nBytes < pTruck->size - pCom->nBytesIn) {
            pCom->pTruckIn = pTruck; // for clarity
            pCom->nBytesIn += nBytes;
            *pStatus = 0; // truck is out fishing
            return pTruck;
        }
        // reset partial truck pointer, because we have a full truck.
        pCom->pTruckIn = NULL;
        pCom->nBytesIn = 0;
    }
    *pStatus = 1;
    return pTruck;
}

int pipeSend(commute_t *pCom, truck *pTruck)
{
    // TODO: pipeWrite might fail (block on non-blocking pipe)
    return pipeWrite(&pCom->pipe, pTruck, pTruck->size);
}

#endif /* ifdef HAVE_METRIC_PIPES */

truck* socketReceive(commute_t *pCom, int *pStatus)
{
    int i;
    mUint32 nBytes;
    char *pBytes;
    truck *pTruck;

    if(pCom->nBytesIn)
    {
        if(pCom->pTruckIn == NULL) {
            LOG_MSG(LOG_LOW,"socketReceive - continuation;  pTruckIn == NULL");
            *pStatus = -1;
            return NULL;
        }
        pTruck = pCom->pTruckIn;
        //
        // we are continuing a read on previously attempt
        if(pCom->nBytesIn < sizeof(truck))
        {
            // didn't finish reading the truck's header. finish it.
            pBytes = (char*)pCom->pTruckIn + pCom->nBytesIn;
            i = socketRead(&pCom->sock, pBytes, sizeof(truck) - pCom->nBytesIn);
            if(i <= 0) {
                if(i == 0) {
                    *pStatus = 0; // no data ready
                    return pTruck;
                }
                LOG_MSG(LOG_LOW,"socketReceive - socketRead failed");
                truckEnqueue(pCom->localParking, pTruck);
                *pStatus = -1;
                return pTruck; // kick client
            }
            nBytes = (mUint32)i;
            if(nBytes + pCom->nBytesIn < sizeof(truck)) {
                //
                // didn't finish reading truck header.
                pCom->nBytesIn += nBytes;
                *pStatus = 0; // no truck ready yet
                return pTruck;
            }
            pCom->nBytesIn = sizeof(truck);
        }
        // We should have a full truck header now. Read the size member
        // of the header to know how much else to read
        truckFix(pTruck); // convert to host byte order
        if(pTruck->size <= sizeof(truck) ||
                pTruck->size > pCom->localParking->size)
        {
            VLOG_MSG(LOG_LOW,"socketReceive - invalid truck size: %d",
                    pTruck->size);
            truckEnqueue(pCom->localParking, pTruck);
            *pStatus = -1; // error
            return pTruck;
        }
        if(pCom->nBytesIn >= pTruck->size) {
            // since we have what we need, return it while ignoring
            // any extra data which I don't think can happen.
            pCom->pTruckIn = NULL; // delete extra information!
            pCom->nBytesIn = 0;
            *pStatus = 1; // return full truck. soon-to-be fatal error
            return pTruck;
        }
        // attempt to read the rest of the truck. (non-blocking)
        pBytes = (char*)pCom->pTruckIn + pCom->nBytesIn;
        i = socketRead(&pCom->sock, pBytes, pTruck->size - pCom->nBytesIn);
        if(i <= 0) {
            if(i == 0) {
                *pStatus = 0;
                return pTruck;
            }
            truckEnqueue(pCom->localParking, pTruck);
            VLOG_MSG(LOG_LOW,"socketReceive - socketRead fail: %d", i);
            *pStatus = -1; // filthy error
            return pTruck;
        }
        nBytes = (mUint32)i;
        if(nBytes < pTruck->size - pCom->nBytesIn) {
            pCom->pTruckIn = pTruck; // for clarity
            pCom->nBytesIn += nBytes;
            *pStatus = 0; // no truck ready, amigo
            return pTruck;
        }
        // reset partial truck pointer, because we have a full truck.
        pCom->pTruckIn = NULL;
        pCom->nBytesIn = 0;
    }
    else
    {
        // unpark a truck for use as our 'partial truck' buffer.
        // since this truck isn't commuting, get it from localParking.
        pTruck = truckDequeue(pCom->localParking);
        if(pTruck == NULL) {
            if(truckQueueRefill(pCom->localParking) < 0) {
                LOG_MSG(LOG_LOW,"socketReceive - truckQueueRefill failed");
                *pStatus = -1;
                return NULL;
            }
            pTruck = truckDequeue(pCom->localParking);
            if(pTruck == NULL) {
                LOG_MSG(LOG_LOW,"socketReceive - dequeue local failed");
                *pStatus = -1;
                return NULL;
            }
        }
        // read the truck header first
        i = socketRead(&pCom->sock, pTruck, sizeof(truck));
        if(i <= 0) {
            truckEnqueue(pCom->localParking, pTruck);
            pCom->pTruckIn = NULL;
            pCom->nBytesIn = 0;
            if(!i) {
                *pStatus = 0; // no data read
                return NULL;
            }
            LOG_MSG(LOG_LOW,"socketReceive - socketRead failed");
            *pStatus = -1;
            return NULL;
        }
        nBytes = (mUint32)i;
        pCom->pTruckIn = pTruck;
        pCom->nBytesIn = nBytes;
        if(nBytes < sizeof(truck)) {
            // didn't finish reading truck header.
            *pStatus = 0; // truck is still in the powder room.
            return pTruck;
        }
        // We should have a full truck header now. Read the size member
        // of the header to know how much else to read
        truckFix(pTruck); // convert to host byte order
        if(pTruck->size <= sizeof(truck) ||
                pTruck->size > pCom->localParking->size)
        {
            VLOG_MSG(LOG_LOW,"socketReceive - invalid truck size: %d",
                    pTruck->size);
            truckEnqueue(pCom->localParking, pTruck);
            pCom->pTruckIn = NULL;
            pCom->nBytesIn = 0;
            *pStatus = -1;
            return NULL;
        }
        // read rest of truck to the location after the header
        pBytes = (char*)pCom->pTruckIn + pCom->nBytesIn;
        i = socketRead(&pCom->sock, pBytes, pTruck->size - pCom->nBytesIn);
        if(i <= 0) {
            if(i == 0) {
                *pStatus = 0; // truck's body is tardy
                return pTruck;
            }
            VLOG_MSG(LOG_LOW,"socketReceive - socketRead fail: %d", i);
            truckEnqueue(pCom->localParking, pTruck);
            pCom->pTruckIn = NULL;
            pCom->nBytesIn = 0;
            *pStatus = -1;
            return NULL;
        }
        nBytes = (mUint32)i;
        if(nBytes < pTruck->size - pCom->nBytesIn) {
            pCom->pTruckIn = pTruck; // for clarity
            pCom->nBytesIn += nBytes;
            *pStatus = 0; // truck is out fishing
            return pTruck;
        }
        // reset partial truck pointer, because we have a full truck.
        pCom->pTruckIn = NULL;
        pCom->nBytesIn = 0;
    }
    *pStatus = 1;
    return pTruck;
}

int socketSend(commute_t *pCom, truck *pTruck)
{
    // TODO: socketWrite might fail (block on non-blocking socket)
    return socketWrite(&pCom->sock, pTruck, pTruck->size);
}

truck* commuteReceive(commute_t *pCom, int *pStatus)
{
    truck *pTruck;
    switch(pCom->type) {
        case COMMUTE_LOCAL:
            pTruck = truckDequeue(&pCom->trafficIn);
            if(pTruck == NULL) {
                *pStatus = 0;
                return NULL;
            }
            *pStatus = 1;
            return pTruck;
        case COMMUTE_SOCKET:
            return socketReceive(pCom, pStatus);
#ifdef HAVE_METRIC_PIPES
        case COMMUTE_PIPE:
            return pipeReceive(pCom, pStatus);
#endif
    }
    VLOG_MSG(LOG_LOW,"commuteReceive - unknown commute type: %d", pCom->type);
    return NULL;
}


int commuteSend(commute_t *pCom, truck *pTruck)
{
    truck *pTempTruck;

    switch(pCom->type) {
        case COMMUTE_LOCAL:
            pTempTruck = truckDequeue(pCom->remoteParking);
            if(pTempTruck == NULL) {
                if(truckQueueRefill(pCom->remoteParking) < 0) {
                    LOG_MSG(LOG_LOW,"commuteSend - remote QueueRefill failed");
                    return -1;
                }
                pTempTruck = truckDequeue(pCom->remoteParking);
                if(pTempTruck == NULL) {
                    LOG_MSG(LOG_LOW,"commuteSend - remote dequeue failed");
                    return -1;
                }
            }
            memcpy(pTempTruck, pTruck, pTruck->size);
            if(truckEnqueue(pCom->trafficOut, pTempTruck) < 0) {
                LOG_MSG(LOG_LOW,"commuteSend - remote truckEnqueue failed");
                truckEnqueue(pCom->remoteParking, pTempTruck);
                return -1;
            }
            *pCom->dataReady = TRUE;
            return 1;
        case COMMUTE_SOCKET:
            return socketSend(pCom, pTruck);
#ifdef HAVE_METRIC_PIPES
        case COMMUTE_PIPE:
            return pipeSend(pCom, pTruck);
#endif
    }
    VLOG_MSG(LOG_LOW,"commuteSend - unknown commute type: %d", pCom->type);
    return -1;
}


int genCommuteInit(generic_t *pGen)
{
    return commuteInit((commute_t*)pGen);
}

int genCommuteFree(generic_t *pGen)
{
    return commuteFree((commute_t*)pGen);
}

int genCommuteCopy(generic_t *pDst, generic_t *pSrc)
{
    return commuteCopy((commute_t*)pDst, (commute_t*)pSrc);
}


