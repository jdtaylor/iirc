
/*
 * Copyright (c) 2001-2002,  James D. Taylor.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * contact James D. Taylor:  james.d.taylor@gmail.com
 */

#include "iirc/truck.h"
#include "iirc/cargo.h"


#define MAX_TRUCK_PTR 40

int truckInit(truck *pTruck)
{
    pTruck->flags = 0;
#ifdef CONFIG_BIGENDIAN
    TRUCK_FLAG_SET(pTruck->flags, TRUCK_BIGENDIAN);
#endif
    pTruck->year = TRUCK_YEAR;
    pTruck->type = 0;//CARGO_UNKNOWN;
    pTruck->size = 0;
    pTruck->srcChan = 0;
    pTruck->srcUser = 0;
    pTruck->dstChan = 0;
    pTruck->dstUser = 0;
    pTruck->module = 0;
    return 1;
}

int truckVerify(truck *pTruck)
{
    if(pTruck->year < TRUCK_YEAR) {
        VLOG_MSG(LOG_LOW,"truckVerify - version: got %d, needed %d",
                pTruck->year, TRUCK_YEAR);
        truckDump(pTruck);
        return 0;
    }
    if(pTruck->type >= MAX_IIRC_CARGO_TYPE) {
        VLOG_MSG(LOG_LOW,"truckVerify - invalid CargoType %d",pTruck->type);
        truckDump(pTruck);
        return 0;
    }
    return 1;
}

int truckFix(truck *pTruck)
{
    TRUCK_FLAG_UNSET(pTruck->flags, TRUCK_BYTESWAP);
    if(TRUCK_NEEDS_BYTESWAP(pTruck->flags)) {
        // pTruck->flags is byte swap immune
        pTruck->year = BYTESWAP32(pTruck->year);
        pTruck->type = BYTESWAP32(pTruck->type);
        pTruck->size = BYTESWAP32(pTruck->size);
        pTruck->srcChan = BYTESWAP32(pTruck->srcChan);
        pTruck->srcUser = BYTESWAP32(pTruck->srcUser);
        pTruck->dstChan = BYTESWAP32(pTruck->dstChan);
        pTruck->dstUser = BYTESWAP32(pTruck->dstUser);
        pTruck->module = BYTESWAP32(pTruck->module);
        TRUCK_FLAG_FLIP(pTruck->flags, TRUCK_BIGENDIAN);
        // warn upper layers that they should swap their layer's bytes
        TRUCK_FLAG_SET(pTruck->flags, TRUCK_BYTESWAP);
    }
    return 1;
}

int truckDump(truck *pTruck)
{
    if(pTruck->type >= MAX_IIRC_CARGO_TYPE) {
        VLOG_MSG(LOG_DUMP,"truck: type:%d year:%d src:[%d:%d] "
                "dst:[%d:%d] mod:%d",
                pTruck->type, pTruck->year,
                pTruck->srcChan, pTruck->srcUser,
                pTruck->dstChan, pTruck->dstUser, pTruck->module);
    } else {
        VLOG_MSG(LOG_DUMP,"truck: type:%s year:%d src:[%d:%d] "
                "dst:[%d:%d] mod:%d",
                iircCargoNames[pTruck->type], pTruck->year,
                pTruck->srcChan, pTruck->srcUser,
                pTruck->dstChan, pTruck->dstUser, pTruck->module);
    }
    return 1;
}

/*
 * @truckQueue
 */

int truckQueueInit(truckQueue *pQ)
{
    pQ->array = NULL;
    pQ->num = 0;
    pQ->max = 0;
    pQ->first = 0;
    pQ->last = 0;
    pQ->size = 0;
    return 1;
}

int truckQueueAlloc(truckQueue *pQ, mUint32 max, mUint32 num, mUint32 size)
{
    mUint32 i;
    truck *pTruck;

    if(pQ == NULL) {
        LOG_MSG(LOG_LOW,"truckQueueAlloc - queue is NULL damnit!");
        return -1;
    }
    pQ->array = (truck**)MALLOC(max * sizeof(truck*));
    if(pQ->array == NULL) {
        LOG_MSG(LOG_SYS,"truckQueueAlloc - MALLOC Failed");
        return -1;
    }
    pQ->num = 0;
    pQ->max = max;
    pQ->first = 0;
    pQ->last = 0;
    pQ->size = size;
    //
    // allocate initial trucks and park them in the queue
    i = 0;
    while(i < num)
    {
        pTruck = (truck*)MALLOC(size);
        if(pTruck == NULL) {
            LOG_MSG(LOG_SYS,"truckQueueAlloc - MALLOC Failed");
            return -1;
        }
        if(truckEnqueue(pQ, pTruck) < 0) {
            LOG_MSG(LOG_LOW,"truckQueueAlloc - truckEnqueue Failed");
            return -1;
        }
        i++;
    }
    pQ->num = num;
    return 1;
}

int truckQueueRefill(truckQueue *pQ)
{
    mUint32 i;
    mUint32 nTrucks;
    truck *pTruck;
    void *pVoid;

    if(pQ == NULL) {
        LOG_MSG(LOG_LOW,"truckQueueRefill - queue is NULL damnit!");
        return -1;
    }
    if(pQ->max == 0) {
        // I am tempted to call truckQueueAlloc, but the truckSize
        // field is probably not set.
        LOG_MSG(LOG_LOW,"truckQueueRefill - call truckQueueAlloc first.");
        return -1;
    }
    pVoid = REALLOC(pQ->array, pQ->max * 2 * sizeof(truck*));
    if(pVoid == NULL) {
        LOG_MSG(LOG_SYS,"truckQueueRefill - REALLOC failed");
        return -1;
    }
    pQ->array = (truck**)pVoid;
    //
    // fix any existing trucks by de-wrapping them from the array
    if(pQ->last < pQ->first) {
        i = 0;
        while(i < pQ->last) {
            pQ->array[pQ->max + i] = pQ->array[i];
            i++;
        }
        pQ->last = pQ->max + i;
    }
    nTrucks = pQ->max; // number of new trucks to allocate
    pQ->max *= 2;
    //
    // allocate new trucks and park them in the queue
    i = 0;
    while(i < nTrucks)
    {
        if(pQ->num >= MAX_TRUCK_PTR) {
            LOG_MSG(LOG_LOW,"truckQueueRefill - reached MAX_TRUCK_PTR");
            return 0;
        }
        pTruck = (truck*)MALLOC(pQ->size);
        if(pTruck == NULL) {
            LOG_MSG(LOG_SYS,"truckQueueRefill - MALLOC Failed");
            return -1;
        }
        pQ->array[pQ->last] = pTruck;
        pQ->last = (pQ->last + 1) % pQ->max;
        pQ->num++;
        i++;
    }
    return 1;
}

int truckQueueFree(truckQueue *pQ)
{
    mUint32 i;

    if(pQ == NULL) {
        LOG_MSG(LOG_LOW,"truckQueueFree - queue is NULL damnit!");
        return -1;
    }
    if(pQ->last < pQ->first) {
        i = 0;
        while(i < pQ->last)
            FREE(pQ->array[i++]);
        i = pQ->first;
        while(i < pQ->max)
            FREE(pQ->array[i++]);
    } else {
        i = pQ->first;
        while(i < pQ->last)
            FREE(pQ->array[i++]);
    }
    if(pQ->max) {
        FREE(pQ->array);
        pQ->array = NULL;
        pQ->max = 0;
    }
    pQ->num = 0;
    pQ->last = 0;
    pQ->first = 0;
    return 1;
}

int truckEnqueue(truckQueue *pQ, truck *pTruck)
{
    void *voidBuf;
    mUint32 newMax;
    mUint32 i;

    if(pQ == NULL) {
        LOG_MSG(LOG_LOW,"truckEnqeue - queue is NULL damnit!");
        return -1;
    }
    if(pQ->num >= MAX_TRUCK_PTR) {
        LOG_MSG(LOG_LOW,"truckEnqueue - reached MAX_TRUCK_PTR");
        return 0;
    }
    if(pQ->num >= pQ->max) {
        // allocate new truck* exponentially
        newMax = pQ->max * 2;
        VLOG_MSG(LOG_DUMP,"truckEnqueue - max: %d, newMax: %d.",
                pQ->max, newMax);
        voidBuf = REALLOC(pQ->array, newMax * sizeof(truck*));
        if(voidBuf == NULL) {
            LOG_MSG(LOG_SYS,"truckEnqueue - REALLOC failed");
            return -1;
        }
        pQ->array = (truck**)voidBuf;
        if(pQ->num && pQ->last < pQ->first) {
            //
            // re-insert queue elements that are 'wrapped'
            for(i = 0; i < pQ->last; i++) {
                pQ->array[i + pQ->max] = pQ->array[i];
            }
            pQ->last += pQ->max;
        }
        pQ->max = newMax;
    }
    pQ->array[pQ->last] = pTruck;
    pQ->last = (pQ->last + 1) % pQ->max;
    pQ->num++;
    return 1;
}

truck* truckDequeue(truckQueue *pQ)
{
    truck *pTruck;

    if(pQ == NULL) {
        LOG_MSG(LOG_LOW,"truckDequeue - queue is NULL damnit!");
        return NULL;
    }
    if(pQ->num == 0)
        return NULL;
    pTruck = pQ->array[pQ->first];
    pQ->first = (pQ->first + 1) % pQ->max;
    pQ->num--;
    return pTruck;
}

int truckQueueCheck(truckQueue *pQ)
{
    if(pQ == NULL) {
        LOG_MSG(LOG_LOW,"truckQueueCheck - queue is NULL damnit!");
        return -1;
    }
    return pQ->num;
}

int truckQueueDump(truckQueue *pQ)
{
    if(pQ == NULL) {
        LOG_MSG(LOG_LOW,"truckQueueDump - queue is NULL damnit!");
        return -1;
    }
    VLOG_MSG(LOG_DUMP,"truckQueueDump: [first: %d] [last: %d] [num: %d]",
            pQ->first, pQ->last, pQ->num);
    return 1;
}

