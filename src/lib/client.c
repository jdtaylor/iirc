
/*
 * Copyright (c) 2001-2002,  James D. Taylor.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * contact James D. Taylor:  james.d.taylor@gmail.com
 */

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>
#include "iirc/client.h"
#include "metric/set.h"
#include "metric/char.h"
#include "iirc/state.h"
#include "iirc/cargo.h"
#include "iirc/struct_client.h"
#include "iirc/struct_user.h"
#include "iirc/struct_chan.h"

#define iircClientSet_t   genericOctetTree
#define iircClientSet(op) genericOctetTree##op
//!
// houseMap is a set of client handles, indexed by house handles
#define houseMap_t   genericOctetTree
#define houseMap(op) genericOctetTree##op

#define INITIAL_CLIENT_TRUCK 5

#define IIRC_CCMD_LEN 9
static mUint32 g_iircClientCmd[MAX_IIRC_CCMDS][IIRC_CCMD_LEN] =
{
    {'N','O','P',0},
    {'P','R','I','V','M','S','G',0},
    {'S','E','R','V','E','R',0},
    {'N','I','C','K',0},
    {'Q','U','I','T',0},
    {'J','O','I','N',0},
    {'P','A','R','T',0},
    {'M','O','D','E',0},
    {'K','I','C','K',0},
    {'L','I','S','T',0},
    {'T','O','P','I','C',0},
    {'W','H','O',0},
    {'N','A','M','E','S',0},
};

iircClientSet_t g_clientSet; //! hClient -> iircClient_t
static int g_bInitialized = FALSE;

//!
// fill client structure with our cargo handler function pointers
static int iircClientFill(iircClient_t *pClient);

static int
iircClientTruckReceive(iircClient_t *pSelf, truck *pTruck, int *pStatus);
static int iircClientSocketCheck(iircClient_t *pSelf, long timeout);

//
// iirc module exports
static int iircEventDispatch(iircClient_t *pSelf, int chan, int user,
        int code, int data1);
static int iircEventBroadcast(iircClient_t *pSelf, int chan,
        int code, int data1);
static int clientInternalTruckHandler(iircClient_t *pSelf, truck *pTruck,
        int *pStatus);
//
// cargo handlers
static int OnCargoIrcModInfo(iircClient_t*, truck*, int*);
static int OnCargoIrcUser(iircClient_t*, truck*, int*);
static int OnCargoIrcChan(iircClient_t*, truck*, int*);

static int OnPostCargoIrcEvent(iircClient_t*, truck*, int*);
static int OnPostCargoIrcSys(iircClient_t*, truck*, int*);
static int OnPostCargoIrcServer(iircClient_t*, truck*, int*);
//static int OnPostCargoIrcReroute(iircClient_t*, truck*, int*);
static int OnPostCargoIrcChanMode(iircClient_t*, truck*, int*);
static int OnPostCargoIrcChanTopic(iircClient_t*, truck*, int*);
static int OnPostCargoIrcChanUsers(iircClient_t*, truck*, int*);
//static int OnPostCargoIrcUsername(iircClient_t*, truck*, int*);
static int OnPostCargoIrcNick(iircClient_t*, truck*, int*);
static int OnPostCargoIrcJoin(iircClient_t*, truck*, int*);
static int OnPostCargoIrcPart(iircClient_t*, truck*, int*);
static int OnPostCargoIrcQuit(iircClient_t*, truck*, int*);
static int OnPostCargoIrcUserMode(iircClient_t*, truck*, int*);
static int OnPostCargoIrcChanBan(iircClient_t*, truck*, int*);
static int OnPostCargoIrcMsg(iircClient_t*, truck*, int*);
static int OnPostCargoIrcChanModArg(iircClient_t*, truck*, int*);
static int OnPostCargoIrcUserModArg(iircClient_t*, truck*, int*);

static int iircCCmdSplit(mUint32 *cmd, mUint32 *params[MAX_IIRC_CCMD_PARAMS]);
static int OnCCmdNop(   iircClient_t *pSelf, iircUser_t *pUser, mUint32 *cmd);
static int OnCCmdPrivmsg(iircClient_t *pSelf, iircUser_t *pUser, mUint32 *cmd);
static int OnCCmdQuit(  iircClient_t *pSelf, iircUser_t *pUser, mUint32 *cmd);
static int OnCCmdServer(iircClient_t *pSelf, iircUser_t *pUser, mUint32 *cmd);
static int OnCCmdNick(  iircClient_t *pSelf, iircUser_t *pUser, mUint32 *cmd);
static int OnCCmdJoin(  iircClient_t *pSelf, iircUser_t *pUser, mUint32 *cmd);
static int OnCCmdPart(  iircClient_t *pSelf, iircUser_t *pUser, mUint32 *cmd);
static int OnCCmdMode(  iircClient_t *pSelf, iircUser_t *pUser, mUint32 *cmd);
static int OnCCmdKick(  iircClient_t *pSelf, iircUser_t *pUser, mUint32 *cmd);
static int OnCCmdList(  iircClient_t *pSelf, iircUser_t *pUser, mUint32 *cmd);
static int OnCCmdWho(   iircClient_t *pSelf, iircUser_t *pUser, mUint32 *cmd);
static int OnCCmdNames( iircClient_t *pSelf, iircUser_t *pUser, mUint32 *cmd);
static int OnCCmdTopic( iircClient_t *pSelf, iircUser_t *pUser, mUint32 *cmd);

int iircClientGlobalInit()
{
    iircCargoGlobalInit();
    iircClientSet(Init)(&g_clientSet, &iircClientInfo, 1, INCR_LINEAR);
    iircClientSet(Alloc)(&g_clientSet);
    g_bInitialized = TRUE;
    return 1;
}

int iircClientGlobalFree()
{
    g_bInitialized = FALSE;
    iircClientSet(Free)(&g_clientSet);
    return 1;
}

int iircClientFill(iircClient_t *pClient)
{
    unsigned int i;

    for(i = 0; i < MAX_IIRC_CARGO_TYPE; i++) {
        pClient->fnPreHandler[i] = NULL;
        pClient->fnPostHandler[i] = NULL;
    }
    pClient->fnPostHandler[CARGO_IIRC_EVENT] = OnPostCargoIrcEvent;
    pClient->fnPostHandler[CARGO_IIRC_SYS] = OnPostCargoIrcSys;
    pClient->fnPostHandler[CARGO_IIRC_SERVER] = OnPostCargoIrcServer;
    //    pClient->fnPostHandler[CARGO_IIRC_REROUTE] = OnPostCargoIrcReroute;
    pClient->fnPostHandler[CARGO_IIRC_CHAN_MODE] = OnPostCargoIrcChanMode;
    pClient->fnPostHandler[CARGO_IIRC_CHAN_TOPIC] = OnPostCargoIrcChanTopic;
    pClient->fnPostHandler[CARGO_IIRC_CHAN_USERS] = OnPostCargoIrcChanUsers;
    //pClient->fnPostHandler[CARGO_IIRC_USERNAME] = OnPostCargoIrcUsername;
    pClient->fnPostHandler[CARGO_IIRC_NICK] = OnPostCargoIrcNick;
    pClient->fnPostHandler[CARGO_IIRC_JOIN] = OnPostCargoIrcJoin;
    pClient->fnPostHandler[CARGO_IIRC_PART] = OnPostCargoIrcPart;
    pClient->fnPostHandler[CARGO_IIRC_QUIT] = OnPostCargoIrcQuit;
    pClient->fnPostHandler[CARGO_IIRC_USER_MODE] = OnPostCargoIrcUserMode;
    pClient->fnPostHandler[CARGO_IIRC_CHAN_BAN] = OnPostCargoIrcChanBan;
    pClient->fnPostHandler[CARGO_IIRC_MSG] = OnPostCargoIrcMsg;
    pClient->fnPostHandler[CARGO_IIRC_CHAN_MOD_ARG] = OnPostCargoIrcChanModArg;
    pClient->fnPostHandler[CARGO_IIRC_USER_MOD_ARG] = OnPostCargoIrcUserModArg;
    // default is to wait for certain initial trucks
    pClient->waitFlags = IIRC_WAIT_MODS;// | IIRC_WAIT_ADDR;
    for(i = 0; i < MAX_IIRC_CCMDS; i++) {
        pClient->fnCmd[i] = NULL;
    }
    pClient->fnCmd[IIRC_CCMD_NOP] = OnCCmdNop;
    pClient->fnCmd[IIRC_CCMD_PRIVMSG] = OnCCmdPrivmsg;
    pClient->fnCmd[IIRC_CCMD_SERVER] = OnCCmdServer;
    pClient->fnCmd[IIRC_CCMD_NICK] = OnCCmdNick;
    pClient->fnCmd[IIRC_CCMD_QUIT] = OnCCmdQuit;
    pClient->fnCmd[IIRC_CCMD_JOIN] = OnCCmdJoin;
    pClient->fnCmd[IIRC_CCMD_PART] = OnCCmdPart;
    pClient->fnCmd[IIRC_CCMD_MODE] = OnCCmdMode;
    pClient->fnCmd[IIRC_CCMD_KICK] = OnCCmdKick;
    pClient->fnCmd[IIRC_CCMD_LIST] = OnCCmdList;
    pClient->fnCmd[IIRC_CCMD_TOPIC] = OnCCmdTopic;
    pClient->fnCmd[IIRC_CCMD_WHO] = OnCCmdWho;
    pClient->fnCmd[IIRC_CCMD_NAMES] = OnCCmdNames;
    return 1;
}

int iircClientAdd(iircClientDescr *pDescr)
{
    int hClient;
    iircClient_t *pSelf;

    if(!g_bInitialized)
        iircClientGlobalInit();
    //
    // shared library version check
    if(pDescr->version < IIRC_CLIENT_DESCR_VERSION) {
        VLOG_MSG(LOG_LOW,"iircClientAdd - pDescr version too old: %d",
                pDescr->version);
        VLOG_MSG(LOG_LOW,"iircClientAdd - needed at least version: %d",
                IIRC_CLIENT_DESCR_VERSION);
        return -1;
    }
    //
    // insert a the new client into the global set
    hClient = iircClientSet(Add)(&g_clientSet, NULL);
    if(hClient == ELEMENT_NULL) {
        LOG_MSG(LOG_LOW,"iircClientAdd - failed to add new iircClient");
        return -1;
    }
    pSelf = (iircClient_t*)iircClientSet(Get)(&g_clientSet, hClient);
    if(pSelf == NULL) {
        VLOG_MSG(LOG_LOW,"iircClientAdd - failed retrieving new iircClient: %d",
                hClient);
        return -1;
    }
    pSelf->id = hClient;
    //
    // copy client description info to our client structure "self"
    strncpy(pSelf->hostname, pDescr->hostname, MAX_HOST_NAME);
    pSelf->hostname[MAX_HOST_NAME-1] = '\0';
    strncpy(pSelf->servername, pDescr->servername, MAX_HOST_NAME);
    pSelf->servername[MAX_HOST_NAME-1] = '\0';
    pSelf->serverPort = pDescr->serverPort;
    pSelf->fnPreClientHandler = pDescr->fnPreCargoHandler;
    pSelf->fnPostClientHandler = pDescr->fnPostCargoHandler;
    pSelf->pClientData = pDescr->pData;
    iircClientFill(pSelf); // fill client's function pointers
    return hClient;
}

int iircClientStart(int hClient, long timeout)
{
    int i;
    iircClient_t *pSelf;
    commute_t *pCom;

    pSelf = (iircClient_t*)iircClientSet(Get)(&g_clientSet, hClient);
    if(pSelf == NULL) {
        VLOG_MSG(LOG_LOW,"iircClientStart - unknown hClient: %d", hClient);
        return -1;
    }
    if(pSelf->bStarted) {
        //
        // iircClientStart already called for this client.  Previous
        // attempt probably timed out.
        // wait 'timeout' ms for reply from the parent
        i = iircClientExecute(pSelf->id, timeout);
        if(i < 0) {
            LOG_MSG(LOG_LOW,"iircClientStart - connection to parent failed");
            return IIRC_CLIENT_ERR_FATAL;
        }
        if(i == 0)
            return IIRC_CLIENT_ERR_TIMEOUT; // didn't receive any trucks
        if(pSelf->waitFlags & IIRC_WAIT_MODS)
            return IIRC_CLIENT_ERR_TIMEOUT; // no module trucks yet
        //
        // client is properly connected to it's parent now.  mamma mia!
        return 1;
    }
    pSelf->bStarted = TRUE; // started, but still waiting on modules...
    //
    // allocate mem for the IIRC state
    if(iircStateAlloc(&pSelf->state, sizeof(iircLargestTruck)) < 0) {
        LOG_MSG(LOG_LOW,"iircClientStart - iircStateAlloc failed");
        return IIRC_CLIENT_ERR_FATAL;
    }
    if(truckQueueAlloc(&pSelf->parking, INITIAL_CLIENT_TRUCK,
                INITIAL_CLIENT_TRUCK, pSelf->state.truckSize) < 0)
    {
        LOG_MSG(LOG_LOW,"iircClientStart - truckQueueAlloc Failed");
        return IIRC_CLIENT_ERR_FATAL;
    }
    pCom = &pSelf->parentCom;
    pCom->flags &= ~COMMUTE_PASSIVE; // active connection
    //
    // connect to iIRC server
    pCom->type = COMMUTE_SOCKET;
    if(socketAddrNameSet(&pCom->remoteSockAddr,
                pSelf->servername, pSelf->serverPort) <= 0)
    {
        VLOG_MSG(LOG_LOW,"iircClientStart - couldn't resolve server: %s : %d",
                pSelf->servername, (int)pSelf->serverPort);
        return IIRC_CLIENT_ERR_FATAL;
    }
    //  if we have already tried to open this socket,
    if(pCom->flags & COMMUTE_CONNECT_FAILED)
        return IIRC_CLIENT_ERR_FATAL;
    if(socketConnect(&pCom->sock, &pCom->remoteSockAddr) < 0)
    {
        LOG_MSG(LOG_LOW,"iircClientStart - socketConnect failed");
        pCom->flags |= COMMUTE_CONNECT_FAILED;
        return IIRC_CLIENT_ERR_FATAL;
    }
    // set socket to non-blocking
    socketOptionSet(&pCom->sock, SOCK_OPT_BLOCKING, 0);
    if(socketSetAdd(&pSelf->sockets, &pCom->sock) < 0) {
        LOG_MSG(LOG_LOW,"iircClientStart - socketSetAdd failed");
        socketClose(&pCom->sock);
        pCom->flags |= COMMUTE_CONNECT_FAILED;
        return IIRC_CLIENT_ERR_FATAL;
    }
    pCom->localParking = &pSelf->parking;
    //
    // wait 'timeout' ms. for reply from the parent
    i = iircClientExecute(pSelf->id, timeout);
    if(i < 0) {
        LOG_MSG(LOG_LOW,"iircClientStart - connection to parent failed");
        return IIRC_CLIENT_ERR_FATAL;
    }
    if(i == 0)
        return IIRC_CLIENT_ERR_TIMEOUT;
    return 1;
}

int iircClientDel(int hClient)
{
    iircClient_t *pSelf;

    pSelf = (iircClient_t*)iircClientSet(Get)(&g_clientSet, hClient);
    if(pSelf == NULL) {
        VLOG_MSG(LOG_LOW,"iircClientDel - unknown hClient: %d", hClient);
        return -1;
    }
    pSelf->bStarted = FALSE;
    // this will eventually call iircClientFree
    iircClientSet(Del)(&g_clientSet, pSelf->id);
    return 1;
}

int iircClientExecute(int hClient, long timeout)
{
    int status;
    unsigned int i, j;
    iircClient_t *pSelf;
    iircModule_t *pMod;
    truck *pTruck;

    i = 0;
    if(hClient < 0) // execute all clients
        pSelf = (iircClient_t*)iircClientSet(Incr)(&g_clientSet, &i);
    else
        pSelf = (iircClient_t*)iircClientSet(Get)(&g_clientSet, hClient);

    while(pSelf != NULL)
    {
        // listen on network until next scheduled interrupt.
        // ignore any errors.
        // TODO: split up 'timeout' between clients
        iircClientSocketCheck(pSelf, timeout);
        //
        // check for any trucks from this client's chan modules
        j = 0;
        pMod = iircStateModuleIncr(&pSelf->state, &j);
        while(pMod != NULL)
        {
            pTruck = iircModuleReceive(pMod, &status);
            if(pTruck != NULL) {
                // TODO: err, process module's truck..
                iircModuleRecycle(pMod, pTruck);
            }
            pMod = iircStateModuleIncr(&pSelf->state, &j);
        }
        if(hClient >= 0)
            break; // executing only one specific iircClient
        pSelf = (iircClient_t*)iircClientSet(Incr)(&g_clientSet, &i);
    }
    return 1;
}

int iircClientGetFD(int hClient)
{
    iircClient_t *pSelf;

    pSelf = (iircClient_t*)iircClientSet(Get)(&g_clientSet, hClient);
    if(pSelf == NULL) {
        VLOG_MSG(LOG_LOW,"iircClientGetFD - unknown hClient: %d", hClient);
        return -1;
    }
    return (int)pSelf->parentCom.sock;
}

int iircClientModuleAdd(int hClient, iircModule_t *pMod)
{
    iircClient_t *pSelf;
    pSelf = (iircClient_t*)iircClientSet(Get)(&g_clientSet, hClient);
    if(pSelf == NULL) {
        VLOG_MSG(LOG_LOW,"iircClientModuleAdd - unknown hClient: %d", hClient);
        return -1;
    }
    if(!g_bInitialized)
        iircClientGlobalInit();
    pMod->hParent = hClient; // so that modules can find there way home
    return iircStateModuleAdd(&pSelf->state, pMod);
}


int iircClientIntrAdd(int hClient, iircInterruptDescr *pDescr)
{
#ifdef HAVE_IIRC_INTR
    int flags;
    int hIntr;
    iircClient_t *pSelf;

    pSelf = (iircClient_t*)iircClientSet(Get)(&g_clientSet, hClient);
    if(pSelf == NULL) {
        VLOG_MSG(LOG_LOW,"iircClientIntrAdd - unknown hClient: %d", hClient);
        return -1;
    }
    if(!pSelf->bStarted) {
        VLOG_MSG(LOG_LOW,"iircClientIntrAdd - client %s hasnt been started yet",
                pSelf->hostname);
        return -1;
    }
    pDescr->version = IIRC_INTR_DESCR_VERSION;
    flags = fcntl(pDescr->fd, F_GETFL, 0);
    if(flags < 0) {
        VLOG_MSG(LOG_SYS,"iircClientIntrAdd - fcntl GETFL: %d", flags);
        return -1;
    }
    flags |= O_NONBLOCK;
    flags = fcntl(pDescr->fd, F_SETFL, flags);
    //    hIntr = houseInterruptAdd(pSelf->hHouse, pDescr);
    //    if(hIntr < 0) {
    //  LOG_MSG(LOG_LOW,"iircClientIntrAdd - console interrupt failed.");
    //  return -1;
    //    }
    return hIntr;
#else
    LOG_MSG(LOG_HIGH,"iircClientIntrAdd - iIRC interrupts not compiled");
    return -1;
#endif
}

int iircClientIntrDel(int hClient, unsigned int hIntr)
{
#ifdef HAVE_IIRC_INTR
    iircClient_t *pSelf;

    pSelf = (iircClient_t*)iircClientSet(Get)(&g_clientSet, hClient);
    if(pSelf == NULL) {
        VLOG_MSG(LOG_LOW,"iircClientIntrDel - unknown hClient: %d", hClient);
        return -1;
    }
    return 1; //TODO houseInterruptDel(pSelf->hHouse, hIntr);
#else
    LOG_MSG(LOG_HIGH,"iircClientIntrAdd - iIRC interrupts not compiled");
    return -1;
#endif
}

int iircClientCmd(int hClient, int hUser, char *cmd)
{
    int status;
    unsigned int i;
    unsigned int cmdLen;
    mUint32 cmdBuf[MAX_IIRC_CLIENT_CMD];
    mUint32 command[MAX_IIRC_CLIENT_CMD];
    mUint32 *curPos;
    iircUser_t *pUser;
    iircClient_t *pSelf;

    pSelf = (iircClient_t*)iircClientSet(Get)(&g_clientSet, hClient);
    if(pSelf == NULL)
        return -1;
    iircStateLock(&pSelf->state, FALSE);
    pUser = iircStateUserGet(&pSelf->state, hUser, &status);
    if(pUser == NULL) {
        iircStateUnlock(&pSelf->state, FALSE);
        return -1;
    }
    unicode_decode(cmdBuf, cmd, UTF8, MAX_IIRC_CLIENT_CMD);
    curPos = cmdBuf;
    if(unicode_tok(command, &curPos, ' ', IIRC_CCMD_LEN) == 0) {
        iircStateUnlock(&pSelf->state, FALSE);
        return -1; // hrm, no command.
    }
    cmdLen = unicode_len(command, UCS4);
    for(i = 0; i < cmdLen; i++) {
        if(command[i] < 256)
            command[i] = toupper((char)command[i]);
    }
    //
    // search for this command by it's name
    for(i = 1; i < MAX_IIRC_CCMDS; i++) {
        if(cmdLen != unicode_len(g_iircClientCmd[i], UCS4))
            continue;
        if(unicode_cmp(command, g_iircClientCmd[i], UCS4, IIRC_CCMD_LEN) == 0)
        {
            if(pSelf->fnCmd[i] != NULL) {
                status = pSelf->fnCmd[i](pSelf, pUser, cmdBuf);
                iircStateUnlock(&pSelf->state, FALSE);
                return status;
            }
        }
    }
    iircStateUnlock(&pSelf->state, FALSE);
    return -1; // no command dispatched
}

int iircClientUserAdd(int hClient, iircUserDescr *pDescr)
{
    int hUser;
    iircUser_t user;
    cargoIrcUser userCargo;
    iircClient_t *pSelf;

    pSelf = (iircClient_t*)iircClientSet(Get)(&g_clientSet, hClient);
    if(pSelf == NULL) {
        VLOG_MSG(LOG_LOW,"iircClientUserAdd - unknown hClient: %d", hClient);
        return -1;
    }
    //
    // TODO: check version of pDescr
    //
    iircUserInit(&user);
    unicode_cpy(user.nick, pDescr->nick, UTF8, MAX_IIRC_NICK);
    strncpy(user.identity, pDescr->identity, MAX_IIRC_IDENTITY);
    strncpy(user.hostname, pSelf->hostname, MAX_HOST_NAME);
    strncpy(user.servername, pSelf->servername, MAX_HOST_NAME);
    unicode_cpy(user.realname, pDescr->realname, UTF8, MAX_IIRC_REAL_NAME);
    user.identity[MAX_IIRC_IDENTITY-1] = '\0';
    user.hostname[MAX_HOST_NAME-1] = '\0';
    user.servername[MAX_HOST_NAME-1] = '\0';
    user.realname[MAX_IIRC_REAL_NAME-1] = '\0';
    //
    // add this new user to the client's set of local users
    hUser = clientUserSet(Add)(&pSelf->users, &user);
    if(hUser == ELEMENT_NULL) {
        LOG_MSG(LOG_LOW,"iircClientUserAdd - failed to add local user");
        return -1;
    }
    // send a connection event to the server
    cargoIrcUserPack(&userCargo, &user);
    userCargo.flags |= IIRC_USER_CONNECT;
    commuteSend(&pSelf->parentCom, (truck*)&userCargo);
    return 1;
}

int iircClientUserDel(int hClient, int hUser)
{
    int i;
    int status;
    cargoIrcUser user;
    iircUser_t *pUser;
    iircClient_t *pSelf;

    pSelf = (iircClient_t*)iircClientSet(Get)(&g_clientSet, hClient);
    if(pSelf == NULL) {
        VLOG_MSG(LOG_LOW,"iircClientUserDel - unknown hClient: %d", hClient);
        return -1;
    }
    pUser = iircStateUserGet(&pSelf->state, hUser, &status);
    if(pUser == NULL) {
        VLOG_MSG(LOG_LOW,"iircClientUserDel - unknown hUser: %d", hUser);
        return -1;
    }
    //
    // delete local user from client structure
    i = clientUserSet(Find)(&pSelf->users, pUser);
    if(i != ELEMENT_NULL) {
        if(clientUserSet(Del)(&pSelf->users, i) < 0)
            LOG_MSG(LOG_LOW,"iircClientUserDel - failed to del local user");
    }
    //
    // send user delete event to the server.  Actual deletion will
    // occur after the server rebound.
    cargoIrcUserInit(&user);
    user.id = pUser->id;
    user.flags |= IIRC_USER_DISCONNECT;
    commuteSend(&pSelf->parentCom, (truck*)&user);
    return 1;
}

int iircClientUserGet(int hClient, int hUser, cargoIrcUser *cargo)
{
    int status;
    iircUser_t *pUser;
    iircClient_t *pSelf;

    pSelf = (iircClient_t*)iircClientSet(Get)(&g_clientSet, hClient);
    if(pSelf == NULL) {
        VLOG_MSG(LOG_LOW,"iircClientUserGet - unknown hClient: %d", hClient);
        return -1;
    }
    pUser = iircStateUserGet(&pSelf->state, hUser, &status);
    if(pUser == NULL) {
        return -1;
    }
    cargoIrcUserPack(cargo, pUser);
    return 1;
}

int iircClientUserFind(int hClient, char *nick)
{
    int status;
    iircUser_t *pUser;
    iircClient_t *pSelf;

    pSelf = (iircClient_t*)iircClientSet(Get)(&g_clientSet, hClient);
    if(pSelf == NULL) {
        VLOG_MSG(LOG_LOW,"iircClientUserFind - unknown hClient: %d", hClient);
        return -1;
    }
    // TODO: needs an iircState lock doesn't it?
    pUser = iircStateUserFind(&pSelf->state, nick, &status);
    if(pUser == NULL)
        return -1;
    return pUser->id;
}

int iircClientUserIncr(int hClient, unsigned int *i, cargoIrcUser *cargo)
{
    iircClient_t *pSelf;
    iircUser_t *pUser;

    pSelf = (iircClient_t*)iircClientSet(Get)(&g_clientSet, hClient);
    if(pSelf == NULL) {
        VLOG_MSG(LOG_LOW,"iircClientUserIncr - unknown hClient: %d", hClient);
        return -1;
    }
    pUser = clientUserSet(Incr)(&pSelf->users, i);
    if(pUser == NULL)
        return 0;
    cargoIrcUserPack(cargo, pUser);
    return 1;
}

int iircClientChanGet(int hClient, int hChan, cargoIrcChan *cargo)
{
    int status;
    iircChan_t *pChan;
    iircClient_t *pSelf;

    pSelf = (iircClient_t*)iircClientSet(Get)(&g_clientSet, hClient);
    if(pSelf == NULL) {
        VLOG_MSG(LOG_LOW,"iircClientChanGet - unknown hClient: %d", hClient);
        return -1;
    }
    pChan = iircStateChanGet(&pSelf->state, hChan, &status);
    if(pChan == NULL) {
        return -1;
    }
    cargoIrcChanPack(cargo, pChan);
    return 1;
}

int iircClientChanFind(int hClient, char *chan)
{
    int status;
    iircClient_t *pSelf;
    iircChan_t *pChan;

    pSelf = (iircClient_t*)iircClientSet(Get)(&g_clientSet, hClient);
    if(pSelf == NULL) {
        VLOG_MSG(LOG_LOW,"iircClientChanFind - unknown hClient: %d", hClient);
        return -1;
    }
    pChan = iircStateChanFind(&pSelf->state, chan, &status);
    if(pChan == NULL)
        return -1;
    return pChan->id;
}

int iircClientChanTopic(int hClient, int hChan, cargoIrcChanTopic *cargo)
{
    int status;
    iircChan_t *pChan;
    iircClient_t *pSelf;

    pSelf = (iircClient_t*)iircClientSet(Get)(&g_clientSet, hClient);
    if(pSelf == NULL) {
        VLOG_MSG(LOG_LOW,"iircClientChanTopic - unknown hClient: %d", hClient);
        return -1;
    }
    pChan = iircStateChanGet(&pSelf->state, hChan, &status);
    if(pChan == NULL) {
        return -1;
    }
    cargoIrcChanTopicInit(cargo);
    unicode_cpy(cargo->topic, pChan->topic, UTF8, MAX_IIRC_CHAN_TOPIC);
    return 1;
}

int iircClientChanUser(int hClient, int hChan, int hUser)
{
    int status;
    void *pVoid;
    unsigned int i;
    iircChan_t *pChan;
    iircClient_t *pSelf;
    iircUser_t *pUser;

    pSelf = (iircClient_t*)iircClientSet(Get)(&g_clientSet, hClient);
    if(pSelf == NULL) {
        VLOG_MSG(LOG_LOW,"iircClientChanUser - unknown hClient: %d", hClient);
        return -1;
    }
    pUser = iircStateUserGet(&pSelf->state, hUser, &status);
    if(pUser == NULL) {
        VLOG_MSG(LOG_LOW,"iircClientChanUser - unknown hUser: %d", hUser);
        return -1;
    }
    i = 0;
    pVoid = hChanSet(Incr)(&pUser->chans, &i);
    while(pVoid != NULL) {
        if(*(int*)pVoid == hChan) {
            pVoid = hChanUserSet(Get)(&pUser->chanUsers, i-1);
            if(pVoid != NULL)
                return *(int*)pVoid;
        }
        pVoid = hChanSet(Incr)(&pUser->chans, &i);
    }
    return -1;
}

int iircClientSrcUser(int hClient, truck *pTruck, cargoIrcUser *cargo)
{
    int status;
    iircUser_t *pUser;
    iircClient_t *pSelf;

    pSelf = (iircClient_t*)iircClientSet(Get)(&g_clientSet, hClient);
    if(pSelf == NULL) {
        VLOG_MSG(LOG_LOW,"iircClientSrcUser - unknown hClient: %d", hClient);
        return -1;
    }
    pUser = iircStateSrcUserGet(&pSelf->state, pTruck, &status);
    if(pUser == NULL) {
        VLOG_MSG(LOG_LOW,"iircClientSrcUser - %s",
                iircStateError(&pSelf->state));
        return -1;
    }
    cargoIrcUserPack(cargo, pUser);
    return 1;
}

int iircClientDstChan(int hClient, truck *pTruck, cargoIrcChan *cargo)
{
    int status;
    iircChan_t *pChan;
    iircClient_t *pSelf;

    pSelf = (iircClient_t*)iircClientSet(Get)(&g_clientSet, hClient);
    if(pSelf == NULL) {
        VLOG_MSG(LOG_LOW,"iircClientSrcUser - unknown hClient: %d", hClient);
        return -1;
    }
    pChan = iircStateDstChanGet(&pSelf->state, pTruck, &status);
    if(pChan == NULL) {
        VLOG_MSG(LOG_LOW,"iircClientDstChan - %s",
                iircStateError(&pSelf->state));
        return -1;
    }
    cargoIrcChanPack(cargo, pChan);
    return 1;
}

int iircClientModGet(int hClient, int hMod, cargoIrcModInfo *pCargo)
{
    unsigned int i;
    //iircModule_t *pMod;
    iircClient_t *pSelf;

    pSelf = (iircClient_t*)iircClientSet(Get)(&g_clientSet, hClient);
    if(pSelf == NULL) {
        VLOG_MSG(LOG_LOW,"iircClientModGet - unknown hClient: %d", hClient);
        return -1;
    }
    for(i = 0; i < MAX_IIRC_MODS; i++) {
        if(pSelf->state.modules[i] == NULL)
            continue;
        if(pSelf->state.modules[i]->global_id == hMod)
            break;
    }
    if(i >= MAX_IIRC_MODS) {
        VLOG_MSG(LOG_LOW,"iircClientModGet - unknown hMod: %d", hMod);
        return -1;
    }
    cargoIrcModInfoCopy(pCargo, pSelf->state.modules[i]);
    return 1;
}

//!
// arg is expected to be of size IIRC_CHAN_ARG_LEN
int iircClientChanMode(int hClient, int hChan, mUint8 *arg)
{
    /* TODO
       mUint32 *iChar;  // indexing character pointer
       mUint32 argBuf[MAX_IIRC_MOD_NAME]; // sub-string buffer
       char argOut[MAX_IIRC_MOD_NAME];
       iircModule_t *pMod;
       cargoIrcModArg modCargo;
       iircClient_t *pSelf;
       pSelf = (iircClient_t*)iircClientSet(Get)(&g_clientSet, hClient);
       if(pSelf == NULL) {
       VLOG_MSG(LOG_LOW,"iircClientChanMode - unknown hClient: %d", hClient);
       return -1;
       }
       if(arg[0] == 0)
       return 1;
       cargoIrcModArgInit(&modCargo);
       modCargo.hChan = hChan;
       iChar = arg;
    //if(Strntok(argBuf, &iChar, ' ', MAX_IIRC_MOD_NAME)) {
    if(unicode_tok(argBuf, &iChar, ' ', MAX_IIRC_MOD_NAME)) {
    //
    // TODO: do mode change correctly eh.
    pMod = iircStateModuleFind(&pSelf->state, argBuf);
    if(pMod == NULL) {
    unicode_encode(argOut, argBuf, UTF8, MAX_IIRC_MOD_NAME);
    VLOG_MSG(LOG_USER,"Unknown module: %s", argOut);
    return -1;
    }
    modCargo.hMod = pMod->id;
    modCargo.function = IIRC_MODULE_FUNC_ENABLE;
    iircClientDispatch(pSelf->id, (truck*)&modCargo);
    return 1; // only 1 is supported so far
    }
     */
    return -1;
}

void *iircClientGet(int hClient)
{
    return iircClientSet(Get)(&g_clientSet, hClient);
}

    int
iircClientTruckHandler(iircClient_t *pSelf, truck *pTruck, int *pStatus)
{
    int i;
    iircClientCargo package;

    if(iircCargoVerify(pTruck) <= 0) {
        *pStatus = PROC_OUT_DISCONNECT; // close connection
        return -1;
    }
    // ok, cargo is clean.. real clean; like my conscience.
    //
    package.hClient = pSelf->id;
    package.pTruck = pTruck;
    package.pData = pSelf->pClientData;
    //
    // send to client pre-state cargo handler
    if(pSelf->fnPreClientHandler != NULL) {
        iircStateLock(&pSelf->state, FALSE);
        pSelf->fnPreClientHandler(&package, pStatus);
        iircStateUnlock(&pSelf->state, FALSE);
    }
    //
    // send to our own pre-state cargo handler
    if(pSelf->fnPreHandler[pTruck->type] != NULL) {
        iircStateLock(&pSelf->state, FALSE);
        pSelf->fnPreHandler[pTruck->type](pSelf, pTruck, pStatus);
        iircStateUnlock(&pSelf->state, FALSE);
    }
    //
    // send to the iircState
    iircStateLock(&pSelf->state, TRUE);
    i = iircStateCargoHandler(&pSelf->state, pTruck);
    if(i < 0) {
        *pStatus = 0;
        if(i <= IIRC_STATE_ERR_FATAL)
            *pStatus |= PROC_OUT_DISCONNECT; // close connection
        VLOG_MSG(LOG_LOW,"iircClientTruckHandler - %s",
                iircStateError(&pSelf->state));
        iircStateUnlock(&pSelf->state, TRUE);
        return -1;
    }
    iircStateUnlock(&pSelf->state, TRUE);
    //
    // send to our own post-state cargo handler
    if(pSelf->fnPostHandler[pTruck->type] != NULL) {
        iircStateLock(&pSelf->state, FALSE);
        pSelf->fnPostHandler[pTruck->type](pSelf, pTruck, pStatus);
        iircStateUnlock(&pSelf->state, FALSE);
    }
    //
    // send to the clients post-state cargo handler... bored yet?
    if(pSelf->fnPostClientHandler != NULL) {
        iircStateLock(&pSelf->state, FALSE);
        pSelf->fnPostClientHandler(&package, pStatus);
        iircStateUnlock(&pSelf->state, FALSE);
    }
    return 1; //TODO check pStatus, crap
}

int iircClientSocketCheck(iircClient_t *pSelf, long timeout)
{
    int i;
    int status;
    truck *pTruck;
    socketSet_t socketsReady;

    i = socketSetCheck(&pSelf->sockets, timeout, &socketsReady);
    if(i == 0) /* no data to read */
        return 0;
    if(i < 0) {
        // TODO: I think I should be deleting failed socket?
        VLOG_MSG(LOG_SYS,"iircClientSocketCheck - socketSetCheck failed %d", i);
        return -1;
    }
    if(socketSetReady(&socketsReady, &pSelf->parentCom.sock))
    {
        while(42) {
            //
            // get a truck from the parent
            pTruck = commuteReceive(&pSelf->parentCom, &status);
            if(pTruck == NULL) {
                if(status < 0)
                    exit(0); // TODO: clean shutdown
                break; // no more trucks
            }
            iircClientTruckReceive(pSelf, pTruck, &status);
            //
            // cleanup state information if necessary
            if(status & PROC_OUT_DISCONNECT) {
                LOG_MSG(LOG_USER,"disconnecting server");
                // TODO send disconnect notice upstream
                iircStateFree(&pSelf->state); //TODO wtf?
            }
            if(!(status & PROC_OUT_TRUCK_USED))
                truckEnqueue(&pSelf->parking, pTruck);
        }
    }
    return status;
}

    int
iircClientTruckReceive(iircClient_t *pSelf, truck *pTruck, int *pStatus)
{
    int status;
    unsigned int i;
    iircChan_t *pChan;
    iircModule_t *pMod;
    iircState_t *pState;
    truck *pTruckBuf;

    pState = &pSelf->state;
    truckFix(pTruck); // set truck header to host byte order
    if(!truckVerify(pTruck)) {
        LOG_MSG(LOG_USER,"iircClientTruckReceive - Invalid IIRC Truck Header.");
        *pStatus = PROC_OUT_DISCONNECT;
        return -1;
    }
    iircCargoFix(pTruck); // set any IIRC specific cargo to host byte order
    if(pTruck->type != CARGO_IIRC_MODULE)
        VLOG_MSG(LOG_DUMP,"-->C %s", iircCargoNames[pTruck->type]);

    if(pTruck->type < CARGO_IIRC_INTERNAL)
        return clientInternalTruckHandler(pSelf, pTruck, pStatus);
    //
    // verify the truck isn't spoofing it's source
    /* TODO: I don't think the client =can= verify
       status = iircStateTruckVerify(pState, pTruck);
       if(status <= 0) {
     *pStatus = 0;
     if(status < 0) {
     *pStatus |= PROC_OUT_DISCONNECT;
     return -1;
     }
    // truck is not saying it is who it really is.  Don't kick the
    // server, but certainly don't process this truck
    return 1;
    }
     */
    //
    // get the channel structure for this truck's destination
    pChan = iircStateChanGet(pState, pTruck->dstChan, &status);
    if(pChan == NULL)
    {
        //
        // well, the destination channel doesn't exist on this client.
        // But we might be able to continue if the truck is destined
        // to create the channel in question.
        if(pTruck->type != CARGO_IIRC_CHAN &&
                pTruck->type != CARGO_IIRC_JOIN)
        {
            VLOG_MSG(LOG_LOW,"iircClientTruckReceive - Invalid dst channel: %d",
                    pTruck->dstChan);
            truckDump(pTruck);
            *pStatus = PROC_OUT_DISCONNECT;
            return -1;
        }
        // truck could be creating a channel, so let it pass.  NONE SHALL PASS!
        if(iircClientTruckHandler(pSelf, pTruck, pStatus) < 0)
            return -1;
        if(*pStatus & PROC_OUT_DISCONNECT)
            return -1;
        if(*pStatus & PROC_OUT_TRUCK_USED) {
            // TODO: verify that this will work correctly :)
            pTruckBuf = (truck*)MALLOC(pSelf->state.truckSize);
            memcpy(pTruckBuf, pTruck, pTruck->size);
            pTruck = (truck*)pTruckBuf;
        }
        return 1;
    }
    //
    // figure out if this should go to a module or not
    if(pTruck->type != CARGO_IIRC_MODULE)
    {
        // not headed for a specific module, so that means it is an
        // IIRC cargo and needs to be given to ALL modules of this chan.
        i = 0;
        pMod = iircStateChanModIncr(pState, pChan, &i);
        while(pMod != NULL)
        {
            //
            // send the truck to the module.
            iircModuleSend(pMod, pTruck);
            //
            // next module enabled on the channel
            pMod = iircStateChanModIncr(pState, pChan, &i);
        }
        if(iircClientTruckHandler(pSelf, pTruck, pStatus) < 0)
            return -1;
        if(*pStatus & PROC_OUT_DISCONNECT)
            return -1;
        if(*pStatus & PROC_OUT_TRUCK_USED) {
            // TODO: verify that this will work correctly :)
            pTruckBuf = (truck*)MALLOC(pSelf->state.truckSize);
            memcpy(pTruckBuf, pTruck, pTruck->size);
            pTruck = (truck*)pTruckBuf;
        }
        return 1;
    }
    //
    // headed for one module
    pMod = iircStateModuleGet(pState, pTruck->module);
    if(pMod == NULL) {
        VLOG_MSG(LOG_USER,"iircClientTruckReceive - %s",
                iircStateError(pState));
        *pStatus = PROC_OUT_DISCONNECT;
        return -1;
    }
    //
    // if the truck has passed the test; made the grade; climbed the
    // ladder; shot the sheriff; and consumed rice pudding.. send it to
    // the appropriate module's truck handler; else the nearest pub.
    iircModuleSend(pMod, pTruck);
    *pStatus = 0;
    return 1;
}

int iircClientDispatch(int hClient, truck *pTruck)
{
    iircClient_t *pSelf;

    if(pTruck->type != CARGO_IIRC_MODULE)
        VLOG_MSG(LOG_DUMP,"<--C %s", iircCargoNames[pTruck->type]);
    pSelf = (iircClient_t*)iircClientSet(Get)(&g_clientSet, hClient);
    if(pSelf == NULL) {
        VLOG_MSG(LOG_LOW,"iircClientDispatch - unknown hClient: %d", hClient);
        return -1;
    }
    // hack the truck's size in the truck header.  What I want is to
    // shorten the last string of each structure. (for iircCargoMsg's)
    iircCargoChop(pTruck);

    return commuteSend(&pSelf->parentCom, pTruck);
}

int iircEventDispatch(iircClient_t *pSelf, int chan, int user,
        int code, int data1)
{
    cargoIrcEvent event;
    event.header.dstChan = chan;
    event.header.dstUser = user;
    event.code = code;
    event.data1 = data1;
    iircClientDispatch(pSelf->id, (truck*)&event);
    return 1;
}

int iircEventBroadcast(iircClient_t *pSelf, int chan, int code, int data1)
{
    cargoIrcEvent event;
    event.header.dstChan = chan;
    event.header.dstUser = IIRC_USER_BROADCAST;
    event.code = code;
    event.data1 = data1;
    iircClientDispatch(pSelf->id, (truck*)&event);
    return 1;
}

    static int
_clientInternalTruckHandler(iircClient_t *pSelf, truck *pTruck, int *pStatus)
{
    cargoIrcEvent *pEvent;

    switch(pTruck->type)
    {
        case CARGO_IIRC_USER:
            return OnCargoIrcUser(pSelf, pTruck, pStatus);
        case CARGO_IIRC_CHAN:
            return OnCargoIrcChan(pSelf, pTruck, pStatus);
        case CARGO_IIRC_MOD_INFO:
            return OnCargoIrcModInfo(pSelf, pTruck, pStatus);
        case CARGO_IIRC_EVENT:
            pEvent = (cargoIrcEvent*)pTruck;
            if(pEvent->code == CODE_IIRC_MODS_READY)
                pSelf->waitFlags &= ~IIRC_WAIT_MODS;
            break;
    }
    *pStatus = 0;
    return 1;
}

    int
clientInternalTruckHandler(iircClient_t *pSelf, truck *pTruck, int *pStatus)
{
    int i;
    iircStateLock(&pSelf->state, TRUE); // write lock
    i = _clientInternalTruckHandler(pSelf, pTruck, pStatus);
    iircStateUnlock(&pSelf->state, TRUE);
    return i;
}


int OnCargoIrcModInfo(iircClient_t *pSelf, truck *pTruck, int *pStatus)
{
    int i;
    cargoIrcModInfo *pCargo = (cargoIrcModInfo*)pTruck;
    //
    // send the new module to the iirc state to be stored
    i = iircStateCargoModInfo(&pSelf->state, pCargo);
    if(i < 0) {
        *pStatus = 0;
        if(i <= IIRC_STATE_ERR_FATAL)
            *pStatus |= PROC_OUT_DISCONNECT; // close connection
        VLOG_MSG(LOG_LOW,"OnCargoIrcModInfo - %s",
                iircStateError(&pSelf->state));
        return -1;
    }
    return 1;
}

int OnCargoIrcUser(iircClient_t *pSelf, truck *pTruck, int *pStatus)
{
    int i;
    unsigned int j;
    iircUser_t *pUser;
    cargoIrcUser *pCargo = (cargoIrcUser*)pTruck;

    LOG_MSG(LOG_USER,"OnCargoIrcUser - client got a new user");
    i = iircStateCargoUser(&pSelf->state, pCargo);
    if(i < 0) {
        *pStatus = 0;
        if(i <= IIRC_STATE_ERR_FATAL)
            *pStatus |= PROC_OUT_DISCONNECT; // close connection
        VLOG_MSG(LOG_LOW,"OnCargoIrcUser - %s",
                iircStateError(&pSelf->state));
        return -1;
    }
    // the client has a list of local users, but their global id's
    // need to be updated to whatever the server says.  Search for
    // the local user with the same nick as pCargo->nick and set it.
    j = 0;
    pUser = clientUserSet(Incr)(&pSelf->users, &j);
    while(pUser != NULL) {
        if(unicode_cmp(pCargo->nick, pUser->nick, UTF8, MAX_IIRC_NICK) == 0) {
            pUser->id = pCargo->id;
            VLOG_MSG(LOG_DUMP,"Found local user's global handle: %d",
                    pUser->id);
            break;
        }
        pUser = clientUserSet(Incr)(&pSelf->users, &j);
    }
    return 1;
}

int OnCargoIrcChan(iircClient_t *pSelf, truck *pTruck, int *pStatus)
{
    int i;
    cargoIrcChan *pCargo = (cargoIrcChan*)pTruck;

    LOG_MSG(LOG_USER,"<client> got a chan");

    i = iircStateCargoChan(&pSelf->state, pCargo);
    if(i < 0) {
        *pStatus = 0;
        if(i <= IIRC_STATE_ERR_FATAL)
            *pStatus |= PROC_OUT_DISCONNECT; // close connection
        VLOG_MSG(LOG_LOW,"OnCargoIrcChan - %s",
                iircStateError(&pSelf->state));
        return -1;
    }
    return 1;
}

    int
OnPostCargoIrcEvent(iircClient_t *pSelf, truck *pTruck, int *pStatus)
{
    cargoIrcEvent *pEvnt = (cargoIrcEvent*)pTruck;

    switch(pEvnt->code) {
        case CODE_IIRC_NOP:
            return 1; // do nothin
        default:
            *pStatus = PROC_OUT_DISCONNECT;
            return -1; // kick client
    }
    return 1;
}

    int
OnPostCargoIrcSys(iircClient_t *pSelf, truck *pTruck, int *pStatus)
{
    return 1;
}

    int
OnPostCargoIrcServer(iircClient_t *pSelf, truck *pTruck, int *pStatus)
{
    //    cargoIrcServer *pCargo = (cargoIrcServer*)pTruck;
    truckDump(pTruck);
    /*
       if(pCargo->flags) {
       if(pCargo->flags & IIRC_SERVER_CONNECT)
       return OnServerConnect(pTruck);
       if(pCargo->flags & IIRC_SERVER_DISCONNECT)
       return OnServerDisconnect(pTruck);
       return -1; // unknown flag set.
       } else {
       return -1; // dummy place holder
       }
     */
    return 1;
}

/*
   int
   OnPostCargoIrcReroute(iircClient_t *pSelf, truck *pTruck, int *pStatus)
   {
   cargoAddress addressCargo;
   cargoIrcUser user;
   houseDescr house;
   cargoIrcReroute *pCargo = (cargoIrcReroute*)pTruck;

   VLOG_MSG(LOG_USER,"Client got a reroute: %s:%d",
   pCargo->hostname, pCargo->port);
   if(interstateHouseGet(&house, pSelf->hHouse) < 0)
   return -1;
//
// disconnect previous house road
interstateHouseDel(pSelf->hHouse);
iircStateFree(&pSelf->state);
iircStateAlloc(&pSelf->state, sizeof(iircLargestTruck));
//
// fill out information for a new house
house.version = HOUSE_DESCR_VERSION;
house.truckSize = pSelf->state.truckSize;
house.fReceiveTruck = iircClientTruckReceive;
house.hParentCity = -1;
strncpy(house.parentName, pCargo->hostname, MAX_HOST_NAME);
house.parentName[MAX_HOST_NAME-1] = '\0';
house.parentPort = pCargo->port;
pSelf->hHouse = interstateHouseAdd(&house, 10000);
if(pSelf->hHouse < 0) {
LOG_MSG(LOG_LOW, "OnPostCargoIrcReroute - interstate rejected house.");
iircStateFree(&pSelf->state);
return -1;
}
if(houseAddressGet(pSelf->hHouse, &addressCargo) < 0) {
LOG_MSG(LOG_LOW, "OnPostCargoIrcReroute - couldnt get houses address.");
iircStateFree(&pSelf->state);
return -1;
}
//
// copy user description info to our user structure "self"
strncpy(pSelf->servername, house.parentName, MAX_HOST_NAME);
pSelf->servername[MAX_HOST_NAME-1] = '\0';
pSelf->serverPort = house.parentPort;
//
// send a connection event to the server
cargoIrcUserInit(&user);
//strncpy(user.name, pSelf->name, MAX_IIRC_USER_NAME);
//user.name[MAX_IIRC_USER_NAME-1] = '\0';
user.flags |= IIRC_USER_CONNECT;
user.houseAddr = addressCargo.houseAddr;
user.cityAddr = addressCargo.cityAddr;
commuteSend(&pSelf->parentCom, (truck*)&user);
return 1;
}
 */

    int
OnPostCargoIrcChanMode(iircClient_t *pSelf, truck *pTruck, int *pStatus)
{
    //    cargoIrcChanMode *pCargo = (cargoIrcChanMode*)pTruck;
    return 1;
}

    int
OnPostCargoIrcChanTopic(iircClient_t *pSelf, truck *pTruck, int *pStatus)
{
    //    cargoIrcChanTopic *pCargo = (cargoIrcChanTopic*)pTruck;
    return 1;
}

    int
OnPostCargoIrcChanUsers(iircClient_t *pSelf, truck *pTruck, int *pStatus)
{
    //    cargoIrcChanUsers *pCargo = (cargoIrcChanUsers*)pTruck;
    return 1;
}

/*
   int
   OnPostCargoIrcUsername(iircClient_t *pSelf, truck *pTruck, int *pStatus)
   {
   int status;
   iircUser_t *pUser;
   cargoIrcUsername *pCargo = (cargoIrcUsername*)pTruck;

   if(!cargoIrcUsernameVerify(pCargo)) {
 *pStatus = PROC_OUT_DISCONNECT; // close connection
 return -1;
 }
 pUser = iircStateSrcUserGet(&pSelf->state, pTruck, &status);
 if(pUser == NULL) {
 VLOG_MSG(LOG_LOW,"OnPostCargoIrcUsername - invalid source [%d:%d]",
 pTruck->srcChan, pTruck->srcUser);
 *pStatus = PROC_OUT_DISCONNECT;
 return -1;
 }
 strncpy(pUser->username, pCargo->username, MAX_IIRC_USERNAME);
 strncpy(pUser->hostname, pCargo->hostname, MAX_IIRC_HOST_NAME);
 strncpy(pUser->realname, pCargo->realname, MAX_IIRC_REAL_NAME);
 return 1;
 }
 */

    int
OnPostCargoIrcNick(iircClient_t *pSelf, truck *pTruck, int *pStatus)
{
    //    cargoIrcNick *pCargo = (cargoIrcNick*)pTruck;
    return 1;
}

    int
OnPostCargoIrcJoin(iircClient_t *pSelf, truck *pTruck, int *pStatus)
{
    //    cargoIrcJoin *pCargo = (cargoIrcJoin *)pTruck;
    return 1;
}

    int
OnPostCargoIrcPart(iircClient_t *pSelf, truck *pTruck, int *pStatus)
{
    //    cargoIrcPart *pCargo = (cargoIrcPart *)pTruck;
    return 1;
}

    int
OnPostCargoIrcQuit(iircClient_t *pSelf, truck *pTruck, int *pStatus)
{
    //    cargoIrcQuit *pCargo = (cargoIrcQuit *)pTruck;
    return 1;
}

    int
OnPostCargoIrcUserMode(iircClient_t *pSelf, truck *pTruck, int *pStatus)
{
    return 1;
}

    int
OnPostCargoIrcChanModArg(iircClient_t *pSelf, truck *pTruck,int *pStatus)
{
    //    cargoIrcChanModArg *pCargo = (cargoIrcChanModArg*)pTruck;
    return 1;
}

    int
OnPostCargoIrcUserModArg(iircClient_t *pSelf, truck *pTruck,int *pStatus)
{
    //    cargoIrcUserModArg *pCargo = (cargoIrcUserModArg*)pTruck;
    return 1;
}

    int
OnPostCargoIrcChanBan(iircClient_t *pSelf, truck *pTruck, int *pStatus)
{
    //    cargoIrcChanBan *pCargo = (cargoIrcChanBan*)pTruck;
    return 1;
}

    int
OnPostCargoIrcMsg(iircClient_t *pSelf, truck *pTruck, int *pStatus)
{
    cargoIrcMsg *pMsg = (cargoIrcMsg*)pTruck;
    VLOG_MSG(LOG_USER, "%s", pMsg->msg);
    //pTruck->dstUser = IIRC_USER_BROADCAST;
    //iircClientDispatch(pSelf->id, pTruck);
    return 1;
}

int iircCCmdSplit(mUint32 *cmd, mUint32 *params[MAX_IIRC_CCMD_PARAMS])
{
    unsigned int i;
    unsigned int cmdLen;
    mUint32 *curParam = cmd;
    mUint32 param[MAX_IIRC_CLIENT_CMD];

    for(i = 0; i < MAX_IIRC_CCMD_PARAMS; i++)
        params[i] = NULL;
    cmdLen = unicode_len(cmd, UCS4);
    if(!cmdLen)
        return IIRC_CCMD_NOP;  // no more messages
    i = 0;
    while(i < cmdLen && curParam[i] != '\n')
        i++;
    if(i < cmdLen) {
        // strip carriage-return and newline characters
        if(i > 0 && curParam[i-1] == '\r')
            curParam[i-1] = 0; // kill the carriage-return character
        else
            curParam[i] = 0;
    }
    params[0] = curParam;
    //
    // split the parameters
    i = 1;
    while(i < MAX_IIRC_CCMD_PARAMS &&
            unicode_tok(param, &curParam, ' ', MAX_IIRC_CLIENT_CMD))
    {
        // insert NIL's in between parameters
        cmdLen = unicode_len(param, UCS4);
        params[i-1][cmdLen] = 0;
        // set parameter location
        params[i] = curParam;
        i++;
    }
    return i;
}

int OnCCmdNop(iircClient_t *pSelf, iircUser_t *pUser, mUint32 *cmd)
{
    LOG_MSG(LOG_DUMP,"OnCCmdNop - entered");
    return 1;
}

int OnCCmdPrivmsg(iircClient_t *pSelf, iircUser_t *pUser, mUint32 *cmd)
{
    int i;
    int status;
    iircChan_t *pChan;
    cargoIrcMsg cargo;
    mUint32 *curParam = cmd;
    mUint32 param[MAX_IIRC_CLIENT_CMD];
    mUint8 chan[MAX_IIRC_CHAN_NAME];

    if(unicode_tok(param, &curParam, ' ', MAX_IIRC_CLIENT_CMD) == 0)
        return -1;
    if(unicode_tok(param, &curParam, ' ', MAX_IIRC_CLIENT_CMD) == 0)
        return -1;
    unicode_encode(chan, param, UTF8, MAX_IIRC_CHAN_NAME);
    cargoIrcMsgInit(&cargo);
    unicode_encode(cargo.msg, curParam, UTF8, MAX_IIRC_MSG);
    if((pChan = iircStateChanFind (&pSelf->state, chan, &status)) == NULL) {
        VLOG_MSG(LOG_LOW,"OnCCmdPrivmsg - \"%s\": %s", cargo.msg,
                iircStateError(&pSelf->state));
        return -1;
    }
    //
    // set header fields' destination to the channel: pChan
    i = iircStateDstChanSet(&pSelf->state, pUser, pChan, (truck*)&cargo);
    if(i < 0) {
        VLOG_MSG(LOG_LOW,"OnCCmdPrivmsg - \"%s\": %s", cargo.msg,
                iircStateError(&pSelf->state));
        return -1;
    }
    //
    // send the cargo
    iircClientDispatch(pSelf->id, (truck*)&cargo);
    return 1;
}

int OnCCmdServer(iircClient_t *pSelf, iircUser_t *pUser, mUint32 *cmd)
{
    LOG_MSG(LOG_DUMP,"OnCCmdServer - entered");
    return 1;
}

int OnCCmdQuit(iircClient_t *pSelf, iircUser_t *pUser, mUint32 *cmd)
{
    mUint32 *params[MAX_IIRC_CCMD_PARAMS];
    cargoIrcQuit cargo;
    mUint8 defaultReason[] = "Interstate IRC";

    if(iircCCmdSplit(cmd, params) <= 0)
        return -1;
    cargoIrcQuitInit(&cargo);
    if(params[1] == NULL)
        unicode_cpy(cargo.reason, defaultReason, UTF8, MAX_IIRC_QUIT_REASON);
    else
        unicode_encode(cargo.reason, params[1], UTF8, MAX_IIRC_QUIT_REASON);
    cargo.header.srcChan = 0;
    cargo.header.srcUser = pUser->id;
    iircClientDispatch(pSelf->id, (truck*)&cargo);
    return 1;
}

int OnCCmdJoin(iircClient_t *pSelf, iircUser_t *pUser, mUint32 *cmd)
{
    mUint32 *params[MAX_IIRC_CCMD_PARAMS];
    cargoIrcJoin join;

    if(iircCCmdSplit(cmd, params) <= 0)
        return -1;
    if(params[1] == NULL) {
        // a channel name is required
        return -1;
    }
    cargoIrcJoinInit(&join);
    join.flags |= IIRC_JOIN_BY_NAME;
    unicode_encode(join.chan, params[1], UTF8, MAX_IIRC_CHAN_NAME);
    if(params[2] != NULL)
        unicode_encode(join.key, params[2], UTF8, MAX_IIRC_CHAN_KEY);
    join.header.srcChan = 0;
    join.header.srcUser = pUser->id;
    iircClientDispatch(pSelf->id, (truck*)&join);
    return 1;
}

int OnCCmdPart(iircClient_t *pSelf, iircUser_t *pUser, mUint32 *cmd)
{
    int i;
    int status;
    mUint32 *params[MAX_IIRC_CCMD_PARAMS];
    mUint8 chan[MAX_IIRC_CHAN_NAME];
    iircChan_t *pChan;
    cargoIrcPart cargo;
    mUint8 defaultReason[] = "Interstate IRC";

    if(iircCCmdSplit(cmd, params) <= 0)
        return -1;
    if(params[1] == NULL) {
        // a channel name is required
        return -1;
    }
    unicode_encode(chan, params[1], UTF8, MAX_IIRC_CHAN_NAME);
    if((pChan = iircStateChanFind (&pSelf->state, chan, &status)) == NULL)
        return -1;
    cargoIrcPartInit(&cargo);
    if(params[2] == NULL)
        unicode_cpy(cargo.reason, defaultReason, UTF8, MAX_IIRC_PART_REASON);
    else
        unicode_encode(cargo.reason, params[2], UTF8, MAX_IIRC_PART_REASON);
    //
    // set header fields' destination to the channel: pChan
    i = iircStateDstChanSet(&pSelf->state, pUser, pChan, (truck*)&cargo);
    if(i < 0) {
        VLOG_MSG(LOG_LOW,"OnCCmdPart - %s", iircStateError(&pSelf->state));
        return -1;
    }
    cargo.hChan = pChan->id;
    //
    // send the cargo
    iircClientDispatch(pSelf->id, (truck*)&cargo);
    return 1;
}

int OnCCmdNick(iircClient_t *pSelf, iircUser_t *pUser, mUint32 *cmd)
{
    mUint32 *params[MAX_IIRC_CCMD_PARAMS];
    cargoIrcNick cargo;

    if(iircCCmdSplit(cmd, params) <= 0)
        return -1;
    if(params[1] == NULL) {
        // should probably send a nick cargo back, TODO
        return -1;
    }
    cargoIrcNickInit (&cargo);
    unicode_encode(cargo.nick, params[1], UTF8, MAX_IIRC_NICK);
    /*  cargo.mode = ? TODO */
    cargo.header.srcChan = 0;
    cargo.header.srcUser = pUser->id;

    iircClientDispatch(pSelf->id, (truck*)&cargo);
    return 1;
}

int OnCCmdMode(iircClient_t *pSelf, iircUser_t *pSrcUser, mUint32 *cmd)
{
    int i, j;
    int status;
    int paramPos;
    int bExpectLimit, bExpectNick, bExpectBan, bExpectKey;
    int paramLen;
    void *pVoid;
    mUint32 mode, modeSwitch;
    mUint32 umodeXOR, umodeState;
    mUint32 modArg[MAX_IIRC_MOD_ARG];
    mUint32 *iParam;
    mUint8 iircChan[MAX_IIRC_CHAN_NAME];
    mUint8 iircNick[MAX_IIRC_NICK];
    mUint8 iircModName[MAX_IIRC_MOD_NAME];
    iircChan_t *pChan;
    iircUser_t *pUser;
    cargoIrcChanMode chanMode;
    cargoIrcChanModArg modCargo;
    cargoIrcUserMode userMode;
    cargoIrcModInfo *pMod;
    mUint32 *params[MAX_IIRC_CCMD_PARAMS];

    VLOG_MSG(LOG_DUMP,"OnCCmdMode - Got MODE from client: %d",
            pSrcUser->id);

    if(iircCCmdSplit(cmd, params) <= 0)
        return -1;
    if(params[1] == NULL) {
        LOG_MSG(LOG_LOW,"OnCCmdMode - no destination specified");
        return -1;
    }
    iParam = params[1];
    if(iParam[0] != '#' && iParam[0] != '&') {
        //
        // no channel found as the first parameter, so maybe it is a
        // nick eh?
        unicode_encode(iircNick, iParam, UTF8, MAX_IIRC_NICK);
        pUser = iircStateUserFind(&pSelf->state, iircNick, &status);
        if(pUser == NULL) {
            VLOG_MSG(LOG_LOW,"OnCCmdMode - unknown nick: %s", iircNick);
            return -1;
        }
        /*
           iParam = params[1];
           if(iParam[0] == '\0') {
        // query for the mode
        snprintf(msgOut, RFC1459_MSG_MAX, ":%s %d %s +", pSelf->hostname,
        RPL_UMODEIS, pSrcUser->nick);
        if(pUser->modes & IIRC_USER_INVISIBLE)
        Strncat(msgOut, "i", RFC1459_MSG_MAX);
        if(pUser->modes & IIRC_USER_SERVER_NOTICE)
        Strncat(msgOut, "n", RFC1459_MSG_MAX);
        if(pUser->modes & IIRC_USER_WALLOPS)
        Strncat(msgOut, "w", RFC1459_MSG_MAX);
        if(pUser->modes & IIRC_USER_SYSOP)
        Strncat(msgOut, "s", RFC1459_MSG_MAX);
        Strncat(msgOut, "\r\n", RFC1459_MSG_MAX);
        msgOut[RFC1459_MSG_MAX-1] = '\0';
        socketWrite(&pClient->sock, msgOut, strlen(msgOut));
        }
         */
        return 1;
    }
    // mode is for a channel
    unicode_encode(iircChan, iParam, UTF8, MAX_IIRC_CHAN_NAME);
    pChan = iircStateChanFind(&pSelf->state, iircChan, &status);
    if(pChan == NULL) {
        VLOG_MSG(LOG_LOW,"OnCCmdMode - unknown channel: %s", iParam);
        return -1;
    }
    if(params[2] == NULL || params[2][0] == '\0') {
        // query for the mode
        /*
           snprintf(msgOut, RFC1459_MSG_MAX, ":%s %d %s %s +", pSelf->hostname,
           RPL_CHANNELMODEIS, pSrcUser->nick, pChan->name);
           if(pChan->modes & IIRC_CHAN_PRIVATE)
           Strncat(msgOut, "p", RFC1459_MSG_MAX);
           if(pChan->modes & IIRC_CHAN_SECRET)
           Strncat(msgOut, "s", RFC1459_MSG_MAX);
           if(pChan->modes & IIRC_CHAN_INVITE)
           Strncat(msgOut, "i", RFC1459_MSG_MAX);
           if(pChan->modes & IIRC_CHAN_EXTERN_MSG)
           Strncat(msgOut, "n", RFC1459_MSG_MAX);
           if(pChan->modes & IIRC_CHAN_TOPIC_PROT)
           Strncat(msgOut, "t", RFC1459_MSG_MAX);
           if(pChan->modes & IIRC_CHAN_USER_LIMIT)
           Strncat(msgOut, "l", RFC1459_MSG_MAX);
           if(pChan->modes & IIRC_CHAN_KEY)
           Strncat(msgOut, "k", RFC1459_MSG_MAX);
           if(pChan->modes & IIRC_CHAN_USER_LIMIT) {
           snprintf(msgBuf, RFC1459_MSG_MAX, " %d", pChan->userLimit);
           msgBuf[RFC1459_MSG_MAX-1] = '\0';
           Strncat(msgOut, msgBuf, RFC1459_MSG_MAX);
           }
           if(pChan->modes & IIRC_CHAN_KEY) {
           Strncat(msgOut, " ", RFC1459_MSG_MAX);
           Strncat(msgOut, pChan->key, RFC1459_MSG_MAX);
           }
           Strncat(msgOut, "\r\n", RFC1459_MSG_MAX);
           msgOut[RFC1459_MSG_MAX-1] = '\0';
           socketWrite(&pClient->sock, msgOut, strlen(msgOut));
           snprintf(msgOut, RFC1459_MSG_MAX, ":%s %d %s %d\r\n", pSelf->hostname,
           329, chan, 1021463254);  // who knows
           socketWrite(&pClient->sock, msgOut, strlen(msgOut));
           snprintf(msgOut, RFC1459_MSG_MAX,
           ":%s %d %s %s :End of Channel Ban List\r\n", pSelf->hostname,
           RPL_ENDOFBANLIST, pSrcUser->nick, chan);
           socketWrite(&pClient->sock, msgOut, strlen(msgOut));
         */
        return 1;
    }
    // fill the cargo with the current channel info
    cargoIrcChanModeInit(&chanMode);
    cargoIrcUserModeInit(&userMode);
    userMode.hChan = pChan->id;
    bExpectLimit = bExpectNick = bExpectBan = bExpectKey = FALSE;
    mode = umodeState = umodeXOR = 0;
    for(paramPos = 2; paramPos < MAX_IIRC_CCMD_PARAMS; paramPos++)
    {
        // string which increments to each parameter
        if(params[paramPos] == NULL || params[paramPos][0] == 0)
            break; // no more parameters
        iParam = params[paramPos];
        paramLen = unicode_len(iParam, UCS4);
        i = 0;
        if(paramPos == 2) {
            //
            // first mode parameter, so expect a '+' or '-'
            while(i < paramLen && iParam[i] != '+' && iParam[i] != '-')
                i++;
            if(i >= paramLen-1) {// room for +|- and one mode char
                paramPos++;
                continue;
            }
        }
        if(bExpectLimit && iParam[0] < 128 && isdigit((char)iParam[0])) {
            //
            // looks like a user limit to me, so use it
            // * I'm calling sscanf with a UTF8 stream here.  I'm
            // hoping that it will ignore any high-value bytes.
            // I use iircModName here for temp storage.
            unicode_encode(iircModName, iParam, UTF8, MAX_IIRC_MOD_NAME);
            sscanf((char*)iircModName, "%u", &chanMode.userLimit);
            bExpectLimit = FALSE;
            paramPos++;
            continue;
        }
        if(bExpectNick) {
            unicode_encode(iircNick, iParam, UTF8, MAX_IIRC_NICK);
            pUser = iircStateUserFind(&pSelf->state, iircNick, &status);
            if(pUser != NULL) {
                j = hChanSet(Find)(&pUser->chans, &pChan->id);
                if(j < 0)
                    continue;
                pVoid = hChanUserSet(Get)(&pUser->chanUsers, (unsigned int)j);
                if(pVoid == NULL)
                    continue;
                userMode.hChanUser[userMode.nUser] = *(unsigned int*)pVoid;
                userMode.modeXOR[userMode.nUser] = umodeXOR;
                userMode.modeState[userMode.nUser] = umodeState;
                userMode.nUser++;
                if(userMode.nUser >= MAX_IIRC_USER_MODE-1)
                    break;
                continue;
            }
        }
        if(bExpectBan) {
        }
        if(bExpectKey) {
            //
            // we are expecting a channel key, so this must be it
            unicode_encode(chanMode.key, iParam, UTF8, MAX_IIRC_CHAN_KEY);
            bExpectKey = FALSE;
            paramPos++;
            continue;
        }
        // magic flag which means that no switch has been specified yet
        modeSwitch = 0x0FFFFFFF;
        while(i < paramLen)
        {
            switch(iParam[i])
            {
                case '+':
                    modeSwitch = 0xFFFFFFFF;
                    break;
                case '-':
                    modeSwitch = 0x00000000;
                    break;
                case 'p':
                    mode |= (IIRC_CHAN_PRIVATE & modeSwitch);
                    chanMode.modeXOR |= IIRC_CHAN_PRIVATE;
                    break;
                case 's':
                    mode |= (IIRC_CHAN_SECRET & modeSwitch);
                    chanMode.modeXOR |= IIRC_CHAN_SECRET;
                    break;
                case 'i':
                    mode |= (IIRC_CHAN_INVITE & modeSwitch);
                    chanMode.modeXOR |= IIRC_CHAN_INVITE;
                    break;
                case 't':
                    mode |= (IIRC_CHAN_TOPIC_PROT & modeSwitch);
                    chanMode.modeXOR |= IIRC_CHAN_TOPIC_PROT;
                    break;
                case 'n':
                    mode |= (IIRC_CHAN_EXTERN_MSG & modeSwitch);
                    chanMode.modeXOR |= IIRC_CHAN_EXTERN_MSG;
                    break;
                case 'l':
                    mode |= (IIRC_CHAN_USER_LIMIT & modeSwitch);
                    // only expect a user limit if '+l' was used
                    if(modeSwitch)
                        bExpectLimit = TRUE;
                    chanMode.modeXOR |= IIRC_CHAN_USER_LIMIT;
                    break;
                case 'k':
                    mode |= (IIRC_CHAN_KEY & modeSwitch);
                    if(modeSwitch)
                        bExpectKey = TRUE;
                    chanMode.modeXOR |= IIRC_CHAN_KEY;
                    break;
                case 'o':
                    umodeXOR |= IIRC_CHAN_OP;
                    umodeState |= (IIRC_CHAN_OP & modeSwitch);
                    bExpectNick = TRUE;
                    break;
                case 'v':
                    umodeXOR |= IIRC_CHAN_VOICE;
                    umodeState |= (IIRC_CHAN_VOICE & modeSwitch);
                    bExpectNick = TRUE;
                    break;
                case '!':
                    // module
                    unicode_encode(iircModName, &iParam[i + 1], /* skip '!' */
                            UTF8, MAX_IIRC_MOD_NAME);
                    pMod = iircStateModuleFind(&pSelf->state, iircModName);
                    if(pMod == NULL) {
                        VLOG_MSG(LOG_LOW,"OnCCmdMode - unknown mod: %s",
                                iircModName);
                        // skip the rest because of error
                        paramPos = MAX_IIRC_CCMD_PARAMS-1;
                        break;
                    }
                    cargoIrcChanModArgInit(&modCargo);
                    modCargo.hChan = pChan->id;
                    modCargo.hMod = pMod->global_id;
                    if(modeSwitch == 0xFFFFFFFF)
                        modCargo.function = IIRC_MOD_FUNC_ENABLE;
                    else if(!modeSwitch)
                        modCargo.function = IIRC_MOD_FUNC_DISABLE;
                    else
                        modCargo.function = IIRC_MOD_FUNC_MODIFY;
                    paramPos++;
                    //
                    // use the rest of the parameters as a module argument
                    modArg[0] = 0;
                    while(paramPos < MAX_IIRC_CCMD_PARAMS) {
                        if(params[paramPos] == NULL)
                            break; // no more parameters
                        iParam = params[paramPos];
                        if(iParam[0] == '\0')
                            break;
                        unicode_cat(modArg, iParam, MAX_IIRC_MOD_ARG);
                        j = unicode_len(modArg, UCS4);
                        if(j >= MAX_IIRC_MOD_ARG - 2)
                            break;
                        if(paramPos+1 < MAX_IIRC_CCMD_PARAMS &&
                                params[paramPos+1] != NULL &&
                                params[paramPos+1][0] != '\0')
                        {
                            modArg[j] = ' '; // has to be a better way...
                            modArg[j+1] = 0;
                        }
                        paramPos++;
                    }
                    unicode_encode(modCargo.arg, modArg,
                            UTF8, MAX_IIRC_MOD_ARG);
                    modCargo.header.srcChan = 0;
                    modCargo.header.srcUser = pSrcUser->id;
                    modCargo.header.dstChan = pChan->id;
                    modCargo.header.dstUser = IIRC_USER_BROADCAST;
                    iircClientDispatch(pSelf->id, (truck*)&modCargo);
                    i = paramLen; // finish off the rest of the param
                    break;
                default:
                    VLOG_MSG(LOG_USER,"OnCCmdMode - unknown mode: %s", iParam);
                    break;
            }
            i++;
        }
        //
        // positive modes will always overwrite negative modes
        chanMode.modeState |= mode;
    }
    //
    // dispatch the new channel and user modes.
    if(mode) {
        chanMode.header.srcChan = 0;
        chanMode.header.srcUser = pSrcUser->id;
        chanMode.header.dstChan = pChan->id;
        chanMode.header.dstUser = IIRC_USER_BROADCAST;
        iircClientDispatch(pSelf->id, (truck*)&chanMode);
    }
    if(umodeXOR && userMode.nUser) {
        userMode.header.srcChan = 0;
        userMode.header.srcUser = pSrcUser->id;
        userMode.header.dstChan = pChan->id;
        userMode.header.dstUser = IIRC_USER_BROADCAST;
        iircClientDispatch(pSelf->id, (truck*)&userMode);
    }
    LOG_MSG(LOG_DUMP,"OnCCmdMode - entered");
    return 1;
}

int OnCCmdKick(iircClient_t *pSelf, iircUser_t *pUser, mUint32 *cmd)
{
    LOG_MSG(LOG_DUMP,"OnCCmdKick - entered");
    return 1;
}

int OnCCmdList(iircClient_t *pSelf, iircUser_t *pUser, mUint32 *cmd)
{
    int status;
    mUint32 *params[MAX_IIRC_CCMD_PARAMS];
    mUint8 chan[MAX_IIRC_CHAN_NAME];
    iircChan_t *pChan;
    cargoIrcChan cargo;

    LOG_MSG(LOG_DUMP,"OnCCmdList - entered");
    if(iircCCmdSplit(cmd, params) <= 0)
        return -1;
    if(params[1] == NULL) {
        // no params given, so list _all_ root channels
        return 0; // TODO
    }
    //
    // /LIST accepts multiple channels comma-seperated... we do not.
    //
    unicode_encode(chan, params[1], UTF8, MAX_IIRC_CHAN_NAME);
    if((pChan = iircStateChanFind(&pSelf->state, chan, &status)) == NULL)
        return -1;

    unicode_cpy(cargo.name, chan, UTF8, MAX_IIRC_CHAN_NAME);
    cargo.header.srcChan = 0;
    cargo.header.srcUser = pUser->id;
    iircClientDispatch(pSelf->id, (truck*)&cargo);
    return 1;
}

int OnCCmdWho(iircClient_t *pSelf, iircUser_t *pUser, mUint32 *cmd)
{
    int status;
    mUint32 *params[MAX_IIRC_CCMD_PARAMS];
    mUint8 chan[MAX_IIRC_CHAN_NAME];
    iircChan_t *pChan;
    cargoIrcWho cargo;

    LOG_MSG(LOG_DUMP,"OnCCmdWho - entered");
    if(iircCCmdSplit(cmd, params) <= 0)
        return -1;
    if(params[1] == NULL) {
        // a channel name is required
        return -1;
    }
    cargoIrcWhoInit(&cargo);
    // TODO: 'name' could be a nick
    unicode_encode(chan, params[1], UTF8, MAX_IIRC_CHAN_NAME);
    if((pChan = iircStateChanFind(&pSelf->state, chan, &status)) == NULL)
        return -1;
    cargo.flags = IIRC_WHO_FLAG_OPS | IIRC_WHO_FLAG_VOICE;
    unicode_cpy(cargo.name, chan, UTF8, MAX_IIRC_WHO_NAME);
    cargo.header.srcChan = 0;
    cargo.header.srcUser = pUser->id;
    iircClientDispatch(pSelf->id, (truck*)&cargo);
    return 1;
}

int OnCCmdNames(iircClient_t *pSelf, iircUser_t *pUser, mUint32 *cmd)
{
    int status;
    mUint32 *params[MAX_IIRC_CCMD_PARAMS];
    mUint8 chan[MAX_IIRC_CHAN_NAME];
    iircChan_t *pChan;
    cargoIrcWho cargo;

    if(iircCCmdSplit(cmd, params) <= 0)
        return -1;
    if(params[1] == NULL) {
        // a channel name is required
        return -1;
    }
    cargoIrcWhoInit(&cargo);
    unicode_encode(chan, params[1], UTF8, MAX_IIRC_CHAN_NAME);
    if((pChan = iircStateChanFind (&pSelf->state, chan, &status)) == NULL)
        return -1;
    cargo.flags = IIRC_WHO_FLAG_OPS | IIRC_WHO_FLAG_VOICE;
    cargo.flags |= IIRC_WHO_FLAG_NAMES; // triggered by a 'NAMES' command
    unicode_cpy(cargo.name, chan, UTF8, MAX_IIRC_WHO_NAME);
    cargo.header.srcChan = 0;
    cargo.header.srcUser = pUser->id;
    iircClientDispatch(pSelf->id, (truck*)&cargo);
    return 1;
}

int OnCCmdTopic(iircClient_t *pSelf, iircUser_t *pUser, mUint32 *cmd)
{
    LOG_MSG(LOG_DUMP,"OnCCmdTopic - entered");
    return 1;
}

static int my_television_is()
{
    iircEventDispatch(NULL, 0, 0, 0, 0);
    iircEventBroadcast(NULL, 0, 0, 0);
    return 1;
}

