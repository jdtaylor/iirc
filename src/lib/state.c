
/*
 * Copyright (c) 2001-2002,  James D. Taylor.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * contact James D. Taylor:  james.d.taylor@gmail.com
 */

#include <stdio.h>
#include <stdarg.h>
#include "iirc/state.h"
#include "metric/char.h"


static int iircError(iircState_t *pState, char *format, ...);


int iircStateLock(iircState_t *pState, int bWrite)
{
#ifdef USE_METRIC_THREADS
    if(bWrite)
    {
        //
        // wait to aquire write lock
        mutex_lock(&pState->write_lock);
        //
        // spin-lock until (read_count == 0)
        while(pState->read_count);
        //
        // we should be the only thread accessing the iircState now
    }
    else
    {
        //
        // wait for any writes to finish
        mutex_lock(&pState->write_lock);
        //
        // keep reads from interfering
        mutex_lock(&pState->read_lock);
        pState->read_count++;
        mutex_unlock(&pState->read_lock);
        //
        // allow simultaneous reads
        mutex_unlock(&pState->write_lock);
    }
#endif
    return 1;
}

int iircStateUnlock(iircState_t *pState, int bWrite)
{
#ifdef USE_METRIC_THREADS
    if(bWrite) {
        mutex_unlock(&pState->write_lock);
    }
    else {
        //
        // keep reads from interfering
        mutex_lock(&pState->read_lock);
        pState->read_count--;
        mutex_unlock(&pState->read_lock);
    }
#endif
    return 1;
}


int iircStateOffset(iircState_t *pState, truck **pTruck)
{
    int offset;
    /* TODO little/big endian */
    offset = (int)pTruck - (int)pState;
    offset /= sizeof(truck*);
    return offset;
}

int iircStateInit(iircState_t *pState)
{
    unsigned int i;

    pState->truckSize = sizeof(iircLargestTruck);
    for(i = 0; i < MAX_IIRC_CARGO_TYPE; i++)
        pState->fnCargoHandler[i] = NULL;
    pState->fnCargoHandler[CARGO_IIRC_CHAN_MODE] = iircStateCargoChanMode;
    pState->fnCargoHandler[CARGO_IIRC_CHAN_TOPIC] = iircStateCargoChanTopic;
    pState->fnCargoHandler[CARGO_IIRC_CHAN_USERS] = iircStateCargoChanUsers;
    pState->fnCargoHandler[CARGO_IIRC_NICK] = iircStateCargoNick;
    pState->fnCargoHandler[CARGO_IIRC_JOIN] = iircStateCargoJoin;
    pState->fnCargoHandler[CARGO_IIRC_PART] = iircStateCargoPart;
    pState->fnCargoHandler[CARGO_IIRC_QUIT] = iircStateCargoQuit;
    pState->fnCargoHandler[CARGO_IIRC_USER_MODE] = iircStateCargoUserMode;
    pState->fnCargoHandler[CARGO_IIRC_CHAN_MOD_ARG] = iircStateCargoChanModArg;
    pState->fnCargoHandler[CARGO_IIRC_USER_MOD_ARG] = iircStateCargoUserModArg;
    chanSet(Init)(&pState->chans, &iircChanInfo, 5, INCR_EXPONENTIAL);
    userSet(Init)(&pState->users, &iircUserInfo, 5, INCR_EXPONENTIAL);
    chanModSet(Init)(&pState->localMods, &iircGenModInfo, 5, INCR_EXPONENTIAL);
    pState->lastError[0] = '\0';
    pState->bAllocated = FALSE;
    pState->sys = NULL;
    for(i = 0; i < MAX_IIRC_MODS; i++)
        pState->modules[i] = NULL;
#ifdef USE_METRIC_THREADS
    pState->read_count = 0;
    mutex_init(&pState->read_lock);
    mutex_init(&pState->write_lock);
#endif
    return 1;
}

int iircStateAlloc(iircState_t *pState, unsigned int truckSize)
{
    int i, status;
    iircChan_t globalChan; // channel 0, which holds every user
    iircChan_t *pChan;

    if(truckSize <= sizeof(truck)) {
        VLOG_MSG(LOG_LOW,"iircStateAlloc - truckSize too small: %d", truckSize);
        return -1;
    }
    if(truckSize > pState->truckSize)
        pState->truckSize = truckSize;
    //
    // general purpose system data
    pState->sys = (cargoIrcSys*)MALLOC(pState->truckSize);
    if(pState->sys == NULL) {
        LOG_MSG(LOG_SYS, "iircStateAlloc - MALLOC failed on sys");
        return -1;
    }
    cargoIrcSysInit(pState->sys);
    userSet(Alloc)(&pState->users);
    chanSet(Alloc)(&pState->chans);
    chanModSet(Alloc)(&pState->localMods);
    for(i = 0; i < MAX_IIRC_MODS; i++) {
        pState->modules[i] = NULL;
    }
    iircChanInit(&globalChan);
    globalChan.name[0] = '#';
    globalChan.name[1] = '/';
    globalChan.name[2] = 0;
    pChan = iircStateChanAdd(pState, &globalChan, &status);
    if(pChan == NULL) {
        LOG_MSG(LOG_LOW,"iircStateAlloc - failed to add root channel");
        iircStateFree(pState);
        return -1;
    }
    if(pChan->id != 0) { //! assuming that the chanSet will start at 0
        LOG_MSG(LOG_LOW,"iircStateAlloc - handle to root channel isn't 0");
        iircStateChanDel(pState, pChan->id);
        iircStateFree(pState);
        return -1;
    }
    pState->bAllocated = TRUE;
    return 1;
}

/*!
 * free all the memory contained in the gameState "pState"
 */

int iircStateFree(iircState_t *pState)
{
    int i;

    iircStateLock(pState, TRUE);
    pState->bAllocated = FALSE;
    pState->truckSize = 0;
    chanSet(Free)(&pState->chans);
    userSet(Free)(&pState->users);
    chanModSet(Free)(&pState->localMods);
    pState->lastError[0] = '\0';
    if(pState->sys != NULL)
        FREE(pState->sys);
    pState->sys = NULL;
    for(i = 0; i < MAX_IIRC_MODS; i++) {
        if(pState->modules[i] != NULL)
            FREE(pState->modules[i]);
        pState->modules[i] = NULL;
    }
#ifdef USE_METRIC_THREADS
    iircStateUnlock(pState, TRUE);
    mutex_destroy(&pState->read_lock);
    mutex_destroy(&pState->write_lock);
#endif
    return 1;
}

int iircStateCopy(iircState_t *pDst, iircState_t *pSrc)
{
    unsigned int i;

    if(pDst == pSrc) // avoid deadlock
        return 1;
    iircStateLock(pDst, TRUE); // for writing
    iircStateLock(pSrc, FALSE); // for reading
    pDst->truckSize = pSrc->truckSize;
    chanSet(Copy)(&pDst->chans, &pSrc->chans);
    userSet(Copy)(&pDst->users, &pSrc->users);
    chanModSet(Copy)(&pDst->localMods, &pSrc->localMods);
    pDst->lastError[0] = '\0';
    pDst->bAllocated = pSrc->bAllocated;
    pDst->sys = pSrc->sys;
    for(i = 0; i < MAX_IIRC_MODS; i++) {
        pDst->modules[i] = pSrc->modules[i];
    }
    iircStateUnlock(pDst, TRUE);
    iircStateUnlock(pSrc, FALSE);
    return 1;
}

int iircError(iircState_t *pState, char *format, ...)
{
    va_list vl;
    va_start(vl, format);
    vsnprintf(pState->lastError, IIRC_STATE_ERR_LEN, format, vl);
    va_end(vl);
    pState->lastError[IIRC_STATE_ERR_LEN-1] = '\0';
    return 1;
}

char* iircStateError(iircState_t *pState)
{
    return pState->lastError;
}

int iircStateCargoHandler(iircState_t *pState, truck *pTruck)
{
    if(pTruck->type >= MAX_IIRC_CARGO_TYPE)
        return -1;
    if(pState->fnCargoHandler[pTruck->type] == NULL)
        return 1;
    return pState->fnCargoHandler[pTruck->type](pState, pTruck);
}

iircChan_t*
iircStateChanAdd(iircState_t *pState, iircChan_t *pChan, int *pStatus)
{
    iircChan_t *pNewChan;
    int hChan;

    if(pChan->id >= 0) {
        // insert the channel at a specific handle.  Most likely
        // by request from the server
        if(chanSet(Ins)(&pState->chans, pChan, pChan->id) < 0) {
            iircError(pState, "couldn't insert channel at %d", pChan->id);
            *pStatus = IIRC_STATE_ERR_INTERNAL;
            return NULL;
        }
        hChan = pChan->id;
    } else {
        // insert the channel at any handle (we are probably the server)
        if((hChan = chanSet(Add)(&pState->chans, pChan)) < 0) {
            iircError(pState, "couldn't add channel");
            *pStatus = IIRC_STATE_ERR_INTERNAL;
            return NULL;
        }
    }
    pNewChan = (iircChan_t*)chanSet(Get)(&pState->chans, hChan);
    if(pNewChan == NULL) {
        iircError(pState, "couldn't retrieve new channel");
        *pStatus = IIRC_STATE_ERR_INTERNAL;
        return NULL;
    }
    pNewChan->id = hChan;
    return pNewChan;
}

int iircStateChanDel(iircState_t *pState, int hChan)
{
    return chanSet(Del)(&pState->chans, hChan);
}

iircChan_t* iircStateChanGet(iircState_t *pState, int hChan, int *pStatus)
{
    *pStatus = 0;
    return (iircChan_t*)chanSet(Get)(&pState->chans, hChan);
}

iircChan_t* iircStateChanIncr(iircState_t *pState, unsigned int *i)
{
    return (iircChan_t*)chanSet(Incr)(&pState->chans, i);
}

iircChan_t* iircStateChanFind(iircState_t *pState, mUint8 *pName, int *pStatus)
{
    iircChan_t *pChan;
    unsigned int i;
    i = 0;
    pChan = (iircChan_t*)chanSet(Incr)(&pState->chans, &i);
    while(pChan != NULL) {
        if(unicode_cmp(pChan->name, pName, UTF8, MAX_IIRC_CHAN_NAME) == 0)
            return pChan;
        pChan = (iircChan_t*)chanSet(Incr)(&pState->chans, &i);
    }
    return NULL;
}

iircModule_t* iircStateChanModIncr(iircState_t *pState, iircChan_t *pChan,
        unsigned int *i)
{
    int hMod = *i;
    void *pVoid;
    pVoid = hChanModSet(Incr)(&pChan->modules, i);
    if(pVoid != NULL)
        return iircStateModuleGet(pState, hMod);
    return NULL;
}

iircUser_t* iircStateUserAdd(iircState_t *pState, iircUser_t *pUser,
        int *pStatus)
{
    int hUser;
    iircUser_t *pNewUser;
    if(pUser->id >= 0) {
        hUser = pUser->id;
        if(userSet(Ins)(&pState->users, pUser, hUser) < 0) {
            iircError(pState, "couldn't add user to: %d", hUser);
            *pStatus = IIRC_STATE_ERR_INTERNAL;
            return NULL;
        }
    } else {
        if((hUser = userSet(Add)(&pState->users, pUser)) < 0) {
            iircError(pState, "couldn't add user");
            *pStatus = IIRC_STATE_ERR_INTERNAL;
            return NULL;
        }
    }
    pNewUser = (iircUser_t*)userSet(Get)(&pState->users, hUser);
    if(pNewUser == NULL) {
        iircError(pState, "couldn't retrieve new user");
        *pStatus = IIRC_STATE_ERR_INTERNAL;
        return NULL;
    }
    pNewUser->id = hUser;
    return pNewUser;
}

int iircStateUserDel(iircState_t *pState, int hUser)
{
    int status;
    void *pVoid;
    int hChanUser;
    unsigned int i;
    iircUser_t *pUser;
    iircChan_t *pChan;

    if((pUser = iircStateUserGet(pState, hUser, &status)) == NULL)
        return status;
    //
    // step to every channel that the user is on.
    i = 0;
    pChan = iircStateUserChanIncr(pState, pUser, &i);
    while(pChan != NULL) {
        //
        // get the user's offset in the channel
        pVoid = hChanUserSet(Get)(&pUser->chanUsers, i);
        if(pVoid == NULL) {
            iircError(pState, "Unable to retrieve chanUser info for %s on %s.",
                    pUser->nick, pChan->name);
            return IIRC_STATE_ERR_INTERNAL;
        }
        hChanUser = *(int*)pVoid;
        // part the user from the channel
        hChanUserSet(Del)(&pUser->chanUsers, i);
        hUserSet(Del)(&pChan->users, hChanUser);
        // pUser->chans will be freed afterwords
        //
        // check if there are any users left ont the channel.
        // If not, then delete the channel.
        if(!hUserSet(Count)(&pChan->users))
            iircStateChanDel(pState, pChan->id);
        //
        // next channel, please
        pChan = iircStateUserChanIncr(pState, pUser, &i);
    }
    //
    // free the mem for the iircUser_t structure.  this will
    // delete the pUser->chans references.
    return userSet(Del)(&pState->users, hUser);
}

iircUser_t* iircStateUserGet(iircState_t *pState, int hUser, int *pStatus)
{
    *pStatus = 0;
    return (iircUser_t*)userSet(Get)(&pState->users, hUser);
}

iircUser_t* iircStateUserFind(iircState_t *pState, mUint8 *pName, int *pStatus)
{
    iircUser_t *pUser;
    unsigned int i;

    i = 0;
    pUser = (iircUser_t*)userSet(Incr)(&pState->users, &i);
    while(pUser != NULL) {
        if(unicode_cmp(pUser->nick, pName, UTF8, MAX_IIRC_NICK) == 0)
            return pUser;
        pUser = (iircUser_t*)userSet(Incr)(&pState->users, &i);
    }
    return NULL;
}

iircUser_t* iircStateChanUserGet(iircState_t *pState, iircChan_t *pChan,
        int hChanUser, int *pStatus)
{
    int hUser;
    iircUser_t *pUser;
    generic_t *pGeneric;
    if(pChan->id == 0) {
        hUser = hChanUser;
    } else {
        //
        // get the global user handle from the relative user handle
        pGeneric = hUserSet(Get)(&pChan->users, hChanUser);
        if(pGeneric == NULL) {
            iircError(pState, "local User: %d isn't on %s.",
                    hUser, pChan->name);
            *pStatus = IIRC_STATE_ERR_INTERNAL;
            return NULL;
        }
        hUser = *(int*)pGeneric;
    }
    // get the user structure from the global user handle
    pUser = (iircUser_t*)userSet(Get)(&pState->users, hUser);
    if(pUser == NULL) {
        iircError(pState, "Unknown srcUser: %d", hUser);
        *pStatus = IIRC_STATE_ERR_INTERNAL;
        return NULL;
    }
    return pUser;
}

iircUser_t*
iircStateChanUserIncr(iircState_t *pState, iircChan_t *pChan, unsigned int *i)
{
    int hUser;
    iircUser_t *pUser;
    generic_t *pGeneric;

    if(pChan->id == 0) {
        hUser = *i;
        pUser = (iircUser_t*)userSet(Incr)(&pState->users, i);
    } else {
        pGeneric = hUserSet(Incr)(&pChan->users, i);
        if(pGeneric == NULL)
            return NULL;
        hUser = *(int*)pGeneric;
        // get the user structure from the global user handle
        pUser = (iircUser_t*)userSet(Get)(&pState->users, hUser);
    }
    if(pUser == NULL) {
        return NULL;
    }
    return pUser;
}

iircChan_t*
iircStateUserChanIncr(iircState_t *pState, iircUser_t *pUser, unsigned int *i)
{
    void *pVoid;
    pVoid = hChanSet(Incr)(&pUser->chans, i);
    if(pVoid == NULL)
        return NULL;
    return (iircChan_t*)chanSet(Get)(&pState->chans, *(int*)pVoid);
}

iircChan_t* iircStateDstChanGet(iircState_t *pState, truck *pTruck,
        int *pStatus)
{
    iircChan_t *pChan;
    pChan = iircStateChanGet(pState, pTruck->dstChan, pStatus);
    if(pChan == NULL) {
        iircError(pState, "invalid destination channel: %d", pTruck->dstChan);
        return NULL;
    }
    return pChan;
}

int iircStateDstChanSet(iircState_t *pState, iircUser_t *pUser,
        iircChan_t *pChan, truck *pTruck)
{
    int i;
    generic_t *pGeneric;
    //
    // we need the user handle relative to the channel.
    // pUser->chanUsers holds these values for each channel the user is on.
    // but we need to search for the channel first.
    i = hChanSet(Find)(&pUser->chans, &pChan->id);
    if(i < 0) {
        iircError(pState, "local User: %s isn't on %s.",
                pUser->nick, pChan->name);
        return IIRC_STATE_ERR_WARN; // warn user that operation failed
    }
    pGeneric = hChanUserSet(Get)(&pUser->chanUsers, (unsigned int)i);
    if(pGeneric == NULL) {
        iircError(pState, "local User: %s isn't on %s.",
                pUser->nick, pChan->name);
        return IIRC_STATE_ERR_WARN; // warn user that operation failed
    }
    pTruck->srcUser = *(unsigned int*)pGeneric;
    pTruck->srcChan = pChan->id;
    pTruck->dstUser = IIRC_USER_BROADCAST;
    pTruck->dstChan = pChan->id;
    return 1;
}

iircUser_t*
iircStateSrcUserGet(iircState_t *pState, truck *pTruck, int *pStatus)
{
    iircChan_t *pChan;
    pChan = iircStateChanGet(pState, pTruck->srcChan, pStatus);
    if(pChan == NULL) {
        iircError(pState, "invalid source channel: %d", pTruck->srcChan);
        return NULL;
    }
    return iircStateChanUserGet(pState, pChan, pTruck->srcUser, pStatus);
}

iircChan_t*
iircStateSrcChanGet(iircState_t *pState, truck *pTruck, int *pStatus)
{
    iircChan_t *pChan;
    pChan = iircStateChanGet(pState, pTruck->srcChan, pStatus);
    if(pChan == NULL) {
        iircError(pState, "invalid source channel: %d", pTruck->srcChan);
        return NULL;
    }
    return pChan;
}

int iircStateModuleAdd(iircState_t *pState, iircModule_t *pMod)
{
    int hMod;
    int hLocalMod;
    iircModule_t mod;
    iircModule_t *pModule;

    iircStateLock(pState, TRUE);
    //
    // search if this module is already loaded.
    //
    // create a dummy chanMod which has the modName we are searching for
    iircModuleInit(&mod);
    unicode_cpy(mod.name, pMod->name, UTF8, MAX_IIRC_MOD_NAME);
    //
    // use the generic set to search the modules by name
    hLocalMod = chanModSet(Find)(&pState->localMods, &mod);
    if(hLocalMod != ELEMENT_NULL) {
        iircStateUnlock(pState, TRUE);
        return 1; // already loaded, man
    }
    //
    // update largest truck info
    if(pMod->truckSize > pState->truckSize)
    {
        if(pState->bAllocated) {
            // don't expand truck size anymore
            VLOG_MSG(LOG_USER,"Module: %s's truckSize: %d is too large.",
                    pMod->name, pMod->truckSize);
            iircStateUnlock(pState, TRUE);
            return -1;
        }
        pState->truckSize = pMod->truckSize;
    }
    hMod = chanModSet(Add)(&pState->localMods, pMod);
    if(hMod == ELEMENT_NULL) {
        iircError(pState,"failed to add channel module");
        iircStateUnlock(pState, TRUE);
        return -1;
    }
    pModule = (iircModule_t*)chanModSet(Get)(&pState->localMods, hMod);
    if(pModule == NULL) {
        iircError(pState,"failed to retreive new channel module");
        iircStateUnlock(pState, TRUE);
        return -1;
    }
    pModule->local_id = hMod;

    /*  functionality now in iircStateRegisterModule
     *
     truck *pTruck;
     int i = 0;
     while(i < MAX_IIRC_MODS) {
     if(pState->modules[i] == NULL)
     break;
     i++;
     }
     if(i >= MAX_IIRC_MODS) {
     iircError(pState,"reached max module limit: %d", i);
     return -1;
     }
     pTruck = (truck*)MALLOC(pState->truckSize);
     if(pTruck == NULL) {
     iircError(pState,"iircStateModuleAdd - MALLOC failed");
     return -1;
     }
     pMod->global_id = i; // since we are root, we get to set this
     pState->modules[i] = (cargoIrcModule*)pTruck;
     cargoIrcModuleInit(pState->modules[i]);
     cargoIrcModulePack(pState->modules[i], pMod);
     pState->modules[i]->bSupported = TRUE;
     */
    iircStateUnlock(pState, TRUE);
    return hMod;
}

iircModule_t* iircStateModuleGet(iircState_t *pState, int hMod)
{
    cargoIrcModInfo *pCargo;
    iircModule_t *pMod;

    if(hMod < 0 || hMod >= MAX_IIRC_MODS) {
        iircError(pState,"iircStateModuleGet - invalid hMod: %d", hMod);
        return NULL;
    }
    if(pState->modules[hMod] == NULL) {
        iircError(pState, "iircStateModuleGet - unknown hMod: %d", hMod);
        return NULL;
    }
    pCargo = pState->modules[hMod];
    if(pState->modules[hMod]->bSupported == FALSE) {
        iircError(pState, "module: %s is unsupported", pCargo->name);
        return NULL;
    }
    pMod = (iircModule_t*)chanModSet(Get)(&pState->localMods,
            pCargo->local_id);
    if(pMod == NULL) {
        iircError(pState, "module: %s is not loaded.", pCargo->name);
        return NULL;
    }
    return pMod;
}

iircModule_t* iircStateModuleIncr(iircState_t *pState, unsigned int *i)
{
    mInt32 local_id;
    iircModule_t *pMod;

    while(*i < MAX_IIRC_MODS && pState->modules[*i] == NULL)
        (*i)++;
    if(*i >= MAX_IIRC_MODS)
        return NULL;
    local_id = pState->modules[*i]->local_id;
    if(local_id < 0)
        return NULL;
    pMod = (iircModule_t*)chanModSet(Get)(&pState->localMods,
            (unsigned int)local_id);
    if(pMod != NULL)
        (*i)++;

    return pMod;
}

iircModule_t* iircStateLocModuleFind(iircState_t *pState, mUint8 *modName)
{
    int hLocalMod;
    iircModule_t mod;
    iircModule_t *pMod;
    //
    // create a dummy chanMod which has the modName we are searching for
    iircModuleInit(&mod);
    unicode_cpy(mod.name, modName, UTF8, MAX_IIRC_MOD_NAME);
    //
    // use the generic set to search the modules by name
    hLocalMod = chanModSet(Find)(&pState->localMods, &mod);
    if(hLocalMod == ELEMENT_NULL) {
        iircError(pState, "module: %s is not loaded.", modName);
        return NULL;
    }
    pMod = (iircModule_t*)chanModSet(Get)(&pState->localMods, hLocalMod);
    while(pMod != NULL) {
        iircError(pState, "module: %s couldn't be retrieved.", modName);
        return NULL;
    }
    return pMod;
}

cargoIrcModInfo* iircStateModuleFind(iircState_t *pState, mUint8 *modName)
{
    int i;
    cargoIrcModInfo *pMod;
    //
    // search all the modules for this modName
    for(i = 0; i < MAX_IIRC_MODS; i++) {
        if(pState->modules[i] == NULL)
            continue;
        pMod = pState->modules[i];
        if(unicode_cmp(modName, pMod->name, UTF8, MAX_IIRC_MOD_NAME) == 0)
            return pMod;
    }
    return NULL;
}

/*!
//  THIS SHOULD ONLY BE CALLED BY THE ROOT SERVER DAMNIT!!
 */
int iircStateRegisterModules(iircState_t *pState)
{
    unsigned int i, j;
    truck *pTruck;
    iircModule_t *pMod;
    j = 0;

    iircStateLock(pState, TRUE);

    if(pState->bAllocated == FALSE) {
        LOG_MSG(LOG_LOW,"iircStateRegisterModules - not allocated yet");
        iircStateUnlock(pState, TRUE);
        return -1;
    }
    pMod = (iircModule_t*)chanModSet(Incr)(&pState->localMods, &j);
    while(pMod != NULL) {
        //
        // find an unused global module handle
        i = 0;
        while(i < MAX_IIRC_MODS && pState->modules[i] != NULL)
            i++;
        if(i >= MAX_IIRC_MODS) {
            iircError(pState,"reached max module limit: %d", i);
            iircStateUnlock(pState, TRUE);
            return 1;
        }
        pTruck = (truck*)MALLOC(pState->truckSize);
        if(pTruck == NULL) {
            iircError(pState,"iircStateRegisterModules - MALLOC failed");
            iircStateUnlock(pState, TRUE);
            return -1;
        }
        pMod->global_id = i; // since we are root, we get to set this
        pMod->truckSize = pState->truckSize; // official truck size
        pState->modules[i] = (cargoIrcModInfo*)pTruck;
        cargoIrcModInfoInit(pState->modules[i]);
        cargoIrcModInfoPack(pState->modules[i], pMod);
        pState->modules[i]->bSupported = TRUE;
        //
        // next local module
        pMod = (iircModule_t*)chanModSet(Incr)(&pState->localMods, &j);
    }
    iircStateUnlock(pState, TRUE);
    return 1;
}

/*
 * @CargoHandlers
 */

int iircStateCargoModInfo(iircState_t *pState, cargoIrcModInfo *pCargo)
{
    int hLocalMod;
    iircModule_t *pMod;
    iircModule_t mod;

    if(pCargo->global_id < 0 || pCargo->global_id >= MAX_IIRC_MODS) {
        iircError(pState,"invalid module id: %d", pCargo->global_id);
        return IIRC_STATE_ERR_FATAL;
    }
    //
    // add module to the iircState's list of supported modules.
    if(pState->modules[pCargo->global_id] == NULL) {
        pState->modules[pCargo->global_id] =
            (cargoIrcModInfo*)MALLOC(pState->truckSize);
        if(pState->modules[pCargo->global_id] == NULL) {
            iircError(pState,"Mem allocation failed for new module");
            return IIRC_STATE_ERR_INTERNAL;
        }
    }
    memcpy(pState->modules[pCargo->global_id], pCargo, pCargo->header.size);
    pState->modules[pCargo->global_id]->bSupported = FALSE;
    //
    // search for this module from the modules that we have locally loaded.
    //
    // create a dummy chanMod which has the modName we are searching for
    iircModuleInit(&mod);
    unicode_cpy(mod.name, pCargo->name, UTF8, MAX_IIRC_MOD_NAME);
    //
    // use the generic set to search the modules by name
    hLocalMod = chanModSet(Find)(&pState->localMods, &mod);
    if(hLocalMod == ELEMENT_NULL) {
        VLOG_MSG(LOG_USER,"server using module: %s \t[not found]",
                pCargo->name);
        return 1; // we don't support this module
    }
    pMod = (iircModule_t*)chanModSet(Get)(&pState->localMods, hLocalMod);
    if(pMod == NULL) {
        iircError(pState, "module: %s couldn't be retrieved.", pCargo->name);
        return IIRC_STATE_ERR_INTERNAL;
    }
    //
    // module is supported on this node. fix it's references
    pMod->global_id = pCargo->global_id;
    pState->modules[pCargo->global_id]->local_id = pMod->local_id;
    pState->modules[pCargo->global_id]->bSupported = TRUE;
    VLOG_MSG(LOG_USER,"server using module: %s \t[found]", pCargo->name);
    return 1;
}

int iircStateCargoUser(iircState_t *pState, cargoIrcUser *pCargo)
{
    int status;
    iircUser_t tempUser;
    iircUser_t *pUser;

    if(pCargo->flags & IIRC_USER_CONNECT) {
        //
        // add this user
        iircUserInit(&tempUser);
        tempUser.id = pCargo->id;
        pUser = iircStateUserAdd(pState, &tempUser, &status);
        if(pUser == NULL) {
            iircError(pState, "iircStateCargoUser - error adding user: %d",
                    pCargo->id);
            return IIRC_STATE_ERR_FATAL;
        }
        cargoIrcUserUnpack(pCargo, pUser);
        return pUser->id;
    }
    if(pCargo->flags & IIRC_USER_DISCONNECT) {
        //
        // delete this user
        return iircStateUserDel(pState, pCargo->id);
    }
    //
    // existing user is changing [his|her|it's] information
    pUser = iircStateUserGet(pState, pCargo->id, &status);
    if(pUser == NULL) {
        iircError(pState,"iircStateCargoUser - failed to get user");
        return IIRC_STATE_ERR_FATAL;
    }
    cargoIrcUserUnpack(pCargo, pUser);
    return pUser->id;
}

int iircStateCargoChan(iircState_t *pState, cargoIrcChan *pCargo)
{
    unsigned int i;
    int status;
    iircChan_t *pChan;
    iircChan_t tempChan;

    if(pCargo->flags & IIRC_CHAN_CREATE) {
        //
        // search for the channel name
        i = 0;
        pChan = (iircChan_t*)iircStateChanIncr(pState, &i);
        while(pChan != NULL) {
            if(unicode_cmp(pChan->name, pCargo->name,
                        UTF8, MAX_IIRC_CHAN_NAME) == 0)
                break;
            pChan = (iircChan_t*)iircStateChanIncr(pState, &i);
        }
        if(pChan != NULL) {
            //
            // the channel already exists, man.
            if(pChan->id != (int)pCargo->hChan) {
                //
                // delete existing channel which is at the wrong handle
                iircStateChanDel(pState, pChan->id);
            }
            cargoIrcChanUnpack(pCargo, &tempChan); // tempChan = *pCargo
            pChan = iircStateChanAdd(pState, &tempChan, &status);
            if(pChan == NULL) {
                iircError(pState, "iircStateCargoChan: add channel failed: %s",
                        pCargo->name);
                return IIRC_STATE_ERR_INTERNAL;
            }
        }
    } else if(pCargo->flags & IIRC_CHAN_DELETE) {
        //
        // delete channel at hChan
        iircStateChanDel(pState, pCargo->hChan);
        return 1;
    } else {
        //
        // get the existing channel sitting at hChan
        pChan = iircStateChanGet(pState, pCargo->hChan, &status);
        if(pChan == NULL) {
            iircError(pState, "iircStateCargoChan: unknown hChan: %d",
                    pCargo->hChan);
            return IIRC_STATE_ERR_FATAL;
        }
    }
    // don't store the create or delete flags with the channel structure
    pChan->flags = pCargo->flags & !(IIRC_CHAN_CREATE | IIRC_CHAN_DELETE);
    return 1;
}

/*
   int iircStateCargoUsername(iircState_t *pState, truck *pTruck)
   {
   int status;
   iircUser_t *pUser;

   cargoIrcUsername *pCargo = (cargoIrcUsername*)pTruck;

   pUser = iircStateSrcUserGet(pState, (truck*)pCargo, &status);
   if(pUser == NULL) {
   return status;
   }
// TODO: check if servername/hostname are valid
strncpy(pUser->username, pCargo->username, MAX_IIRC_NICK);
strncpy(pUser->hostname, pCargo->hostname, MAX_IIRC_HOST_NAME);
strncpy(pUser->realname, pCargo->realname, MAX_IIRC_REAL_NAME);
return 1;
}
 */

int iircStateCargoChanMode(iircState_t *pState, truck *pTruck)
{
    int status;
    iircChan_t *pChan;
    cargoIrcChanMode *pCargo = (cargoIrcChanMode*)pTruck;

    // get the destination channel
    pChan = iircStateChanGet(pState, pCargo->header.dstChan, &status);
    if(pChan == NULL) {
        iircError(pState, "iircStateCargoChanMode: unknown dstChan: %d",
                pCargo->header.dstChan);
        return IIRC_STATE_ERR_FATAL;
    }
    // modify the existing channel mode with the modes that have changed
    pChan->modes = (pChan->modes & ~pCargo->modeXOR) |
        (pCargo->modeState & pCargo->modeXOR);
    pChan->userLimit = pCargo->userLimit;
    pCargo->modeActive = pChan->modes; // set to be read only
    unicode_cpy(pChan->key, pCargo->key, UTF8, MAX_IIRC_CHAN_KEY);
    return 1;
}

int iircStateCargoChanTopic(iircState_t *pState, truck *pTruck)
{
    int status;
    iircChan_t *pChan;
    cargoIrcChanTopic *pCargo = (cargoIrcChanTopic*)pTruck;

    // get the destination channel
    pChan = iircStateChanGet(pState, pCargo->header.dstChan, &status);
    if(pChan == NULL) {
        iircError(pState, "iircStateCargoChanTopic: unknown dstChan: %d",
                pCargo->header.dstChan);
        return IIRC_STATE_ERR_FATAL;
    }
    unicode_cpy(pChan->topic, pCargo->topic, UTF8, MAX_IIRC_CHAN_TOPIC);
    return 1;
}

int iircStateCargoChanUsers(iircState_t *pState, truck *pTruck)
{
    int j;
    unsigned int i;
    int status;
    iircUser_t *pUser;
    iircChan_t *pChan;
    cargoIrcChanUsers *pCargo = (cargoIrcChanUsers*)pTruck;

    // get the target channel
    pChan = iircStateChanGet(pState, pCargo->hChan, &status);
    if(pChan == NULL) {
        iircError(pState, "iircStateCargoChanUsers: unknown hChan: %d",
                pCargo->hChan);
        return IIRC_STATE_ERR_FATAL;
    }
    for(i = 0; i < pCargo->nUser; i++) {
        pUser = iircStateUserGet(pState, pCargo->hClient[i], &status);
        if(pUser == NULL) {
            VLOG_MSG(LOG_LOW,"iircStateCargoChanUsers - unknown hClient: %d",
                    pCargo->hClient[i]);
            continue;
        }
        // modify our channel and user structures to agree with the
        // new cargoIrcChanUsers.  Overwrite any previous references.
        hUserSet(Ins)(&pChan->users, &pCargo->hClient[i], pCargo->hUser[i]);
        userFlagSet(Ins)(&pChan->userFlags, &pCargo->hUserFlags[i],
                pCargo->hUser[i]);
        j = hChanSet(Add)(&pUser->chans, &pCargo->hChan);
        if(j < 0) {
            VLOG_MSG(LOG_LOW,"iircStateCargoChanUsers - failed to add %s to %s",
                    pUser->nick, pChan->name);
            hUserSet(Del)(&pChan->users, pCargo->hUser[i]);
            userFlagSet(Del)(&pChan->userFlags, pCargo->hUser[i]);
            continue;
        }
        hChanUserSet(Ins)(&pUser->chanUsers, &pCargo->hUser[i],(unsigned int)j);
    }
    return 1;
}

int iircStateCargoNick(iircState_t *pState, truck *pTruck)
{
    int status;
    iircUser_t *pUser;
    cargoIrcNick *pCargo = (cargoIrcNick*)pTruck;

    pUser = iircStateSrcUserGet(pState, (truck*)pCargo, &status);
    if(pUser == NULL) {
        return status;
    }
    unicode_cpy(pUser->nick_old, pUser->nick, UTF8, MAX_IIRC_NICK);
    unicode_cpy(pUser->nick, pCargo->nick, UTF8, MAX_IIRC_NICK);
    return 1;
}

int iircStateCargoJoin(iircState_t *pState, truck *pTruck)
{
    int j;
    int status;
    unsigned int i;
    iircUser_t *pUser;
    iircChan_t *pChan;
    iircChan_t tempChan;
    cargoIrcJoin *pCargo = (cargoIrcJoin*)pTruck;

    if((pUser = iircStateSrcUserGet(pState, pTruck, &status)) == NULL) {
        return status;
    }
    if(pCargo->flags & IIRC_JOIN_BY_NAME)
    {
        // Channel to join was specified using the channel's name.
        // Joining by name should only be done by the root server.
        // Everyone else should join by hChan using the hChanUser.

        // search for the channel name
        i = 0;
        pChan = iircStateChanIncr(pState, &i);
        while(pChan != NULL) {
            if(!unicode_cmp(pChan->name, pCargo->chan, UTF8,MAX_IIRC_CHAN_NAME))
                break;
            pChan = iircStateChanIncr(pState, &i);
        }
        if(pChan == NULL) {
            // no channel with this name was found, so create a new one
            iircChanInit(&tempChan);
            //TODO: verify pCargo->chan as a valid channel name
            unicode_cpy(tempChan.name, pCargo->chan, UTF8, MAX_IIRC_CHAN_NAME);
            pChan = iircStateChanAdd(pState, &tempChan, &status);
            if(pChan == NULL) {
                iircError(pState, "add channel failed: %s", pCargo->chan);
                return IIRC_STATE_ERR_INTERNAL;
            }
        }
        // instruct children to put this new chan at the same location
        pCargo->hChan = pChan->id;
        pCargo->flags &= ~IIRC_JOIN_BY_NAME; // join by handle
        //
        // check if this user is already on this channel
        if(hChanSet(Find)(&pUser->chans, &pChan->id) != ELEMENT_NULL) {
            // user is already on this channel
            return pChan->id;
        }
        //
        // Add this userID to the channel's list of users.  Since we
        // are joining by name, we don't know what position to insert it at.
        pCargo->hChanUser = hUserSet(Add)(&pChan->users, &pUser->id);
        if(pCargo->hChanUser < 0) {
            iircError(pState, "failed to add hUser of: %s to chan: %s",
                    pUser->nick, pCargo->chan);
            return IIRC_STATE_ERR_INTERNAL;
        }
    } else {
        // channel to join was specified using a handle
        pChan = iircStateChanGet(pState, pCargo->hChan, &status);
        if(pChan == NULL) {
            // no channel with this handle was found, so create a new one
            iircChanInit(&tempChan);
            //TODO: verify pCargo->chan as a valid channel name
            unicode_cpy(tempChan.name, pCargo->chan, UTF8, MAX_IIRC_CHAN_NAME);
            tempChan.id = pCargo->hChan; // insert new channel at this handle
            pChan = iircStateChanAdd(pState, &tempChan, &status);
            if(pChan == NULL) {
                iircError(pState, "add channel failed: %s", pCargo->chan);
                return IIRC_STATE_ERR_INTERNAL;
            }
        }
        //
        // check if this user is already on this channel
        if(hChanSet(Find)(&pUser->chans, &pChan->id) != ELEMENT_NULL) {
            // user is already on this channel
            return pChan->id;
        }
        //
        // Insert this userID to the channel's list of users
        if(hUserSet(Ins)(&pChan->users, &pUser->id, pCargo->hChanUser) < 0) {
            iircError(pState, "failed to add hUser of: %s to chan: %s",
                    pUser->nick, pCargo->chan);
            return IIRC_STATE_ERR_INTERNAL;
        }
    }
    j = hChanSet(Add)(&pUser->chans, &pChan->id);
    if(j < 0) {
        iircError(pState, "failed to add hChan of: %s to user: %s",
                pCargo->chan, pUser->nick);
        hUserSet(Del)(&pChan->users, pCargo->hChanUser); // remove chans hUser
        return IIRC_STATE_ERR_INTERNAL;
    }
    i = (unsigned int)j;
    //
    // Record the user's offset in the chan
    if(hChanUserSet(Ins)(&pUser->chanUsers, &pCargo->hChanUser, i) < 0)
    {
        iircError(pState, "failed to insert hChanUser of: %s to user: %s",
                pCargo->chan, pUser->nick);
        hChanSet(Del)(&pUser->chans, i); // remove user's hChan
        hUserSet(Del)(&pChan->users, pCargo->hChanUser); // remove chans hUser
        return IIRC_STATE_ERR_INTERNAL;
    }
    VLOG_MSG(LOG_DUMP,"%s has joined channel: %s.", pUser->nick, pCargo->chan);
    return pChan->id;
}

int iircStateCargoPart(iircState_t *pState, truck *pTruck)
{
    int j;
    int status;
    void *pVoid;
    int hChanUser;
    iircUser_t *pUser;
    iircChan_t *pChan;
    //cargoIrcPart *pCargo = (cargoIrcPart*)pTruck;

    if((pChan = iircStateSrcChanGet(pState, pTruck, &status)) == NULL) {
        return status;
    }
    if((pUser = iircStateSrcUserGet(pState, pTruck, &status)) == NULL) {
        return status;
    }
    //
    // cleanup and delete this user's references in the users and
    // chans structures.
    j = hChanSet(Find)(&pUser->chans, &pChan->id);
    if(j < 0) {
        iircError(pState, "User %s isn't on channel %s.", pUser->nick,
                pChan->name);
        return IIRC_STATE_ERR_WARN;
    }
    hChanSet(Del)(&pUser->chans, (unsigned int)j);
    pVoid = hChanUserSet(Get)(&pUser->chanUsers, (unsigned int)j);
    if(pVoid == NULL) {
        iircError(pState, "Unable to retrieve chanUser info for %s on %s.",
                pUser->nick, pChan->name);
        return IIRC_STATE_ERR_INTERNAL;
    }
    hChanUser = *(int*)pVoid;
    hChanUserSet(Del)(&pUser->chanUsers, (unsigned int)j);
    hUserSet(Del)(&pChan->users, hChanUser);
    //
    // check if there are any users left ont the channel.
    // If not, then delete the channel.
    if(!hUserSet(Count)(&pChan->users))
        iircStateChanDel(pState, pChan->id);
    return 1;
}

int iircStateCargoQuit(iircState_t *pState, truck *pTruck)
{
    // quite message should be sent in a pre-state handler
    iircStateUserDel(pState, pTruck->srcUser);
    return 1;
}

int iircStateCargoUserMode(iircState_t *pState, truck *pTruck)
{
    int status;
    void *pVoid;
    unsigned int i;
    unsigned int hChanUser;
    mUint32 modes;
    cargoIrcUserMode *pCargo = (cargoIrcUserMode*)pTruck;
    iircChan_t *pChan;
    iircUser_t *pUser;

    pChan = iircStateChanGet(pState, pCargo->hChan, &status);
    if(pChan == NULL) {
        iircError(pState, "failed to get hChan of: %d", pCargo->hChan);
        return IIRC_STATE_ERR_CHAN_HANDLE;
    }
    for(i = 0; i < pCargo->nUser; i++)
    {
        hChanUser = pCargo->hChanUser[i];
        pUser = iircStateChanUserGet(pState, pChan, hChanUser, &status);
        if(pUser == NULL)
            continue; // user not on channel
        pVoid = userFlagSet(Get)(&pChan->userFlags, hChanUser);
        if(pVoid == NULL)
            continue; // uhh, ignore fatal error
        modes = *(mUint32*)pVoid;
        //
        // modify the existing user mode with the modes that have changed
        modes = (modes & ~pCargo->modeXOR[i]) |
            (pCargo->modeState[i] & pCargo->modeXOR[i]);
        pCargo->modeActive[i] = modes; // help out the cargo handlers
        //
        // put the user flag back where we found it
        userFlagSet(Ins)(&pChan->userFlags, &modes, hChanUser);
    }
    return 1;
}

int iircStateCargoChanModArg(iircState_t *pState, truck *pTruck)
{
    int i;
    int status;
    //mUint8 modArg[MAX_IIRC_MOD_ARG];
    iircChan_t *pChan;
    iircModule_t *pMod;
    cargoIrcChanModArg *pCargo = (cargoIrcChanModArg*)pTruck;

    pChan = iircStateChanGet(pState, pCargo->hChan, &status);
    if(pChan == NULL) {
        iircError(pState, "unknown hChan: %d", pCargo->hChan);
        return IIRC_STATE_ERR_CHAN_HANDLE;
    }
    pMod = iircStateModuleGet(pState, pCargo->hMod);
    if(pMod == NULL) {
        iircError(pState, "unknown hMod: %d", pCargo->hMod);
        return 1; // we probably don't support this module
    }
    switch(pCargo->function)
    {
        case IIRC_MOD_FUNC_DISABLE:
            //
            // tell the module to close that one instance
            iircModuleClose(pMod);
            //
            // Delete the channel's reference of the module's instance
            i = hChanModSet(Find)(&pChan->modules, &pMod->global_id);
            if(i < 0) {
                iircError(pState,"%s failed to find module instance: %s",
                        pChan->name, pMod->name);
                return IIRC_STATE_ERR_INTERNAL;
            }
            hChanModSet(Del)(&pChan->modules, i);
            break;
        case IIRC_MOD_FUNC_ENABLE:
            //
            // open a new instance of the module
            iircModuleOpen(pMod, pCargo->hChan);
            //
            // Add the moduleId to the set of module's for this channel
            if(hChanModSet(Add)(&pChan->modules, &pMod->global_id) < 0)
            {
                iircModuleClose(pMod);
                iircError(pState,"%s failed to add module instance: %s",
                        pChan->name, pMod->name);
                return IIRC_STATE_ERR_INTERNAL;
            }
            break;
        case IIRC_MOD_FUNC_MODIFY:
            break; // cargo should be forwarded to module
        default:
            iircError(pState,"unknown module function: %d", pCargo->function);
            return IIRC_STATE_ERR_FATAL;
    }
    return 1;
}

int iircStateCargoUserModArg(iircState_t *pState, truck *pTruck)
{
    int status;
    //mUint8 modArg[MAX_IIRC_MOD_ARG];
    iircUser_t *pUser;
    iircModule_t *pMod;
    cargoIrcUserModArg *pCargo = (cargoIrcUserModArg*)pTruck;

    pUser = iircStateUserGet(pState, pCargo->hUser, &status);
    if(pUser == NULL) {
        iircError(pState, "unknown hUser: %d", pCargo->hUser);
        return IIRC_STATE_ERR_SOURCE_USER;
    }
    pMod = iircStateModuleGet(pState, pCargo->hMod);
    if(pMod == NULL) {
        iircError(pState, "unknown hMod: %d", pCargo->hMod);
        return 1; // we probably don't support this module
    }
    switch(pCargo->function)
    {
        case IIRC_MOD_FUNC_DISABLE:
            //i = hUserModSet(Find)(&pUser->modEnabled
            break;
        case IIRC_MOD_FUNC_ENABLE:
            break;
        case IIRC_MOD_FUNC_MODIFY:
            break; // cargo should be forwarded to module
        default:
            iircError(pState,"unknown module function: %d", pCargo->function);
            return IIRC_STATE_ERR_FATAL;
    }
    return 1;
}

