
/*
 * Copyright (c) 2001-2002,  James D. Taylor.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * contact James D. Taylor:  james.d.taylor@gmail.com
 */

#ifndef IIRC_STATE_H
#define IIRC_STATE_H

#include "config.h"
#include "iirc/common.h"
#include "iirc/defs.h"
#include "iirc/struct_user.h"
#include "iirc/struct_chan.h"
#include "iirc/cargo.h"
#ifdef USE_METRIC_THREADS
  #include "metric/thread.h"
#endif

#define IIRC_STATE_ERR_LEN 512

#define IIRC_STATE_ERR_INTERNAL -1
#define IIRC_STATE_ERR_WARN -2
#define IIRC_STATE_ERR_FATAL -1024
#define IIRC_STATE_ERR_SOURCE_USER -1025
#define IIRC_STATE_ERR_CHAN_HANDLE -1026

#define chanSet_t   genericOctetTree
#define chanSet(op) genericOctetTree##op
#define userSet_t   genericOctetTree
#define userSet(op) genericOctetTree##op

#define IIRC_STATE_NUM_TRUCKS (1+MAX_IIRC_MODULE)

struct _iircState;

typedef int (*iircStateCargoHandler_t)(struct _iircState *, truck *);

typedef struct _iircState {
    unsigned int truckSize;
    iircStateCargoHandler_t fnCargoHandler[MAX_IIRC_CARGO_TYPE];
    chanSet_t chans;
    userSet_t users;
    chanModSet_t localMods; //! local storage of modules
    char lastError[IIRC_STATE_ERR_LEN];
    int bAllocated;
    cargoIrcSys *sys;
    cargoIrcModInfo *modules[MAX_IIRC_MODS]; //! global storage of modules
#ifdef USE_METRIC_THREADS
    unsigned int read_count;
    mutex_t write_lock;
    mutex_t read_lock;
#endif
} iircState_t;



#ifdef __cplusplus
extern "C" {
#endif

/*
 * IIRC State Operations.
 * (functions to modify the IIRC state)
 * (also used by clients to modify server cache)
 */
int iircStateLock(iircState_t *pState, int bWrite);
int iircStateUnlock(iircState_t *pState, int bWrite);
int iircStateOffset(iircState_t *pState, truck **pTruck);
int iircStateInit(iircState_t *pState);
int iircStateAlloc(iircState_t *pState, unsigned int truckSize);
int iircStateFree(iircState_t *pState);
int iircStateCopy(iircState_t *pDst, iircState_t *pSrc);
char* iircStateError(iircState_t *pState);

int iircStateCargoHandler(iircState_t*, truck *pTruck);

iircChan_t* iircStateChanAdd(iircState_t*, iircChan_t*, int *pStatus);
int     iircStateChanDel(iircState_t*, int hChan);
iircChan_t* iircStateChanGet(iircState_t*, int hChan, int *pStatus);
iircChan_t* iircStateChanIncr(iircState_t*, unsigned int *i);
iircChan_t* iircStateChanFind(iircState_t*, mUint8 *pName, int *pStatus);
iircModule_t*
        iircStateChanModIncr(iircState_t*, iircChan_t*, unsigned int *i);

iircUser_t* iircStateUserAdd(iircState_t*, iircUser_t*, int *pStatus);
int     iircStateUserDel(iircState_t*, int hUser);
iircUser_t* iircStateUserGet(iircState_t*, int hUser, int *pStatus);
iircUser_t* iircStateUserFind(iircState_t*, mUint8 *pName, int *pStatus);

iircUser_t* iircStateChanUserGet(iircState_t*, iircChan_t*,
                 int hChanUser, int *pStatus);
iircUser_t* iircStateChanUserIncr(iircState_t*, iircChan_t*, unsigned int *i);
iircChan_t* iircStateUserChanIncr(iircState_t*, iircUser_t*, unsigned int *i);


iircChan_t* iircStateDstChanGet(iircState_t*, truck*, int *pStatus);
int     iircStateDstChanSet(iircState_t*, iircUser_t*, iircChan_t*,
                                truck*);
iircUser_t* iircStateSrcUserGet(iircState_t*, truck*, int *pStatus);
iircChan_t* iircStateSrcChanGet(iircState_t*, truck*, int *pStatus);


int       iircStateModuleAdd(iircState_t*, iircModule_t *pMod);
iircModule_t* iircStateModuleGet(iircState_t*, int hMod);
iircModule_t* iircStateModuleIncr(iircState_t*, unsigned int *i);
int       iircStateRegisterModules(iircState_t*);
cargoIrcModInfo* iircStateModuleFind(iircState_t*, mUint8 *modName);


int iircStateCargoModInfo(  iircState_t*, cargoIrcModInfo *pCargo);
int iircStateCargoUser(     iircState_t*, cargoIrcUser *pCargo);
int iircStateCargoChan(     iircState_t*, cargoIrcChan *pCargo);

int iircStateCargoChanMode( iircState_t*, truck *pTruck);
int iircStateCargoChanTopic(    iircState_t*, truck *pTruck);
int iircStateCargoChanUsers(    iircState_t*, truck *pTruck);
int iircStateCargoNick(     iircState_t*, truck *pTruck);
int iircStateCargoPart(     iircState_t*, truck *pTruck);
int iircStateCargoQuit(     iircState_t*, truck *pTruck);
int iircStateCargoJoin(     iircState_t*, truck *pTruck);
//int iircStateCargoUsername(   iircState_t*, truck *pTruck);
int iircStateCargoUserMode( iircState_t*, truck *pTruck);
int iircStateCargoChanModArg(   iircState_t*, truck *pTruck);
int iircStateCargoUserModArg(   iircState_t*, truck *pTruck);

#ifdef __cplusplus
}
#endif

#endif

