
/*
 * Copyright (c) 2001-2002,  James D. Taylor.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * contact James D. Taylor:  james.d.taylor@gmail.com
 */

#include "iirc/struct_user.h"
#include "metric/char.h"


//!
// UCS4 characters rejected from nick names
static mUint32 g_nickReject[] = {
    0x00000020, // SPACE
    0x00000007, // BELL
    0x0000000d, // CR
    0x0000000a, // LF
    0x0000002c, // comma
    0x00000000  // NUL
};
static mUint32 g_nickRejectOR = 0x20 | 0x7 | 0xd | 0xa | 0x2c;

//
// Generic user operations.  for use with genericSet (metric/set.h)
static int genUserInit(generic_t *pGen);
static int genUserAlloc(generic_t *pGen);
static int genUserFree(generic_t *pGen);
static int genUserCmp(generic_t *pGen1, generic_t *pGen2);
static int genUserCopy(generic_t *pGenDst, generic_t *pGenSrc);

genericInfo_t iircUserInfo = {
    size: sizeof(iircUser_t),
    fnInit: genUserInit,
    fnAlloc: genUserAlloc,
    fnFree: genUserFree,
    fnCmp: genUserCmp,
    fnCopy: genUserCopy
};

int nickVerify(mUint8 *nick)
{
    mUint32 wnick[MAX_IIRC_NICK];
    unsigned int i, j;
    //
    // convert UTF8 character stream to 32bit UCS4 characters
    unicode_decode(wnick, nick, UTF8, MAX_IIRC_NICK);
    //
    // touch all characters in the nick
    i = 0;
    while(i < MAX_IIRC_NICK && wnick[i]) {
        //
        // if the nickname character has only bits of combined char
        if(nick[i] & g_nickRejectOR) {
            //
            // verify if this is a rejected character
            j = 0;
            while(g_nickReject[j]) {
                if(wnick[i] == g_nickReject[j])
                    return 0; // bad nick
                j++;
            }
        }
        i++;
    }
    if(i)
        return 1;
    return 0;
}

/**********************************************
 * @irUserc
 */

int iircUserInit(iircUser_t *pUser)
{
    pUser->id = -1;
    pUser->flags = 0;
    pUser->modes = 0;
    pUser->nick[0] = 0;
    pUser->nick_old[0] = 0;
    pUser->identity[0] = 0;
    pUser->hostname[0] = 0;
    pUser->servername[0] = 0;
    pUser->realname[0] = 0;
    hUserModSet(Init)(&pUser->modules, &genericInt, 5, INCR_EXPONENTIAL);
    hUserModSet(Init)(&pUser->modEnabled, &genericInt, 5, INCR_EXPONENTIAL);
    hChanSet(Init)(&pUser->chans, &genericInt, 1, INCR_LINEAR);
    //
    // initialize a dynamic set of chanUser handles (integers)
    // (initial 1 handle, and increase linearly when needed)
    hChanUserSet(Init)(&pUser->chanUsers, &genericInt, 1, INCR_LINEAR);
    pUser->hCom = 0;
    pUser->fnCargoReceiver = NULL;
    return 1;
}

int iircUserAlloc(iircUser_t *pUser)
{
    hUserModSet(Alloc)(&pUser->modules);
    hUserModSet(Alloc)(&pUser->modEnabled);
    hChanSet(Alloc)(&pUser->chans);
    hChanUserSet(Alloc)(&pUser->chanUsers);
    return 1;
}

int iircUserFree(iircUser_t *pUser)
{
    hUserModSet(Free)(&pUser->modules);
    hUserModSet(Free)(&pUser->modEnabled);
    hChanSet(Free)(&pUser->chans);
    hChanUserSet(Free)(&pUser->chanUsers);
    return 1;
}

int iircUserCmp(iircUser_t *pUser1, iircUser_t *pUser2)
{
    //
    // Equality of two users can be simplified to the user's nicknames
    // since every user must have a unique nick.
    //
    /*
       int i, len1, len2;
       i = pUser1->id - pUser2->id;
       if(i) return i;
    // nick
    i = unicode_cmp(pUser1->nick, pUser2->nick, UTF8, MAX_IIRC_NICK);
    if(i) return i;
    // ident
    len1 = strlen(pUser1->identity);
    len2 = strlen(pUser2->identity);
    if(len1 < len2)
    return -1;
    else if(len2 < len1)
    return 1;
    i = strncmp(pUser1->identity, pUser2->identity, MAX_IIRC_IDENTITY);
    if(i) return i;
    // hostname
    len1 = strlen(pUser1->hostname);
    len2 = strlen(pUser2->hostname);
    if(len1 < len2)
    return -1;
    else if(len2 < len1)
    return 1;
    i = strncmp(pUser1->hostname, pUser2->hostname, MAX_HOST_NAME);
    if(i) return i;
    // servername
    len1 = strlen(pUser1->servername);
    len2 = strlen(pUser2->servername);
    if(len1 < len2)
    return -1;
    else if(len2 < len1)
    return 1;
    i = strncmp(pUser1->servername, pUser2->servername, MAX_HOST_NAME);
    if(i) return i;
    i = unicode_cmp(pUser1->realname, pUser2->realname,UTF8,MAX_IIRC_REAL_NAME);
    if(i) return i;
    i = pUser1->flags - pUser2->flags;
    if(i) return i;
    i = pUser1->modes - pUser2->modes;
    if(i) return i;
    i = pUser1->cityAddr - pUser2->cityAddr;
    if(i) return i;
    i = pUser1->houseAddr - pUser2->houseAddr;
    if(i) return i;
    // TODO
    //i = genericArrayCmp(&pUser1->chans, &pUser2->chans);
    //if(i) return i;
    return 0; // equal
     */
    return unicode_cmp(pUser1->nick, pUser2->nick, UTF8, MAX_IIRC_NICK);
}

int iircUserCopy(iircUser_t *pDst, iircUser_t *pSrc)
{
    pDst->id = pSrc->id;
    pDst->flags = pSrc->flags;
    pDst->modes = pSrc->modes;
    unicode_cpy(pDst->nick, pSrc->nick, UTF8, MAX_IIRC_NICK);
    unicode_cpy(pDst->nick_old, pSrc->nick_old, UTF8, MAX_IIRC_NICK);
    strncpy(pDst->identity, pSrc->identity, MAX_IIRC_IDENTITY);
    pDst->identity[MAX_IIRC_IDENTITY-1] = 0;
    strncpy(pDst->hostname, pSrc->hostname, MAX_HOST_NAME);
    pDst->hostname[MAX_HOST_NAME-1] = 0;
    strncpy(pDst->servername, pSrc->servername, MAX_HOST_NAME);
    pDst->servername[MAX_HOST_NAME-1] = 0;
    unicode_cpy(pDst->realname, pSrc->realname, UTF8, MAX_IIRC_REAL_NAME);
    hUserModSet(Copy)(&pDst->modules, &pSrc->modules);
    hUserModSet(Copy)(&pDst->modEnabled, &pSrc->modEnabled);
    hChanSet(Copy)(&pDst->chans, &pSrc->chans);
    hChanUserSet(Copy)(&pDst->chanUsers, &pSrc->chanUsers);
    pDst->hCom = pSrc->hCom;
    pDst->fnCargoReceiver = pSrc->fnCargoReceiver;
    return 1;
}

int genUserInit(generic_t *pGen)
{
    return iircUserInit((iircUser_t*)pGen);
}

int genUserAlloc(generic_t *pGen)
{
    return iircUserAlloc((iircUser_t*)pGen);
}

int genUserFree(generic_t *pGen)
{
    return iircUserFree((iircUser_t*)pGen);
}

int genUserCmp(generic_t *pGen1, generic_t *pGen2)
{
    return iircUserCmp((iircUser_t*)pGen1, (iircUser_t*)pGen2);
}

int genUserCopy(generic_t *pGenDst, generic_t *pGenSrc)
{
    return iircUserCopy((iircUser_t*)pGenDst, (iircUser_t*)pGenSrc);
}

