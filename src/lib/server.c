
/*
 * Copyright (c) 2001-2002,  James D. Taylor.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * contact James D. Taylor:  james.d.taylor@gmail.com
 */

#include "config.h"
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>
#include <errno.h>
#include "iirc/server.h"
#include "metric/sock.h"
#include "metric/set.h"
#include "metric/char.h"
#include "iirc/truck.h"
#include "iirc/cargo.h"
#include "iirc/state.h"
#include "iirc/struct_user.h"
#include "iirc/struct_chan.h"
#include "iirc/struct_server.h"
#include "iirc/rfc1459.h"

#define DEFAULT_SERVER_TIMEOUT 10000
#define MAX_SERVER_TRUCKS 20
#define INITIAL_SERVER_TRUCK 5

#define SERV iircServer_t *pServ

//!
// generic set of all iirc servers
#define iircServerSet_t   genericOctetTree
#define iircServerSet(op) genericOctetTree##op

#define IIRC_SCMD_LEN 11
static mUint32 g_iircServerCmd[MAX_IIRC_SCMDS][IIRC_SCMD_LEN] =
{
    {'N','O','P',0},
    {'H','O','S','T','N','A','M','E',0},
    {'S','E','R','V','E','R','N','A','M','E',0},
    {'L','O','A','D',0},
    //    {'S','T','A','R','T',0},
    {'Q','U','I','T', 0}
};

iircServerSet_t g_serverSet; //! hServer -> iircServer_t
static int g_bInitialized = FALSE;

//! fill cargo handler functions with ones from this file
static int iircServerFill(iircServer_t *pServer);


int servTruckProcess(SERV, truck *pTruck, int *pStatus);
int servTruckInternal(SERV, mUint32 hCom, truck *pTruck, int *pStatus);

static int servParentDisconnect(SERV, commute_t *pCom);
static int servServerDisconnect(SERV, commute_t *pCom);
static int servClientDisconnect(SERV, commute_t *pCom);

static int servSocketCheck(SERV, long timeout);
static int servSocketCheckParent(SERV, socketSet_t *pReady);
static int servSocketCheckServers(SERV, socketSet_t *pReady);
static int servSocketCheckClients(SERV, socketSet_t *pReady);
static truck* servCommuteReceive(SERV, commute_t *pCom, int *pStatus);
static int servCommuteSend(iircServer_t *pServ, commute_t *pCom, truck *pTruck);

static int servTruckDispatch(SERV, truck *pTruck);
static int iircEventDispatch(SERV, int chan, int user, int code, int data1);
static int iircEventBroadcast(SERV, int chan, int code, int data1);

static int rfc1459ServerCallback(unsigned int hIntr, int data, void *pData);
static int rfc1459ClientCallback(unsigned int hIntr, int data, void *pData);

//
// cargo handlers
static int OnCargoIrcModInfo(SERV, mUint32 hCom, truck *pTruck, int *pStatus);
static int OnCargoIrcChan(   SERV, mUint32 hCom, truck *pTruck, int *pStatus);
static int OnCargoIrcUser(   SERV, mUint32 hCom, truck *pTruck, int *pStatus);

static int OnCargoIrcPrePart(         SERV, truck *pTruck, int *pStatus);
static int OnCargoIrcPreQuit(         SERV, truck *pTruck, int *pStatus);

static int OnCargoIrcPostEvent(       SERV, truck *pTruck, int *pStatus);
static int OnCargoIrcPostSys(         SERV, truck *pTruck, int *pStatus);
static int OnCargoIrcPostServer(      SERV, truck *pTruck, int *pStatus);
static int OnCargoIrcPostChanMode(    SERV, truck *pTruck, int *pStatus);
static int OnCargoIrcPostChanTopic(   SERV, truck *pTruck, int *pStatus);
static int OnCargoIrcPostChanUsers(   SERV, truck *pTruck, int *pStatus);
//static int OnCargoIrcPostUsername(  SERV, truck *pTruck, int *pStatus);
static int OnCargoIrcPostNick(        SERV, truck *pTruck, int *pStatus);
static int OnCargoIrcPostJoin(        SERV, truck *pTruck, int *pStatus);
static int OnCargoIrcPostWho(         SERV, truck *pTruck, int *pStatus);
static int OnCargoIrcPostUserMode(    SERV, truck *pTruck, int *pStatus);
static int OnCargoIrcPostChanBan(     SERV, truck *pTruck, int *pStatus);
static int OnCargoIrcPostMsg(         SERV, truck *pTruck, int *pStatus);
static int OnCargoIrcPostChanModArg(  SERV, truck *pTruck, int *pStatus);
static int OnCargoIrcPostUserModArg(  SERV, truck *pTruck, int *pStatus);

static int iircSCmdSplit(mUint32 *cmd, mUint32 *params[MAX_IIRC_SCMD_PARAMS]);
static int OnSCmdNop(       iircServer_t *pSelf, mUint32 *cmd);
static int OnSCmdHostname(  iircServer_t *pSelf, mUint32 *cmd);
static int OnSCmdServername(    iircServer_t *pSelf, mUint32 *cmd);
static int OnSCmdLoad(      iircServer_t *pSelf, mUint32 *cmd);
static int OnSCmdQuit(      iircServer_t *pSelf, mUint32 *cmd);


int iircServerGlobalInit()
{
    iircCargoGlobalInit();
    iircServerSet(Init)(&g_serverSet, &iircServerInfo, 1, INCR_LINEAR);
    g_bInitialized = TRUE;
    return 1;
}

int iircServerGlobalFree()
{
    g_bInitialized = FALSE;
    iircServerSet(Free)(&g_serverSet);
    return 1;
}

int iircServerFill(iircServer_t *pServer)
{
    unsigned int i;

    for(i = 0; i < MAX_IIRC_CARGO_TYPE; i++) {
        pServer->fnPreHandler[i] = NULL;
        pServer->fnPostHandler[i] = NULL;
    }
    pServer->fnPreHandler[CARGO_IIRC_PART] = OnCargoIrcPrePart;
    pServer->fnPreHandler[CARGO_IIRC_QUIT] = OnCargoIrcPreQuit;

    pServer->fnPostHandler[CARGO_IIRC_EVENT] = OnCargoIrcPostEvent;
    pServer->fnPostHandler[CARGO_IIRC_SYS] = OnCargoIrcPostSys;
    pServer->fnPostHandler[CARGO_IIRC_SERVER] = OnCargoIrcPostServer;
    pServer->fnPostHandler[CARGO_IIRC_CHAN_MODE] = OnCargoIrcPostChanMode;
    pServer->fnPostHandler[CARGO_IIRC_CHAN_TOPIC] = OnCargoIrcPostChanTopic;
    pServer->fnPostHandler[CARGO_IIRC_CHAN_USERS] = OnCargoIrcPostChanUsers;
    //    pServer->fnPostHandler[CARGO_IIRC_USERNAME] = OnCargoIrcPostUsername;
    pServer->fnPostHandler[CARGO_IIRC_NICK] = OnCargoIrcPostNick;
    pServer->fnPostHandler[CARGO_IIRC_JOIN] = OnCargoIrcPostJoin;
    pServer->fnPostHandler[CARGO_IIRC_PART] = NULL;
    pServer->fnPostHandler[CARGO_IIRC_QUIT] = NULL;
    pServer->fnPostHandler[CARGO_IIRC_WHO] = OnCargoIrcPostWho;
    pServer->fnPostHandler[CARGO_IIRC_USER_MODE] = OnCargoIrcPostUserMode;
    pServer->fnPostHandler[CARGO_IIRC_CHAN_BAN] = OnCargoIrcPostChanBan;
    pServer->fnPostHandler[CARGO_IIRC_MSG] = OnCargoIrcPostMsg;
    pServer->fnPostHandler[CARGO_IIRC_CHAN_MOD_ARG] = OnCargoIrcPostChanModArg;
    pServer->fnPostHandler[CARGO_IIRC_USER_MOD_ARG] = OnCargoIrcPostUserModArg;
    for(i = 0; i < MAX_IIRC_SCMDS; i++)
        pServer->fnCmd[i] = NULL;
    pServer->fnCmd[IIRC_SCMD_NOP] = OnSCmdNop;
    pServer->fnCmd[IIRC_SCMD_HOSTNAME] = OnSCmdHostname;
    pServer->fnCmd[IIRC_SCMD_SERVERNAME] = OnSCmdServername;
    pServer->fnCmd[IIRC_SCMD_LOAD] = OnSCmdLoad;
    pServer->fnCmd[IIRC_SCMD_QUIT] = OnSCmdQuit;
    return 1;
}


int iircServerAdd(iircServerDescr *pDescr)
{
    int hServer;
    iircServer_t *pServ;
    if(!g_bInitialized)
        iircServerGlobalInit();
    //
    // shared library version check
    if(pDescr->version < IIRC_SERVER_DESCR_VERSION) {
        VLOG_MSG(LOG_LOW,"iircServerAdd - pDescr version too old: %d",
                pDescr->version);
        VLOG_MSG(LOG_LOW,"iircServerAdd - needed at least version: %d",
                IIRC_SERVER_DESCR_VERSION);
        return -1;
    }
    //
    // add the new server into the global set
    hServer = iircServerSet(Add)(&g_serverSet, NULL);
    if(hServer == ELEMENT_NULL) {
        LOG_MSG(LOG_LOW,"iircServerAdd - failed to add new iircServer");
        return -1;
    }
    pServ = (iircServer_t*)iircServerSet(Get)(&g_serverSet, hServer);
    if(pServ == NULL) {
        VLOG_MSG(LOG_LOW,"iircServerAdd - failed retrieving new iircServer: %d",
                hServer);
        return -1;
    }
    pServ->id = hServer;

    strncpy(pServ->name, pDescr->name, MAX_IIRC_SERVER_NAME);
    pServ->name[MAX_IIRC_SERVER_NAME-1] = '\0';
    strncpy(pServ->hostname, pDescr->hostname, MAX_HOST_NAME);
    pServ->hostname[MAX_HOST_NAME-1] = '\0';
    pServ->isRoot = pDescr->isRoot;
    pServ->listenForServers = pDescr->listenForServers;
    pServ->listenForClients = pDescr->listenForClients;
    pServ->listenForRFC1459 = pDescr->listenForRFC1459;
    strncpy(pServ->parentName, pDescr->parentName, MAX_HOST_NAME);
    strncpy(pServ->serverName, pDescr->serverName, MAX_HOST_NAME);
    strncpy(pServ->clientName, pDescr->clientName, MAX_HOST_NAME);
    strncpy(pServ->rfc1459Name, pDescr->rfc1459Name, MAX_HOST_NAME);
    pServ->parentName[MAX_HOST_NAME-1] = '\0';
    pServ->serverName[MAX_HOST_NAME-1] = '\0';
    pServ->clientName[MAX_HOST_NAME-1] = '\0';
    pServ->serverName[MAX_HOST_NAME-1] = '\0';
    pServ->rfc1459Name[MAX_HOST_NAME-1] = '\0';
    pServ->parentPort = pDescr->parentPort;
    pServ->serverPort = pDescr->serverPort;
    pServ->clientPort = pDescr->clientPort;
    pServ->rfc1459Port = pDescr->rfc1459Port;
    iircServerFill(pServ);

    if(pDescr->listenForRFC1459) {
        if(socketAddrNameSet(&pServ->rfc1459SockAddr, pServ->rfc1459Name,
                    pServ->rfc1459Port) <= 0)
        {
            VLOG_MSG(LOG_LOW,"iircServerAdd - resolve failed: %s : %d",
                    pServ->rfc1459Name, (int)pServ->rfc1459Port);
            // continue without rfc1459
            pServ->listenForRFC1459 = FALSE;
        }
    }
    return hServer;
}

int iircServerStart(int hServer, long timeout)
{
    int i;
    iircInterruptDescr intrDesc;
    socketAddr_t sockAddr;
    iircServer_t *pServ;
    commute_t *pCom;

    pServ = (iircServer_t*)iircServerSet(Get)(&g_serverSet, hServer);
    if(pServ == NULL) {
        VLOG_MSG(LOG_LOW,"iircServerStart - unknown hServer: %d", hServer);
        return IIRC_SERVER_ERR_FATAL;
    }
    if(pServ->bStarted)
    {
        // iircServerStart already called for this server.  Previous
        // attempt probably timed out.
        // wait 'timeout' ms for reply from the parent
        i = iircServerExecute(pServ->id, timeout);
        if(i < 0) {
            LOG_MSG(LOG_LOW,"iircServerStart - connection to parent failed");
            return IIRC_SERVER_ERR_FATAL;
        }
        if(i == 0)
            return IIRC_SERVER_ERR_TIMEOUT;
        //
        // house is properly connected to it's parent now.  mamma mia!
        return 1;
    }
    pServ->bStarted = TRUE;
    //
    // allocate mem for the iIRC state
    if(iircStateAlloc(&pServ->state, sizeof(iircLargestTruck)) < 0) {
        LOG_MSG(LOG_LOW,"iircServerStart - iircStateAlloc failed");
        return IIRC_SERVER_ERR_FATAL;
    }
    if(truckQueueAlloc(&pServ->parking, INITIAL_SERVER_TRUCK,
                INITIAL_SERVER_TRUCK, pServ->state.truckSize) < 0)
    {
        LOG_MSG(LOG_LOW,"iircServerStart - truckQueueAlloc Failed");
        return IIRC_SERVER_ERR_FATAL;
    }
    //
    // Connect to parent
    //
    if(pServ->isRoot == FALSE)
    {
        //
        // this is a child server, so connect to our parent
        pCom = &pServ->parentCom;
        pCom->flags &= ~COMMUTE_PASSIVE; // we initiate the connection
        pCom->type = COMMUTE_SOCKET;
        if(socketAddrNameSet(&pCom->remoteSockAddr, pServ->parentName,
                    pServ->parentPort) <= 0)
        {
            VLOG_MSG(LOG_LOW,"iircServerStart - no resolve el parento: %s : %d",
                    pServ->parentName, (int)pServ->parentPort);
            return IIRC_SERVER_ERR_FATAL;
        }
        // check if we have already tried to open this socket,
        // if so, then exit silently.
        if(pCom->flags & COMMUTE_CONNECT_FAILED)
            return IIRC_SERVER_ERR_FATAL;
        // We start connection to remote
        if(socketConnect(&pCom->sock, &pCom->remoteSockAddr) < 0) {
            pCom->flags |= COMMUTE_CONNECT_FAILED;
            return IIRC_SERVER_ERR_FATAL;
        }
        // set socket to non-blocking
        socketOptionSet(&pCom->sock, SOCK_OPT_BLOCKING, 0);
        //
        // add the new iIRC parent to the server's set-o-sockets
        if(socketSetAdd(&pServ->sockets, &pCom->sock) < 0) {
            LOG_MSG(LOG_LOW,"iircServerStart - socketSetAdd failed");
            return IIRC_SERVER_ERR_FATAL;
        }
        pCom->localParking = &pServ->parking;
        i = iircServerExecute(hServer, timeout);
        if(i < 0) {
            LOG_MSG(LOG_LOW,"iircServerStart - connection to parent failed");
            return IIRC_SERVER_ERR_FATAL;
        }
        if(i == 0)
            return IIRC_SERVER_ERR_TIMEOUT;
        //
        // send a connection event to everything
        // TODO: obviously
    }
    //
    //  Listen for child servers
    //
    if(pServ->listenForServers)
    {
        if(socketAddrNameSet(&sockAddr, pServ->serverName,
                    pServ->serverPort) <= 0)
        {
            VLOG_MSG(LOG_LOW,"iircServerStart - couldn't resolve: %s : %d",
                    pServ->serverName, (int)pServ->serverPort);
            return IIRC_SERVER_ERR_FATAL;
        }
        // We listen on local
        if(socketListen(&pServ->serverConnect, &sockAddr) < 0) {
            VLOG_MSG(LOG_LOW,"iircServerStart - couldn't listen on: %s : %d",
                    pServ->serverName, (int)pServ->serverPort);
            return IIRC_SERVER_ERR_FATAL;
        }
        if(socketSetAdd(&pServ->sockets, &pServ->serverConnect) < 0) {
            LOG_MSG(LOG_LOW,"iircServerStart - socketSetAdd failed");
            return IIRC_SERVER_ERR_FATAL;
        }
    }
    //
    //  Listen for child clients
    //
    if(pServ->listenForClients)
    {
        if(socketAddrNameSet(&sockAddr, pServ->clientName,
                    pServ->clientPort) <= 0)
        {
            VLOG_MSG(LOG_LOW,"iircServerStart - couldn't resolve: %s : %d",
                    pServ->clientName, (int)pServ->clientPort);
            return IIRC_SERVER_ERR_FATAL;
        }
        // We listen on local
        if(socketListen(&pServ->clientConnect, &sockAddr) < 0) {
            VLOG_MSG(LOG_LOW,"iircServerStart - couldn't listen on: %s : %d",
                    pServ->clientName, (int)pServ->clientPort);
            return IIRC_SERVER_ERR_FATAL;
        }
        if(socketSetAdd(&pServ->sockets, &pServ->clientConnect) < 0) {
            LOG_MSG(LOG_LOW,"iircServerStart - socketSetAdd failed");
            return IIRC_SERVER_ERR_FATAL;
        }
    }
    //
    // listen for (port 6667) IRC clients
    //
    while(pServ->listenForRFC1459)
    {
        if(socketListen(&pServ->rfc1459Sock, &pServ->rfc1459SockAddr) < 0) {
            VLOG_MSG(LOG_LOW,"iircServerStart - listen failed: %s : %d",
                    pServ->rfc1459Name, (int)pServ->rfc1459Port);
            pServ->listenForRFC1459 = FALSE;
            break;
        }
        intrDesc.version = IIRC_INTR_DESCR_VERSION;
        intrDesc.fd = (unsigned int)pServ->rfc1459Sock; // yikes!  TODO
        intrDesc.fnCallback = rfc1459ServerCallback;
        intrDesc.data = pServ->id;
        intrDesc.pData = NULL;
        iircServerIntrAdd(pServ->id, &intrDesc);
        break;
    }
    if(pServ->isRoot) {
        //
        // we need to reserve global module handles
        iircStateRegisterModules(&pServ->state);
    }
    pServ->bStarted = TRUE;
    return 1;
}

int iircServerDel(int hServer)
{
    iircServer_t *pServ;
    pServ = (iircServer_t*)iircServerSet(Get)(&g_serverSet, hServer);
    if(pServ == NULL) {
        VLOG_MSG(LOG_LOW,"iircServerDel - invalid hServer: %d", hServer);
        return -1;
    }
    pServ->bStarted = FALSE;
    // this will call iircServerFree
    iircServerSet(Del)(&g_serverSet, hServer);
    return 1;
}

int iircServerModuleAdd(int hServer, iircModule_t *pMod)
{
    iircServer_t *pServ;
    if(!g_bInitialized)
        iircServerGlobalInit();
    pServ = (iircServer_t*)iircServerSet(Get)(&g_serverSet, hServer);
    if(pServ == NULL) {
        VLOG_MSG(LOG_LOW,"iircServerModuleAdd - invalid hServer: %d", hServer);
        return -1;
    }
    VLOG_MSG(LOG_USER,"<server> iircServer added module %s - %d bytes",
            pMod->name, pMod->truckSize);
    pMod->hParent = hServer;
    iircStateModuleAdd(&pServ->state, pMod);
    if(pServ->bStarted) {
        // TODO: if we are root, we should send this module info to everyone
        //
    }
    return 1;
}

int iircServerExecute(int hServer, long timeout)
{
    int status;
    unsigned int i, j;
    iircModule_t *pMod;
    iircServer_t *pServ;
    truck *pTruck;

    i = 0;
    if(hServer == -1)
        pServ = (iircServer_t*)iircServerSet(Incr)(&g_serverSet, &i);
    else
        pServ = (iircServer_t*)iircServerSet(Get)(&g_serverSet, hServer);

    while(pServ != NULL)
    {
        //
        // listen on network until next scheduled interrupt.
        // ignore any errors.
        // TODO: split timeout between servers
        servSocketCheck(pServ, timeout);
        //
        // check for any trucks from this server's chan modules
        j = 0;
        iircStateLock(&pServ->state, FALSE);
        pMod = iircStateModuleIncr(&pServ->state, &j);
        while(pMod != NULL) {
            pTruck = iircModuleReceive(pMod, &status);
            if(pTruck != NULL) {
                servTruckDispatch(pServ, (truck*)pTruck);
                iircModuleRecycle(pMod, pTruck);
            }
            pMod = iircStateModuleIncr(&pServ->state, &j);
        }
        iircStateUnlock(&pServ->state, FALSE); // pMod + j
        if(hServer != -1)
            break; // only executing one server
        pServ = (iircServer_t*)iircServerSet(Incr)(&g_serverSet, &i);
    }
    return 1;
}

/*
//!
// arg is expected to be of size IIRC_SERVER_ARG_LEN
int iircServerChanCreate(SERV, char *chanName, char *arg)
{
unsigned int i;
int status;
int modInst;
char **iChar;  // indexing character pointer
char argBuf[MAX_IIRC_MOD_NAME]; // sub-string buffer
iircModule_t *pMod;
iircChan_t *pChan;
iircChan_t tempChan;
//
// paranoia/insanity (important to force rules)
arg[IIRC_SERVER_ARG_LEN-1] = '\0';
//
// construct and add the new channel.  Immediately retrieve the
// channel because genericArrayAdd will copy tempChan.
iircChanInit(&tempChan);
strncpy(tempChan.name, chanName, MAX_IIRC_CHAN_NAME);
tempChan.name[MAX_IIRC_CHAN_NAME-1] = '\0';
pChan = iircStateChanAdd(&pServ->state, &tempChan, &status);
if(pChan == NULL) {
VLOG_MSG(LOG_LOW,"iircServerCreateChan - %s",
iircStateError(&pServ->state));
return -1;
}
//
// split argument into sub-strings seperated by spaces
 *iChar = arg;
 while(!Strntok(argBuf, iChar, ' ', MAX_IIRC_MOD_NAME)) {
//
// search all the modules for this modName
for(i = 0; i < pServ->nModule; i++) {
if(pServ->modules[i] == NULL)
continue;
pMod = pServ->modules[i];
if(strncasecmp(argBuf, pMod->name, MAX_IIRC_MOD_NAME) != 0)
continue; // doesn't match, keep searching
//
// open the module, and get it's instance
if((modInst = pMod->fnOpen(pChan->id, pMod)) < 0)
continue;
//
// Add the module instance to the set of module's for this channel
if(hChanModSet(Ins)(&pChan->modules, &modInst, pMod->id) < 0) {
pMod->fnClose(modInst); // close only the new instance
continue;
}
break;
}
if(i >= pServ->nModule) {
VLOG_MSG(LOG_USER,"<server> CreateChan - unknown module: %s", argBuf);
continue;
}
}
return pChan->id;
}
 */

int iircServerIntrAdd(int hServer, iircInterruptDescr *pDescr)
{
#ifdef HAVE_IIRC_INTR
    int flags;
    int hIntr;
    iircInterrupt_t *pIntr;
    iircServer_t *pServ;

    pServ = (iircServer_t*)iircServerSet(Get)(&g_serverSet, hServer);
    if(pServ == NULL) {
        VLOG_MSG(LOG_LOW,"iircServerIntrAdd - unknown hServer: %d", hServer);
        return -1;
    }
    //
    // shared library version check
    if(pDescr->version < IIRC_INTR_DESCR_VERSION) {
        VLOG_MSG(LOG_LOW,"iircServerIntrAdd - interruptDescr version old %d",
                pDescr->version);
        VLOG_MSG(LOG_LOW,"iircServerIntrAdd - needed at least: %d",
                IIRC_INTR_DESCR_VERSION);
        return -1;
    }
    //
    // set file descriptor to non-blocking
    flags = fcntl(pDescr->fd, F_GETFL, 0);
    if(flags < 0) {
        VLOG_MSG(LOG_SYS,"iircServerIntrAdd - fcntl GETFL: %d", flags);
        return -1;
    }
    flags |= O_NONBLOCK;
    flags = fcntl(pDescr->fd, F_SETFL, flags);
    //
    // add and get the new interrupt
    hIntr = intrSet(Add)(&pServ->interrupts, NULL);
    if(hIntr < 0) {
        VLOG_MSG(LOG_LOW,"iircServerIntrAdd - intrSet(Add) failed on fd: %d",
                pDescr->fd);
        return -1;
    }
    pIntr = (iircInterrupt_t*)intrSet(Get)(&pServ->interrupts, hIntr);
    if(pIntr == NULL) {
        VLOG_MSG(LOG_LOW,"iircServerIntrAdd - unable to retrieve %d", hIntr);
        return -1;
    }
    pIntr->id = (unsigned int)hIntr;
    pIntr->fd = pDescr->fd;
    pIntr->fnCallback = pDescr->fnCallback;
    pIntr->data = pDescr->data;
    pIntr->pData = pDescr->pData;
    //
    // TODO: this call has some serious issues.  For one thing it
    // kills support for SDLnet because sockets on other platforms
    // obviously aren't socket file descriptors.  To fix this, I would
    // need a new abstraction in servSocketCheck in place of the
    // socketSetCheck call to handle all types of I/O.
    socketSetAdd(&pServ->sockets, (socket_t*)&pIntr->fd);
    return hIntr;
#else
    LOG_MSG(LOG_HIGH,"iircServerIntrAdd - interrupt support not compiled");
    return -1;
#endif
}

int iircServerIntrDel(int hServer, unsigned int hIntr)
{
#ifdef HAVE_IIRC_INTR
    iircServer_t *pServ;

    pServ = (iircServer_t*)iircServerSet(Get)(&g_serverSet, hServer);
    if(pServ == NULL) {
        VLOG_MSG(LOG_LOW,"iircServerIntrDel - unknown hServer: %d", hServer);
        return -1;
    }
    return intrSet(Del)(&pServ->interrupts, hIntr);
#else
    LOG_MSG(LOG_HIGH,"iircServerIntrAdd - interrupt support not compiled");
    return -1;
#endif
}

int iircServerDispatch(int hServer, truck *pTruck)
{
    iircServer_t *pServ;

    pServ = (iircServer_t*)iircServerSet(Get)(&g_serverSet, hServer);
    if(pServ == NULL) {
        VLOG_MSG(LOG_LOW,"iircServerDispatch - unknown hServer: %d", hServer);
        return -1;
    }
    return servTruckDispatch(pServ, pTruck);
}

int iircServerCmd(int hServer, char *cmd)
{
    int status;
    unsigned int i;
    unsigned int cmdLen;
    mUint32 cmdBuf[MAX_IIRC_SERVER_CMD];
    mUint32 command[MAX_IIRC_SERVER_CMD];
    mUint32 *curPos;
    iircServer_t *pSelf;

    pSelf = (iircServer_t*)iircServerSet(Get)(&g_serverSet, hServer);
    if(pSelf == NULL)
        return -1;
    unicode_decode(cmdBuf, cmd, UTF8, MAX_IIRC_SERVER_CMD);
    curPos = cmdBuf;
    if(unicode_tok(command, &curPos, ' ', IIRC_SCMD_LEN) == 0)
        return -1; // hrm, no command.
    cmdLen = unicode_len(command, UCS4);
    for(i = 0; i < cmdLen; i++) {
        if(command[i] < 256)
            command[i] = toupper((char)command[i]);
    }
    //
    // search for this command by it's name
    for(i = 1; i < MAX_IIRC_SCMDS; i++) {
        if(cmdLen != unicode_len(g_iircServerCmd[i], UCS4))
            continue;
        if(unicode_cmp(command, g_iircServerCmd[i], UCS4, IIRC_SCMD_LEN) == 0) {
            if(pSelf->fnCmd[i] != NULL) {
                iircStateLock(&pSelf->state, FALSE);
                status = pSelf->fnCmd[i](pSelf, cmdBuf);
                iircStateUnlock(&pSelf->state, FALSE);
                return status;
            }
        }
    }
    return -1;
}

int iircServerUserGet(int hServer, int hUser, cargoIrcUser *cargo)
{
    int status;
    iircUser_t *pUser;
    iircServer_t *pSelf;

    pSelf = (iircServer_t*)iircServerSet(Get)(&g_serverSet, hServer);
    if(pSelf == NULL) {
        VLOG_MSG(LOG_LOW,"iircServerUserGet - unknown hServer: %d", hServer);
        return -1;
    }
    pUser = iircStateUserGet(&pSelf->state, hUser, &status);
    if(pUser == NULL) {
        return -1;
    }
    cargoIrcUserPack(cargo, pUser);
    return 1;
}

int servParentDisconnect(iircServer_t *pServ, commute_t *pCom)
{
    // TODO: err, netsplit.. eek, wtf do I do?
    socketSetDel(&pServ->sockets, &pCom->sock);
    socketClose(&pCom->sock);
    return 1;
}

int servServerDisconnect(iircServer_t *pServ, commute_t *pCom)
{
    // TODO: err, netsplit.. eek, wtf do I do?
    socketSetDel(&pServ->sockets, &pCom->sock);
    socketClose(&pCom->sock);
    return 1;
}

int servClientDisconnect(iircServer_t *pServ, commute_t *pCom)
{
    // TODO: delete any users associated with pCom
    // 2003-03-08 - hrm, iircStateCargoUser does this
    /*
       pUser = iircStateSrcUserGet(&pServ->state, pTruck, &status);
       if(pUser != NULL) {
       LOG_MSG(LOG_USER,"<server> client must be kicked.");
       iircStateUserDel(&pServ->state, pUser->id);
    // TODO send user disconnect notice upstream
    }
     */
    socketSetDel(&pServ->sockets, &pCom->sock);
    socketClose(&pCom->sock);
    return 1;
}

int servSocketCheck(iircServer_t *pServ, long timeout)
{
    int i;
    socketSet_t socketsReady;

    i = socketSetCheck(&pServ->sockets, timeout, &socketsReady);
    if(i == 0) /* no data to read */
        return 0;
    if(i < 0) {
        // TODO: I think I should be deleting failed socket?
        VLOG_MSG(LOG_SYS,"servSocketCheck - socketSetCheck failed %d", i);
        return -1;
    }
    //
    // get trucks from parent
    if(!pServ->isRoot && servSocketCheckParent(pServ, &socketsReady) < 0)
        return -1;
    //
    // check existing child servers
    if(pServ->listenForServers)
        if(servSocketCheckServers(pServ, &socketsReady) < 0)
            return -1;
    //
    // check existing clients
    if(pServ->listenForClients)
        if(servSocketCheckClients(pServ, &socketsReady) < 0)
            return -1;
    //
    // the fault should be obvious here.  I'm forcing the interrupts
    // to use the socket system.
    if(servInterruptCheck(pServ, &socketsReady) < 0)
        return -1;
    return 1;
}

int servSocketCheckParent(iircServer_t *pServ, socketSet_t *pReady)
{
    int status;
    truck *pTruck;
    commute_t *pCom = &pServ->parentCom;

    if(!socketSetReady(pReady, &pCom->sock))
        return 0;

    while(42)
    {
        pTruck = servCommuteReceive(pServ, pCom, &status);
        if(status <= 0 || pTruck == NULL) {
            if(status < 0)
                servParentDisconnect(pServ, pCom);
            return status; // no more trucks, or error
        }
        //
        // process this truck depending on it's cargo type
        status = PROC_IN_AUTHORITATIVE;
        if(pTruck->type < CARGO_IIRC_INTERNAL)
            servTruckInternal(pServ, IIRC_COM_REMOTE, pTruck, &status);
        else
            servTruckProcess(pServ, pTruck, &status); // <-- deep call
        //
        // cleanup state information if necessary
        if(status & PROC_OUT_DISCONNECT) {
            LOG_MSG(LOG_USER,"disconnecting server");
            servParentDisconnect(pServ, pCom);
        }
        if(!(status & PROC_OUT_TRUCK_USED))
            truckEnqueue(&pServ->parking, pTruck);
    }
    return 1;
}

int servSocketCheckServers(iircServer_t *pServ, socketSet_t *pReady)
{
    int hCom;
    int status;
    unsigned int i;
    unsigned int truckCount;
    commute_t *pCom;
    truck *pTruck;
    iircUser_t *pUser;
    socket_t socket;
    socketAddr_t sockAddr;
    //
    // check for new child servers
    if(socketSetReady(pReady, &pServ->serverConnect))
    {
        status = socketAccept(&pServ->serverConnect, &socket, &sockAddr);
        if(status < 0) {
            LOG_MSG(LOG_SYS,"servSocketCheckServers - server Accept failed");
            return -1;
        }
        LOG_MSG(LOG_USER,"servSocketCheckServers - new child server connect");
        hCom = serverComSet(Add)(&pServ->serverComs, NULL);
        if(hCom < 0) {
            LOG_MSG(LOG_LOW,"servSocketCheckServers - unable to add new com");
            return -1;
        }
        pCom = serverComSet(Get)(&pServ->serverComs, hCom);
        if(pCom == NULL)
            return -1;
        pCom->type = COMMUTE_SOCKET;
        pCom->localParking = &pServ->parking;
        socketCopy(&pCom->sock, &socket);
        socketOptionSet(&pCom->sock, SOCK_OPT_BLOCKING, 0);
        socketAddrCopy(&pCom->remoteSockAddr, &sockAddr);
        socketSetAdd(&pServ->sockets, &pCom->sock);
    }
    i = 0;
    pCom = (commute_t*)serverComSet(Incr)(&pServ->serverComs, &i);
    while(pCom != NULL)
    {
        if(!socketSetReady(pReady, &pCom->sock)) {
            // server's socket isn't ready, check next server socket
            pCom = (commute_t*)serverComSet(Incr)(&pServ->serverComs, &i);
            continue;
        }
        truckCount = 0;
        while(420)
        {
            // put a limit on the number of trucks we can read at
            // one time.  check if we have exceeded that value here
            if(truckCount >= MAX_SERVER_TRUCKS) {
                LOG_MSG(LOG_USER,"servSocketCheckServers - MAX_SERVER_TRUCKS");
                break; // don't read anymore trucks
            }
            // read one truck from this server
            pTruck = servCommuteReceive(pServ, pCom, &status);
            if(status <= 0 || pTruck == NULL) {
                if(status < 0) {
                    // do disconnect on this server.  We cannot continue
                    // because it will screw the out (Incr) up.
                    return servServerDisconnect(pServ, pCom);
                }
                break; // no trucks ready,  next server
            }
            truckCount++;
            //
            // process the truck depending on it's cargo type
            status = 0;
            if(pTruck->type < CARGO_IIRC_INTERNAL)
                servTruckInternal(pServ, IIRC_COM_REMOTE, pTruck, &status);
            else
                servTruckProcess(pServ, pTruck, &status);
            //
            // clean user state information if necessary
            if(status & PROC_OUT_DISCONNECT) {
                // disconnect child server
                return servServerDisconnect(pServ, pCom);
            }
            truckEnqueue(&pServ->parking, pTruck);
        } // while(life && universe && everything || (beer > 0))

        pCom = (commute_t*)serverComSet(Incr)(&pServ->serverComs, &i);
    } // while(pCom != NULL)
    return 1;
}

int servSocketCheckClients(iircServer_t *pServ, socketSet_t *pReady)
{
    int hCom;
    int status;
    unsigned int i;
    unsigned int truckCount;
    cargoIrcEvent event;
    cargoIrcModInfo *pModInfo;
    commute_t *pCom;
    iircUser_t *pUser;
    truck *pTruck;
    socket_t socket;
    socketAddr_t sockAddr;
    //
    // check for new child clients
    if(socketSetReady(pReady, &pServ->clientConnect))
    {
        status = socketAccept(&pServ->clientConnect, &socket, &sockAddr);
        if(status < 0) {
            LOG_MSG(LOG_SYS,"servSocketCheckClients - client Accept failed");
            return -1;
        }
        LOG_MSG(LOG_USER,"servSocketCheckClients - new client connection");
        hCom = clientComSet(Add)(&pServ->clientComs, NULL);
        if(hCom < 0) {
            LOG_MSG(LOG_LOW,"servSocketCheckClients - unable to add new com");
            return -1;
        }
        pCom = clientComSet(Get)(&pServ->clientComs, hCom);
        if(pCom == NULL)
            return -1;
        pCom->type = COMMUTE_SOCKET;
        pCom->localParking = &pServ->parking;
        socketCopy(&pCom->sock, &socket);
        socketOptionSet(&pCom->sock, SOCK_OPT_BLOCKING, 0);
        socketAddrCopy(&pCom->remoteSockAddr, &sockAddr);
        socketSetAdd(&pServ->sockets, &pCom->sock);
        // send supported modules to new client
        for(i = 0; i < MAX_IIRC_MODS; i++) {
            if(pServ->state.modules[i] == NULL)
                break;
            pModInfo = pServ->state.modules[i];
            servCommuteSend(pServ, pCom, (truck*)pModInfo);
        }
        // send event with the MODULES_READY code
        cargoIrcEventInit(&event);
        event.code = CODE_IIRC_MODS_READY;
        servCommuteSend(pServ, pCom, (truck*)&event);
    }
    i = 0;
    pCom = (commute_t*)clientComSet(Incr)(&pServ->clientComs, &i);
    while(pCom != NULL)
    {
        if(!socketSetReady(pReady, &pCom->sock)) {
            // client's socket isn't ready, check next client socket
            pCom = (commute_t*)clientComSet(Incr)(&pServ->clientComs, &i);
            continue;
        }
        truckCount = 0;
        while(420)
        {
            // put a limit on the number of trucks we can read at
            // one time.  check if we have exceeded that value here
            if(truckCount >= MAX_SERVER_TRUCKS) {
                LOG_MSG(LOG_USER,"servSocketCheckClients - MAX_SERVER_TRUCKS");
                break; // don't read anymore trucks
            }
            // read one truck from this client
            pTruck = servCommuteReceive(pServ, pCom, &status);
            if(status <= 0 || pTruck == NULL) {
                if(status < 0) {
                    // disconnect this client. Don't continue, because
                    // that will screw the (Incr) loop up.
                    return servClientDisconnect(pServ, pCom);
                }
                break;; // no trucks ready yet I guess.
            }
            truckCount++;
            //
            // process the truck depending on it's cargo type
            status = 0;
            if(pTruck->type < CARGO_IIRC_INTERNAL) {
                // TODO: ATTENTION.  extra security is needed here
                // because *Internal doesn't check source chan/user
                // TODO: store an 'id' value in commute and replace
                // 'i-1' with it.
                servTruckInternal(pServ, i-1, pTruck, &status);
            } else {
                servTruckProcess(pServ, pTruck, &status);
            }
            //
            // clean user state information if necessary
            if(status & PROC_OUT_DISCONNECT) {
                // disconnect child client
                return servClientDisconnect(pServ, pCom);
            }
            truckEnqueue(&pServ->parking, pTruck);
        } // while(life && universe && everything || (beer > 0))

        pCom = (commute_t*)clientComSet(Incr)(&pServ->clientComs, &i);
    } // while(pCom != NULL)
    return 1;
}

int servInterruptCheck(iircServer_t *pServ, socketSet_t *pReady)
{
    unsigned int i = 0;
    iircInterrupt_t *pIntr;

    pIntr = (iircInterrupt_t*)intrSet(Incr)(&pServ->interrupts, &i);
    while(pIntr != NULL)
    {
        if(socketSetReady(pReady, &pIntr->fd)) {
            //
            // interrupt is ready, so execute it's callback
            if(pIntr->fnCallback(pIntr->id, pIntr->data, pIntr->pData) < 0)
                iircServerIntrDel(pServ->id, pIntr->id);
            // TODO: InterruptDel is going to screw the Incr up.
        }
        pIntr = (iircInterrupt_t*)intrSet(Incr)(&pServ->interrupts, &i);
    }
    return 1;
}

truck* servCommuteReceive(iircServer_t *pServ, commute_t *pCom, int *pStatus)
{
    int status;
    truck *pTruck;

    // get a truck from the parent
    pTruck = commuteReceive(pCom, &status);
    if(pTruck == NULL) {
        *pStatus = status; // in case of error
        return NULL; // no trucks to read
    }
    truckFix(pTruck); // set truck header to host byte order
    // reject any odd data fields in the header
    if(!truckVerify(pTruck)) {
        // TODO disconnect
        *pStatus = -1;
        return NULL;
    }
    iircCargoFix(pTruck); // set iIRC cargo to host byte order
    if(pTruck->type != CARGO_IIRC_MODULE) {
        // else the module should do any transmission dumping
        VLOG_MSG(LOG_DUMP,"-->S %s", iircCargoNames[pTruck->type]);
    }
    *pStatus = 1;
    return pTruck;
}

int servCommuteSend(iircServer_t *pServ, commute_t *pCom, truck *pTruck)
{
    if(pTruck->type != CARGO_IIRC_MODULE) {
        // don't debug for the module
        VLOG_MSG(LOG_DUMP,"<--S %s", iircCargoNames[pTruck->type]);
    }
    return commuteSend(pCom, pTruck);
}

int servTruckInternal(SERV, mUint32 hCom, truck *pTruck, int *pStatus)
{
    cargoIrcEvent *pEvent;

    switch(pTruck->type)
    {
        case CARGO_IIRC_USER:
            return OnCargoIrcUser(pServ, hCom, (truck*)pTruck, pStatus);
        case CARGO_IIRC_CHAN:
            return OnCargoIrcChan(pServ, hCom, (truck*)pTruck, pStatus);
        case CARGO_IIRC_MOD_INFO:
            return OnCargoIrcModInfo(pServ, hCom, (truck*)pTruck, pStatus);
        case CARGO_IIRC_EVENT:
            pEvent = (cargoIrcEvent*)pTruck;
            if(pEvent->code == CODE_IIRC_MODS_READY)
                pServ->waitFlags &= ~IIRC_SERVER_WAIT_MODS;
            break;
    }
    *pStatus = 0;
    return 1;
}

int servTruckProcess(iircServer_t *pServ, truck *pTruck, int *pStatus)
{
    int status;
    unsigned int i;
    iircChan_t *pChan;
    iircModule_t *pMod;

    if(pTruck->type < CARGO_IIRC_INTERNAL) {
        VLOG_MSG(LOG_LOW,"servTruckProcess - cannot process internal type: %s",
                iircCargoNames[pTruck->type]);
        truckDump(pTruck);
        *pStatus = PROC_OUT_DISCONNECT;
        return -1;
    }
    iircStateLock(&pServ->state, FALSE); // lock state for read access
    //
    // get the channel structure for this truck's destination
    pChan = iircStateChanGet(&pServ->state, pTruck->dstChan, &status);
    if(pChan == NULL)
    {
        iircStateUnlock(&pServ->state, FALSE); //pChan
        //
        // well, the destination channel doesn't exist on this server.
        // But we might be able to continue if the truck is destined
        // to create the channel in question.
        if(pTruck->type != CARGO_IIRC_JOIN) {
            VLOG_MSG(LOG_LOW,"servTruckProcess - Invalid dst channel: %d",
                    pTruck->dstChan);
            truckDump(pTruck);
            *pStatus = PROC_OUT_DISCONNECT;
            return -1;
        }
        // truck could be creating a channel, so let it pass
        return servTruckHandler(pServ, pTruck, pStatus);
    }
    //
    // if this truck holds an iIRC cargo
    if(pTruck->type != CARGO_IIRC_MODULE)
    {
        // not headed for a specific module, so that means it is an
        // iIRC cargo and needs to be given to all modules which are
        // enabled on the channel.
        i = 0;
        pMod = iircStateChanModIncr(&pServ->state, pChan, &i);
        while(pMod != NULL) {
            //
            // send the truck to the module
            iircModuleSend(pMod, pTruck);
            // next module enabled on the channel
            pMod = iircStateChanModIncr(&pServ->state, pChan, &i);
        }
        // finally, the truck ends up at it's iIRC cargo handler
        iircStateUnlock(&pServ->state, FALSE); // pChan
        return servTruckHandler(pServ, pTruck, pStatus);
    }
    iircStateUnlock(&pServ->state, FALSE); // pChan
    //
    // If the truck made it here, then it is headed for one module
    // in particular.
    pMod = iircStateModuleGet(&pServ->state, pTruck->module);
    if(pMod == NULL) {
        VLOG_MSG(LOG_LOW,"servTruckProcess - %s",
                iircStateError(&pServ->state));
        *pStatus = PROC_OUT_DISCONNECT;
        return -1;
    }
    //
    // if the truck has passed the test; made the grade; climbed the
    // ladder; shot the sheriff; and consumed rice pudding.. send it to
    // the appropriate module's truck handler.
    iircModuleSend(pMod, pTruck);
    //
    // terminal design failure here.  the module has no way of telling
    // us if we should continue routing or not.  I have hard coded it
    // to not route.  The module should almost always echo it's trucks.
    // sorry.
    return 1;//servTruckDispatch(pServ, pTruck);
}

int servTruckHandler(iircServer_t *pServ, truck *pTruck, int *pStatus)
{
    int i;

    if(iircCargoVerify(pTruck) <= 0) {
        *pStatus = PROC_OUT_DISCONNECT; // close connection
        return -1;
    }
    if(pServ->isRoot || (*pStatus & PROC_IN_AUTHORITATIVE))
    {
        // this an authoritative request, so execute it
        //
        // preprocess the truck before sending to the iircState
        if(pServ->fnPreHandler[pTruck->type] != NULL) {
            iircStateLock(&pServ->state, FALSE);
            pServ->fnPreHandler[pTruck->type](pServ, pTruck, pStatus);
            iircStateUnlock(&pServ->state, FALSE);
        }
        //
        // update the iircState with the truck
        iircStateLock(&pServ->state, TRUE); // for writing
        i = iircStateCargoHandler(&pServ->state, pTruck);
        if(i < 0) {
            if(i <= IIRC_STATE_ERR_FATAL)
                *pStatus |= PROC_OUT_DISCONNECT; // close connection
            VLOG_MSG(LOG_LOW,"servTruckHandler - %s",
                    iircStateError(&pServ->state));
            iircStateUnlock(&pServ->state, TRUE);
            return -1;
        }
        iircStateUnlock(&pServ->state, TRUE);
        //
        // postprocess the truck after sending to the iircState
        if(pServ->fnPostHandler[pTruck->type] != NULL) {
            iircStateLock(&pServ->state, FALSE);
            pServ->fnPostHandler[pTruck->type](pServ, pTruck, pStatus);
            iircStateUnlock(&pServ->state, FALSE);
        }
    }
    else
        servCommuteSend(pServ, &pServ->parentCom, pTruck);

    return 1;
}

/*!
 *  The sole job of 'servTruckDispatch' is to read the
 *  destination user and channel fields of 'pTruck' and route the
 *  truck to the appropriate place.
 */
int servTruckDispatch(iircServer_t *pServ, truck *pTruck)
{
    unsigned int i;
    int status;
    commute_t *pCom;
    iircChan_t *pDstChan;
    iircUser_t *pSrcUser;
    iircUser_t *pDstUser;
    iircState_t *pState = &pServ->state;
    mUint32 hDstUser = pTruck->dstUser;
    mUint32 hDstChan = pTruck->dstChan;

    iircStateLock(pState, FALSE);
    pSrcUser = iircStateSrcUserGet(pState, pTruck, &status);
    if(pSrcUser == NULL) {
        VLOG_MSG(LOG_LOW,"servTruckDispatch - unknown source: [%d:%d]",
                pTruck->srcChan, pTruck->srcUser);
        iircStateUnlock(pState, FALSE);
        return -1;
    }
    //
    // hack the truck's size in the truck header.  What I want
    // is to chop the last string of each structure. (ie. iircCargoMsg)
    iircCargoChop(pTruck);
    //
    // Regardless of the trucks destination, we send the truck to all
    // child servers.
    i = 0;
    pCom = (commute_t*)serverComSet(Incr)(&pServ->serverComs, &i);
    while(pCom != NULL) {
        servCommuteSend(pServ, pCom, pTruck);
        pCom = (commute_t*)serverComSet(Incr)(&pServ->serverComs, &i);
    }
    //
    // determine truck's destination
    //
    if(hDstChan == IIRC_CHAN_BROADCAST)
    {
        /* TODO
           pTruck->dstCity = CITY_BROADCAST;
           pTruck->dstHouse = HOUSE_BROADCAST;
           cityTruckDispatch(pServ->hCity, pTruck);
         */
        // TODO: this is definately missing something. rfc1459 for one.
        ASSERT(0);
    }
    else
    {
        pDstChan = iircStateChanGet(pState, hDstChan, &status);
        if(pDstChan == NULL) {
            VLOG_MSG(LOG_LOW,"servTruckDispatch - unknown channel: %d",
                    hDstChan);
            iircStateUnlock(pState, FALSE);
            return -1;
        }
        if(hDstUser == IIRC_USER_BROADCAST)
        {
            //
            // increment to every user on the channel 'pDstChan'
            i = 0;
            pDstUser = iircStateChanUserIncr(pState, pDstChan, &i);
            while(pDstUser != NULL) {
                //
                // only send truck to clients on this server.
                // Also, do not broadcast back onto whoever sent the truck.
                if(!(pDstUser->flags & IIRC_USER_LOCAL))// ||
                    //         pDstUser->id == pSrcUser->id)
                {
                    pDstUser = iircStateChanUserIncr(pState, pDstChan, &i);
                    continue;
                }
                if(pDstUser->flags & IIRC_USER_1459) {
                    rfc1459TruckDispatch(pServ, pDstUser, pTruck);
                    pDstUser = iircStateChanUserIncr(pState, pDstChan, &i);
                    continue;
                }
                //
                // sending to a local iIRC client
                pCom = (commute_t*)serverComSet(Get)(&pServ->clientComs,
                        pDstUser->hCom);
                if(pCom == NULL) {
                    VLOG_MSG(LOG_LOW,"servTruckDispatch - unknown hCom: %d",
                            pDstUser->hCom);
                    // TODO: disconnect client?
                } else {
                    servCommuteSend(pServ, pCom, pTruck);
                }
                //
                // get next client on the channel 'pDstChan'
                pDstUser = iircStateChanUserIncr(pState, pDstChan, &i);
            }
        } else {
            //
            // get the targeted destination client
            pDstUser = iircStateChanUserGet(pState, pDstChan,
                    pTruck->dstUser, &status);
            if(pDstUser == NULL) {
                VLOG_MSG(LOG_LOW,"servTruckDispatch - unknown dst:[%d:%d]",
                        pTruck->dstChan, pTruck->dstUser);
                iircStateUnlock(pState, FALSE);
                return -1;
            }
            //
            // only send truck to clients on this server.
            // Also, do not echo back onto whoever sent the truck.
            if(!(pDstUser->flags & IIRC_USER_LOCAL))// ||
                //      pDstUser->id == pSrcUser->id)
            {
                iircStateUnlock(pState, FALSE);
                return 1;
            }
            if(pDstUser->flags & IIRC_USER_1459) {
                status = rfc1459TruckDispatch(pServ, pDstUser, pTruck);
                iircStateUnlock(pState, FALSE);
                return status;
            }
            //
            // sending to a local iIRC client
            pCom = (commute_t*)serverComSet(Get)(&pServ->clientComs,
                    pDstUser->hCom);
            if(pCom == NULL) {
                VLOG_MSG(LOG_LOW,"servTruckDispatch - unknown hCom: %d",
                        pDstUser->hCom);
                // TODO: disconnect client?
                iircStateUnlock(pState, FALSE);
                return -1;
            }
            status = servCommuteSend(pServ, pCom, pTruck);
            iircStateUnlock(pState, FALSE);
            return status;
        } //if(hDstUser == IIRC_USER_BROADCAST) { } else {
    } //if(hDstChan == IIRC_CHAN_BROADCAST) { } else {

    iircStateUnlock(pState, FALSE);
    return 1;
}

int iircServerTruckIdentify(SERV, truck *pTruck)
{
    int status;
    iircUser_t *pSrcUser;
    pTruck = (truck*)pTruck;

    if(pTruck->srcChan != pTruck->dstChan) {
        LOG_MSG(LOG_LOW,"iircServerTruckIdentify - srcChan != dstChan");
        return -1; //kick
    }
    iircStateLock(&pServ->state, FALSE);
    pSrcUser = iircStateSrcUserGet(&pServ->state, pTruck, &status);
    if(pSrcUser == NULL) {
        VLOG_MSG(LOG_LOW,"iircServerTruckIdentify - %s",
                iircStateError(&pServ->state));
        iircStateUnlock(&pServ->state, FALSE); //pSrcUser
        if(status <= IIRC_STATE_ERR_FATAL)
            return -1;
        return 0;
    }
    // TODO: compare IP addresses!
    iircStateUnlock(&pServ->state, FALSE); //pSrcUser
    return 1;
}

int iircEventDispatch(SERV, int chan, int user, int code, int data1)
{
    cargoIrcEvent event;

    cargoIrcEventInit(&event);
    event.header.dstChan = chan;
    event.header.dstUser = user;
    event.code = code;
    event.data1 = data1;
    servTruckDispatch(pServ, (truck*)&event);
    return 1;
}

int iircEventBroadcast(SERV, int chan, int code, int data1)
{
    cargoIrcEvent event;

    cargoIrcEventInit(&event);
    event.header.dstChan = chan;
    event.header.dstUser = IIRC_USER_BROADCAST;
    event.code = code;
    event.data1 = data1;
    servTruckDispatch(pServ, (truck*)&event);
    return 1;
}

int OnCargoIrcModInfo(SERV, mUint32 hCom, truck *pTruck, int *pStatus)
{
    int i;
    cargoIrcModInfo *pCargo = (cargoIrcModInfo*)pTruck;

    if(!cargoIrcModInfoVerify(pCargo)) {
        *pStatus = PROC_OUT_DISCONNECT;
        return -1;
    }
    if(!pServ->isRoot && !(*pStatus & PROC_IN_AUTHORITATIVE)) {
        *pStatus = 0;
        return 1;
    }
    //
    // update the iIRC state
    iircStateLock(&pServ->state, TRUE);
    i = iircStateCargoModInfo(&pServ->state, pCargo);
    iircStateUnlock(&pServ->state, TRUE);
    if(i < 0) {
        *pStatus = 0;
        if(i <= IIRC_STATE_ERR_FATAL)
            *pStatus |= PROC_OUT_DISCONNECT; // close connection
        VLOG_MSG(LOG_LOW,"OnCargoIrcModInfo - %s",
                iircStateError(&pServ->state));
        return -1;
    }
    return 1;
}

int OnCargoIrcUser(SERV, mUint32 hCom, truck *pTruck, int *pStatus)
{
    int i, status;
    //int hUserC
    //iircUser_t client;
    iircUser_t *pUser;
    commute_t *pCom;
    rawClient_t *pRawClient;
    cargoIrcUser *pCargo = (cargoIrcUser*)pTruck;

    VLOG_MSG(LOG_DUMP,"<server> got cargoIrcUser from %s", pCargo->nick);
    //LOG_MSG(LOG_LOW,"OnCargoIrcUser - truck:");
    //trckDump((truck*)pTruck);
    if(!pServ->isRoot && !(*pStatus & PROC_IN_AUTHORITATIVE)) {
        *pStatus = 0;
        return 1;
    }
    // TODO: verify hCom's user id == pUser->id
    // don't want users disconnecting each other.
    //
    if(pServ->isRoot && pCargo->flags & IIRC_USER_CONNECT)
        pCargo->id = -1; // we will pick a user id for it
    //
    // update the iIRC state
    iircStateLock(&pServ->state, TRUE); // write lock
    i = iircStateCargoUser(&pServ->state, pCargo);
    if(i < 0) {
        *pStatus = 0;
        if(i <= IIRC_STATE_ERR_FATAL)
            *pStatus |= PROC_OUT_DISCONNECT; // close connection
        //VLOG_MSG(LOG_LOW,"OnCargoIrcUser - %s",
        //iircStateError(&pServ->state));
        iircStateUnlock(&pServ->state, TRUE);
        return -1;
    }
    // do any extra-special processing needed
    pUser = iircStateUserGet(&pServ->state, i, &status);
    if(pUser == NULL) {
        *pStatus = 0;
        if(i <= IIRC_STATE_ERR_FATAL)
            *pStatus |= PROC_OUT_DISCONNECT; // close connection
        VLOG_MSG(LOG_LOW,"OnCargoIrcUser - %s",
                iircStateError(&pServ->state));
        iircStateUnlock(&pServ->state, TRUE);
        return -1;
    }
    if(pCargo->flags & IIRC_USER_CONNECT)
    {
        //
        // this user has just connected the network.  Whether it has
        // connected to this server directly is determined by 'hCom'.
        if(hCom != IIRC_COM_REMOTE) {
            //
            // User is connected to this server directly.
            pUser->hCom = hCom;
            if(pUser->flags & IIRC_USER_1459) {
                //
                // hCom should be a rawClient_t handle for RFC1459 connections
                pRawClient = (rawClient_t*)rawClientSet(Get)(&pServ->rfc1459Set,
                        hCom);
                if(pRawClient == NULL) {
                    VLOG_MSG(LOG_LOW,"OnCargoIrcUser - bad 1459 hCom %d", hCom);
                    *pStatus = PROC_OUT_DISCONNECT;
                    return -1;
                }
                // reverse reference used in rfc1459TruckDispatch
                pRawClient->hUser = pUser->id;
            } else {
                //
                // hCom should be a commute_t handle for iIRC connections
                pCom = (commute_t*)clientComSet(Get)(&pServ->clientComs, hCom);
                if(pCom == NULL) {
                    VLOG_MSG(LOG_LOW,"OnCargoIrcUser - bad hCom: %d", hCom);
                    *pStatus = PROC_OUT_DISCONNECT;
                    return -1;
                }
                // reverse reference used in servTruckDispatch
                pCom->pData = (void*)pUser->id;
            }
            pUser->flags |= IIRC_USER_LOCAL;
        } else {
            //
            // User must have connected to some other server
            pUser->flags &= ~IIRC_USER_LOCAL;
        }
        pUser->flags &= ~IIRC_USER_CONNECT;
        //
        // done modifying the user structure
        iircStateUnlock(&pServ->state, TRUE);
        pCargo->id = i; // tell client where to store itself
    } else {
        // Locking is the biggest pain in the ass
        iircStateUnlock(&pServ->state, TRUE);
    }
    if(pCargo->flags & IIRC_USER_DISCONNECT)
        *pStatus |= PROC_OUT_DISCONNECT;

    pCargo->header.srcUser = 0;
    pCargo->header.srcChan = 0;
    pCargo->header.dstUser = i;
    pCargo->header.dstChan = 0;
    return servTruckDispatch(pServ, (truck*)pCargo);
}

int OnCargoIrcChan(SERV, mUint32 hCom, truck *pTruck, int *pStatus)
{
    int status;
    iircChan_t *pChan;
    cargoIrcChan *pCargo = (cargoIrcChan*)pTruck;

    if(!pServ->isRoot && !(*pStatus & PROC_IN_AUTHORITATIVE)) {
        *pStatus = 0;
        return 1;
    }
    iircStateLock(&pServ->state, TRUE);
    status = iircStateCargoChan(&pServ->state, pCargo);
    iircStateUnlock(&pServ->state, TRUE);
    if(status < 0) {
        *pStatus = 0;
        if(status <= IIRC_STATE_ERR_FATAL)
            *pStatus |= PROC_OUT_DISCONNECT;
        return -1;
    }
    pCargo->header.dstUser = IIRC_USER_BROADCAST;
    pCargo->header.dstChan = pCargo->hChan;
    return servTruckDispatch(pServ, (truck*)pCargo);
}

int OnCargoIrcPrePart(SERV, truck *pTruck, int *pStatus)
{
    cargoIrcPart *pCargo = (cargoIrcPart*)pTruck;

    //
    // a part cargo needs to be sent to the client before the client
    // parts the channel.
    pTruck->dstUser = IIRC_USER_BROADCAST;
    pTruck->dstChan = pCargo->hChan;
    return servTruckDispatch(pServ, pTruck);
}

int OnCargoIrcPreQuit(SERV, truck *pTruck, int *pStatus)
{
    int status;
    unsigned int i;
    iircChan_t *pChan;
    iircUser_t *pUser;

    pUser = iircStateSrcUserGet(&pServ->state, pTruck, &status);
    if(pUser == NULL) {
        VLOG_MSG(LOG_LOW,"OnCargoIrcPreQuit - %s",
                iircStateError(&pServ->state));
        *pStatus = 0;
        if(status <= IIRC_STATE_ERR_FATAL)
            *pStatus |= PROC_OUT_DISCONNECT; // close connection
        return -1;
    }
    if(pServ->isRoot) {
        //
        // the root server is resposible for spliting up the quit
        // message to each channel the user is on.
        //
        pTruck->dstUser = IIRC_USER_BROADCAST;
        //
        // step to every channel the user is on
        i = 0;
        pChan = iircStateUserChanIncr(&pServ->state, pUser, &i);
        while(pChan != NULL) {
            //
            // send the quit truck to pChan
            pTruck->dstChan = pChan->id;
            servTruckDispatch(pServ, pTruck);
            //
            // next channel, please
            pChan = iircStateUserChanIncr(&pServ->state, pUser, &i);
        }
    } else {
        //
        // client servers just forward the quit cargo
        servTruckDispatch(pServ, pTruck);
    }
    return 1;
}

int OnCargoIrcPostEvent(SERV, truck *pTruck, int *pStatus)
{
    cargoIrcEvent *pEvnt = (cargoIrcEvent*)pTruck;
    switch(pEvnt->code) {
        case CODE_IIRC_NOP:
            return 1; // do nothin
        case CODE_IIRC_PING:
            return 1;
        case CODE_IIRC_PONG:
            return 1;
        default:
            return -1; // kick client
    }
    return 1;
}

int OnCargoIrcPostSys(SERV, truck *pTruck, int *pStatus)
{
    return 1;
}

int OnCargoIrcPostServer(SERV, truck *pTruck, int *pStatus)
{
    cargoIrcServer *pCargo = (cargoIrcServer*)pTruck;
    LOG_MSG(LOG_LOW,"OnCargoIrcPostServer - truck:");
    truckDump((truck*)pTruck);
    if(pCargo->flags) {
        /*  TODO: ?
            if(pCargo->flags & IIRC_SERVER_CONNECT)
            return OnIrcServerConnect(pServ, pTruck);
            if(pCargo->flags & IIRC_SERVER_DISCONNECT)
            return OnIrcServerDisconnect(pServ, pTruck);
         */
        return -1; // unknown flag set.
    } else {
        return -1; // dummy place holder
    }
    return 1;
}

int OnCargoIrcPostChanMode(SERV, truck *pTruck, int *pStatus)
{
    //    cargoIrcChanMode *pCargo = (cargoIrcChanMode*)pTruck;
    servTruckDispatch(pServ, pTruck);
    return 1;
}

int OnCargoIrcPostChanTopic(SERV, truck *pTruck, int *pStatus)
{
    //    cargoIrcChanTopic *pCargo = (cargoIrcChanTopic*)pTruck;
    servTruckDispatch(pServ, pTruck);
    return 1;
}

int OnCargoIrcPostChanUsers(SERV, truck *pTruck, int *pStatus)
{
    //    cargoIrcChanUsers *pCargo = (cargoIrcChanUsers*)pTruck;
    servTruckDispatch(pServ, pTruck);
    return 1;
}


/*
   int OnCargoIrcPostUsername(SERV, truck *pTruck, int *pStatus)
   {
   cargoIrcUsername *pCargo = (cargoIrcUsername*)pTruck;
// TODO: broadcast new client cargo
return 1;
}
 */
int OnCargoIrcPostNick(SERV, truck *pTruck, int *pStatus)
{
    //    cargoIrcNick *pCargo = (cargoIrcNick*)pTruck;
    //
    // first, send nick cargo to the user who changed her nick
    pTruck->dstChan = pTruck->srcChan;
    pTruck->dstUser = pTruck->srcUser;
    pTruck->srcChan = 0;
    pTruck->srcUser = 0;
    servTruckDispatch(pServ, pTruck);
    //
    // next send nick cargo to each channel she is on. TODO
    return 1;
}

int OnCargoIrcPostJoin(SERV, truck *pTruck, int *pStatus)
{
    unsigned int i;
    int status;
    iircUser_t *pSrcUser;
    iircUser_t *pUser;
    iircChan_t *pChan;
    cargoIrcJoin *pCargo = (cargoIrcJoin *)pTruck;
    //cargoIrcChan chanCargo;
    cargoIrcUser userCargo;
    cargoIrcUser srcUserCargo;
    cargoIrcChanUsers chanUsers;

    pSrcUser = iircStateSrcUserGet(&pServ->state, pTruck, &status);
    if(pSrcUser == NULL) {
        VLOG_MSG(LOG_LOW,"OnCargoIrcPostJoin - %s",
                iircStateError(&pServ->state));
        if(status <= IIRC_STATE_ERR_FATAL)
            *pStatus |= PROC_OUT_DISCONNECT; // close connection
        return -1;
    }
    pChan = iircStateChanGet(&pServ->state, pCargo->hChan, &status);
    if(pChan == NULL) {
        if(status <= IIRC_STATE_ERR_FATAL)
            *pStatus |= PROC_OUT_DISCONNECT; // close connection
        VLOG_MSG(LOG_LOW,"OnCargoIrcPostJoin - %s",
                iircStateError(&pServ->state));
        return -1;
    }
    VLOG_MSG(LOG_DUMP,"OnCargoIrcPostJoin - %s JOIN'd %s", pSrcUser->nick,
            pChan->name);
    //
    // send the joining user's cargo to all members of the target channel
    // if the channel member is on this server
    cargoIrcUserInit(&userCargo);
    cargoIrcUserPack(&srcUserCargo, pSrcUser);
    i = 0;
    pUser = iircStateChanUserIncr(&pServ->state, pChan, &i);
    while(pUser != NULL)
    {
        if(pUser->id != pSrcUser->id) {
            //
            // send the chan user's info to the joining user
            if(pSrcUser->flags & IIRC_USER_LOCAL) {
                cargoIrcUserPack(&userCargo, pUser);
                userCargo.flags |= IIRC_USER_CONNECT;
                userCargo.header.dstUser = pSrcUser->id;
                userCargo.header.dstChan = 0;
                servTruckDispatch(pServ, (truck*)&userCargo);
            }
            //
            // send joining user's info to the chan user
            if(pUser->flags & IIRC_USER_LOCAL) {
                srcUserCargo.flags |= IIRC_USER_CONNECT;
                srcUserCargo.header.dstUser = pUser->id;
                srcUserCargo.header.dstChan = 0;
                servTruckDispatch(pServ, (truck*)&srcUserCargo);
            }
        }
        pUser = iircStateChanUserIncr(&pServ->state, pChan, &i);
    }
    //
    // send Join cargo to other servers and every1 on the chan
    pTruck->dstChan = pChan->id;
    pTruck->dstUser = IIRC_USER_BROADCAST;
    servTruckDispatch(pServ, pTruck);
    //
    // if the joining user is directly connected to this server,
    // send her a list of users on the joined channel.
    if(pSrcUser->flags & IIRC_USER_LOCAL)
    {
        cargoIrcChanUsersInit(&chanUsers);
        chanUsers.flags |= IIRC_CHAN_USERS_NAME;
        chanUsers.hChan = pChan->id;
        chanUsers.header.srcChan = 0;
        chanUsers.header.srcUser = 0;
        chanUsers.header.dstChan = 0;
        chanUsers.header.dstUser = pSrcUser->id;
        i = 0;
        pUser = iircStateChanUserIncr(&pServ->state, pChan, &i);
        while(pUser != NULL) {
            chanUsers.hUser[chanUsers.nUser] = i-1;  // TODO: kludge
            chanUsers.hClient[chanUsers.nUser] = pUser->id;
            chanUsers.nUser++;
            if(chanUsers.nUser >= MAX_IIRC_CHAN_USERS) {
                servTruckDispatch(pServ, (truck*)&chanUsers);
                chanUsers.nUser = 0;
            }
            pUser = iircStateChanUserIncr(&pServ->state, pChan, &i);
        }
        chanUsers.flags |= IIRC_CHAN_USERS_LAST; // last cargoChanUsers
        servTruckDispatch(pServ, (truck*)&chanUsers);
    }
    return 1;
}

int OnCargoIrcPostWho(SERV, truck *pTruck, int *pStatus)
{
    int status;
    unsigned int i;
    //mUint32 nickName[MAX_IIRC_NICK];
    mUint8 chanName[MAX_IIRC_CHAN_NAME];
    iircChan_t *pChan;
    iircUser_t *pUser;
    iircUser_t *pSrcUser;
    cargoIrcWho *pCargo = (cargoIrcWho*)pTruck;
    cargoIrcChanUsers chanUsers;
    cargoIrcUser user;

    pSrcUser = iircStateSrcUserGet(&pServ->state, pTruck, &status);
    if(pSrcUser == NULL) {
        VLOG_MSG(LOG_LOW,"OnCargoIrcPostWho - %s",
                iircStateError(&pServ->state));
        *pStatus = 0;
        if(status <= IIRC_STATE_ERR_FATAL)
            *pStatus = PROC_OUT_DISCONNECT;
        return -1;
    }
    VLOG_MSG(LOG_USER,"OnCargoIrcPostWho - %s has WHO'd %s", pSrcUser->nick,
            pCargo->name);
    //
    // find out if this is a channel name
    if(chanNameVerify(pCargo->name)) {
        //
        // copy channel name to proper sized buffer, and retrieve pChan
        unicode_cpy(chanName, pCargo->name, UTF8, MAX_IIRC_CHAN_NAME);
        pChan = iircStateChanFind(&pServ->state, chanName, &status);
        if(pChan == NULL) {
            *pStatus = 0;
            if(status <= IIRC_STATE_ERR_FATAL)
                *pStatus |= PROC_OUT_DISCONNECT; // close connection
            VLOG_MSG(LOG_LOW,"OnCargoIrcPostWho - %s",
                    iircStateError(&pServ->state));
            return -1;
        }
        //
        // send a list of clients on the channel to the client
        cargoIrcChanUsersInit(&chanUsers);
        if(pCargo->flags & IIRC_WHO_FLAG_NAMES)
            chanUsers.flags |= IIRC_CHAN_USERS_NAME;
        else
            chanUsers.flags |= IIRC_CHAN_USERS_WHO;
        chanUsers.hChan = pChan->id;
        chanUsers.header.srcChan = 0;
        chanUsers.header.srcUser = 0; // TODO: how do I send from server?
        chanUsers.header.dstChan = pTruck->srcChan;
        chanUsers.header.dstUser = pTruck->srcUser;
        i = 0;
        pUser = iircStateChanUserIncr(&pServ->state, pChan, &i);
        while(pUser != NULL) {
            //
            // send a cargoIrcUser for each user on the channel
            cargoIrcUserPack(&user, pUser);
            user.flags |= IIRC_USER_CONNECT;
            user.header.dstUser = pSrcUser->id;
            user.header.dstChan = 0;
            servTruckDispatch(pServ, (truck*)&user);
            //
            // enter the user's handles into the chanUser cargo
            chanUsers.hUser[chanUsers.nUser] = i-1;  // TODO: kludge
            chanUsers.hClient[chanUsers.nUser] = pUser->id;
            chanUsers.nUser++;
            if(chanUsers.nUser >= MAX_IIRC_CHAN_USERS) {
                // ship the cargo when the chanUser limit is reached,
                // then start over with a fresh one.
                servTruckDispatch(pServ, (truck*)&chanUsers);
                chanUsers.nUser = 0;
            }
            pUser = iircStateChanUserIncr(&pServ->state, pChan, &i);
        }
        chanUsers.flags |= IIRC_CHAN_USERS_LAST; // last cargoChanUsers
        servTruckDispatch(pServ, (truck*)&chanUsers);
    } else {
        // not who'ing a channel
    }
    return 1;
}

int OnCargoIrcPostUserMode(SERV, truck *pTruck, int *pStatus)
{
    servTruckDispatch(pServ, pTruck);
    return 1;
}

int OnCargoIrcPostChanModArg(SERV, truck *pTruck, int *pStatus)
{
    cargoIrcChanModArg *pCargo = (cargoIrcChanModArg*)pTruck;
    iircModule_t *pMod;

    LOG_MSG(LOG_DUMP,"OnCargoIrcPostChanModArg - got cargoIrcChanModArg");

    if(pCargo->function == IIRC_MOD_FUNC_ENABLE) {
        //
        // If this is the case, then the module has not been given any
        // notice of being enabled yet.  As a special case, send it to
        // the module now.  The module will then allocate resources
        // for the channel and begin execution.
        //
        pMod = iircStateModuleGet(&pServ->state, pCargo->hMod);
        if(pMod == NULL) {
            LOG_MSG(LOG_DUMP,"OnCargoIrcPostChanModArg - argh! unknown hMod.");
            *pStatus = 0;
            return -1;
        }
        iircModuleSend(pMod, (truck*)pTruck);
    }
    //
    // send cargo to other servers and every1 on the chan
    pTruck->dstChan = pTruck->srcChan;
    pTruck->dstUser = IIRC_USER_BROADCAST;
    servTruckDispatch(pServ, pTruck);
    return 1;
}

int OnCargoIrcPostUserModArg(SERV, truck *pTruck, int *pStatus)
{
    //    cargoIrcUserModArg *pCargo = (cargoIrcUserModArg*)pTruck;

    LOG_MSG(LOG_DUMP,"OnCargoIrcPostUserModArg - got cargoIrcUserModArg");
    //
    // send cargo to other servers and every1 on the chan
    pTruck->dstChan = pTruck->srcChan;
    pTruck->dstUser = IIRC_USER_BROADCAST;
    servTruckDispatch(pServ, pTruck);
    return 1;
}

int OnCargoIrcPostChanBan(SERV, truck *pTruck, int *pStatus)
{
    LOG_MSG(LOG_DUMP,"OnCargoIrcPostChanBan - got cargoIrcChanBan");
    return 1;
}

int OnCargoIrcPostMsg(SERV, truck *pTruck, int *pStatus)
{
    int status;
    iircUser_t *pUser;
    cargoIrcMsg *pCargo = (cargoIrcMsg*)pTruck;

    pUser = iircStateSrcUserGet(&pServ->state, pTruck, &status);
    if(pUser == NULL) {
        *pStatus = PROC_OUT_DISCONNECT;
        VLOG_MSG(LOG_LOW,"<server> %s", iircStateError(&pServ->state));
        return -1;
    }
    VLOG_MSG(LOG_USER, "<%s> %s", pUser->nick, pCargo->msg);

    pTruck->dstUser = IIRC_USER_BROADCAST;
    servTruckDispatch(pServ, pTruck);
    return 1;
}

int rfc1459ServerCallback(unsigned int hIntr, int data, void *pData)
{
    int i, hClient, hServer;
    char serverResponse[RFC1459_MSG_MAX];
    iircInterruptDescr intrDesc;
    iircServer_t *pServ;
    rawClient_t *pClient;
    socket_t clientSock;
    socketAddr_t clientAddr;

    hServer = data;
    pServ = (iircServer_t*)iircServerSet(Get)(&g_serverSet, hServer);
    if(pServ == NULL) {
        VLOG_MSG(LOG_LOW,"rfc1459ServerCallback - unknown hServer: %d",hServer);
        return -1;
    }
    i = socketAccept(&pServ->rfc1459Sock, &clientSock, &clientAddr);
    if(i < 0) {
        LOG_MSG(LOG_LOW,"rfc1459ServerCallback - socketAccept failed");
        return -1;
    }
    // set client socket to non-blocking
    socketOptionSet(&clientSock, SOCK_OPT_BLOCKING, 0);
    hClient = rawClientSet(Add)(&pServ->rfc1459Set, NULL);
    if(hClient < 0) {
        LOG_MSG(LOG_LOW,"rfc1459ServerCallback - failed to add new client");
        return -1;
    }
    pClient = (rawClient_t*)rawClientSet(Get)(&pServ->rfc1459Set, hClient);
    if(pClient == NULL) {
        LOG_MSG(LOG_LOW,"rfc1459ServerCallback - failed to get new client");
        return -1;
    }
    VLOG_MSG(LOG_DUMP,"Got RFC1459 connection (new RAW client): %d", hClient);
    pClient->id = hClient;
    pClient->pServer = pServ;
    pClient->sock = clientSock;
    intrDesc.version = IIRC_INTR_DESCR_VERSION;
    intrDesc.fd = (unsigned int)clientSock;
    intrDesc.fnCallback = rfc1459ClientCallback;
    intrDesc.data = pServ->id;
    intrDesc.pData = (void*)pClient->id;
    pClient->hInterrupt = iircServerIntrAdd(pServ->id, &intrDesc);
    if(pClient->hInterrupt < 0) {
        LOG_MSG(LOG_LOW,"rfc1459ServerCallback - interrupt add failed");
        rawClientSet(Del)(&pServ->rfc1459Set, hClient);
        return -1;
    }
    strncpy(serverResponse, "NOTICE AUTH :*** Skipping authentication.\r\n",
            RFC1459_MSG_MAX);
    socketWrite(&clientSock, serverResponse, strlen(serverResponse));
    return 1;
}

int rfc1459ClientCallback(unsigned int hIntr, int data, void *pData)
{
    int hServer;
    char msg[RFC1459_MSG_MAX];
    int msgLen, hClient;
    rawClient_t *pClient;
    iircServer_t *pServ;

    hClient = (int)pData;
    hServer = data;
    pServ = (iircServer_t*)iircServerSet(Get)(&g_serverSet, hServer);
    if(pServ == NULL) {
        VLOG_MSG(LOG_LOW,"rfc1459ClientCallback - unknown hServer: %d",hServer);
        return -1;
    }
    pClient = (rawClient_t*)rawClientSet(Get)(&pServ->rfc1459Set, hClient);
    if(pClient == NULL) {
        LOG_MSG(LOG_LOW,"rfc1459ClientCallback - failed to get source client");
        return -1;
    }
    msgLen = socketRead(&pClient->sock, msg, RFC1459_MSG_MAX);
    if(msgLen <= 0) {
        if(msgLen < 0) {
            iircStateUserDel(&pServ->state, pClient->hUser);
            rawClientSet(Del)(&pServ->rfc1459Set, hClient);
        }
        return 0;
    }
    if(msgLen >= RFC1459_MSG_MAX)
        msgLen = RFC1459_MSG_MAX - 1;
    msg[msgLen] = '\0';
    VLOG_MSG(LOG_DUMP,"<client> %s", msg);
    if(rfc1459MsgHandler(pServ, pClient, msg) < 0) {
        //
        // delete this client and delete it's interrupt
        iircStateUserDel(&pServ->state, pClient->hUser);
        rawClientSet(Del)(&pServ->rfc1459Set, hClient);
    }
    return 1;
}

int iircSCmdSplit(mUint32 *cmd, mUint32 *params[MAX_IIRC_SCMD_PARAMS])
{
    unsigned int i;
    unsigned int cmdLen;
    mUint32 *curParam = cmd;
    mUint32 param[MAX_IIRC_SERVER_CMD];

    for(i = 0; i < MAX_IIRC_SCMD_PARAMS; i++)
        params[i] = NULL;
    cmdLen = unicode_len(cmd, UCS4);
    if(!cmdLen)
        return IIRC_SCMD_NOP;  // no more messages
    i = 0;
    while(i < cmdLen && curParam[i] != '\n')
        i++;
    if(i < cmdLen) {
        // strip carriage-return and newline characters
        if(i > 0 && curParam[i-1] == '\r')
            curParam[i-1] = 0; // kill the carriage-return character
        else
            curParam[i] = 0;
    }
    params[0] = curParam;
    //
    // split the parameters
    i = 1;
    while(i < MAX_IIRC_SCMD_PARAMS &&
            unicode_tok(param, &curParam, ' ', MAX_IIRC_SERVER_CMD))
    {
        // insert NUL's in between parameters
        cmdLen = unicode_len(param, UCS4);
        params[i-1][cmdLen] = 0;
        // set parameter location
        params[i] = curParam;
        i++;
    }
    return i;
}

int OnSCmdNop(iircServer_t *pSelf, mUint32 *cmd)
{
    return 1;
}

int OnSCmdHostname(iircServer_t *pSelf, mUint32 *cmd)
{
    mUint32 *params[MAX_IIRC_SCMD_PARAMS];

    if(iircSCmdSplit(cmd, params) <= 0)
        return -1;
    if(params[1] == NULL) {
        LOG_MSG(LOG_WARN,"OnSCmdHostname - <hostname> required");
        return -1;
    }
    unicode_encode(pSelf->hostname, params[1], UTF8, MAX_HOST_NAME);
    VLOG_MSG(LOG_USER,"Setting server hostname to <%s>", pSelf->hostname);
    return 1;
}

int OnSCmdServername(iircServer_t *pSelf, mUint32 *cmd)
{
    mUint32 len;
    mUint32 *server_name;

    len = unicode_len(cmd, UCS4);
    if(len < 12) // strlen("SERVERNAME ") + 1
        return -1; // should never happen
    if(cmd[len-1] == '\n')
        cmd[len-1] = '\0';
    server_name = cmd + 11;
    unicode_encode(pSelf->name, server_name, UTF8, MAX_IIRC_SERVER_NAME);
    VLOG_MSG(LOG_USER,"Setting servername to <%s>", pSelf->name);
    return 1;
}

int OnSCmdLoad(iircServer_t *pSelf, mUint32 *cmd)
{
    iircModule_t mod;
    mUint32 *params[MAX_IIRC_SCMD_PARAMS];

    if(iircSCmdSplit(cmd, params) <= 0)
        return -1;
    if(params[1] == NULL) {
        LOG_MSG(LOG_WARN,"OnSCmdLoad - module type required");
        return -1;
    }
    if(params[2] == NULL) {
        LOG_MSG(LOG_WARN,"OnSCmdLoad - module location required");
        return -1;
    }
    iircModuleInit(&mod);
    mod.version = IIRC_MOD_VERSION;
    //
    // determine module type
    switch(params[1][0]) {
        case 'l':
            if(params[1][1] == 'i' && params[1][2] == 'b')
                mod.locationType = IIRC_MOD_LIB;
            break;
        case 'e':
            if(params[1][1] == 'x' && params[1][2] == 'e') {
#ifdef HAVE_METRIC_PIPES
                mod.locationType = IIRC_MOD_EXE;
#else
                LOG_MSG(LOG_LOW,"OnSCmdLoad - libmetric needs pipe support.");
                return -1;
#endif
            }
            break;
        default:
            VLOG_MSG(LOG_WARN,"OnSCmdLoad - unknown mod type: %s", params[1]);
            return -1;
    }
    //
    // copy module location
    unicode_encode(mod.modPath, params[2], UTF8, PATH_MAX);
    //
    // retrieve module's name and information/truck size etc.
    if(iircModuleQuery(&mod) < 0)
        return -1;
    iircStateUnlock(&pSelf->state, FALSE); // need to write to the state
    iircServerModuleAdd(pSelf->id, &mod);
    iircStateLock(&pSelf->state, FALSE);
    return 1;
}

/*
   int OnSCmdStart(iircServer_t *pSelf, mUint32 *cmd)
   {
   long timeo = DEFAULT_SERVER_TIMEOUT;
   mUint32 *params[MAX_IIRC_SCMD_PARAMS];
   char szTimeo[11];

   if(iircSCmdSplit(cmd, params) <= 0)
   return -1;
   if(params[1] != NULL) {
   unicode_encode(szTimeo, params[1], UTF8, sizeof(szTimeo));
   timeo = atol(szTimeo);
   }
   if(iircServerStart(pSelf->id, timeo) < 0)
   return -1;
   return 1;
   }
 */

int OnSCmdQuit(iircServer_t *pSelf, mUint32 *cmd)
{
    return 1;
}
