
/*
 * Copyright (c) 2001-2002,  James D. Taylor.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * contact James D. Taylor:  james.d.taylor@gmail.com
 */

#ifndef IIRC_STRUCT_INTR_H
#define IIRC_STRUCT_INTR_H

#include "iirc/common.h"
#include "metric/set.h"

#define intrSet_t   genericIncrArray
#define intrSet(op) genericIncrArray##op

typedef int (*iircInterruptFunc)(unsigned int hIntr, int data, void *pData);

typedef struct _iircInterruptDescr {
    int version;
    unsigned int fd;
    iircInterruptFunc fnCallback;
    int data;
    void *pData;
} iircInterruptDescr;
#define IIRC_INTR_DESCR_VERSION 0x00000100

typedef struct iircInterrupt {
    unsigned int id;    //! handle to this interrupt
    unsigned int fd; //! file descriptor used to wait on (used in 'select')
    iircInterruptFunc fnCallback; //! function to call when 'fd' is active
    int data;
    void *pData;
} iircInterrupt_t;

extern genericInfo_t interruptInfo;

#endif


