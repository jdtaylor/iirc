
/*
 * Copyright (c) 2001-2002,  James D. Taylor.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * contact James D. Taylor:  james.d.taylor@gmail.com
 */

#include <stdio.h>
#include "iirc/cargo.h"
#include "metric/char.h"

iircCargoVerify_t g_iircCargoVerify[MAX_IIRC_CARGO_TYPE];
iircCargoByteSwap_t g_iircCargoByteSwap[MAX_IIRC_CARGO_TYPE];
iircCargoChop_t g_iircCargoChop[MAX_IIRC_CARGO_TYPE];

char iircCargoNames[MAX_IIRC_CARGO_TYPE][32] =
{
    "CARGO_IIRC_USER",
    "CARGO_IIRC_CHAN",
    "CARGO_IIRC_MOD_INFO",
    "CARGO_IIRC_EVENT",
    "CARGO_IIRC_SYS",
    "CARGO_IIRC_SERVER",
    "CARGO_IIRC_MODULE",
    "CARGO_IIRC_CHAN_MODE",
    "CARGO_IIRC_CHAN_TOPIC",
    "CARGO_IIRC_CHAN_USERS",
//    "CARGO_IIRC_USERNAME",
    "CARGO_IIRC_NICK",
    "CARGO_IIRC_JOIN",
    "CARGO_IIRC_PART",
    "CARGO_IIRC_QUIT",
    "CARGO_IIRC_WHO",
    "CARGO_IIRC_USER_MODE",
    "CARGO_IIRC_CHAN_BAN",
    "CARGO_IIRC_MSG",
    "CARGO_IIRC_CHAN_MOD_ARG",
    "CARGO_IIRC_USER_MOD_ARG",
};


int cargoIrcChanInit(cargoIrcChan *pCargo)
{
    truckInit(&pCargo->header);
    pCargo->header.type = CARGO_IIRC_CHAN;
    pCargo->header.size = sizeof(cargoIrcChan);
    pCargo->hChan = 0;
    pCargo->name[0] = 0;
    pCargo->flags = 0;
    return 1;
}

int cargoIrcChanVerify(cargoIrcChan *pCargo)
{
    if(pCargo->header.type != CARGO_IIRC_CHAN) {
        VLOG_MSG(LOG_LOW,"cargoIrcChanVerify - %d: isn't CARGO_IIRC_CHAN",
                pCargo->header.type);
        return 0;
    }
    if(pCargo->header.size != sizeof(cargoIrcChan)) {
        VLOG_MSG(LOG_LOW,"cargoIrcChanVerify - cargo size mismatch: %d != %d",
                pCargo->header.size, sizeof(cargoIrcChan));
        return 0;
    }
    pCargo->name[MAX_IIRC_CHAN_NAME-1] = 0;
    if(unicode_verify(pCargo->name, UTF8, MAX_IIRC_CHAN_NAME) <= 0) {
        LOG_MSG(LOG_LOW,"cargoIrcChanVerify - couldn't verify channel name");
        return 0;
    }
    return 1;
}

int cargoIrcChanByteSwap(cargoIrcChan *pCargo)
{
    pCargo->hChan = BYTESWAP32(pCargo->hChan);
    pCargo->flags = BYTESWAP32(pCargo->flags);
    //    unicode_byteSwap(pCargo->name, UTF8, MAX_IIRC_CHAN_NAME);
    return 1;
}

int cargoIrcChanPack(cargoIrcChan *pCargo, iircChan_t *pChan)
{
    cargoIrcChanInit(pCargo);
    pCargo->hChan = pChan->id;
    //TODO: verify pCargo->name as a valid channel name
    unicode_cpy(pCargo->name, pChan->name, UTF8, MAX_IIRC_CHAN_NAME);
    pCargo->flags = pChan->flags;
    return 1;
}

int cargoIrcChanUnpack(cargoIrcChan *pCargo, iircChan_t *pChan)
{
    //    iircChanInit(pChan);
    pChan->id = pCargo->hChan;
    //TODO: verify pCargo->name as a valid channel name
    unicode_cpy(pChan->name, pCargo->name, UTF8, MAX_IIRC_CHAN_NAME);
    pChan->flags = pCargo->flags;
    return 1;
}

int cargoIrcUserInit(cargoIrcUser *pCargo)
{
    truckInit(&pCargo->header);
    pCargo->header.type = CARGO_IIRC_USER;
    pCargo->header.size = sizeof(cargoIrcUser);
    pCargo->id = -1;
    pCargo->nick[0] = 0;
    pCargo->identity[0] = 0;
    pCargo->hostname[0] = 0;
    pCargo->servername[0] = 0;
    pCargo->realname[0] = 0;
    pCargo->flags = 0;
    pCargo->modes = 0;
    return 1;
}

int cargoIrcUserVerify(cargoIrcUser *pCargo)
{
    if(pCargo->header.type != CARGO_IIRC_USER) {
        VLOG_MSG(LOG_LOW,"cargoIrcUserVerify - %d: isn't CARGO_IIRC_USER",
                pCargo->header.type);
        return 0;
    }
    if(pCargo->header.size != sizeof(cargoIrcUser)) {
        VLOG_MSG(LOG_LOW,"cargoIrcUserVerify - cargo size mismatch: %d != %d",
                pCargo->header.size, sizeof(cargoIrcUser));
        return 0;
    }
    pCargo->nick[MAX_IIRC_NICK-1] = 0;
    pCargo->identity[MAX_IIRC_IDENTITY-1] = 0;
    pCargo->hostname[MAX_HOST_NAME-1] = 0;
    pCargo->servername[MAX_HOST_NAME-1] = 0;
    pCargo->realname[MAX_IIRC_REAL_NAME-1] = 0;
    if(unicode_verify(pCargo->nick, UTF8, MAX_IIRC_NICK) <= 0) {
        LOG_MSG(LOG_LOW,"cargoIrcUserVerify - couldn't verify nick");
        return 0;
    }
    if(unicode_verify(pCargo->realname, UTF8, MAX_IIRC_REAL_NAME) <= 0)
    {
        LOG_MSG(LOG_LOW,"cargoIrcUserVerify - couldn't verify nick");
        return 0;
    }
    return 1;
}

int cargoIrcUserByteSwap(cargoIrcUser *pCargo)
{
    pCargo->id = BYTESWAP32(pCargo->id);
    //    unicode_byteSwap(pCargo->nick, UTF8, MAX_IIRC_NICK);
    //    unicode_byteSwap(pCargo->realname, UTF8, MAX_IIRC_REAL_NAME);
    pCargo->modes = BYTESWAP32(pCargo->modes);
    pCargo->flags = BYTESWAP32(pCargo->flags);
    return 1;
}

int cargoIrcUserPack(cargoIrcUser *pCargo, iircUser_t *pUser)
{
    cargoIrcUserInit(pCargo);
    pCargo->id = pUser->id;
    unicode_cpy(pCargo->nick, pUser->nick, UTF8, MAX_IIRC_NICK);
    strncpy(pCargo->identity, pUser->identity, MAX_IIRC_IDENTITY);
    strncpy(pCargo->hostname, pUser->hostname, MAX_HOST_NAME);
    strncpy(pCargo->servername, pUser->servername, MAX_HOST_NAME);
    unicode_cpy(pCargo->realname, pUser->realname, UTF8, MAX_IIRC_REAL_NAME);
    pCargo->identity[MAX_IIRC_IDENTITY-1] = 0;
    pCargo->hostname[MAX_HOST_NAME-1] = 0;
    pCargo->servername[MAX_HOST_NAME-1] = 0;
    pCargo->flags = pUser->flags;
    pCargo->modes = pUser->modes;
    return 1;
}

int cargoIrcUserUnpack(cargoIrcUser *pCargo, iircUser_t *pUser)
{
    //    iircUserInit(pUser);
    unicode_cpy(pUser->nick, pCargo->nick, UTF8, MAX_IIRC_NICK);
    strncpy(pUser->identity, pCargo->identity, MAX_IIRC_IDENTITY);
    strncpy(pUser->hostname, pCargo->hostname, MAX_HOST_NAME);
    strncpy(pUser->servername, pCargo->servername, MAX_HOST_NAME);
    unicode_cpy(pUser->realname, pCargo->realname, UTF8, MAX_IIRC_REAL_NAME);
    pUser->identity[MAX_IIRC_IDENTITY-1] = '\0';
    pUser->hostname[MAX_HOST_NAME-1] = '\0';
    pUser->servername[MAX_HOST_NAME-1] = '\0';
    pUser->modes = pCargo->modes;
    pUser->flags = pCargo->flags;
    return 1;
}

int cargoIrcModInfoInit(cargoIrcModInfo *pCargo)
{
    truckInit(&pCargo->header);
    pCargo->header.type = CARGO_IIRC_MOD_INFO;
    pCargo->header.size = sizeof(cargoIrcModInfo);
    pCargo->version = 0; // TODO:
    pCargo->bSupported = FALSE;
    pCargo->global_id = -1;
    pCargo->local_id = -1;
    pCargo->truckSize = 0;
    pCargo->name[0] = 0;
    pCargo->info[0] = 0;
    return 1;
}

int cargoIrcModInfoVerify(cargoIrcModInfo *pCargo)
{
    if(pCargo->header.type != CARGO_IIRC_MOD_INFO) {
        VLOG_MSG(LOG_LOW,"cargoIrcModInfoVerify - %d != CARGO_IIRC_MOD_INFO",
                pCargo->header.type);
        return 0;
    }
    if(pCargo->header.size != sizeof(cargoIrcModInfo)) {
        VLOG_MSG(LOG_LOW,"cargoIrcModInfoVerify - cargo size mismatch %d != %d",
                pCargo->header.size, sizeof(cargoIrcModInfo));
        return 0;
    }
    unicode_verify(pCargo->name, UTF8, MAX_IIRC_MOD_NAME);
    unicode_verify(pCargo->info, UTF8, MAX_IIRC_MOD_INFO);
    return 1;
}

int cargoIrcModInfoByteSwap(cargoIrcModInfo *pCargo)
{
    pCargo->version = BYTESWAP32(pCargo->version);
    pCargo->bSupported = BYTESWAP32(pCargo->bSupported);
    pCargo->global_id = BYTESWAP32(pCargo->global_id);
    //! this isnt really necessary because local_id's change across servers
    //pCargo->local_id = BYTESWAP32(pCargo->local_id);
    unicode_byteSwap(pCargo->name, UTF8, MAX_IIRC_MOD_NAME);
    unicode_byteSwap(pCargo->info, UTF8, MAX_IIRC_MOD_INFO);
    pCargo->truckSize = BYTESWAP32(pCargo->truckSize);
    return 1;
}

int cargoIrcModInfoCopy(cargoIrcModInfo *pDst, cargoIrcModInfo *pSrc)
{
    truckInit(&pDst->header);
    pDst->header.type = CARGO_IIRC_MOD_INFO;
    pDst->header.size = sizeof(cargoIrcModInfo);
    pDst->version = pSrc->version;
    pDst->bSupported = pSrc->bSupported;
    pDst->global_id = pSrc->global_id;
    pDst->local_id = pSrc->local_id;
    pDst->truckSize = pSrc->truckSize;
    unicode_cpy(pDst->name, pSrc->name, UTF8, MAX_IIRC_MOD_NAME);
    unicode_cpy(pDst->info, pSrc->info, UTF8, MAX_IIRC_MOD_INFO);
    return 1;
}

int cargoIrcModInfoPack(cargoIrcModInfo *pCargo, iircModule_t *pMod)
{
    pCargo->version = 0;
    pCargo->bSupported = FALSE;
    pCargo->global_id = pMod->global_id;
    pCargo->local_id = pMod->local_id;
    pCargo->truckSize = pMod->truckSize;
    unicode_cpy(pCargo->name, pMod->name, UTF8, MAX_IIRC_MOD_NAME);
    unicode_cpy(pCargo->info, pMod->info, UTF8, MAX_IIRC_MOD_INFO);
    return 1;
}


int iircCargoGlobalInit()
{
    unsigned int i;

    for(i = 0; i < MAX_IIRC_CARGO_TYPE; i++) {
        g_iircCargoVerify[i] = NULL;
        g_iircCargoByteSwap[i] = NULL;
        g_iircCargoChop[i] = NULL;
    }
    g_iircCargoVerify[CARGO_IIRC_EVENT] = cargoIrcEventVerify;
    g_iircCargoVerify[CARGO_IIRC_SYS] = cargoIrcSysVerify;
    g_iircCargoVerify[CARGO_IIRC_SERVER] = cargoIrcServerVerify;
    g_iircCargoVerify[CARGO_IIRC_CHAN_MODE] = cargoIrcChanModeVerify;
    g_iircCargoVerify[CARGO_IIRC_CHAN_TOPIC] = cargoIrcChanModeVerify;
    g_iircCargoVerify[CARGO_IIRC_CHAN_USERS] = cargoIrcChanUsersVerify;
    g_iircCargoVerify[CARGO_IIRC_NICK] = cargoIrcNickVerify;
    g_iircCargoVerify[CARGO_IIRC_JOIN] = cargoIrcJoinVerify;
    g_iircCargoVerify[CARGO_IIRC_PART] = cargoIrcPartVerify;
    g_iircCargoVerify[CARGO_IIRC_QUIT] = cargoIrcQuitVerify;
    g_iircCargoVerify[CARGO_IIRC_WHO] = cargoIrcWhoVerify;
    g_iircCargoVerify[CARGO_IIRC_USER_MODE] = cargoIrcUserModeVerify;
    g_iircCargoVerify[CARGO_IIRC_CHAN_BAN] = cargoIrcChanBanVerify;
    g_iircCargoVerify[CARGO_IIRC_MSG] = cargoIrcMsgVerify;
    g_iircCargoVerify[CARGO_IIRC_CHAN_MOD_ARG] = cargoIrcChanModArgVerify;
    g_iircCargoVerify[CARGO_IIRC_USER_MOD_ARG] = cargoIrcUserModArgVerify;

    g_iircCargoByteSwap[CARGO_IIRC_EVENT] = cargoIrcEventByteSwap;
    g_iircCargoByteSwap[CARGO_IIRC_SYS] = cargoIrcSysByteSwap;
    g_iircCargoByteSwap[CARGO_IIRC_SERVER] = cargoIrcServerByteSwap;
    g_iircCargoByteSwap[CARGO_IIRC_CHAN_MODE] = cargoIrcChanModeByteSwap;
    g_iircCargoByteSwap[CARGO_IIRC_CHAN_TOPIC] = cargoIrcChanModeByteSwap;
    g_iircCargoByteSwap[CARGO_IIRC_CHAN_USERS] = cargoIrcChanUsersByteSwap;
    g_iircCargoByteSwap[CARGO_IIRC_NICK] = cargoIrcNickByteSwap;
    g_iircCargoByteSwap[CARGO_IIRC_JOIN] = cargoIrcJoinByteSwap;
    g_iircCargoByteSwap[CARGO_IIRC_PART] = cargoIrcPartByteSwap;
    g_iircCargoByteSwap[CARGO_IIRC_QUIT] = cargoIrcQuitByteSwap;
    g_iircCargoByteSwap[CARGO_IIRC_WHO] = cargoIrcWhoByteSwap;
    g_iircCargoByteSwap[CARGO_IIRC_USER_MODE] = cargoIrcUserModeByteSwap;
    g_iircCargoByteSwap[CARGO_IIRC_CHAN_BAN] = cargoIrcChanBanByteSwap;
    g_iircCargoByteSwap[CARGO_IIRC_MSG] = cargoIrcMsgByteSwap;
    g_iircCargoByteSwap[CARGO_IIRC_CHAN_MOD_ARG] = cargoIrcChanModArgByteSwap;
    g_iircCargoByteSwap[CARGO_IIRC_USER_MOD_ARG] = cargoIrcUserModArgByteSwap;

    g_iircCargoChop[CARGO_IIRC_MSG] = cargoIrcMsgChop;

    return 1;
}

int iircCargoFix(truck *pTruck)
{
    //
    // only swap bytes if the lower level has swapped bytes.
    if(TRUCK_FLAG_ISSET(((truck*)pTruck)->flags, TRUCK_BYTESWAP))
    {
        if(pTruck->type < MAX_IIRC_CARGO_TYPE)
            if(g_iircCargoByteSwap[pTruck->type] != NULL)
                return g_iircCargoByteSwap[pTruck->type](pTruck);
    }
    return 1;
}

int iircCargoVerify(truck *pTruck)
{
    if(pTruck->type < MAX_IIRC_CARGO_TYPE)
        if(g_iircCargoVerify[pTruck->type] != NULL)
            return g_iircCargoVerify[pTruck->type](pTruck);
    return 0;
}

int iircCargoChop(truck *pTruck)
{
    if(pTruck->type < MAX_IIRC_CARGO_TYPE)
        if(g_iircCargoChop[pTruck->type] != NULL)
            return g_iircCargoChop[pTruck->type](pTruck);
    return 0;
}

int cargoIrcEventInit(cargoIrcEvent *pCargo)
{
    truckInit(&pCargo->header);
    pCargo->header.type = CARGO_IIRC_EVENT;
    pCargo->header.size = sizeof(cargoIrcEvent);
    return 1;
}

int cargoIrcEventVerify(truck *pTruck)
{
    cargoIrcEvent *pCargo = (cargoIrcEvent*)pTruck;
    if(pCargo->code >= CODE_IIRC_MAX) {
        VLOG_MSG(LOG_LOW,"cargoIrcEventVerify - invalid code %d", pCargo->code);
        return 0;
    }
    return 1;
}

int cargoIrcEventByteSwap(truck *pTruck)
{
    cargoIrcEvent *pCargo = (cargoIrcEvent*)pTruck;
    pCargo->code = BYTESWAP32(pCargo->code);
    pCargo->data1 = BYTESWAP32(pCargo->data1);
    return 1;
}

int cargoIrcSysInit(cargoIrcSys *pCargo)
{
    truckInit(&pCargo->header);
    pCargo->header.type = CARGO_IIRC_SYS;
    pCargo->header.size = sizeof(cargoIrcSys);
    return 1;
}

int cargoIrcSysVerify(truck *pTruck)
{
    cargoIrcSys *pCargo = (cargoIrcSys*)pTruck;
    if(pCargo->header.type != CARGO_IIRC_SYS) {
        VLOG_MSG(LOG_LOW,"cargoIrcSysVerify - %d: isn't CARGO_IIRC_SYS",
                pCargo->header.type);
        return 0;
    }
    if(pCargo->header.size != sizeof(cargoIrcSys)) {
        VLOG_MSG(LOG_LOW,"cargoIrcSysVerify - cargo size mismatch: %d != %d",
                pCargo->header.size, sizeof(cargoIrcSys));
        return 0;
    }
    return 1;
}

int cargoIrcSysByteSwap(truck *pTruck)
{
    return 1;
}

int cargoIrcServerInit(cargoIrcServer *pCargo)
{
    truckInit(&pCargo->header);
    pCargo->header.type = CARGO_IIRC_SERVER;
    pCargo->header.size = sizeof(cargoIrcServer);
    pCargo->name[0] = 0;
    pCargo->flags = 0;
    return 1;
}

int cargoIrcServerVerify(truck *pTruck)
{
    cargoIrcServer *pCargo = (cargoIrcServer*)pTruck;
    if(pCargo->header.type != CARGO_IIRC_SERVER) {
        VLOG_MSG(LOG_LOW,"cargoIrcServerVerify - %d: isn't CARGO_IIRC_SERVER",
                pCargo->header.type);
        return 0;
    }
    if(pCargo->header.size != sizeof(cargoIrcServer)) {
        VLOG_MSG(LOG_LOW,"cargoIrcServerVerify - cargo size mismatch: %d != %d",
                pCargo->header.size, sizeof(cargoIrcServer));
        return 0;
    }
    pCargo->name[MAX_IIRC_SERVER_NAME-1] = 0;
    if(unicode_verify(pCargo->name, UTF8, MAX_IIRC_SERVER_NAME) <= 0) {
        LOG_MSG(LOG_LOW,"cargoIrcServerVerify - cannot verify server name.");
        return 0;
    }
    return 1;
}

int cargoIrcServerByteSwap(truck *pTruck)
{
    cargoIrcServer *pCargo = (cargoIrcServer*)pTruck;
    pCargo->flags = BYTESWAP32(pCargo->flags);
    //    unicode_byteSwap(pCargo->name, UTF8, MAX_IIRC_SERVER_NAME);
    return 1;
}

int cargoIrcChanModeInit(cargoIrcChanMode *pCargo)
{
    truckInit(&pCargo->header);
    pCargo->header.type = CARGO_IIRC_CHAN_MODE;
    pCargo->header.size = sizeof(cargoIrcChanMode);
    pCargo->modeXOR = 0;
    pCargo->modeState = 0;
    pCargo->modeActive = 0;
    pCargo->userLimit = 0;
    pCargo->key[0] = 0;
    return 1;
}

int cargoIrcChanModeVerify(truck *pTruck)
{
    cargoIrcChanMode *pCargo = (cargoIrcChanMode*)pTruck;
    if(pCargo->header.type != CARGO_IIRC_CHAN_MODE) {
        VLOG_MSG(LOG_LOW,"cargoIrcChanModeVerify - %d != CARGO_IIRC_CHAN_MODE",
                pCargo->header.type);
        return 0;
    }
    if(pCargo->header.size != sizeof(cargoIrcChanMode)) {
        VLOG_MSG(LOG_LOW,"cargoIrcChanModeVerify - size mismatch: %d != %d",
                pCargo->header.size, sizeof(cargoIrcChanMode));
        return 0;
    }
    if(pCargo->modeXOR & IIRC_CHAN_MODE_UNUSED) {
        VLOG_MSG(LOG_LOW,"cargoIrcChanModeVerify - invalid modeXOR: %d",
                pCargo->modeXOR);
        return 0;
    }
    if(pCargo->modeState & IIRC_CHAN_MODE_UNUSED) {
        VLOG_MSG(LOG_LOW,"cargoIrcChanModeVerify - invalid modeState: %d",
                pCargo->modeState);
        return 0;
    }
    if(pCargo->modeActive & IIRC_CHAN_MODE_UNUSED) {
        VLOG_MSG(LOG_LOW,"cargoIrcChanModeVerify - invalid modeActive: %d",
                pCargo->modeActive);
        return 0;
    }
    pCargo->key[MAX_IIRC_CHAN_KEY-1] = 0;
    if(unicode_verify(pCargo->key, UTF8, MAX_IIRC_CHAN_KEY) <= 0) {
        LOG_MSG(LOG_LOW,"cargoIrcChanModeVerify - couldn't verify key");
        return 0;
    }
    return 1;
}

int cargoIrcChanModeByteSwap(truck *pTruck)
{
    cargoIrcChanMode *pCargo = (cargoIrcChanMode*)pTruck;
    pCargo->modeXOR = BYTESWAP32(pCargo->modeXOR);
    pCargo->modeState  = BYTESWAP32(pCargo->modeState);
    pCargo->modeActive  = BYTESWAP32(pCargo->modeActive);
    pCargo->userLimit  = BYTESWAP32(pCargo->userLimit);
    //    unicode_byteSwap(pCargo->key, UTF8, MAX_IIRC_CHAN_KEY);
    return 1;
}

int cargoIrcChanModeActive(cargoIrcChanMode *pCargo, mUint8 *modes, mUint32 nBuf)
{
    unsigned int i;
    unsigned int iMode;
    int curMode;
    char tempMode[512];

    if(nBuf <= 2)
        return -1;
    if(!pCargo->modeState) {
        modes[0] = '\0';
        return 1; // no modes active
    }
    modes[0] = '+';
    modes[1] = '\0';
    curMode = 1;
    i = 1;
    for(iMode = 0; iMode < IIRC_CHAN_NUM_MODES; iMode++) {
        if(pCargo->modeActive & curMode) {
            if(i >= nBuf-1) {
                modes[nBuf-1] = '\0';
                break;
            }
            modes[i] = iircChanMode[iMode];
            modes[i+1] = '\0';
            i++;
        }
        curMode = curMode << 1;
    }
    if(pCargo->modeActive & IIRC_CHAN_USER_LIMIT) {
        snprintf(tempMode, sizeof(tempMode), " %d", pCargo->userLimit);
        Strncat((char*)modes, tempMode, nBuf);
    }
    if(pCargo->modeActive & IIRC_CHAN_KEY)
        Strncat((char*)modes, (char*)pCargo->key, nBuf);
    return 1;
}

int cargoIrcChanModeDiff(cargoIrcChanMode *pCargo, mUint8 *modes, mUint32 nBuf)
{
    int i;
    char posModes[IIRC_CHAN_NUM_MODES+2] = "+";
    char negModes[IIRC_CHAN_NUM_MODES+2] = "-";
    int posLen, negLen;
    int curMode;
    char tempBuf[32];

    if(nBuf <= 2)
        return -1;
    if(!pCargo->modeXOR) {
        modes[0] = '\0';
        return 1; // no mode change
    }
    modes[0] = '\0';
    //
    // find the modes that have changed by looking at the exclusive OR
    // then append the character representing that mode to either the
    // positive array of modes or negative array.
    posLen = 1;
    negLen = 1;
    curMode = 1;
    for(i = 0; i < IIRC_CHAN_NUM_MODES; i++) {
        if(pCargo->modeXOR & curMode) {
            if(pCargo->modeState & curMode)
                posModes[posLen++] = iircChanMode[i];
            else
                negModes[negLen++] = iircChanMode[i];
        }
        curMode = curMode << 1;
    }
    if(posLen > 1) {
        posModes[posLen] = '\0';
        Strncat((char*)modes, posModes, nBuf);
    }
    if(negLen > 1) {
        negModes[negLen] = '\0';
        Strncat((char*)modes, negModes, nBuf);
    }
    if(pCargo->modeXOR & IIRC_CHAN_USER_LIMIT &&
            pCargo->modeState & IIRC_CHAN_USER_LIMIT)
    {
        snprintf(tempBuf, sizeof(tempBuf), " %d", pCargo->userLimit);
        Strncat((char*)modes, tempBuf, nBuf);
    }
    if(pCargo->modeXOR & IIRC_CHAN_KEY &&
            pCargo->modeState & IIRC_CHAN_KEY)
    {
        Strncat((char*)modes, (char*)pCargo->key, nBuf);
    }
    return 1;
}

int cargoIrcChanTopicInit(cargoIrcChanTopic *pCargo)
{
    truckInit(&pCargo->header);
    pCargo->header.type = CARGO_IIRC_CHAN_TOPIC;
    pCargo->header.size = sizeof(cargoIrcChanTopic);
    pCargo->topic[0] = 0;
    return 1;
}

int cargoIrcChanTopicVerify(truck *pTruck)
{
    cargoIrcChanTopic *pCargo = (cargoIrcChanTopic*)pTruck;
    if(pCargo->header.type != CARGO_IIRC_CHAN_TOPIC) {
        VLOG_MSG(LOG_LOW,"cargoIrcChanVerify - %d: isn't CARGO_IIRC_CHAN_TOPIC",
                pCargo->header.type);
        return 0;
    }
    if(pCargo->header.size != sizeof(cargoIrcChanTopic)) {
        VLOG_MSG(LOG_LOW,"cargoIrcChanTopicVerify - size mismatch: %d != %d",
                pCargo->header.size, sizeof(cargoIrcChanTopic));
        return 0;
    }
    pCargo->topic[MAX_IIRC_CHAN_TOPIC-1] = 0;
    if(unicode_verify(pCargo->topic, UTF8, MAX_IIRC_CHAN_TOPIC) <= 0) {
        LOG_MSG(LOG_LOW,"cargoIrcTopicVerify - couldn't verify key");
        return 0;
    }
    return 1;
}

int cargoIrcChanTopicByteSwap(truck *pTruck)
{
    //    unicode_byteSwap(pCargo->topic, UTF8, MAX_IIRC_CHAN_TOPIC);
    return 1;
}

int cargoIrcChanUsersInit(cargoIrcChanUsers *pCargo)
{
    int i;
    truckInit(&pCargo->header);
    pCargo->header.type = CARGO_IIRC_CHAN_USERS;
    pCargo->header.size = sizeof(cargoIrcChanUsers);
    pCargo->flags = 0;
    pCargo->hChan = 0;
    pCargo->nUser = 0;
    for(i = 0; i < MAX_IIRC_CHAN_USERS; i++) {
        pCargo->hUser[i] = 0;
        pCargo->hClient[i] = 0;
    }
    return 1;
}

int cargoIrcChanUsersVerify(truck *pTruck)
{
    cargoIrcChanUsers *pCargo = (cargoIrcChanUsers*)pTruck;
    if(pCargo->header.type != CARGO_IIRC_CHAN_USERS) {
        VLOG_MSG(LOG_LOW,"cargoIrcChanVerify - %d: isn't CARGO_IIRC_CHAN_USERS",
                pCargo->header.type);
        return 0;
    }
    if(pCargo->header.size != sizeof(cargoIrcChanUsers)) {
        VLOG_MSG(LOG_LOW,"cargoIrcChanUsersVerify - size mismatch: %d != %d",
                pCargo->header.size, sizeof(cargoIrcChanUsers));
        return 0;
    }
    if(pCargo->flags & IIRC_CHAN_USERS_UNUSED) {
        VLOG_MSG(LOG_LOW,"cargoIrcChanUsersVerify - contains unused flags: %d",
                pCargo->flags);
        return 0;
    }
    if(pCargo->nUser >= MAX_IIRC_CHAN_USERS) {
        VLOG_MSG(LOG_LOW,"cargoIrcChanUsersVerify - nUser exceeds maximum: %d",
                pCargo->nUser);
        return 0;
    }
    return 1;
}

int cargoIrcChanUsersByteSwap(truck *pTruck)
{
    cargoIrcChanUsers *pCargo = (cargoIrcChanUsers*)pTruck;
    unsigned int i;
    pCargo->flags = BYTESWAP32(pCargo->flags);
    pCargo->hChan  = BYTESWAP32(pCargo->hChan);
    pCargo->nUser  = BYTESWAP32(pCargo->nUser);
    if(pCargo->nUser >= MAX_IIRC_CHAN_USERS)
        pCargo->nUser = MAX_IIRC_CHAN_USERS-1;
    for(i = 0; i < pCargo->nUser; i++) {
        pCargo->hUser[i]  = BYTESWAP32(pCargo->hUser[i]);
        pCargo->hClient[i]  = BYTESWAP32(pCargo->hClient[i]);
    }
    return 1;
}

/*
   int cargoIrcUsernameInit(cargoIrcUsername *pCargo)
   {
   truckInit(&pCargo->header);
   pCargo->header.type = CARGO_IIRC_USERNAME;
   pCargo->header.size = sizeof(cargoIrcUsername);
   pCargo->username[0] = 0;
   pCargo->hostname[0] = 0;
   pCargo->servername[0] = 0;
   pCargo->realname[0] = 0;
   return 1;
   }

   int cargoIrcUsernameVerify(truck *pTruck)
   {
   cargoIrcUsername *pCargo = (oIrcUsername*)pTruck;
   if(pCargo->header.type < MAX_IIRC_CARGO_TYPE) {
   if(pCargo->header.type != CARGO_IIRC_USERNAME) {
   VLOG_MSG(LOG_LOW,"cargoIrcUsernameVerify - wrong cargo of type: %s",
   iircCargoNames[pCargo->header.type]);
   return 0;
   }
   } else {
   if(pCargo->header.type != CARGO_IIRC_USERNAME) {
   VLOG_MSG(LOG_LOW,"cargoIrcUsernameVerify - wrong cargo of type: %d",
   pCargo->header.type);
   return 0;
   }
   }
   if(pCargo->header.size != sizeof(cargoIrcUsername)) {
   VLOG_MSG(LOG_LOW,"cargoIrcUsernameVerify - size mismatch: %d != %d",
   pCargo->header.size, sizeof(cargoIrcUsername));
   return 0;
   }
   pCargo->username[MAX_IIRC_USERNAME-1] = 0;
   pCargo->hostname[MAX_HOST_NAME-1] = 0;
   pCargo->servername[MAX_HOST_NAME-1] = 0;
   pCargo->realname[MAX_IIRC_REAL_NAME-1] = 0;
   return 1;
   }

   int cargoIrcUsernameByteSwap(truck *pTruck)
   {
   cargoIrcUsername *pCargo = (cargoIrcUsername*)pTruck;
   return 1;
   }
 */

int cargoIrcNickInit(cargoIrcNick *pCargo)
{
    truckInit(&pCargo->header);
    pCargo->header.type = CARGO_IIRC_NICK;
    pCargo->header.size = sizeof(cargoIrcNick);
    pCargo->nick[0] = 0;
    pCargo->modes = 0;
    return 1;
}

int cargoIrcNickVerify(truck *pTruck)
{
    cargoIrcNick *pCargo = (cargoIrcNick*)pTruck;
    if(pCargo->header.type != CARGO_IIRC_NICK) {
        VLOG_MSG(LOG_LOW,"cargoIrcNickVerify - %d: isn't CARGO_IIRC_NICK",
                pCargo->header.type);
        return 0;
    }
    if(pCargo->header.size != sizeof(cargoIrcNick)) {
        VLOG_MSG(LOG_LOW,"cargoIrcNickVerify - cargo size mismatch: %d != %d",
                pCargo->header.size, sizeof(cargoIrcNick));
        return 0;
    }
    pCargo->nick[MAX_IIRC_NICK-1] = 0;
    if(unicode_verify(pCargo->nick, UTF8, MAX_IIRC_NICK) <= 0) {
        LOG_MSG(LOG_LOW,"cargoIrcNickVerify - couldn't verify nick");
        return 0;
    }
    return 1;
}

int cargoIrcNickByteSwap(truck *pTruck)
{
    cargoIrcNick *pCargo = (cargoIrcNick*)pTruck;
    pCargo->modes = BYTESWAP32(pCargo->modes);
    //    unicode_byteSwap(pCargo->nick, UTF8, MAX_IIRC_NICK);
    return 1;
}

int cargoIrcJoinInit(cargoIrcJoin *pCargo)
{
    truckInit(&pCargo->header);
    pCargo->header.type = CARGO_IIRC_JOIN;
    pCargo->header.size = sizeof(cargoIrcJoin);
    pCargo->flags = 0;
    pCargo->hChan = 0;
    pCargo->hChanUser = 0;
    pCargo->chan[0] = 0;
    pCargo->key[0] = 0;
    return 1;
}

int cargoIrcJoinVerify(truck *pTruck)
{
    cargoIrcJoin *pCargo = (cargoIrcJoin*)pTruck;
    if(pCargo->header.type != CARGO_IIRC_JOIN) {
        VLOG_MSG(LOG_LOW,"cargoIrcJoinVerify - %d: isn't CARGO_IIRC_JOIN",
                pCargo->header.type);
        return 0;
    }
    if(pCargo->header.size != sizeof(cargoIrcJoin)) {
        VLOG_MSG(LOG_LOW,"cargoIrcJoinVerify - cargo size mismatch: %d != %d",
                pCargo->header.size, sizeof(cargoIrcJoin));
        return 0;
    }
    pCargo->chan[MAX_IIRC_CHAN_NAME - 1] = 0;
    if(unicode_verify(pCargo->chan, UTF8, MAX_IIRC_CHAN_NAME) <= 0) {
        LOG_MSG(LOG_LOW,"cargoIrcJoinVerify - couldn't verify nick");
        return 0;
    }
    pCargo->key[MAX_IIRC_CHAN_KEY - 1] = 0;
    if(unicode_verify(pCargo->key, UTF8, MAX_IIRC_CHAN_KEY) <= 0) {
        LOG_MSG(LOG_LOW,"cargoIrcJoinVerify - couldn't verify key");
        return 0;
    }
    if(pCargo->flags & IIRC_JOIN_FLAGS_UNUSED) {
        VLOG_MSG(LOG_LOW,"cargoIrcJoinVerify - %s contains unused flags: %d",
                pCargo->chan, pCargo->flags);
        return 0;
    }
    return 1;
}

int cargoIrcJoinByteSwap(truck *pTruck)
{
    cargoIrcJoin *pCargo = (cargoIrcJoin*)pTruck;
    pCargo->flags = BYTESWAP32(pCargo->flags);
    pCargo->hChan = BYTESWAP32(pCargo->hChan);
    pCargo->hChanUser = BYTESWAP32(pCargo->hChanUser);
    //    unicode_byteSwap(pCargo->chan, UTF8, MAX_IIRC_CHAN_NAME);
    //    unicode_byteSwap(pCargo->key, UTF8, MAX_IIRC_CHAN_KEY);
    return 1;
}

int cargoIrcPartInit(cargoIrcPart *pCargo)
{
    truckInit(&pCargo->header);
    pCargo->header.type = CARGO_IIRC_PART;
    pCargo->header.size = sizeof(cargoIrcPart);
    pCargo->hChan = 0;
    pCargo->reason[0] = 0;
    return 1;
}

int cargoIrcPartVerify(truck *pTruck)
{
    cargoIrcPart *pCargo = (cargoIrcPart*)pTruck;
    if(pCargo->header.type != CARGO_IIRC_PART) {
        VLOG_MSG(LOG_LOW,"cargoIrcPartVerify - %d: isn't CARGO_IIRC_PART",
                pCargo->header.type);
        return 0;
    }
    if(pCargo->header.size != sizeof(cargoIrcPart)) {
        VLOG_MSG(LOG_LOW,"cargoIrcPartVerify - cargo size mismatch: %d != %d",
                pCargo->header.size, sizeof(cargoIrcPart));
        return 0;
    }
    pCargo->reason[MAX_IIRC_PART_REASON - 1] = 0;
    if(unicode_verify(pCargo->reason, UTF8, MAX_IIRC_PART_REASON) <= 0)
    {
        LOG_MSG(LOG_LOW,"cargoIrcPartVerify - couldn't verify \'reason\'");
        return 0;
    }
    return 1;
}

int cargoIrcPartByteSwap(truck *pTruck)
{
    cargoIrcPart *pCargo = (cargoIrcPart*)pTruck;
    pCargo->hChan = BYTESWAP32(pCargo->hChan);
    //    unicode_byteSwap(pCargo->reason, UTF8, MAX_IIRC_PART_REASON);
    return 1;
}

int cargoIrcQuitInit(cargoIrcQuit *pCargo)
{
    truckInit(&pCargo->header);
    pCargo->header.type = CARGO_IIRC_QUIT;
    pCargo->header.size = sizeof(cargoIrcQuit);
    pCargo->reason[0] = 0;
    return 1;
}

int cargoIrcQuitVerify(truck *pTruck)
{
    cargoIrcQuit *pCargo = (cargoIrcQuit*)pTruck;
    if(pCargo->header.type != CARGO_IIRC_QUIT) {
        VLOG_MSG(LOG_LOW,"cargoIrcQuitVerify - %d: isn't CARGO_IIRC_QUIT",
                pCargo->header.type);
        return 0;
    }
    if(pCargo->header.size != sizeof(cargoIrcQuit)) {
        VLOG_MSG(LOG_LOW,"cargoIrcQuitVerify - cargo size mismatch: %d != %d",
                pCargo->header.size, sizeof(cargoIrcQuit));
        return 0;
    }
    pCargo->reason[MAX_IIRC_QUIT_REASON - 1] = 0;
    if(unicode_verify(pCargo->reason, UTF8, MAX_IIRC_QUIT_REASON) <= 0)
    {
        LOG_MSG(LOG_LOW,"cargoIrcQuitVerify - couldn't verify \'reason\'");
        return 0;
    }
    return 1;
}

int cargoIrcQuitByteSwap(truck *pTruck)
{
    //    unicode_byteSwap(pCargo->reason, UTF8, MAX_IIRC_QUIT_REASON);
    return 1;
}

int cargoIrcWhoInit(cargoIrcWho *pCargo)
{
    truckInit(&pCargo->header);
    pCargo->header.type = CARGO_IIRC_WHO;
    pCargo->header.size = sizeof(cargoIrcWho);
    pCargo->flags = IIRC_WHO_FLAG_OPS | IIRC_WHO_FLAG_VOICE | IIRC_WHO_FLAG_USER;
    pCargo->name[0] = 0;
    return 1;
}

int cargoIrcWhoVerify(truck *pTruck)
{
    cargoIrcWho *pCargo = (cargoIrcWho*)pTruck;
    if(pCargo->header.type != CARGO_IIRC_WHO) {
        VLOG_MSG(LOG_LOW,"cargoIrcWhoVerify - %d: isn't CARGO_IIRC_WHO",
                pCargo->header.type);
        return 0;
    }
    if(pCargo->header.size != sizeof(cargoIrcWho)) {
        VLOG_MSG(LOG_LOW,"cargoIrcWhoVerify - cargo size mismatch: %d != %d",
                pCargo->header.size, sizeof(cargoIrcWho));
        return 0;
    }
    if(pCargo->flags & IIRC_WHO_FLAG_UNUSED) {
        VLOG_MSG(LOG_LOW,"cargoIrcWhoVerify - unused flag is set %d",
                pCargo->flags);
        return 0;
    }
    pCargo->name[MAX_IIRC_WHO_NAME-1] = 0;
    if(unicode_verify(pCargo->name, UTF8, MAX_IIRC_WHO_NAME) <= 0) {
        LOG_MSG(LOG_LOW,"cargoIrcWhoVerify - couldn't verify name");
        return 0;
    }
    return 1;
}

int cargoIrcWhoByteSwap(truck *pTruck)
{
    cargoIrcWho *pCargo = (cargoIrcWho*)pTruck;
    pCargo->flags = BYTESWAP32(pCargo->flags);
    //    unicode_byteSwap(pCargo->name, UTF8, MAX_IIRC_WHO_NAME);
    return 1;
}

int cargoIrcUserModeInit(cargoIrcUserMode *pCargo)
{
    truckInit(&pCargo->header);
    pCargo->header.type = CARGO_IIRC_USER_MODE;
    pCargo->header.size = sizeof(cargoIrcUserMode);
    pCargo->hChan = 0;
    pCargo->nUser = 0;
    return 1;
}

int cargoIrcUserModeVerify(truck *pTruck)
{
    cargoIrcUserMode *pCargo = (cargoIrcUserMode*)pTruck;
    if(pCargo->header.type != CARGO_IIRC_USER_MODE) {
        VLOG_MSG(LOG_LOW,"cargoIrcUserModeVerify - got a %s instead",
                iircCargoNames[pCargo->header.type]);
        return 0;
    }
    if(pCargo->header.size != sizeof(cargoIrcUserMode)) {
        VLOG_MSG(LOG_LOW,"cargoIrcUserModeVerify - size mismatch: %d != %d",
                pCargo->header.size, sizeof(cargoIrcUserMode));
        return 0;
    }
    if(pCargo->nUser >= MAX_IIRC_USER_MODE) {
        VLOG_MSG(LOG_LOW,"cargoIrcUserModeVerify - nUser: %d >= %d",
                pCargo->nUser, MAX_IIRC_USER_MODE);
        return 0;
    }
    return 1;
}

int cargoIrcUserModeByteSwap(truck *pTruck)
{
    unsigned int i;
    cargoIrcUserMode *pCargo = (cargoIrcUserMode*)pTruck;

    pCargo->hChan = BYTESWAP32(pCargo->hChan);
    pCargo->nUser = BYTESWAP32(pCargo->nUser);
    if(pCargo->nUser >= MAX_IIRC_USER_MODE)
        pCargo->nUser = MAX_IIRC_USER_MODE;
    for(i = 0; i < pCargo->nUser; i++) {
        pCargo->hChanUser[i] = BYTESWAP32(pCargo->hChanUser[i]);
        pCargo->modeXOR[i] = BYTESWAP32(pCargo->modeXOR[i]);
        pCargo->modeState[i] = BYTESWAP32(pCargo->modeState[i]);
        pCargo->modeActive[i] = BYTESWAP32(pCargo->modeActive[i]);
    }
    return 1;
}

int cargoIrcChanBanInit(cargoIrcChanBan *pCargo)
{
    pCargo->hChan = 0;
    pCargo->mask[0] = 0;
    return 1;
}

int cargoIrcChanBanVerify(truck *pTruck)
{
    cargoIrcChanBan *pCargo = (cargoIrcChanBan*)pTruck;
    if(pCargo->header.type != CARGO_IIRC_CHAN_BAN) {
        LOG_MSG(LOG_LOW,"cargoIrcChanBanVerify - %d not CARGO_IIRC_CHAN_BAN");
        return 0;
    }
    if(pCargo->header.size != sizeof(cargoIrcChanBan)) {
        VLOG_MSG(LOG_LOW,"cargoIrcChanBanVerify - size mismatch: %d != %d",
                pCargo->header.size, sizeof(cargoIrcChanBan));
        return 0;
    }
    pCargo->mask[MAX_IIRC_BAN_MASK-1] = 0;
    if(unicode_verify(pCargo->mask, UTF8, MAX_IIRC_BAN_MASK) <= 0) {
        LOG_MSG(LOG_LOW,"cargoIrcChanBanVerify - couldn't verify mask");
        return 0;
    }
    return 1;
}

int cargoIrcChanBanByteSwap(truck *pTruck)
{
    cargoIrcChanBan *pCargo = (cargoIrcChanBan*)pTruck;
    pCargo->hChan = BYTESWAP32(pCargo->hChan);
    //    unicode_byteSwap(pCargo->mask, UTF8, MAX_IIRC_BAN_MASK);
    return 1;
}

int cargoIrcMsgInit(cargoIrcMsg *pCargo)
{
    truckInit(&pCargo->header);
    pCargo->header.type = CARGO_IIRC_MSG;
    pCargo->header.size = sizeof(cargoIrcMsg);
    pCargo->msg[0] = 0;
    return 1;
}

int cargoIrcMsgVerify(truck *pTruck)
{
    cargoIrcMsg *pMsg = (cargoIrcMsg*)pTruck;

    pMsg->msg[MAX_IIRC_MSG-1] = 0;
    if(unicode_verify(pMsg->msg, UTF8, MAX_IIRC_MSG) <= 0) {
        LOG_MSG(LOG_LOW,"cargoIrcMsgVerify - couldn't verify message");
        return 0;
    }
    return 1;
}

int cargoIrcMsgByteSwap(truck *pTruck)
{
    return 1;
}

int cargoIrcMsgChop(truck *pTruck)
{
    mUint32 len;
    cargoIrcMsg *pMsg = (cargoIrcMsg*)pTruck;

    len = unicode_len(pMsg->msg, UTF8) + 1; // +1 for NUL!
    if(len > MAX_IIRC_MSG)
        len = MAX_IIRC_MSG;
    pTruck->size = sizeof(truck) + len;
    return 1;
}

int cargoIrcChanModArgInit(cargoIrcChanModArg *pCargo)
{
    truckInit(&pCargo->header);
    pCargo->header.type = CARGO_IIRC_CHAN_MOD_ARG;
    pCargo->header.size = sizeof(cargoIrcChanModArg);
    pCargo->hChan = 0;
    pCargo->hMod = 0;
    pCargo->function = 0;
    pCargo->arg[0] = 0;
    return 1;
}

int cargoIrcChanModArgVerify(truck *pTruck)
{
    cargoIrcChanModArg *pCargo = (cargoIrcChanModArg*)pTruck;
    if(pCargo->header.type != CARGO_IIRC_CHAN_MOD_ARG) {
        LOG_MSG(LOG_LOW,"cargoIrcChanModArgVerify - !CARGO_IIRC_CHAN_MOD_ARG");
        return 0;
    }
    if(pCargo->header.size != sizeof(cargoIrcChanModArg)) {
        VLOG_MSG(LOG_LOW,"cargoIrcChanModArgVerify - size mismatch: %d != %d",
                pCargo->header.size, sizeof(cargoIrcChanModArg));
        return 0;
    }
    if(pCargo->function >= IIRC_MOD_FUNC_MAX) {
        VLOG_MSG(LOG_LOW,"cargoIrcChanModArgVerify - invalid function: %d",
                pCargo->function);
        return 0;
    }
    pCargo->arg[MAX_IIRC_MOD_ARG-1] = 0;
    if(unicode_verify(pCargo->arg, UTF8, MAX_IIRC_MOD_ARG) <= 0) {
        LOG_MSG(LOG_LOW,"cargoIrcChanModArgVerify - couldn't verify argument");
        return 0;
    }
    return 1;
}

int cargoIrcChanModArgByteSwap(truck *pTruck)
{
    cargoIrcChanModArg *pCargo = (cargoIrcChanModArg*)pTruck;
    pCargo->hChan = BYTESWAP32(pCargo->hChan);
    pCargo->hMod = BYTESWAP32(pCargo->hMod);
    pCargo->function = BYTESWAP32(pCargo->function);
    //    unicode_byteSwap(pCargo->arg, UTF8, MAX_IIRC_MOD_ARG);
    return 1;
}

int cargoIrcUserModArgInit(cargoIrcUserModArg *pCargo)
{
    truckInit(&pCargo->header);
    pCargo->header.type = CARGO_IIRC_USER_MOD_ARG;
    pCargo->header.size = sizeof(cargoIrcUserModArg);
    pCargo->hUser = 0;
    pCargo->hMod = 0;
    pCargo->function = 0;
    pCargo->arg[0] = 0;
    return 1;
}

int cargoIrcUserModArgVerify(truck *pTruck)
{
    cargoIrcUserModArg *pCargo = (cargoIrcUserModArg*)pTruck;
    if(pCargo->header.type != CARGO_IIRC_USER_MOD_ARG) {
        LOG_MSG(LOG_LOW,"cargoIrcUserModArgVerify - !CARGO_IIRC_USER_MOD_ARG");
        return 0;
    }
    if(pCargo->header.size != sizeof(cargoIrcUserModArg)) {
        VLOG_MSG(LOG_LOW,"cargoIrcUserModArgVerify - size mismatch: %d != %d",
                pCargo->header.size, sizeof(cargoIrcUserModArg));
        return 0;
    }
    if(pCargo->function >= IIRC_MOD_FUNC_MAX) {
        VLOG_MSG(LOG_LOW,"cargoIrcUserModArgVerify - invalid function: %d",
                pCargo->function);
        return 0;
    }
    pCargo->arg[MAX_IIRC_MOD_ARG-1] = 0;
    if(unicode_verify(pCargo->arg, UTF8, MAX_IIRC_MOD_ARG) <= 0) {
        LOG_MSG(LOG_LOW,"cargoIrcUserModArgVerify - couldn't verify argument");
        return 0;
    }
    return 1;
}

int cargoIrcUserModArgByteSwap(truck *pTruck)
{
    cargoIrcUserModArg *pCargo = (cargoIrcUserModArg*)pTruck;

    pCargo->hUser = BYTESWAP32(pCargo->hUser);
    pCargo->hMod = BYTESWAP32(pCargo->hMod);
    pCargo->function = BYTESWAP32(pCargo->function);
    //    unicode_byteSwap(pCargo->arg, UTF8, MAX_IIRC_MOD_ARG);
    return 1;
}


