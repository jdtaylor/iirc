dnl Process this file with autoconf to produce a configure script.

dnl random file so autoconf can sync/verify the source dir
AC_INIT(src/main_iircd.c)
AC_CONFIG_AUX_DIR(config)

dnl Set various version strings - taken gratefully from the SDL sources
#
# Making releases:
# Edit include/iirc/iirc_version.h and change the version, then:
#   IIRC_MICRO_VERSION += 1;
#   IIRC_INTERFACE_AGE += 1;
#   IIRC_BINARY_AGE += 1;
# if any functions have been added, set IIRC_INTERFACE_AGE to 0.
# if backwards compatibility has been broken,
# set IIRC_BINARY_AGE and IIRC_INTERFACE_AGE to 0.
#
IIRC_MAJOR_VERSION=0
IIRC_MINOR_VERSION=1
IIRC_MICRO_VERSION=0
IIRC_INTERFACE_AGE=0
IIRC_BINARY_AGE=0
IIRC_VERSION=$IIRC_MAJOR_VERSION.$IIRC_MINOR_VERSION.$IIRC_MICRO_VERSION

AC_SUBST(IIRC_MAJOR_VERSION)
AC_SUBST(IIRC_MINOR_VERSION)
AC_SUBST(IIRC_MICRO_VERSION)
AC_SUBST(IIRC_INTERFACE_AGE)
AC_SUBST(IIRC_BINARY_AGE)
AC_SUBST(IIRC_VERSION)

# libtool versioning
LT_RELEASE=$IIRC_MAJOR_VERSION.$IIRC_MINOR_VERSION
LT_CURRENT=`expr $IIRC_MICRO_VERSION - $IIRC_INTERFACE_AGE`
LT_REVISION=$IIRC_INTERFACE_AGE
LT_AGE=`expr $IIRC_BINARY_AGE - $IIRC_INTERFACE_AGE`

AC_SUBST(LT_RELEASE)
AC_SUBST(LT_CURRENT)
AC_SUBST(LT_REVISION)
AC_SUBST(LT_AGE)

dnl Detect the canonical host and target build environment
AC_CANONICAL_HOST
AC_CANONICAL_TARGET

AM_INIT_AUTOMAKE(iirc, $IIRC_VERSION)
AM_CONFIG_HEADER(config.h)

AC_CONFIG_SUBDIRS(libltdl)
AC_LIBTOOL_WIN32_DLL
AC_LIBTOOL_DLOPEN
AC_LIBLTDL_CONVENIENCE
AC_PROG_LIBTOOL
AC_SUBST(INCLTDL)
AC_SUBST(LIBLTDL)

AC_PROG_CC
AC_ISC_POSIX
AC_PROG_INSTALL

dnl Enable/disable various subsystems of the IIRC library

AC_ARG_WITH(pkgconfigdir,
[  --with-pkgconfigdir=DIR pkgconfig file in DIR @<:@LIBDIR/pkgconfig@:>@],
[pkgconfigdir=$withval],
[pkgconfigdir='${libdir}/pkgconfig'])
AC_SUBST(pkgconfigdir)

AC_ARG_ENABLE(metric_threads,
[  --enable-threads             Use threads [default=yes]],
              metric_threads=$enableval, metric_threads=yes)

AC_ARG_ENABLE(iirc,
[  --disable-iirc               Disable build of generic iIRC client],
              iirc=$enableval, iirc=yes)

AC_ARG_ENABLE(iircd,
[  --disable-iircd              Disable build of generic iIRC server],
              iircd=$enableval, iircd=yes)

AC_ARG_ENABLE(static_libiirc,
[  --enable-static-libiirc      Statically link iIRC library],
              static_libiirc=$enableval, static_libiirc=no)

AC_ARG_ENABLE(static_iirc,
[  --enable-static-iirc         Statically link iIRC client],
              static_iirc=$enableval, static_iirc=no)

AC_ARG_ENABLE(static_iircd,
[  --enable-static-iircd        Statically link iIRC server],
              static_iircd=$enableval, static_iircd=no)

AC_ARG_ENABLE(iirc_intr,
[  --disable-iirc-intr          Dont compile iIRC interrupt support],
              iirc_intr=$enableval, iirc_intr=yes)

AC_ARG_WITH(metric_prefix,
[  --with-metric-install=DIR    Path of libmetric installation (optional)],
		metric_prefix="$withval", metric_prefix="")

AC_ARG_WITH(metric_devel,
[  --with-metric-devel=DIR      Path of libmetric devel tree (optional)],
		metric_devel="$withval", metric_devel="")

AC_ARG_ENABLE(fuse,
[  --enable-fuse                Enable use of FS in Userspace [default=no]],
              fuse=$enableval, fuse=no)

AC_ARG_ENABLE(mod_echo,
[  --enable-mod-echo            Build echo module [default=no]],
              mod_echo=$enableval, mod_echo=no)

AC_ARG_ENABLE(mod_ogg,
[  --enable-mod-ogg             Build ogg vorbis module [default=no]],
              mod_ogg=$enableval, mod_ogg=no)

if test x$metric_threads = xyes; then
	AC_DEFINE([USE_METRIC_THREADS], [], [Use libmetric thread lib])
fi
AH_VERBATIM([USE_METRIC_THREADS],
		[/* Enable threads */
#ifndef USE_METRIC_THREADS
#define USE_METRIC_THREADS 1
#endif])

if test x$iirc = xyes; then
    BIN_IIRC="iirc"
fi
AC_SUBST(BIN_IIRC)

if test x$iircd = xyes; then
    BIN_IIRCD="iircd"
fi
AC_SUBST(BIN_IIRCD)

if test x$static_libiirc = xyes; then
    LIBIIRC_LDFLAGS="$LIBIIRC_LDFLAGS -static "
fi
AC_SUBST(LIBIIRC_LDFLAGS)

if test x$static_iirc = xyes; then
    IIRC_LDFLAGS="$IIRC_LDFLAGS -static "
fi
AC_SUBST(IIRC_LDFLAGS)

if test x$static_iircd = xyes; then
    IIRCD_LDFLAGS="$IIRCD_LDFLAGS -static "
fi
AC_SUBST(IIRCD_LDFLAGS)

if test x$iirc_intr = xyes; then
    `sed 's/\/\*##sub##\*\//\/\*##sub##\*\/\n#define HAVE_IIRC_INTR 1/' src/lib/common.h > .common.h; mv .common.h src/lib/common.h`
dnl    AC_DEFINE(HAVE_IIRC_INTR, 1, [iIRC interrupt support])
else
    `sed 's/#define HAVE_IIRC_INTR 1//' src/lib/common.h > .common.h; mv .common.h src/lib/common.h`
fi

METRIC_LIBS="-lmetric"
METRIC_CFLAGS=""
if test x$metric_prefix != x; then
    METRIC_CFLAGS=" -I$metric_prefix/include "
    METRIC_LIBS=" $metric_prefix/lib/libmetric.la "
else
	if test x$metric_devel != x; then
		METRIC_CFLAGS=" -I$metric_devel/include "
		METRIC_LIBS=" $metric_devel/src/libmetric.la "
	else
		PKG_CHECK_MODULES(METRIC, metric >= 0.0.1)
	fi
fi
AC_SUBST(METRIC_CFLAGS)
AC_SUBST(METRIC_LIBS)

dnl FUSE requires FILE_OFFSET_BITS=64:
AC_SYS_LARGEFILE()
if test x$fuse = xyes; then
	PKG_CHECK_MODULES(FUSE, fuse >= 2.0)
	AH_VERBATIM([HAVE_FUSE],
	[/* Enable File System in User-Space feature */
	#ifndef HAVE_FUSE
	#define HAVE_FUSE 1
	#endif])
fi
AC_SUBST(FUSE_CFLAGS)
AC_SUBST(FUSE_LIBS)

MODULE_DIRS=""
MODULE_LIBS=""
if test x$mod_echo = xyes; then
    MODULE_DIRS += " mod_echo"
	MODULE_LIBS += " ../mod_echo/mod_echo.la"
fi
if test x$mod_ogg = xyes; then
    MODULE_DIRS += " mod_ogg"
	MODULE_LIBS += " ../mod_echo/mod_ogg.la"
fi
AC_SUBST(MODULE_DIRS)
AC_SUBST(MODULE_LIBS)

dnl Checks for libraries.
dnl Replace `main' with a function in -ldl:
AC_CHECK_LIB(dl, main)
dnl Replace `main' with a function in -lm:
AC_CHECK_LIB(m, main)

# Checks for header files.
AC_HEADER_STDC
AC_HEADER_DIRENT
AC_CHECK_HEADERS(fcntl.h malloc.h sys/time.h unistd.h)

# Checks for typedefs, structures, and compiler characteristics.
AC_C_CONST
AC_TYPE_PID_T
AC_TYPE_SIZE_T
AC_STRUCT_ST_BLKSIZE
AC_HEADER_TIME

# Checks for library functions.
AC_FUNC_MMAP
#AC_FUNC_FORK
AC_CHECK_FUNCS(getcwd gettimeofday select socket strcspn)

AC_CONFIG_FILES([iirc.pc src/Makefile src/lib/Makefile Makefile])
AC_OUTPUT

