
/*
 * Copyright (c) 2001-2002,  James D. Taylor.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * contact James D. Taylor:  james.d.taylor@gmail.com
 */

#ifndef IIRC_CLIENT_H
#define IIRC_CLIENT_H

#include "iirc/common.h"
#include "iirc/defs.h"
#include "iirc/module.h"
#include "iirc/cargo.h"
#include "iirc/struct_intr.h"

#define MAX_IIRC_CLIENT_CMD 1024
#define IIRC_CLIENT_ERR_TIMEOUT -1
#define IIRC_CLIENT_ERR_FATAL -2


typedef struct _iircClientCargo {
    int hClient;
    truck *pTruck;
    void *pData;
} iircClientCargo;

typedef int (*iircClientHandler)(iircClientCargo *pCargo, int *pStatus);


typedef struct _iircClientDescr {
    int version;
    char hostname[MAX_HOST_NAME];
    char servername[MAX_HOST_NAME]; //! remote hostname of server
    short serverPort;   //! remote tcp port of server
    iircClientHandler fnPreCargoHandler; //! callback to send incoming trucks to
    iircClientHandler fnPostCargoHandler;
    void *pData; // arbitrary pData field of cargoContainer
} iircClientDescr;
#define IIRC_CLIENT_DESCR_VERSION 0x00000100 /* version 0.1.0 */

typedef struct _iircUserDescr {
    int version;
    mUint8 nick[MAX_IIRC_NICK];
    mInt8 identity[MAX_IIRC_IDENTITY];
    mUint8 realname[MAX_IIRC_REAL_NAME];
} iircUserDescr;
#define IIRC_USER_DESCR_VERSION 0x00000100 /* version 0.1.0 */


#ifdef __cplusplus
extern "C" {
#endif

int iircClientGlobalInit();
int iircClientGlobalFree();

int iircClientAdd(iircClientDescr *pDesc);
int iircClientDel(      int hClient);
int iircClientStart(    int hClient, long timeout);
int iircClientModuleAdd(int hClient, iircModule_t *pMod);
int iircClientIntrAdd(  int hClient, iircInterruptDescr *pDescr);
int iircClientIntrDel(  int hClient, unsigned int hIntr);
int iircClientExecute(  int hClient, long timeout);
int iircClientGetFD(    int hClient);
int iircClientDispatch( int hClient, truck *pTruck);
int iircClientCmd(      int hClient, int hUser, char *cmd);

int iircClientUserAdd(  int hClient, iircUserDescr *pDescr);
int iircClientUserDel(  int hClient, int hUser);
int iircClientUserGet(  int hClient, int hUser, cargoIrcUser *cargo);
int iircClientUserFind( int hClient, char *nick);
int iircClientUserIncr( int hClient, unsigned int *i, cargoIrcUser *cargo);

int iircClientChanGet(  int hClient, int hChan, cargoIrcChan *cargo);
int iircClientChanFind( int hClient, char *chan);
int iircClientChanTopic(int hClient, int hChan, cargoIrcChanTopic *cargo);
int iircClientChanUser( int hClient, int hChan, int hUser); // global userId

int iircClientSrcUser(  int hClient, truck *pTruck, cargoIrcUser *cargo);
int iircClientDstChan(  int hClient, truck *pTruck, cargoIrcChan *cargo);

int iircClientModGet(   int hClient, int hMod, cargoIrcModInfo *pMod);

//!
// return a memory location of the iircClient_t structure at hClient.
// This breaks the interface, but it is here if thats what you want to do.
void *iircClientGet(    int hClient);

#ifdef __cplusplus
}
#endif

#endif

