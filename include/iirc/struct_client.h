
/*
 * Copyright (c) 2001-2002,  James D. Taylor.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * contact James D. Taylor:  james.d.taylor@gmail.com
 */

#ifndef IIRC_STRUCT_CLIENT_H
#define IIRC_STRUCT_CLIENT_H

#include "iirc/common.h"
#include "metric/set.h"
#include "iirc/cargo.h"
#include "iirc/state.h"
#include "iirc/client.h"
#include "iirc/struct_user.h"
#include "iirc/struct_intr.h"

//!
// set of users local to a client
#define clientUserSet_t  genericArray
#define clientUserSet(op) genericArray##op


#define MAX_IIRC_CCMD_PARAMS 32
#define IIRC_WAIT_MODS 0x00000002

//! client commands
enum {
    IIRC_CCMD_NOP = 0,
    IIRC_CCMD_PRIVMSG,
    IIRC_CCMD_SERVER,
    IIRC_CCMD_NICK,
    IIRC_CCMD_QUIT,
    IIRC_CCMD_JOIN,
    IIRC_CCMD_PART,
    IIRC_CCMD_MODE,
    IIRC_CCMD_KICK,
    IIRC_CCMD_LIST,
    IIRC_CCMD_TOPIC,
    IIRC_CCMD_WHO,
    IIRC_CCMD_NAMES,
    MAX_IIRC_CCMDS
};

struct iircClient;

//! client cargo handler type
typedef int (*iircCCargoHandler)(struct iircClient *pSelf, truck *pTruck,
                int *pStatus);
//! client command handler type
typedef int (*iircCCmdHandler)(struct iircClient *pSelf, iircUser_t *pUser,
                mUint32 *cmd);

enum {
    IIRC_CLIENT_RFC1459 = 1,
};

typedef struct iircClient {
    unsigned int id;
    unsigned int flags;
    char hostname[MAX_HOST_NAME];
    int bStarted;
    iircState_t state;  //! iirc state data
    clientUserSet_t users; //! users local to this client
    truckQueue  parking; //! pool (queue) of unused trucks
    socketSet_t sockets; //! used to wait for any ready sockets. (metric/sock.h)
    commute_t parentCom; //! connection to iIRC server
    intrSet_t interrupts; //! set of I/O interrupts to watch for
    iircCCargoHandler fnPreHandler[MAX_IIRC_CARGO_TYPE]; //! pre-state
    iircCCargoHandler fnPostHandler[MAX_IIRC_CARGO_TYPE]; //! post-state
    iircClientHandler fnPreClientHandler; //! callback function for this client
    iircClientHandler fnPostClientHandler;
    void *pClientData;  //! passed to fnClienthandler
    char servername[MAX_HOST_NAME];
    short serverPort;
    unsigned int waitFlags;
    iircCCmdHandler fnCmd[MAX_IIRC_CCMDS]; //! client command handlers
} iircClient_t;

extern genericInfo_t iircClientInfo;

#ifdef __cplusplus
extern "C" {
#endif

int iircClientInit(iircClient_t *pClient);
int iircClientAlloc(iircClient_t *pClient);
int iircClientFree(iircClient_t *pClient);

#ifdef __cplusplus
}
#endif

#endif


