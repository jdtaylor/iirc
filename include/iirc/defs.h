
/*
 * Copyright (c) 2001-2002,  James D. Taylor.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * contact James D. Taylor:  james.d.taylor@gmail.com
 */

#ifndef IIRC_DEFS_H
#define IIRC_DEFS_H

#include "iirc/common.h"

//!
// time spent waiting for connections/data
#define IIRC_POLL_TIME 100
//!
// maximum time with which a module may
// go without an interrupt. default: 5000 ms.
#define MAX_IIRC_MOD_WAIT_TIME 5000
#define MAX_IIRC_MODS 64

//!
// ###### -  ATTENTION! WARNING!  - ######
//
// the following defines, are used in the IIRC protocol.  Changing them
// will cause the protocol to change, so don't unless YOU KNOW WHAT
// YOU ARE DOING!
//
#define MAX_IIRC_NICK 32
#define MAX_IIRC_SERVER_NAME 64
#define MAX_IIRC_IDENTITY 32
#define MAX_IIRC_CHAN_NAME 16
#define MAX_IIRC_REAL_NAME 32
#define MAX_IIRC_CHAN_TOPIC 128
#define MAX_IIRC_CHAN_KEY 16
#define MAX_IIRC_WHO_NAME (MAX_HOST_NAME + MAX_IIRC_NICK + MAX_IIRC_IDENTITY)
//! username ! nickname @ hostname
#define MAX_IIRC_BAN_MASK (MAX_HOST_NAME + MAX_IIRC_NICK + MAX_IIRC_IDENTITY)
#define MAX_IIRC_CHAN_ARG 256
//! maximum user modes one can set simultaneously -1sp
#define MAX_IIRC_USER_MODE 5
#define MAX_IIRC_MSG 512
#define MAX_IIRC_PART_REASON 64
#define MAX_IIRC_QUIT_REASON 64

#define MAX_IIRC_MOD_NAME 16
#define MAX_IIRC_MOD_INFO 128
#define MAX_IIRC_MOD_ARG 64

#endif

