
/*
 * Copyright (c) 2001-2002,  James D. Taylor.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * contact James D. Taylor:  james.d.taylor@gmail.com
 */


#ifndef IIRC_SERVER_H
#define IIRC_SERVER_H

#include "iirc/common.h"
#include "iirc/defs.h"
#include "iirc/module.h"
#include "iirc/struct_intr.h"
#include "iirc/cargo.h"

#define MAX_IIRC_SERVER_CMD 1024
#define IIRC_SERVER_ERR_TIMEOUT -1
#define IIRC_SERVER_ERR_FATAL -2

typedef struct _iircServerDescr {
    int version;
    char name[MAX_IIRC_SERVER_NAME];
    char hostname[MAX_HOST_NAME];
    int isRoot; //! whether this server is the root server. (no parent)
    char parentName[MAX_HOST_NAME]; //! remote hostname of parent city
    short parentPort;       //! remote tcp port of parent city
    int listenForServers; //! set to TRUE and fill in 'listenServer'
    char serverName[MAX_HOST_NAME]; //! address to listen for child servers
    short serverPort;
    int listenForClients;   //! set to TRUE and fill in 'listenClient'
    char clientName[MAX_HOST_NAME]; //! address to listen for clients
    short clientPort;
    int listenForRFC1459;
    char rfc1459Name[MAX_HOST_NAME];
    short rfc1459Port;
} iircServerDescr;
#define IIRC_SERVER_DESCR_VERSION 0x00000100 /* version 0.1.0 */

#ifdef __cplusplus
extern "C" {
#endif

int iircServerGlobalInit();
int iircServerGlobalFree();

int iircServerAdd(iircServerDescr *pDesc);
int iircServerDel(int hServer);
int iircServerStart(int hServer, long timeout);
int iircServerExecute(int hServer, long timeout);
int iircServerModuleAdd(int hServer, iircModule_t *pMod);
int iircServerIntrAdd(int hServer, iircInterruptDescr *pDescr);
int iircServerIntrDel(int hServer, unsigned int hIntr);
int iircServerDispatch(int hServer, truck *pTruck);
int iircServerCmd(int hServer, char *cmd);

int iircServerUserGet(int hServer, int hUser, cargoIrcUser *cargo);

#ifdef __cplusplus
}
#endif

#endif


