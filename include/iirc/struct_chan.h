
/*
 * Copyright (c) 2001-2002,  James D. Taylor.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * contact James D. Taylor:  james.d.taylor@gmail.com
 */

#ifndef IIRC_STRUCT_CHAN_H
#define IIRC_STRUCT_CHAN_H

#include "iirc/common.h"
#include "metric/type.h"
#include "metric/set.h"
#include "iirc/truck.h"
#include "iirc/defs.h"

//!
//  index: hMod -- lows first (should be few in number)
//  usage: 2*INS / DEL / GET / INCR
#define hChanModSet_t   genericInsArray
#define hChanModSet(op) genericInsArray##op

//!
//  index: hChanUser -- lows always first (possibly high count)
//  usage: ADD / nChanUser*INS / DEL / GET / nChanUser*INCR
#define hUserSet_t  genericInsArray
#define hUserSet(op)    genericInsArray##op

//!
//  index: same as hUserSet
//  usage: nChanUser*INS
#define userFlagSet_t   genericInsArray
#define userFlagSet(op) genericInsArray##op

//!
//  channel flags
enum {
    IIRC_CHAN_CREATE = 0x00000001,
    IIRC_CHAN_DELETE = 0x00000002,
};

//!
// channel modes
enum {
    IIRC_CHAN_PRIVATE       = 0x00000001, //! p
    IIRC_CHAN_SECRET        = 0x00000002, //! s
    IIRC_CHAN_INVITE        = 0x00000004, //! i
    IIRC_CHAN_TOPIC_PROT    = 0x00000008, //! t
    IIRC_CHAN_EXTERN_MSG    = 0x00000010, //! n
    IIRC_CHAN_USER_LIMIT    = 0x00000020, //! l
    IIRC_CHAN_KEY       = 0x00000040, //! k
    IIRC_CHAN_MODE_UNUSED   = 0xFFFFFF80
};
#define IIRC_CHAN_NUM_MODES 7
extern char iircChanMode[IIRC_CHAN_NUM_MODES+1];

//!
// channel user modes
enum {
    IIRC_CHAN_OP    = 0x00000001, //! o
    IIRC_CHAN_VOICE = 0x00000002, //! v
};

typedef struct iircChan {
    mInt32 id;  //! handle to ourself
    mUint8 name[MAX_IIRC_CHAN_NAME];
    mUint8 topic[MAX_IIRC_CHAN_TOPIC];
    mUint8 key[MAX_IIRC_CHAN_KEY];
    mUint32 flags;
    mUint32 modes;
    mUint32 userLimit; //! maximum number of users allowed in chan
    hChanModSet_t modules; //! module instances. "modInstance[hMod]"
    hUserSet_t users; //! user handles.  "hUser[hChanUser]"
    userFlagSet_t userFlags; //! user flags.  "userFlags[hChanUser]"
    // TODO: set of ban's
    cargoProcessor fnCargoReceiver;
} iircChan_t;

extern genericInfo_t iircChanInfo;

#ifdef __cplusplus
extern "C" {
#endif

int chanNameVerify(mUint8 *chan);

int iircChanInit(iircChan_t *pChan);
int iircChanAlloc(iircChan_t *pChan);
int iircChanFree(iircChan_t *pChan);
int iircChanCmp(iircChan_t *pChan1, iircChan_t *pChan2);
int iircChanCopy(iircChan_t *pDst, iircChan_t *pSrc);

int iircChanUserNum(iircChan_t *pChan);

#ifdef __cplusplus
}
#endif

#endif

