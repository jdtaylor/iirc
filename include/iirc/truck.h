
/*
 * Copyright (c) 2001-2002,  James D. Taylor.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * contact James D. Taylor:  james.d.taylor@gmail.com
 */

#ifndef IIRC_TRUCK_H
#define IIRC_TRUCK_H

#include "iirc/common.h"
#include "metric/type.h"

/*!
 *  The source and destination addresses used to be a combination of
 *  the channel and clientId.  I figured this would be a good place
 *  to maximize the number of clients and channels supported; adding
 *  an additional 8 bytes to the iircHeader.
 */
typedef struct _truck {
    mUint32 flags;
    mUint32 year;
    mUint32 type; //! one of cargo enumerations below
    mUint32 size; //! size in bytes of the cargo
    mUint32 srcChan; //! source channel
    mUint32 srcUser; //! source client relative to chan
    mUint32 dstChan; //! destination channel
    mUint32 dstUser; //! destination client relative to chan
    mUint32 module; //! module handle. (iirc_module.h)
} truck;
#define TRUCK_YEAR 0x00000100 /* version 0.1.0 */ 

typedef struct _truckQueue {
    truck **array;
    mUint32 num; //! number of trucks in the queue
    mUint32 max; //! maximum trucks the queue can currently hold
    mUint32 first; //! index of first truck in the queue
    mUint32 last; //! index of last ''
    mUint32 size; //! truck size in bytes
} truckQueue;

//!
// CargoProcessors are for processing trucks.
// Receiving of trucks on both client and server
// sides should use them.
//  hInstance - value used by the cargoProcessor to determine the source
//  pTruck - data being transported
//  pStatus - method of passing flags to/from the cargoProcessor
typedef int (*cargoProcessor)(int hInstance, truck *pTruck, int *pStatus);
//!
// One or more flags that a cargoProcessor may be given in 'pStatus'
enum {
    PROC_IN_AUTHORITATIVE = 1, //! 'pTruck' is from parent
};
//!
// One or more flags that a cargoProcessor may return in 'pStatus'
enum {
    PROC_OUT_TRUCK_USED =   1, //! cargoProcessor consumed pTruck's mem
    PROC_OUT_STOP_ROUTING = 2, //! don't route pTruck to parent/child
    PROC_OUT_DISCONNECT =   4, //! disconnect whoever sent 'pTruck'
};

//! whether the truck is in big-endian format
#define TRUCK_BIGENDIAN_INNER   0x01
#define TRUCK_BIGENDIAN_OUTER   0x00
//! whether the truck can be dropped if necessary
#define TRUCK_DISPOSABLE_INNER  0x02
#define TRUCK_DISPOSABLE_OUTER  0x00
//! whether bytes were swapped (internal use only)
#define TRUCK_BYTESWAP_INNER    0x00
#define TRUCK_BYTESWAP_OUTER    0x80

#define TRUCK_FLAG_SET(t,f) t |= (f##_INNER << 16); \
                t |= (f##_INNER << 8); \
                t |= (f##_OUTER << 24); \
                t |= (f##_OUTER)
#define TRUCK_FLAG_UNSET(t,f) t &= ~(f##_INNER << 16); \
                  t &= ~(f##_INNER << 8); \
                  t &= ~(f##_OUTER << 24); \
                  t &= ~(f##_OUTER)
#define TRUCK_FLAG_FLIP(t,f) t ^= (f##_INNER << 16); \
                     t ^= (f##_INNER << 8); \
                     t ^= (f##_OUTER << 24); \
                     t ^= (f##_INNER)
#define TRUCK_FLAG_ISSET(t,f) (((t & 0xFF00) & (f##_INNER << 8)) && \
                       ((t & 0xFF) & f##_OUTER))

//!
// called like: if(TRUCK_NEEDS_BYTESWAP(pTruck->flags))
#ifdef CONFIG_BIGENDIAN
#define TRUCK_NEEDS_BYTESWAP(t) !TRUCK_FLAG_ISSET(t, TRUCK_BIGENDIAN)
#else
#define TRUCK_NEEDS_BYTESWAP(t) TRUCK_FLAG_ISSET(t, TRUCK_BIGENDIAN)
#endif

//!
// it is a broadcast address, if the highest bit is set.  any
#define IIRC_CHAN_BROADCAST  0x80000000
#define IIRC_USER_BROADCAST  0x80000000
//!
// number of each is whatever is left after removing the
// broadcast and network bits.
// MAX_IIRC_CHAN_ID = (2^31)-1 = 2,147,483,647
#define MAX_IIRC_CHAN_ID 0x7FFFFFFF
// MAX_IIRC_CLIENT_ID = (2^31)-1 = 2,147,483,647
#define MAX_IIRC_USER_ID 0x7FFFFFFF

#ifdef __cplusplus
extern "C" {
#endif

int truckInit(truck *pTruck);
int truckVerify(truck *pTruck);
int truckFix(truck *pTruck);
int truckDump(truck *pTruck);

int truckQueueInit(truckQueue *pQ);
   /*!
    * Allocate 'max' slots for trucks, and allocate/enqueue
    * 'num' trucks of size 'size' bytes
    */
int truckQueueAlloc(truckQueue *pQ, mUint32 max, mUint32 num, mUint32 size);
int truckQueueRefill(truckQueue *pQ);
int truckQueueFree(truckQueue *pQ);
int truckEnqueue(truckQueue *pQ, truck *pTruck);
truck* truckDequeue(truckQueue *pQ);
int truckQueueCheck(truckQueue *pQ);
int truckQueueDump(truckQueue *pQ);

#ifdef __cplusplus
}
#endif

#endif

